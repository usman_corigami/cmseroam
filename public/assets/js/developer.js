$(document).ready(function() {
    $('.has-tip').tooltip();
    $('#side-nav ul li a, .left-off-canvas-menu ul li a').hover(function() {
      $(this).addClass('hover-side-nav');
    }, function() {
      $(this).removeClass('hover-side-nav');
    });

   $(document).on('click','.open_pushy_cls',function(){ 
   	if($(this).closest('.pushy-submenu-closed')){
   		$(this).removeClass('pushy-submenu-closed');
   		$(this).addClass('close_pushy_cls');
   		$(this).removeClass('open_pushy_cls');
   	}
   });
   $(document).on('click','.close_pushy_cls',function(){
   		$(this).addClass('pushy-submenu-closed');
   		$(this).addClass('open_pushy_cls');
   		$(this).removeClass('close_pushy_cls');
   });
   $('.open_pushy_cls').trigger('click');
});
function siteUrl(route){
    //var url = '{{ url() }}';
    var url = $('#new_site_url').val();
    return url + '/' + route;
}
function roundToTwo(num) {    
        return +(Math.round(num + "e+2")  + "e-2");
    }