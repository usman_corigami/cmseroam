$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });
});

$(document).on('click','.geo-links',function(e){
	e.preventDefault();
	var domain_id = $(this).data('id');
	if(domain_id == ""){
		alert('domain id not found on this domain.');return false;
	}
	var url = $(this).data('url');
	if(url == ""){
		alert('route not found on this domain.');return false;
	}
	if(domain_id && url){
		$("#Loader").show();
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type			: 	'POST',
			dataType 	: 	'JSON',
			data			:	{'url':url,'domain_id':domain_id},
            url			: 	'set-domain-geolink',
            success	: 	function (response) {
					if(response.url){
						//$("#Loader").hide();
						window.location.href = response.url;
					}
            }
        });
	}
	
});

function selectDomainForGeoData(value)
{	
	var valuetrim = $.trim(value);
	if(valuetrim == ""){
		$(".geo-links").hide();
		$("#no-data").hide();
		//alert("Please select one domain.");return false;
	}else{
		$("#Loader").show();
		$("#no-data").hide();
		  $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type			: 	'POST',
			dataType 	: 	'JSON',
			data			:	{'domain_id':valuetrim},
            url			: 	'check-domain-geodata',
            success	: 	function (response) {
					if(response.status == 200){
						$(".geo-links").attr('data-id',valuetrim);
						
						$("#Loader").hide();
						$.each(response.data.type, function (key, val) {
							$('#'+val.type).show();
						});
					}else{
						$("#Loader").hide();
						$("#no-data").show();
					}
            }
        });
	}
	
}

//multiselect in listing
function setupLabel() 
{
    if ($('.label_check input').length) {
        $('.label_check').each(function(){ 
            $(this).removeClass('c_on');
        });
        $('.label_check input:checked').each(function(){ 
            $(this).parent('label').addClass('c_on');
        });                
    };
    if ($('.label_radio input').length) {
        $('.label_radio').each(function(){ 
            $(this).removeClass('r_on');
        });
        $('.label_radio input:checked').each(function(){ 
            $(this).parent('label').addClass('r_on');
        });
    };
}

function selectAllRow(ele)
{
    if(ele.checked === true) {
        $('.cmp_check').each(function() {
            this.checked = true;
        });
        
        //$('#dropdownMenu1').prop('disabled', false);
    }else{
        $('.cmp_check').each(function() {
            this.checked = false;
        });
        //$('#dropdownMenu1').prop('disabled', true);
    }
    setupLabel();
}
//image delete or set primary
$(document).on('click','.image-delete',function()
{  
    if (confirm('Are you sure?')) 
    {
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var url = siteUrl('common/');
        console.log(id);
        if (action == 'delete')
            method = 'delete';
        else
            method = 'primary';

        switch (type) {
            case 'hotel':
                url += '/hotel-images/' + id;
                break;
            case 'hotelimg':
                url += method+'-hotel-images/' + id;
                break;
            case 'transport':
                url += '/transport-images/' + id;
                break;
            case 'activity':
                url = siteUrl('activity/delete-activity-images/')+ id;
                break;
            case 'city':
                url += method+'-city-images/' + id;
                break;
            case 'tour':
                url = siteUrl('tour/')+ method+'-tour-images/' + id;
                break;
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: url,
            success: function (data) {

                if (data.success === true) {
                    if (action == 'delete') {
                        alert('Image removed successfully.')
                        location.reload();
                    } else {
                        location.reload();
                    }
                } else {
                    alert('Sorry please try again.')
                }
            }
        });
    }
});
function getOrderField(){
    return $("input[name='order_field']").val();
}
function getCurrentOrderField(){
    return $("input[name='current_order_field']").val();
}
function getCurrentSortingOrder(){
    return $("input[name='current_order_by']").val();
}

function getSortingOrder(){
    return $("input[name='order_by']").val();
}
function getSearchString(){
    return $("input[name='search_str']").val();
}
function getShowRecord(){
    return $(".select-entry").val();
}

function callDeleteRecord(oEle,sUrl, sMsg)
{
    if(confirm(sMsg))
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: sUrl,
            success: function () {
                $(oEle).parents('tr').remove();
                $(".delete-box").removeClass("hidden");
                $(".delete-box").text("Record deleted successfully.");
            },
            error: function (data){
            }
        });
    }
}

function callPublishRecord(oEle,sUrl)
{ 
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: sUrl,
            success: function (data) {
                console.log(data);  
                $(".delete-box").removeClass("hidden");
                $(".delete-box").text("Record updated successfully.");
                location. reload(true);
            },
            error: function (data){
            }
        });
    
}

function callUserListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var sUserType = $("input[name='user_type']").val();
    showLoader();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('user/list/'+sUserType+'?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord},
        success: function (data) {
            $('.'+sClass).html(data);
            hideLoader();
        },
        error: function (data) {
        }
    });
}

function callAgentUserListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    showLoader();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('users/user-management/'+'?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord},
        success: function (data) {
            $('.'+sClass).html(data);
            hideLoader();
        },
        error: function (data) {
        }
    });
}

function callLabelListing(oEvent,sClass,nPageNumber=1)
{
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('common/label-list'+'?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}

function showEroamSwitch(sUrl,nShow)
{
    $.ajax({
        method: 'post',
        url: sUrl,
        data: {show: nShow},
        success: function() {
                console.log('success');
        }
    });
}
function callRegionListing(oEvent,sClass)
{
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('common/region-list'),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord},
        success: function (data) {
            $('.'+sClass).html(data);
            $('.switch1-state1').bootstrapSwitch({
                'onSwitchChange':function(event,state){
                    switchChange(state,this);
                }
            });
        },
        error: function (data) {
        }
    });
}

function callCountryListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('common/country-list?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
            $('.switch1-state1').bootstrapSwitch({
                'onSwitchChange':function(event,state){
                    switchChange(state,this);
                }
            });
        },
        error: function (data) {
        }
    });
}

function callCityListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('common/city-list?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}

function callCouponListing(oEvent,sClass){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('common/coupon-list'),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}

function callHotelListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('acomodation/hotel-list?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}

function callTourListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('tour/tour-list?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}
function callSpecialOfferListing(oEvent,sClass,nPageNumber=1) {
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();
    showLoader();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('special-offer/list?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
            hideLoader();
        },
        error: function (data) {
        }
    });

}
function callTourLogoListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('tour/tour-type-logo-list'),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}
function callRouteListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();
    var start = $("#starting-point").val();
    var finish = $("#destination").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('route/route-list?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy,start:start,finish:finish},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}
function callTransportListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('transport/transport-list?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}
function callTransportSeasonListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('transport/season/?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}
function callTransportSupplierListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('transport/supplier/?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}
function callTransportOperatorListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('transport/operator/?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}
function callTransportMarkupListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('transport-markups/?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}
function callActivityListing(oEvent,sClass,nPageNumber=1){
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: siteUrl('activity/activity-list?page='+nPageNumber),
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
        },
        error: function (data) {
        }
    });
}
function getMoreListing(sUrl,oEvent,sClass)
{
    showLoader();
    var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: sUrl,
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
        success: function (data) {
            $('.'+sClass).html(data);
            $('.switch1-state1').bootstrapSwitch();
            hideLoader();
        },
        error: function (data) {
        }
    });
}
function getMoreListingPending(sUrl,oEvent,sClass)
{ 
    showLoader();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: sUrl,
        data: {order_field: orderField, order_by: orderBy},
        success: function (data) {
            $('.'+sClass).html(data);
            $('.switch1-state1').bootstrapSwitch();
            hideLoader();
        },
        error: function (data) {
        }
    });
}
function getMoreListingCurrent(sUrl,oEvent,sClass)
{ 
    showLoader();
    var orderField = getCurrentOrderField();
    var orderBy = getCurrentSortingOrder();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: sUrl,
        data: {current_order_field: orderField, current_order_by: orderBy},
        success: function (data) {
            $('.'+sClass).html(data);
            $('.switch1-state1').bootstrapSwitch();
            hideLoader();
        },
        error: function (data) {
        }
    });
}

function getMoreNotification(sUrl,oEvent,sClass)
{ 
    showLoader();
    var orderField = getCurrentOrderField();
    var orderBy = getCurrentSortingOrder();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: sUrl,
        data: {order_field: orderField, order_by: orderBy},
        success: function (data) {
            $('.'+sClass).html(data);
            $('.switch1-state1').bootstrapSwitch();
            hideLoader();
        },
        error: function (data) {
        }
    });
}

function getPaginationListing(sUrl,oEvent,sClass)
{
    showLoader();
    //var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    //var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: sUrl,
        data: {order_field: orderField, order_by: orderBy,show_record:showRecord},
        success: function (data) {
            $('.'+sClass).html(data);
            $('.switch1-state1').bootstrapSwitch();
            hideLoader();
        },
        error: function (data) {
        }
    });
}

function showLoader()
{
    $('.loader').show();
}
function hideLoader()
{
    $('.loader').hide();
}

var expanded = false;    
function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
 
