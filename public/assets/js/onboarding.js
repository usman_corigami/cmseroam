var Onboarding = function() {
	this.__construct = function() {
		this.addForm();
		this.getGeoData();
		this.init();
		this.saveGeoData();
		this.showloader();
		this.hideloader();
	};

	this.addForm = function() {
		$(document).on('submit','.add-form',function(evt) {
			evt.preventDefault();
			onboard.showloader();
			$('label.error').remove();
			var url = $(this).attr('action');
			var postData = $(this).serialize();
			var form = $(this)[0];
			var e = $(this);
			$.post(url,postData,function(out) {
				if(out.result === 0) {
					for(var i in out.errors) {
						$("#"+i).parent('.form-group').after('<label class="error" style="margin-bottom:10px;margin-top:0px !important;">'+out.errors[i]+'</label>');
						if (i == 'products') {
							$('input[name="products[]"]').parents().eq(4).after('<label class="error" style="margin-bottom:10px;margin-top:0px !important;">'+out.errors[i]+'</label>');
						}else if(i == 'image_name'){
							$("#"+i).siblings('.file-upload1').after('<label class="error" style="margin-bottom:10px;margin-top:0px !important;">'+out.errors[i]+'</label>');
						}else if(i == 'domain_backend'){
							$('.error_here').append('<label class="error" style="margin-bottom:10px;margin-top:0px !important;">'+out.errors[i]+'</label>');
							$('.error_here').focus();
						}else if(i == 'inventory'){
							$('.error_here').append('<label class="error" style="margin-bottom:10px;margin-top:0px !important;">'+out.errors[i]+'</label>');
							$('.error_here').focus();
						}
					}
					onboard.hideloader();
				}
				if(out.result === 1) {
					$("#error_msg").addClass('alert-success').text(out.msg);
					$("html, body").animate({ scrollTop: 0 }, "slow");
					if(e.attr('id') === 'user-form') {
						window.location.reload();
					}
					if(out.url) {
						window.location.href = out.url;
					}if(out.flag){
						
					}else{
						//form.reset();
						//$('.label_check').removeClass('c_on');
						//$('#image-submit-load').children('img').remove();
						if($('.domains').length != 0) {
							$('.domains')[0].selectize.destroy();
							$('.domains').selectize();
						}
					}
					onboard.hideloader();
				}
			});
		});
	};

	/*this.saveGeoData = function() {
		$(document).on('submit','.save-geodata',function(evt) {
			
			evt.preventDefault();
			
			onboard.showloader();

			var url = $(this).attr('action');
			var cities = [];
			var values = [];
			var domain_ids = [];
			$.each($("input[name='data[]']:checked"), function() {
			  values.push($(this).val());
			});
			$.each($("input[name='domains[]']:checked"), function() {
			  domain_ids.push($(this).val());
			});
		
			// $.each($('.geocities:checked'), function() {
			  	// cities.push({'city_id':$(this).val(),licensee_id:$('#licensee_id').val(),domain_id:$('#domain_id').val(),'region_id':$(this).data('region'),'country_id':$(this).data('country')});
			// });
			
			// $.each($('.geocountries:checked'), function() {
			  	// cities.push({'city_id':null,licensee_id:$('#licensee_id').val(),domain_id:$('#domain_id').val(),'region_id':$(this).data('region'),'country_id':$(this).val()});
			// });

			// if (cities.length < 1) {
				// $(".error_here").append('<label class="error" style="margin-bottom:10px;margin-top:0px !important;">You must select a city or country to continue</label>');
				// $(".error_here").focus();
				// onboard.hideloader();
				// return;
			// }
			var data = {
				_token:$('input[name="_token"]').val(),
				licensee_id:$('#licensee_id').val(),
				domain_id:$('#domain_id').val(),
				data:values,
				domain_ids:domain_ids
			}
			$.post(url, data, function(out) {
				if(out.result === 0) {
					for(var i in out.errors) {
						$("#"+i).parent('.form-group').after('<label class="error" style="margin-bottom:10px;margin-top:0px !important;">'+out.errors[i]+'</label>');
					}
				}

				if(out.result === 1) {
					$("#error_msg").addClass('alert-success').text(out.msg);
					if(out.flag){
						$("html, body").animate({ scrollTop: 0 }, "slow");
					}else{
						window.location.href = out.url;
					}
				}
				onboard.hideloader();
			});
		});
	};*/

	this.saveGeoData = function() {
		$(document).on('click','.save_geodata',function(evt) {
			
			evt.preventDefault();			
			onboard.showloader();

			var url = $(this).attr('action');
			var licensee_id = $(this).attr('data-id');
			var domain_id = $(this).attr('data-val');
			//alert(licensee_id);alert(domain_id);return false;
			var values = [];
			$.each($("input[name='data_"+domain_id+"[]']:checked"), function() {
			  values.push($(this).val());
			});

			var data = {
				_token:$('input[name="_token"]').val(),
				licensee_id:licensee_id,
				domain_id:domain_id,
				data:values
			}
			//alert(JSON.stringify(data));return false;
			$.post(url, data, function(out) {
				if(out.result === 0) {
					for(var i in out.errors) {
						$("#"+i).parent('.form-group').after('<label class="error" style="margin-bottom:10px;margin-top:0px !important;">'+out.errors[i]+'</label>');
					}
				}

				if(out.result === 1) {
					$("#error_msg").addClass('alert-success').text(out.msg);
					if(out.flag){
						$("html, body").animate({ scrollTop: 0 }, "slow");
					}else{
						window.location.href = out.url;
					}
				}
				onboard.hideloader();
			});
		});
	};

	this.getGeoData = function() {
		$(document).on('change', '.georegions', function(){
			if($(this).is(":checked")){
				//onboard.showloader();
				$('.countriesLoader').show();
				$('.countries').hide();

				var regions = [];
				regions.push($(this).val());

	            var url = $('#geodata').val();
	            $.post(url, {regions:regions}, function(res){
					$('.countries').append(res.countries);
					$('.noresults_country').hide();
					$('.countriesLoader').hide();
					$('.countries').show();
					$('#countries').show();
					//onboard.hideloader();
				});
			}else{
				region = $(this).val();
				$("input[data-region='"+region+"']").parent('label').parent('.form-group').parent('.col-sm-6').remove();
				$(".cregion"+region).remove();
			}
		});

		$(document).on('click', '#clearall', function(evt){
			evt.preventDefault();
			$('.geocities').attr('checked',false);
			$('.geocities').parent('label').removeClass('c_on');
		})

		$(document).on('change', '.geocountries', function(){

			if($(this).is(":checked")){
				$('.citiesLoader').show();
				$('.cities').hide();
				//onboard.showloader();
				var countries = [];
	            countries.push($(this).val());

	            var url = $('#geodata').val();
	            $.post(url, {countries:countries}, function(res){
					$('.cities').append(res.cities);
					$('.citiesLoader').hide();
					$('.noresults_city').hide();
					$('.cities').show();
					$('#citiesList').show();
					//onboard.hideloader();
				});
        	}else{
        		country = $(this).val();
				$("input[data-country='"+country+"']").parent('label').parent('.form-group').parent('.col-sm-6').remove();
        		$(".ccountry"+country).remove();
        	}
		});
	};

	this.init = function(){
		$('.slim-scroll').slimScroll({
	    	height: '100%',
	    	color: '#000',
	    	opacity: '0.7',
	    	size: '5px',
	    	allowPageScroll: true
	  	});
	  	$('[data-toggle="tooltip"]').tooltip();
	  	$('.tooltip-wrapper').tooltip();
	}

	this.showloader = function() {
        $.blockUI({ 
            css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff'
            },
            message: '<h3>Please Wait...</h3>'
        }); 
    }

    this.hideloader = function() {
        $.unblockUI();
    }

	this.__construct();
}
var onboard = new Onboarding();