var global_currency = getCookie('currency') != null ? getCookie('currency') : 'AUD';
var global_curr_value = getCookie('curr_value') != null ? getCookie('curr_value') : 1;
var global_currency_symbol = currency_symbol(global_currency);
selectCurrency();
get_base_currency();
itineraryCredentials('set');
modify_topbar();
get_custom_map_styles();
/**
 * get all currencies with AUD base and save to cookie
 */
function get_base_currency()
{
    aud_curr = JSON.parse(getCookie('aud_curr'));
    if (aud_curr === null) {
        endpoint = 'live';
        access_key = _currencyObj._key;
        data = {};
        $.ajax({
            url: _currencyObj._url+endpoint+'?access_key='+access_key+'&source=AUD',
            dataType: 'jsonp',
            success: function(response) {
                var data = {};
                $.each(response.quotes, function(k, value) {
                    data[k.substring(3)] = value;
                });
                document.cookie = "aud_curr="+JSON.stringify(data);
                create_currency_log();
            }
        });
    }
}

function convert_to_au(currency, value)
{
    currency = currency.replace('$', 'D');
    var aud_curr = JSON.parse(getCookie('aud_curr'));
    // console.log('orig value: '+ currency + ' ' + value + ' -- converted value: '+ parseFloat(value / aud_curr[currency]).toFixed() );
    return value / aud_curr[currency];
}


function selectCurrency()
{
    currency_select = $("#currency-converter");
    cookie = getCookie('currency');
    if (cookie !== null) {
      currency_select.find('option[value="'+cookie+'"]').prop('selected', true);
    } else {
      currency_select.find('option[value="default"]').prop('selected', true);
    }
}

function changeCurrency(value)
{
    document.cookie="currency="+value;
    getCurrencyValue();
}

function getCurrencyValue()
{
    cookie = getCookie('currency');
    if (cookie !== null) {
        global_currency = cookie;
        global_currency_symbol = currency_symbol(global_currency);
        aud_curr = JSON.parse(getCookie('aud_curr'));
        global_curr_value = aud_curr[global_currency];;
        document.cookie="curr_value="+global_curr_value;
        if (typeof buildSummary != 'undefined') { 
            buildSummary();
        }
    }
}

function getCookie(name)
{
  match = document.cookie.match(new RegExp(name + '=([^;]+)'));
  if (match) return match[1];
  return null;
}

// convert currency from usd to (parameter)
function convert_currency(value)
{
    return parseFloat(global_curr_value * value).toFixed(2);
}

function currency_symbol(currency)
{
    switch (currency) {
        case 'USD':
        case 'AUD':
        case 'CAD':
        case 'FJD':
        case 'HKD':
        case 'NZD':
        case 'SGD':
            //Dollar sign
            return "&#36;";
            break;
        case 'CNY':
        case 'JPY':
            //Yen sign
            return "&#165;";
            break;
        case 'EUR':
            //Euro sign
            return "&#128;";
            break;
        case 'THB':
            //Baht sign
            return "&#3647;";
            break;
        case 'GBP':
            //Pound sign
            return "&#163;";
            break;

    }
}

function ucfirst(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function query_string(name)
{
    /*name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));*/
    var result = itineraryCredentials('get', name);
    return (!result) ? "" : result;
}

function itineraryCredentials(type, index, value){

    var result, status;

    switch(type){

        case 'set':

            if($('#ItineraryData').val() && $('#ItinerarySettings').val()){

                var itinerary_raw_data = $('#ItineraryData').val().split(',');
                var itinerary_raw_settings = $('#ItinerarySettings').val().split(',');

                _currentItineraryData = {
                    type : itinerary_raw_data[0],
                    origin : itinerary_raw_data[1],
                    destination : itinerary_raw_data[2],
                    date : itinerary_raw_data[3],
                    traveller : itinerary_raw_data[4],
                    alter : itinerary_raw_data[5],
                    group : itinerary_raw_data[6],
                    nights : itinerary_raw_data[7],
                };

                _currentItinerarySettings = {
                    country : itinerary_raw_settings[0],
                    category : itinerary_raw_settings[1],
                    room_type : itinerary_raw_settings[2],
                    transport_preferred : itinerary_raw_settings[3],
                    travel_preferred : itinerary_raw_settings[4],
                    interest : itinerary_raw_settings[5]
                };

            }

            result = true;

            break;

        case 'update':          

            if(_currentItineraryData[index] !== undefined){

                _currentItineraryData[index] = value;


            }else if(_currentItinerarySettings[index] !== undefined){

                _currentItinerarySettings[index] = value;
                
            }

            dom_data.val([
                    _currentItineraryData.type, 
                    _currentItineraryData.origin, 
                    _currentItineraryData.destination, 
                    _currentItineraryData.date, 
                    _currentItineraryData.traveller,
                    _currentItineraryData.alter,
                    _currentItineraryData.group,
                    _currentItineraryData.nights,
            ].join(','));

            dom_settings.val([
                    _currentItinerarySettings.country, 
                    _currentItinerarySettings.category, 
                    _currentItinerarySettings.room_type, 
                    _currentItinerarySettings.transport_preferred, 
                    _currentItinerarySettings.travel_preferred,
                    _currentItinerarySettings.interests
            ].join(','));

            result = true;

            break;

        case 'get':

            if(_currentItineraryData[index]){

                result = _currentItineraryData[index];

            }else if(_currentItinerarySettings[index]){

                result = _currentItinerarySettings[index];
                
            }else{

                result = false;

            }

            break;

    };

    return result;

}

function create_currency_log()
{
   $.ajax({
       type: 'POST',
       url: 'KeywordSearch.asmx/logCurrency',
       data: "{  }",
       contentType: 'application/json; charset=utf-8',
       dataType: 'json',
       success: function(response) {
           console.log(response.d);
       }
   })
}

function modify_topbar()
{
    switch ( _currentItineraryData['type'] ) {
        case 'multistop':
            $('#ddlTo').parent().css('display', 'none');
            $('#ddlTo').parent().parent().css('width', '150%');
            break;
        case 'singlestop':
            $('#ddlFrom').parent().css('display', 'none');
            $('#ddlFrom').parent().parent().css('width', '150%');
            break;
    }
}

function get_custom_map_styles() {
    /* by rgr - get custom google maps styles */

    $.ajax( {
        url: 'http://cms.testeroam.com/get-map-styles-jsonp',
        crossDomain: true,
        aysnc: false,
        dataType: 'jsonp',
        data: {
            domain: document.domain
        },
        success: function( response ) {
            document.cookie = 'customMapStyles=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            document.cookie = "customMapStyles=" + JSON.stringify( response );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log( 'ajax error: ' + xhr.status );
        }
    });
}

/**
 * Call Eroam API method thru AJAX
 * @param  route   Name of method or route.
 * @param  method   Method of request
 * @param  data     Data to be passed, could be null or empty.
 * @param  success  Reference to success
 * @param  before   Reference to beforeSend
 * @param  complete Reference to complete
 */
function callApi(route, method, data, success, before, complete) {

    $.ajax({
        url: site_url('eroam/api/v1/' + route),
        method: method,
        headers: {
            'X-Authorization': _API._key
        },
        data: data,
        beforeSend: before,
        complete: complete,
        success: success,
        error: function(xhr) {
            console.log(xhr.statusText + xhr.responseText);
        },
    });

}

function callWebMethod(route, method, data, success, before, complete) {
    $.ajax({
        url: '/' + route,
        method: method,
        data: data,
        beforeSend: before,
        complete: complete,
        success: success,
        error: function(xhr) {
            console.log(xhr.statusText + xhr.responseText);
        },
    });
}

function logoutUser() {
    $.post('/User.asmx/logoutUser', function() {
        location.href = '/login.aspx';
    });
}

function site_url(path) {
    return 'http://cms.testeroam.com/' + path;
}

function getTransportIcon(transport_type)
{
    if (transport_type === undefined) {
        return '';
    }
    switch (transport_type.toLowerCase()) {
        case 'coach morning':
        case 'coach afternoon':
        case 'coach':
        case 'minibus':
        case 'minivan':
            fa_icon = 'bus';
            break;
        case 'train':
            fa_icon = 'train';
            break;
        case 'flight':
            fa_icon = 'plane';
            break;
        case 'self drive':
        case 'private car':
            fa_icon = 'car';
            break;
        case 'speed boat':
        case 'ferry':
            fa_icon = 'ship';
            break;
        default:
            fa_icon = '';
            break;
    }
    return fa_icon;
}