/*------------- Set equal height function Starts -------------- */
function setEqualHeight_CommonClass(arr) {
    var x = new Array([]);
    $(arr).each(function(i) {
        $(this).height('auto');
        x[i] = $(this).height();
    });
    Max_Value = Array.max(x);
    $(arr).each(function(i) {
        $(this).height(Max_Value);
    });
}

function setEqualHeight(arr) {
    var x = new Array([]);
    for (i = 0; i < arr.length; i++) {
        x[i] = $(arr[i]).height("auto");
        x[i] = $(arr[i]).height();
    }
    Max_Value = Array.max(x);
    for (i = 0; i < arr.length; i++) {
        if ($(arr[i]).height() != Max_Value) {
            if ($("body").hasClass("ie7")) {
                x[i] = $(arr[i]).attr("style", "height:" + Max_Value + "px");
            } else {
                x[i] = $(arr[i]).height(Max_Value);
            }
        }
    }
}
Array.max = function(array) {
    return Math.max.apply(Math, array);
};
/*------------- Set equal height function Ends -------------- */

function setupLabel() {
  if ($('.label_check input').length) {
      $('.label_check').each(function(){ 
          $(this).removeClass('c_on');
      });
      $('.label_check input:checked').each(function(){ 
          $(this).parent('label').addClass('c_on');
      });                
  };
  if ($('.label_radio input').length) {
      $('.label_radio').each(function(){ 
          $(this).removeClass('r_on');
      });
      $('.label_radio input:checked').each(function(){ 
          $(this).parent('label').addClass('r_on');
      });
  };
};

var w = $( document ).width();
function mobileMenu(){
    if(w < 992){
    //document.body.className = document.body.className.replace("pushy-open-left","");
     $('body').removeClass('pushy-open-left');
    } else{
      $('body').addClass('pushy-open-left');
    }
}

$(window).resize(function(){
   mobileMenu();
  setEqualHeight_CommonClass(".dashboard-box");
  setEqualHeight_CommonClass(".users-inner");
  setEqualHeight_CommonClass(".dashboard-box-three");
  //setEqualHeight([".dashboard-box2", ".dashboard-box3", ".dashboard-box1"]);
  setEqualHeight([".analytics-inner", ".card-inner"]);
  setEqualHeight([".dashboard-box7 .page-title", ".dashboard-box8 .page-title"]);
  //setEqualHeight([".users-inner", ".userRetention-inner"]);
});

$(document).ready(function(){
  setEqualHeight_CommonClass(".dashboard-box");
  setEqualHeight_CommonClass(".users-inner");
  setEqualHeight_CommonClass(".dashboard-box-three");
  //setEqualHeight([".dashboard-box2", ".dashboard-box3", ".dashboard-box1"]);
  setEqualHeight([".analytics-inner", ".card-inner"]);
  setEqualHeight([".dashboard-box7 .page-title", ".dashboard-box8 .page-title"]);
  //setEqualHeight([".users-inner", ".userRetention-inner"]);
  $(document).on('click','.label_check, .label_radio',function() {
      setupLabel();
  })
  setupLabel(); 
  mobileMenu();
  /*--- Start For menu ---*/
  $('.pushy-height').slimScroll({
      height: '100%',
      color: '#000',
      opacity: '0.7',
      size: '5px',
      allowPageScroll: true
  });
  $('.expand-all').click(function(){
   $('.pushy-submenu').removeClass('pushy-submenu-closed');
   $('.pushy-submenu').addClass('pushy-submenu-open');
   $(this).addClass('active');
   $('.collapse-all').removeClass('active');
  });

  $('.collapse-all').click(function(){
   $('.pushy-submenu').addClass('pushy-submenu-closed');
   $('.pushy-submenu').removeClass('pushy-submenu-open');
   $(this).addClass('active');
   $('.expand-all').removeClass('active');
  });

  /*---End For menu ---*/


});