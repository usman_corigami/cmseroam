<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ trans('messages.eroam_header_title') }}</title>
        <link href="{{ asset('assets/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/jquery-ui/jquery-ui.theme.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/datepicker.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/clockface.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/pushy.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/common.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/selectize.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/bootstrap-switch.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/js/MultipleDatesPicker/jquery-ui.multidatespicker.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/js/MultipleDatesPicker/jquery-ui.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/media.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/prettify.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/developer.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/highlight.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/main1.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/jquery.fileuploader.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/jquery.fileuploader-theme-dragdrop.css') }}" rel="stylesheet" />
        <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/js/common.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.simplePagination.js') }}" ></script>
        @stack('styles')
        @yield( 'custom-css' )    
    </head>
    <body class="has-js pushy-open-left">
        <div class="loader">
	    <i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i>
	</div>
        <header>
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-2 col-sm-2 col-xs-4">
                    <a class="navbar-brand" href="{{ URL::to('/') }}"><img src="{{ url( 'assets/images/logo.png' ) }}" alt="eroam" class="img-responsive"></a>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-8">
                    <div>
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{ \Auth::user()->name??"" }} <i class="icon-user-round"></i></a>
                          <ul class="dropdown-menu">
                          <li><a href="{{URL::to('/user/setting')}}">{{ trans('messages.setting') }}</a></li>
                            <li><a class="logout" href="{{ URL::to('/logout') }}">{{ trans('messages.log_out') }}</a></li>
                          </ul>
                        </li>
                        <li><a href="#" class="navbtn menu-btn hidden-lg hidden-md"><i class="fa fa-navicon"></i></a></li>
                      </ul>
                    </div>
                  </div>
               </div>
             </div>
            </nav>
            <input type="hidden" id="new_site_url" value="{{ secure_asset('/') }}">
        </header>
        