<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserIpDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_ip_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('search_type');
            $table->string('ip_address');
            $table->string('city');
            $table->string('country_code');
            $table->string('country_name');
            $table->text('interestlist');
            $table->integer('nationality');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('region');
            $table->integer('accommodation');
            $table->string('room');
            $table->integer('transport');
            $table->string('age_group');
            $table->string('gender');
            $table->string('interest_list_id');
            $table->string('tour_type');
            $table->string('project');
            $table->string('type_option');
            $table->string('package_option');
            $table->string('start_location');
            $table->string('end_location');
            $table->string('start_date');
            
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
