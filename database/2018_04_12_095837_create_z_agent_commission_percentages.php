<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZAgentCommissionPercentages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zAgentCommissionPercentages')){  
            Schema::create('zAgentCommissionPercentages', function($table)
            {
                $table->increments('id');
                $table->integer('agent_id')->unsigned();
                $table->Integer('percentage');
                $table->timestamps();
                $table->softDeletes();
                
                $table->foreign('agent_id')->references('id')->on('zAgents');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zAgentCommissionPercentages');
    }
}
