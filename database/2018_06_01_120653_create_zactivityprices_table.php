<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZactivitypricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zactivityprices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_id');
            $table->integer('allotment');
            $table->integer('release');
            $table->integer('minimum_pax');
            $table->timestamps();
            $table->string('operates')->nullable();
            $table->string('name')->nullable();
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->softDeletes();
            $table->integer('activity_base_price_id')->nullable();
            $table->integer('activity_markup_id')->nullable();
            $table->integer('activity_markup_percentage_id')->nullable();
            $table->integer('activity_supplier_id')->nullable();
            $table->text('cancellation_policy')->nullable();
            $table->text('cancellation_formula')->nullable();
            $table->integer('currency_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('licensee_id')->nullable();
            $table->string('domain_ids')->nullable();
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zactivityprices');
    }
}
