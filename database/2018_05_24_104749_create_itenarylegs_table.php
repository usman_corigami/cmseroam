<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItenarylegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itenarylegs', function (Blueprint $table) {
             $table->increments('itenary_leg_id');
            $table->integer('itenary_order_id')->nullable();
            $table->string('from_city_name')->nullable();
            $table->string('to_city_name')->nullable();
            $table->integer('from_city_id')->nullable();
            $table->integer('to_city_id')->nullable();
            $table->date('from_date');
            $table->date('to_date');
            $table->string('country_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itenarylegs');
    }
}
