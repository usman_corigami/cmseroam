<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItenaryordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itenaryorders', function (Blueprint $table) {
            $table->bigIncrements('order_id');
            $table->integer('user_id')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('from_city_id')->nullable();
            $table->string('to_city_id')->nullable();
            $table->string('from_city_name')->nullable();
            $table->string('to_city_name')->nullable();
            $table->integer('num_of_travellers')->nullable();
            $table->date('travel_date')->nullable();
            $table->integer('total_itenary_legs')->nullable();
            $table->string('total_amount')->nullable();
            $table->string('total_days')->nullable();
            $table->string('currency')->nullable();
            $table->string('cost_per_day')->nullable();
            $table->string('total_per_person')->nullable();
            $table->string('evay_transcation_id')->nullable();
            $table->string('card_number')->nullable();
            $table->string('expiry_month')->nullable();
            $table->string('expiry_year')->nullable();
            $table->string('cvv')->nullable();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->text('from_city_to_city')->nullable();
            $table->string('voucher')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itenaryorders');
    }
}
