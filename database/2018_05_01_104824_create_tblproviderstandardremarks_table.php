<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblproviderstandardremarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tblproviderstandardremarks'))
        {
        Schema::create('tblproviderstandardremarks', function (Blueprint $table) {
            $table->increments('standard_remarks_id');
            $table->integer('provider_id');
            $table->longText('standard_desc')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblproviderstandardremarks');
    }
}
