<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbllocalpaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbllocalpayment', function (Blueprint $table) {
            $table->increments('Payment_Id');
            $table->integer('tour_id');
            $table->decimal('Price',16,4)->nullable();
            $table->string('description',255)->nullable();
            $table->string('currency')->nullable();
            $table->bigInteger('season_id')->nullable();
            $table->tinyInteger('isMandatory')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbllocalpayment');
    }
}
