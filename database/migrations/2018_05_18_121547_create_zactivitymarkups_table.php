<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZactivitymarkupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zactivitymarkups'))
        {
        Schema::create('zactivitymarkups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->longText('description')->nullable();
            $table->string('allocation_type')->nullable();
            $table->string('allocation_id')->nullable();
            $table->tinyInteger('is_default');
            $table->tinyInteger('is_active');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('activity_markup_percentage_id')->nullable();
            $table->integer('activity_markup_agent_commission_id')->nullable();
            $table->integer('activity_markup_supplier_commission_id')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zactivitymarkups');
    }
}
