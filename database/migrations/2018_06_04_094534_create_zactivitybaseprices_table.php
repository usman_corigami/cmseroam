<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZactivitybasepricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zactivitybaseprices'))
        {
        Schema::create('zactivitybaseprices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_price_id')->nullable();
            $table->decimal('base_price',16,2);
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zactivitybaseprices');
    }
}
