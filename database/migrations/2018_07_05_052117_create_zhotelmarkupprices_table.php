<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhotelmarkuppricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhotelmarkupprices')){ 
        Schema::create('zhotelmarkupprices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_price_id');
            $table->integer('hotel_markup_id');
            $table->integer('hotel_markup_percentage_id');
            $table->integer('hotel_base_price_id');
            $table->decimal('marked_up_price',12,2);
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhotelmarkupprices');
    }
}
