<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhotel'))
        {
        Schema::create('zhotel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('city_id');
            $table->boolean('optional_city')->nullable();
            $table->integer('default_nights')->nullable();
            $table->boolean('is_approved')->nullable();
            $table->integer('currency_id')->nullable();
            $table->string('ranking')->nullable();
            $table->text('short_description')->nullable(); 
            $table->text('description')->nullable();
            $table->text('room_description')->nullable();
            $table->text('near_by_activities')->nullable();
            $table->text('internet')->nullable();
            $table->text('where_to_eat')->nullable();
            $table->text('we_should_mention')->nullable();
            $table->text('fees')->nullable();
            $table->text('refundable')->nullable();
            $table->text('voucher_comments')->nullable();
            $table->text('cancellation_policy')->nullable();
            $table->text('address_1')->nullable();
            $table->text('address_2')->nullable();
            $table->text('notes')->nullable();
            $table->string('reception_phone')->nullable();
            $table->string('reception_email')->nullable();
            $table->string('website')->nullable(); 
            $table->timestamps();
            $table->integer('hotel_category_id')->nullable();
            $table->integer('default_hotel_supplier_id')->nullable();
            $table->string('nationality')->nullable();
            $table->string('age_group')->nullable();
            $table->tinyInteger('eroam_stamp');
            $table->softDeletes();
            $table->longText('suburb_id')->nullable();
            $table->integer('star_rating')->nullable();
            $table->longText('geo_location')->nullable();  
            $table->bigInteger('supplier_id')->nullable();  
            $table->string('eroam_code')->nullable();
            $table->bigInteger('country_id')->nullable(); 
            $table->string('latitude')->nullable(); 
            $table->string('longitude')->nullable(); 
            $table->longText('intro_description')->nullable(); 
            $table->string('postcode')->nullable();
            $table->tinyInteger('is_publish')->nullable();
            $table->Integer('user_id');
            $table->string('domain_ids')->nullable();
            $table->tinyInteger('licensee_id');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhotel');
    }
}
