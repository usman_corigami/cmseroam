<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhbzonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhbzones'))
        {
        Schema::create('zhbzones', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('hb_destination_id');
            $table->string('zone_name');
            $table->string('zone_number');
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhbzones');
    }
}
