<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltourstandardremarkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tbltourstandardremarks'))
        {
        Schema::create('tbltourstandardremarks', function (Blueprint $table) {
            $table->increments('tour_standard_id');
            $table->integer('standard_remarks_id');
            $table->integer('tour_id');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltourstandardremarks');
    }
}
