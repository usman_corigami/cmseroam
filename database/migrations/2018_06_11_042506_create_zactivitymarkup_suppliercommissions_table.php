<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZactivitymarkupSuppliercommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zactivitymarkupsuppliercommissions'))
        {
        Schema::create('zactivitymarkupsuppliercommissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_markup_id');
            $table->float('percentage');
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zactivitymarkupsuppliercommissions');
    }
}
