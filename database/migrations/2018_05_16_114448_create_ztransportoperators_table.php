<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZtransportoperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('ztransportoperators'))
        {
        Schema::create('ztransportoperators', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->longText('logo')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->string('marketing_contact_name')->nullable();
            $table->string('marketing_contact_email')->nullable();
            $table->string('marketing_contact_title')->nullable();
            $table->string('marketing_contact_phone')->nullable();
            $table->string('reservation_contact_name')->nullable();
            $table->string('reservation_contact_email')->nullable();
            $table->string('reservation_contact_landline')->nullable();
            $table->string('reservation_contact_free_phone')->nullable();
            $table->string('accounts_contact_name')->nullable();
            $table->string('accounts_contact_email')->nullable();
            $table->string('accounts_contact_title')->nullable();
            $table->string('accounts_contact_phone')->nullable();
            $table->string('special_notes')->nullable();
            $table->string('remarks')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ztransportoperators');
    }
}
