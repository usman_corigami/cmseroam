<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhotelmarkupagentcommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhotelmarkupprices')){
        Schema::create('zhotelmarkupagentcommissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_markup_id');
            $table->float('percentage');
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhotelmarkupagentcommissions');
    }
}
