<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZactivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zactivity'))
        {
        Schema::create('zactivity', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('city_id')->nullable();
            $table->string('[default]')->default(0);
            $table->string('pickup')->nullable();
            $table->string('dropoff')->nullable();
            $table->string('has_accomodation');
            $table->string('has_transport');
            $table->tinyInteger('approved_by_eroam')->nullable();
            $table->integer('activity_category_id')->nullable();
            $table->integer('default_activity_supplier_id')->nullable();
            $table->integer('currency_id')->nullable();
            $table->string('ranking')->nullable();
            $table->text('description')->nullable();
            $table->string('voucher_comments')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('notes')->nullable();
            $table->string('reception_phone')->nullable();
            $table->string('reception_email')->nullable();
            $table->string('website')->nullable();
            $table->string('pax_type')->nullable();
            $table->timestamps();
            $table->integer('activity_operator_id')->nullable();
            $table->string('duration')->nullable();
            $table->integer('country_id')->nullable();
            $table->tinyInteger('eroam_stamp')->nullable();
            $table->softDeletes();
            $table->integer('destination_city_id')->nullable();
            $table->integer('accommodation_nights')->nullable();
            $table->text('accommodation_details')->nullable();
            $table->text('transport_details')->nullable();
            $table->tinyInteger('is_publish')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zactivity');
    }
}
