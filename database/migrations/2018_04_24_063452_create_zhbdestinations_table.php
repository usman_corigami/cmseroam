<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhbdestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhbdestinations'))
        {
        Schema::create('zhbdestinations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('destination_name',255)->nullable();
            $table->string('destination_code',255)->nullable();
            $table->string('country_code',255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhbdestinations');
    }
}
