<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\ApiFormRequest;


class UserRequest extends FormRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = request('user_id') ?:'';
        
        switch ($this->method()){
            case 'POST':
                return [
                    'name'=>'required',
                    'username'    => 'required|unique:users,username,'.$user_id,
                    'password' => 'required|min:6',

                                    
                ];
                break;

            default:
                return [];
                break;
        }

    }
    
    public function messages()
    {

        return [          
            'name.required'=>'error_name_required',
            'username.unique' => 'error_user_name_has_already_been_taken',
           
        ];

                    

    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


}
