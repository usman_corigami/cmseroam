<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\User;
use Hash,Request,View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    public function __construct(){
        $segments = Request::segments();
        
        View::share('segments',$segments);
    }
    public function loginUser($credentials) {
        $username = $credentials['username'];
        $password = $credentials['password'];

        $user = User::where(['username' => $username])->first();
        if ($user && Hash::check($password, $user->password) && ($user->type == 'admin' || $user->type == 'licensee_administrator' || $user->type == 'agent' || $user->type== 'brand_administrator' || $user->type== 'product_manager' || $user->type== 'customer' || $user->type== 'eroamProduct')) {
            // exists
            $user['auth'] = 'valid';
            // exsits but inactive
            if ($user->active != 1) {
                $user['auth'] = 'inactive';
            }
        } else {
            //does not exists
            $user['auth'] = 'invalid';
        }
        return $user;
    }

}
