<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\RoomerFlex\RoomerFlexClient;
use Illuminate\Support\Facades\Input;
use Log;

class RoomerFlexController extends Controller
{
	private $RoomerFlex;

	
	public function requestToken(){
		
    $success = TRUE;
    $error   = array();

		$this->RoomerFlex = RoomerFlexClient::createRequestToken();

		$data   = Input::all();
		$response = array();
		
		try {

			Log::channel('roomerflex_request')->info('Request----------------------------');
			Log::channel('roomerflex_request')->info(json_encode($data));


			$response = $this->RoomerFlex->requestToken($data);
			$response = $response->toArray();

			Log::channel('roomerflex_request')->info('Response----------------'); 
			Log::channel('roomerflex_request')->info(json_encode($response));
      
      $returnData = response_format( $success, $response, $error );  	   

		} catch (Exception $e) {
			
			Log::channel( '----------------in------------------------------' );
		                    Log::channel( [
		                       'headers' => 'requestToken',
		                       'error' => $response
		                   ] );

      $success = FALSE;
      $data    = array();
      $error   = array();                  
      $returnData = response_format($success, $data, $error);
      return $returnData;                  
		}

		return $returnData;

	}

	public function protectionToken(){
		
    $success = TRUE;
    $error   = array();

		$this->RoomerFlex = RoomerFlexClient::createProtectionToken();

		$data   = Input::all();
    $response = array();
		
		try {

			Log::channel('roomerflex_protection')->info('Request----------------------------');
			Log::channel('roomerflex_protection')->info(json_encode($data));


			$response = $this->RoomerFlex->protectionToken($data);
			$response = $response->toArray();
      

			Log::channel('roomerflex_protection')->info('Response----------------'); 
			Log::channel('roomerflex_protection')->info(json_encode($response));
      
      $returnData = response_format( $success, $response, $error );  	   

		} catch (Exception $e) {

			Log::channel( '----------------in------------------------------' );
		                    Log::channel( [
		                       'headers' => 'protectionToken',
		                       'error' => $response
		                   ] );

      $success = FALSE;
      $data    = array();
      $error   = array();                  
      $returnData = response_format($success, $data, $error);
      return $returnData;

		}

		return $returnData;
    
	}

  public function response_format($success, $data, $error){
    if(is_array($data)){
      if(count($data) < 1)
        $data = NULL;
    }
    if(is_array($error)){
      if(count($error) < 1)
        $error = NULL;
    }   
    $result = array(
          'success' => $success,
          'data' => $data,
          'error' => $error
          );
    return \Response::json( $result );
  }

}