<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Illuminate\Support\Facades\Input;
use DB;
use SoapClient;
use App\MystiflySession;
use App\CityIata;
use App\Airline;
use Config;
use stdClass;
use Log;

class MystiflyNewController extends Controller
{

	private $client;
	private $account_number;
	private $user_name;
	private $password;
	private $target;
	private $endpoint_url;
	

	public function __construct(){
		$this->account_number = Config::get('services.mystifly.account_number');	
		$this->user_name  = Config::get('services.mystifly.user_name');
		$this->password = Config::get('services.mystifly.password');	
		$this->target  = Config::get('services.mystifly.target');
		$this->endpoint_url = Config::get('services.mystifly.endpoint_url');
		$this->client = new SoapClient(	$this->endpoint_url, 
										array(	'trace'      => 1, 
												'cache_wsdl' => WSDL_CACHE_NONE
												)
										);	
		}


	public function create_session_id(){
		$session_id = $this->createSession(	$this->account_number, 
											$this->user_name, 
											$this->password, 
											$this->target
											);
		MystiflySession::where(['id' => 1])->update(['session_id' => $session_id]);
		return $session_id;
	}



	public function get_session_id(){
		$data = MystiflySession::where([ 'id' => 1 ])->first();
		if( $this->sessionIsExpired( $data->updated_at ) ){
			$session_id = $this->create_session_id();
		}else{
			if( $data->session_id != NULL ){
				$session_id = $data->session_id;
			}else{
				$session_id = $this->create_session_id();
			}
		}
		return $session_id;
	}

	public function create_session(){
		$session_id = $this->createSession(	$this->account_number, 
											$this->user_name, 
											$this->password, 
											$this->target
											);
		MystiflySession::truncate();
		$values = array(
			'account_number' => $this->account_number,
			'user_name'       => $this->user_name,
			'password'       => $this->password,
			'target'         => $this->target,
			'endpoint_url'   => $this->endpoint_url,
			'session_id'     => $session_id 
			);
		return MystiflySession::create($values);
	}

	
	private function createSession($AccountNumber, $user_name, $password, $target){
		$params = new stdClass();
		$params->rq = new stdClass();
		$params->rq->AccountNumber = $AccountNumber;
		$params->rq->user_name      = $user_name;
		$params->rq->password      = $password;
		$params->rq->target        = $target;
		//$this->logRequest('CreateSession', $params);
		try {
			$response              = $this->client->CreateSession( $params );
			$res                   = get_object_vars($response);
			//$this->logResponse('CreateSession', $response);
			$create_session_result = get_object_vars($res['CreateSessionResult']);
			$session_id            = $create_session_result['SessionId'];
			return $session_id;
		} catch (Exception $e) {
			// return 'An errors has occured while calling the mystifly api create session';
			Log::error('Caught exception createSession : '. $e->getMessage() );
		}
	}

	
	private function getSessionExpiryTime($dt){
		$datetime = new \DateTime($dt, new \DateTimeZone(Config::get('app.timezone')));
		$datetime->add(new \DateInterval('PT19M'));
		return $datetime; 
	}


	private function sessionIsExpired($updated_at){
		$now = new \DateTime(NULL, new \DateTimeZone(Config::get('app.timezone')));
		if( $this->getSessionExpiryTime( $updated_at ) < $now ){
			return TRUE;
		}
		return FALSE;
	}



	public function airLowFareSearchCall(){

		$Input   = Input::all();
		// $Input = '{"DepartureDate":"2018-09-25","OriginLocationCode":"LHR","DestinationLocationCode":"CDG","CabinPreference":"Y","Code":["ADT"],"Quantity":["2"]}';
		//$Input = json_decode($Input,true);


		Log::channel('mystifly')->info('===============Request=============');
        // Log::channel('mystifly')->info([
        //    'headers' => 'Mystifly request  Request',
        //    'data' => json_decode($Input,true)
        // ]);

		// Input::merge(['Code' => array('ADT','CHD','INF') ]);
		// Input::merge(['Quantity' => array('2','1','1') ]);
		// Input::merge(['DepartureDate' => '2018-10-10']);
		// Input::merge(['DestinationLocationCode' => 'GLA']);
		// Input::merge(['OriginLocationCode' => 'DUB']);
		// Input::merge(['CabinPreference' => 'Y']);
		
		$CabinPreference = Input::get('CabinPreference');
		
		if($CabinPreference == 'Any')
		{
			Input::merge(['CabinPreference' => 'Y']);
			Input::merge(['CabinType' => 'Y']);
			Input::merge(['PreferenceLevel' => 'Preferred']);
		}
		if($CabinPreference == 'Y')
		{
			Input::merge(['CabinPreference' => 'Y']);
			Input::merge(['CabinType' => 'Y']);
			Input::merge(['PreferenceLevel' => 'Restricted']);
		}
		if($CabinPreference == 'C')
		{
			Input::merge(['CabinPreference' => 'C']);
			Input::merge(['CabinType' => 'C']);
			Input::merge(['PreferenceLevel' => 'Restricted']);
		}
		if($CabinPreference == 'F')
		{
			Input::merge(['CabinPreference' => 'F']);
			Input::merge(['CabinType' => 'F']);
			Input::merge(['PreferenceLevel' => 'Restricted']);
		}
		if($CabinPreference == 'S')
		{
			Input::merge(['CabinPreference' => 'S']);
			Input::merge(['CabinType' => 'S']);
			Input::merge(['PreferenceLevel' => 'Restricted']);
		}

		 $PricingSourceType = config('mystifly.PricingSourceType');
		 $IsRefundable = config('mystifly.IsRefundable');
		 $IsResidentFare = config('mystifly.IsResidentFare'); 
		

		$success = TRUE;
		$data    = array();
		$error   = array();

		$result = $this->airLowFareSearch(
				$this->get_session_id(),								
				$PricingSourceType,    				
				$IsRefundable,						
				$this->passengerTypeQuantities(  					
					$this->passengerTypeQuantity(					
						Input::get('Code'),          				
						Input::get('Quantity') 					
						)
					),	
				Input::get('RequestOptions', 'Fifty'),     			
				$this->convertToBoolean(
					Input::get('IsResidentFare',$IsResidentFare)					
					),				
				$this->target,												
				$this->convertToBoolean(
					Input::get('NearByAirports', 0)					
					),				
				$this->originDestinationInformations( 				
					$this->originDestinationInformation( 			
						$this->departureDateTime(					
							Input::get('DepartureDate'),
							Input::get('DepartureTime', '00:00:00')
							),
						Input::get('DestinationLocationCode'),		
						Input::get('OriginLocationCode')			
						)
					), 
				$this->travelPreferences(							
					Input::get('AirTripType', 'OneWay'),  					
					Input::get('CabinPreference'),					
					Input::get('MaxStopsQuantity', 'All'),
					$this->cabinClassPreference(				
						Input::get('CabinType'),
						Input::get('PreferenceLevel')
						),
					Input::get('VendorPreferenceCodes', NULL) 			
					)
				);


		$origin_airport_temp      = CityIata::where(['iata_code' => $Input['OriginLocationCode']])->first();
		$destination_airport_tmp  = CityIata::where(['iata_code' => $Input['DestinationLocationCode']])->first();
		$origin_airport_info      = !empty($origin_airport_temp) ? $origin_airport_temp->toArray() : [];
		$destination_airport_info = !empty($destination_airport_tmp) ? $destination_airport_tmp->toArray() : [];
 		$result                   = json_decode( json_encode($result), TRUE);
		$result                   = $result['AirLowFareSearchResult'];
		$result                   = array_merge($result, [
			'origin_airport_info' => $origin_airport_info,
			'destination_airport_info' => $destination_airport_info
			]);
		if( $result['Success'] === TRUE )
		{
			if( is_array($result['PricedItineraries']) && count( $result['PricedItineraries']) >= 1 )
			{
				$priced_itineraries = $result['PricedItineraries']['PricedItinerary'];
				foreach( $priced_itineraries as $pr_it_key => $priced_itinerary )
				{
					if( isset( $priced_itinerary['OriginDestinationOptions'] ) )
					{
						$flight_segments = $priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'];	
						if(! isset($flight_segments['FlightSegment']['ArrivalAirportLocationCode'])  )
						{
							foreach($flight_segments['FlightSegment'] as $fl_se_key => $flight_segment)
							{
								$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
								$operating_airline_code = $flight_segment['OperatingAirline']['Code'];
								$arrival_data           = CityIata::where([
										'iata_code' => $flight_segment['ArrivalAirportLocationCode']
									])->first();
								$departure_data         = CityIata::where([
										'iata_code' => $flight_segment['DepartureAirportLocationCode']
									])->first();
								
								$marketing_airline      = Airline::where([
										'airline_code' => $marketing_airline_code
									])->orWhere([
										'airline_code' => $marketing_airline_code
									])->first();
								
								$flight_segment = array_merge( $flight_segment, [
									'MarketingAirlineName' => ($marketing_airline) ? $marketing_airline->airline_name: NULL,
									'ArrivalData'          => ($arrival_data) ? $arrival_data->airport_name.', '.$arrival_data->iata_code.' '.$arrival_data->city_name: NULL,
									'DepartureData'        => ($departure_data) ? $departure_data->airport_name.', '.$departure_data->iata_code.' '.$departure_data->city_name: NULL,
									]
								);

								$operating_airline = Airline::where([
										'airline_code' => $operating_airline_code
									])->orWhere([
										'airline_code' => $operating_airline_code
									])->first();

								$flight_segments['FlightSegment'][$fl_se_key] = array_merge( $flight_segments['FlightSegment'][$fl_se_key], $flight_segment );		

								if( $operating_airline )
								{
									$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
										['Name' => $operating_airline->airline_name]
										);
								}							
								$flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
							}

						}
						else
						{

							foreach($flight_segments as $fl_se_key => $flight_segment)
							{
								$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
								$operating_airline_code = $flight_segment['OperatingAirline']['Code'];
								$arrival_data      = CityIata::where([
										'iata_code' => $flight_segment['ArrivalAirportLocationCode']
									])->first();
								$departure_data    = CityIata::where([
										'iata_code' => $flight_segment['DepartureAirportLocationCode']
									])->first();

								if( !empty($arrival_data->country_code) && !empty($departure_data->country_code) ){
									$marketing_airline = Airline::where([
											'airline_code' => $marketing_airline_code,
											'country_code' => $arrival_data->country_code
										])->orWhere([
											'airline_code' => $marketing_airline_code,
											'country_code' => $departure_data->country_code
										])->first();
								}

								$flight_segment = array_merge( $flight_segment, [
									'MarketingAirlineName' => !empty($marketing_airline) ? $marketing_airline->airline_name: NULL,
									'ArrivalData'          => !empty($arrival_data) ? $arrival_data->airport_name.', '.$arrival_data->iata_code.' '.$arrival_data->city_name: NULL,
									'DepartureData'        => !empty($departure_data) ? $departure_data->airport_name.', '.$departure_data->iata_code.' '.$departure_data->city_name: NULL,
									]
								);

								if( !empty($arrival_data->country_code) && !empty($departure_data->country_code) ){
									$operating_airline = Airline::where([
											'airline_code' => $operating_airline_code,
											'country_code' => $arrival_data->country_code
										])->orWhere([
											'airline_code' => $operating_airline_code,
											'country_code' => $departure_data->country_code
										])->first();
								}

								$flight_segments[$fl_se_key] = array_merge( $flight_segments[$fl_se_key], $flight_segment );
								if( !empty($operating_airline) ){
									$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
										['Name' => $operating_airline->airline_name]
										);
								}		
								$flight_segments[$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments[$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
							}
						}
						$priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'] = $flight_segments;
						$priced_itineraries[ $pr_it_key ] = $priced_itinerary;
						$validating_airline_code          = $priced_itinerary['ValidatingAirlineCode'];
						$airline                          = Airline::where(['airline_code' => $validating_airline_code])->first();
						if( !empty($airline) ){
							$validating_airline_name        = array('ValidatingAirlineName' => $airline->airline_name);
							$priced_itineraries[$pr_it_key] = array_merge($priced_itineraries[$pr_it_key], $validating_airline_name );
						}	

					}
					else
					{
						$success = FALSE;
						foreach( $result['Errors'] as $k => $v){
							array_push($error, $v);
						}	
					}	
				}
				$result['PricedItineraries']['PricedItinerary'] = $priced_itineraries;
				$data = $result;

			}else{
				$success = FALSE;
				foreach( $result['Errors'] as $k => $v){
					array_push($error, $v);
				}	
			}
		}else{
			$success = FALSE;
			foreach( (array)$result['Errors'] as $k => $v){
				array_push($error, $v);
			}
		}
		
        Log::error( [
           'headers' => 'Mystifly error',
           'error' => $error
       ] );
		return response_format($success, $data, $error);		
	}
	

	private function airLowFareSearch(
		$SessionId, 
		$PricingSourceType, 
		$IsRefundable, 
		$PassengerTypeQuantities, 
		$RequestOptions, 
		$IsResidentFare, 
		$target, 
		$NearByAirports, 
		$OriginDestinationInformations, 
		$TravelPreferences 
	)
	{	
		$ptq_max_count = 2; // PassengerTypeQuantity maximum times can be nested in PassengerTypeQuantities
		$ptq_count     = 0;
		$params = new stdClass();
		$params->rq = new stdClass();
		$params->rq->SessionId                     = $SessionId;	
		$params->rq->OriginDestinationInformations = $OriginDestinationInformations;
		$params->rq->TravelPreferences             = $TravelPreferences;
		$params->rq->PricingSourceType             = $PricingSourceType;		
		$params->rq->IsRefundable                  = $IsRefundable;
		$params->rq->PassengerTypeQuantities       = array();
		foreach($PassengerTypeQuantities as $key => $value)
		{
			if($ptq_count < $ptq_max_count)
			{
				$params->rq->PassengerTypeQuantities = array_merge( $params->rq->PassengerTypeQuantities, array($key => $value) );
				$ptq_count++;
			}
		}
		$params->rq->RequestOptions                = $RequestOptions;
		$params->rq->NearByAirports                = $NearByAirports;
		$params->rq->IsResidentFare                = $IsResidentFare;
		$params->rq->target                        = $target;
		
		$send_success = TRUE;
		try{
			$response = $this->client->AirLowFareSearch( $params );
			$send_success = FALSE;
		}catch(Exception $e){
			
			Log::error('ERROR MESSAGE MSYTIFLY CALL:'.$e->getMessage() );
		}
		
		return (array)$response;
	}

	public function airRevalidateCall()
	{
		
		//Input::merge(['FareSourceCode' => $this->FareSourceCode]);
		
		$success = FALSE;
		$data    = [];
		$error   = [];
		
		$params = new stdClass();
		$params->rq = new stdClass();
		$params->rq->SessionId = $this->get_session_id();
		$params->rq->FareSourceCode = Input::get('FareSourceCode');
		$params->rq->target = $this->target;
		
		try {
				$response = $this->client->AirRevalidate( $params );
				$result = $response->AirRevalidateResult;
				//echo "<pre>";print_r($result);exit;
			if($result->Success){
				$success = TRUE;
				$data = $result->PricedItineraries;

			} else {
				foreach( $result->Errors as $k => $v ) {
					array_push( $error, $v );
				}	
			}
		}
		catch( Exception $e ) {
			$error = ['An error has occured during mystifly airRevalidateCall'];
		}
		return response_format( $success, $data, $error );
	}

	public function bookingCall(){


		//"Message":"Invalid Child's Age. Should be less than or equal to 12 years"
		//"Message":"Invalid Infant's Age. Should be less than or equal to 2 years."
		//Max 9 Pax
		//No of infant should be less or equal to adult
		//"Message":"Passengers (PassengerFirstName) name cannot be same"


		Input::merge(['Code' => array('ADT','CHD','INF') ]);
		Input::merge(['Quantity' => array('2','1','1') ]);

		$email = array('irfan@corigami.com.au','irfan@corigami.com.au','irfan@corigami.com.au','irfan@corigami.com.au');
		$PhoneNumber = array('9455451554','9455451554','9455451554','9455451554');
		$DateOfBirth = array('1990-07-07T00:00:00','1990-07-07T00:00:00','2010-07-07T00:00:00','2017-07-07T00:00:00');
		$Gender = array('M','M','M','M');
		$PassengerTitle = array('MR','MR','MR','MR');
		$PassengerFirstName = array('Irfan','Chintan','Anjali','Rekha');

		$PassengerLastName = array('Bagwala','Patel','Vadukar','Patel');
		$PassengerType = array('ADT','ADT','CHD','INF');
		$Country = array('IN','IN','IN','IN');
		$ExpiryDate = array('2030-07-07T00:00:00','2030-07-07T00:00:00','2030-07-07T00:00:00','2030-07-07T00:00:00');
		$PassportNumber = array('HG412310','HG412310','HG412310','HG412310');
		$MealPreference = array('Any','Any','Any','Any');
		$SeatPreference = array('Any','Any','Any','Any');

		Input::merge(['FareSourceCode' => $this->FareSourceCode]);
		Input::merge(['passenger_dob' => $DateOfBirth]);
		Input::merge(['passenger_contact_no' => $PhoneNumber]);
		Input::merge(['passenger_email' => $email]);
		Input::merge(['passenger_gender' => $Gender]);
		Input::merge(['passenger_title' => $PassengerTitle]);
		Input::merge(['passenger_first_name' => $PassengerFirstName]);
		Input::merge(['passenger_last_name' => $PassengerLastName]);
		Input::merge(['passenger_type' => $PassengerType]);
		Input::merge(['passenger_country' => $Country]);
		Input::merge(['passenger_passport_expiry_date' => $ExpiryDate]);
		Input::merge(['passenger_passport_number' => $PassportNumber]);
		Input::merge(['passenger_meal_preference' => $MealPreference]);
		Input::merge(['passenger_seat_preference' => $SeatPreference]);

		$cnt = count(Input::get('passenger_dob'));
		$input = Input::all();
		$TravelerInfo = array();
		$PassengerName = array();
		for($i=0;$i<$cnt;$i++) {
			$AirTraveler['AirTraveler'][$i]['DateOfBirth'] = Input::get('passenger_dob')[$i];
			$AirTraveler['AirTraveler'][$i]['Gender'] = Input::get('passenger_gender')[$i];

			$PassengerName['PassengerFirstName'] = Input::get('passenger_first_name')[$i];
			$PassengerName['PassengerLastName'] = Input::get('passenger_last_name')[$i];
			$PassengerName['PassengerTitle'] = Input::get('passenger_title')[$i];
			$PassengerName['PassengerType'] = Input::get('passenger_type')[$i];
			
			$AirTraveler['AirTraveler'][$i]['PassengerName'] = $PassengerName;

			$AirTraveler['AirTraveler'][$i]['PassengerNationality'] = Input::get('passenger_country')[$i];
			$AirTraveler['AirTraveler'][$i]['PassengerType'] = Input::get('passenger_type')[$i];

			$Passport['Country'] = Input::get('passenger_country')[$i];
			$Passport['ExpiryDate'] = Input::get('passenger_passport_expiry_date')[$i];
			$Passport['PassportNumber'] = Input::get('passenger_passport_number')[$i];

			//$AirTraveler['AirTraveler'][$i]['Passport'] = $Passport;

			$SpecialServiceRequest['MealPreference'] = Input::get('passenger_meal_preference')[$i];
			$SpecialServiceRequest['SeatPreference'] = Input::get('passenger_seat_preference')[$i];

			$AirTraveler['AirTraveler'][$i]['SpecialServiceRequest'] = $SpecialServiceRequest;

			$AirTraveler['AirTraveler'][$i]['AreaCode'] = '080';
			$AirTraveler['AirTraveler'][$i]['CountryCode'] = '091';
			$AirTraveler['AirTraveler'][$i]['Email'] = 'abc@mystifly.com';
			$AirTraveler['AirTraveler'][$i]['PhoneNumber'] = '7748057256';
			$AirTraveler['AirTraveler'][$i]['PostCode'] = '462030';
			
		}

		$TravelerInfo['AirTravelers'] = $AirTraveler;
		
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->SessionId       = $this->get_session_id();
		$params->rq->FareSourceCode  = Input::get('FareSourceCode');
		$params->rq->TravelerInfo    = $TravelerInfo;
		$params->rq->target          = $this->target;
		
		$response              = $this->client->BookFlight( $params );
		$res                   = get_object_vars($response);
		$res['successful'] =true;
		echo '<pre>';print_r($res);
		echo '<pre>';print_r($TravelerInfo);
		exit;
		return json_encode($res);
	} 

	public function messageQueueCall(){
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		
		$params->rq->CategoryId  = 'Cancelled';
		$params->rq->SessionId       = $this->get_session_id();
		$params->rq->target          = $this->target;
		$response              = $this->client->MessageQueues( $params );
		$res                   = get_object_vars($response);
		$res['successful'] =true;
		// echo '<pre>';print_r($res);
		// exit;
		return json_encode($res);
	}

	public function fareRulesCall(){
	
		$success = TRUE;
		$data    = array();
		$error   = array();
		
		//$FareSourceCode = $this->FareSourceCode;
		$params     = new stdClass();
		$params->rq = new stdClass();
		$params->rq->SessionId      = $this->get_session_id();
		$params->rq->FareSourceCode = Input::get('FareSourceCode');
		$params->rq->target         = $this->target;
		$response                   = $this->client->FareRules1_1( $params );
		$result                        = get_object_vars($response);

		if( (int)$result['FareRules1_1Result']->Success === 1 ){
			$data = ['BaggageInfos' => (array)$result['FareRules1_1Result']->BaggageInfos,
					'FareRules' => (array)$result['FareRules1_1Result']->FareRules
					];
		}else{
			$success = FALSE;
			foreach( (array)$result['FareRules1_1Result']->Errors as $k => $v ){
				array_push($error, $v);
			}	
		}
		return response_format($success, $data, $error);
	}

	public function ticketOrderCall(){
		$FareSourceCode = $this->FareSourceCode;
		$UniqueID = $this->UniqueID;
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->SessionId       = $this->get_session_id();
		$params->rq->FareSourceCode  = $FareSourceCode;
		$params->rq->UniqueID        = $UniqueID;
		$params->rq->target          = $this->target;
		//echo json_encode($params);
		$response              = $this->client->TicketOrder( $params );
		$res                   = get_object_vars($response);
		
		echo '<pre>';print_r($res);
		exit;	
		echo json_encode($res);	
		//echo response()->json(get_object_vars($response));		
	}
	
	public function addBookingNotesCall()
	{
		$UniqueID = $this->UniqueID;
		$Notes = $this->Notes;
		$params                = new stdClass();
		$params->rq            = new stdClass();
		$params->rq->SessionId = $this->get_session_id();
		$params->rq->UniqueID  = $UniqueID;
		$params->rq->Notes     = $Notes;
		$params->rq->target    = $this->target;
		$response              = $this->client->AddBookingNotes( $params );
		$res                   = get_object_vars($response);
		
		echo '<pre>';print_r($res);
		exit;
		echo json_encode($res);	
	}
	public function tripDetailsCall(){
		$UniqueID = $this->UniqueID;
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->SessionId       = $this->get_session_id();
		$params->rq->UniqueID        = $UniqueID;
		$params->rq->target          = $this->target;
		//echo json_encode($params);
		$response              = $this->client->TripDetails( $params );
		$res                   = get_object_vars($response);
		echo '<pre>';print_r($res);
		exit;	
		echo json_encode($res);			
		//echo response()->json(get_object_vars($response));
	}

	private function extraServices1_1($ExtraServiceId){
		$esi_max_count = 8; // maximum of 8 values for Services1_1
		$d                  = array();
		$data               = array();
		$data['Services']   = array();
		if($ExtraServiceId){
			foreach($ExtraServiceId as $key => $value){
				$values = array();
				$values['Services'] = array();
				foreach($value as $k => $v){
					array_push($values['Services'], array('ExtraServiceId' => $v));
				}
				array_push($data, $values);				
			}	
		}	
		return $data;
	}
	private function originDestinationInformations($OriginDestinationInformation){
		$data = array();
		$data['OriginDestinationInformation'] = $OriginDestinationInformation;
		return $data;
	}

	
	private function originDestinationInformation(
		$DepartureDateTime, 
		$DestinationLocationCode, 
		$OriginLocationCode
	)
	{
		$data   = array();
		$values = array();
		if(!is_array($DepartureDateTime)){
			$data['DepartureDateTime']       = $DepartureDateTime;
			$data['DestinationLocationCode'] = $DestinationLocationCode;
			$data['OriginLocationCode']      = $OriginLocationCode;
			}
		if(is_array($DepartureDateTime)){
			foreach($DepartureDateTime as $key => $value){
				$values['DepartureDateTime']       = $value;
				$values['DestinationLocationCode'] = $DestinationLocationCode[$key];
				$values['OriginLocationCode']      = $OriginLocationCode[$key];
				array_push($data, $values);
				}
			}
		return $data;
	}

	
	private function passengerTypeQuantities($PassengerTypeQuantity){
		
		$data = array();
		$data['PassengerTypeQuantity'] = array();
		if($PassengerTypeQuantity){
			foreach($PassengerTypeQuantity as $key => $value){
				$data['PassengerTypeQuantity'] = array_merge($data['PassengerTypeQuantity'], array($key => $value));
			}
		}
		return $data;
	}	

	
	private function passengerTypeQuantity($Code, $Quantity){
		
		$data = array();
		foreach($Code as $key => $code){
			$data[$key]['Code'] = $code;
			$data[$key]['Quantity'] = $Quantity[$key];
		}
		return $data;
	}	

	
	private function travelPreferences(
		$AirTripType, 
		$CabinPreference, 
		$MaxStopsQuantity,
		$Preferences, 
		$VendorPreferenceCodes)
	{
		$data = array();
		$data['AirTripType']      = $AirTripType;
		$data['CabinPreference']  = $CabinPreference;
		$data['MaxStopsQuantity'] = $MaxStopsQuantity;		
		$data['Preferences']      = $Preferences;
		return $data;
	}

	
	private function cabinClassPreference($CabinType, $PreferenceLevel){
		$data = array();
		$data['CabinType']        = $CabinType;
		$data['PreferenceLevel']  = $PreferenceLevel;
		return $data;
	}	

	public function departureDateTime($date, $time){
		$t = new \DateTime('2000-01-01');
		return $date.'T'.$t->format('H:i:s');
	}

	

	public function convertToBoolean($val){
		if($val == 0)
			return FALSE;
		return TRUE;
	}	
	public function response_format($success, $data, $error){
		if(is_array($data)){
			if(count($data) < 1)
				$data = NULL;
		}
		if(is_array($error)){
			if(count($error) < 1)
				$error = NULL;
		}		
		$result = array(
					'success' => $success,
					'data' => $data,
					'error' => $error
					);
		return \Response::json( $result );
	}

}
