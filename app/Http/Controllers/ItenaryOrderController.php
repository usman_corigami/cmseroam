<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\ItenaryOrder;
use App\Itinerary;
use App\ItenaryLeg;
use App\LegDetail;
use App\PassengerInformation;
use App\BookingCommission;
use App\ViatorBookingDetail;
use App\AgentNotification;

use App\Country;
use App\CitiesLatLong;
use App\City;
use App\BookingAmount;
use App\Roomerflex;
use DB;
use Log;

class ItenaryOrderController extends Controller
{
	public function __construct(){
	}

	public function saveOrderDetail(){ 

		$data = Input::All();
		$ItenaryOrder = new  ItenaryOrder;

        if(isset($data['user_id']) && $data['user_id'] != ''){
			$ItenaryOrder->user_id = $data['user_id'];
		}else{
			$ItenaryOrder->user_id = '';
		}
		$ItenaryOrder->invoice_no = $data['invoiceNumber'];
		$ItenaryOrder->from_city_id = $data['from_city_id'];
		$ItenaryOrder->to_city_id = $data['to_city_id'];
		$ItenaryOrder->from_city_name = $data['from_city_name'];
		$ItenaryOrder->to_city_name = $data['to_city_name'];
		$ItenaryOrder->num_of_travellers = $data['travellers'];
		$ItenaryOrder->travel_date = $data['travel_date'];
		$ItenaryOrder->total_itenary_legs = $data['total_itenary_legs'];
		$ItenaryOrder->total_amount = $data['total_amount'];
		$ItenaryOrder->total_days = $data['total_days'];
		$ItenaryOrder->currency = $data['currency'];
		$ItenaryOrder->total_per_person = $data['total_per_person'];
		$ItenaryOrder->evay_transcation_id = isset($data['evay_transcation_id']) ? $data['evay_transcation_id'] : null;
		$ItenaryOrder->payment_method = isset($data['payment_method']) ? $data['payment_method'] : null;
		$ItenaryOrder->recipt_number = isset($data['recipt_number']) ? $data['recipt_number'] : null;
		$ItenaryOrder->card_number = isset($data['card_number']) ? $data['card_number'] : null;
		$ItenaryOrder->expiry_month = isset($data['expiry_month']) ? $data['expiry_month'] : null;
		$ItenaryOrder->expiry_year = isset($data['expiry_year']) ? $data['expiry_year'] : null;
		$ItenaryOrder->cvv = isset($data['expiry_year']) ? $data['cvv'] : null;
		$ItenaryOrder->cost_per_day = $data['cost_per_day'];
		$ItenaryOrder->from_date = $data['from_date'];
		$ItenaryOrder->to_date = $data['to_date'];
		$ItenaryOrder->voucher = $data['voucher'];
		$ItenaryOrder->status = $data['status'];
		$ItenaryOrder->from_city_to_city = $data['from_city_to_city'];
		$ItenaryOrder->agent_id = isset($data['agent_id'])?$data['agent_id']:'';
		$ItenaryOrder->deposit_amount = isset($data['deposit_amount'])?$data['deposit_amount']:0;
		$ItenaryOrder->pending_amount = isset($data['pending_amount'])?$data['pending_amount']:0;
		        	
		$ItenaryOrder->save();
   
        $ItenaryOrderLastId = $ItenaryOrder->order_id;
        /*For Notification save start here*/
		
		if($ItenaryOrderLastId) {
			
			$ba_array = array(
			"order_id" => $ItenaryOrderLastId,
			"total" => $data['total_amount'],
			"deposit" => isset($data['deposit_amount'])?$data['deposit_amount']:0,
			"pending" => isset($data['pending_amount'])?$data['pending_amount']:0
			);	
			
			//echo "<pre>"; print_r($ba_array);die;
			
			$store_booking_amount = BookingAmount::create($ba_array);
			
		}
		
        if(!empty($data['agent_id'])){
        	$agent_strtotime = !empty($data['to_date']) ? $data['to_date'] :date('Y-m-d',time());
			$agent_to_date = strtotime($agent_strtotime);
			$agentDate = strtotime("+2 day", $agent_to_date);
	        $AgentNotification2 = AgentNotification::create([
					'agent_id'=> !empty($data['agent_id']) ? $data['agent_id'] : '',
					'booking_id'=> $ItenaryOrderLastId,
					'read'=> 0,
					'notification'=> !empty($data['agent_notification']) ? $data['agent_notification'] :'',
					'showing_date'=> date('Y-m-d', $agentDate),
					'type'=> 'Itenary Home Follow-Up',
				]);	
        }
        
        /*For Notification save finish here*/

        //echo $ItenaryOrderLastId;exit;
        foreach($data['leg'] as $key => $leg){
        	Log::debug('Log'.$key);
        	Log::debug($leg);

        	$ItenaryLeg = ItenaryLeg::create([
				'itenary_order_id'=> $ItenaryOrderLastId,
				'from_city_id'=>$leg['from_city_id'],
				'to_city_id'=>$leg['to_city_id'],
				'from_date'=>$leg['from_date'],
				'to_date'=>$leg['to_date'],
				'country_code'=>$leg['country_code'],
				'to_city_name'=>$leg['to_city_name'],
				'from_city_name'=>$leg['from_city_name'],
			]);
			$ItenaryLegLastId = $ItenaryLeg->itenary_leg_id;

			if(!empty($leg['hotel'])){
				foreach($leg['hotel'] as $key => $hotel){
					$hotel_price = $hotel['price'];
					if(empty($hotel['price'])){
						$hotel_price = 0.00;
					}else{
						$hotel_price = $hotel['price'];
						$hotel_price = str_replace(',','',$hotel_price);
					}

					$hotel_notes = isset($data['hotel_notes']) ? $data['hotel_notes'] : '';

					$specialInformation = (isset($hotel['specialInformation']))? $hotel['specialInformation'] : '';
					$voucher = (isset($hotel['voucher']))? $hotel['voucher'] : '';

					$bedTypeIds = '';
					if(isset($hotel['room'])){
						$rooms = $hotel['room'];
						$bt = 0;
						foreach ($rooms as $room) {
							$bedTypeIds .= $room['bedTypeId'];
							if($bt > 0) { $bedTypeIds .= ','; }
							$bt++;
						}
					} else {
						$bedTypeIds = isset($hotel['bedTypeId']) ? $hotel['bedTypeId'] : '';
					}

					$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> $hotel['leg_type'],
						'leg_id'=> 0,
						'leg_name'=> $hotel['leg_name'],
						'hotel_room_type_id'=> $hotel['hotel_room_type_id'],
						'hotel_room_type_name'=> $hotel['hotel_room_type_name'],
						'hotel_room_code'=> $hotel['hotel_room_code'],
						'hotel_rate_code'=> $hotel['hotel_rate_code'],
						'hotel_price_per_night'=> $hotel['hotel_price_per_night'],
						'hotel_tax'=> $hotel['hotel_tax'],
						'hotel_expedia_subtotal'=> $hotel['hotel_expedia_subtotal'],
						'hotel_expedia_total'=> $hotel['hotel_expedia_total'],
						'hotel_eroam_subtotal'=> $hotel['hotel_eroam_subtotal'],
						'currency'=> $hotel['currency'],
						'customer_currency'=>isset($hotel['customer_currency']) ? $hotel['customer_currency'] : 'AUD',
						'conversion_rate'=> isset($hotel['conversion_rate']) ? $hotel['conversion_rate'] : '1',
						'provider'=>$hotel['provider'],
						'booking_id'=> $hotel['booking_id'],
						'provider_booking_status'=> $hotel['provider_booking_status'],
						'hotel_total_room'=> isset($hotel['hotel_total_room']) ? $hotel['hotel_total_room'] : count($hotel['room']),
						'booking_error_code'=> $hotel['booking_error_code'],
						'nights'=> $hotel['nights'],
						'from_date'=> $hotel['checkin'],
						'to_date'=> $hotel['checkout'],
						'price'=> $hotel_price,
						'notes'=> $hotel_notes,
						'SpecialInstructions' => $specialInformation,
						'BedTypeIds' => $bedTypeIds,
						'special_requirements' => $voucher,
					]);

					if(isset($hotel['detailPricing'])) {
						$detailPricing = $hotel['detailPricing'];
						$licensee_applied_rrp = isset($detailPricing['licensee_applied_rrp']) ? $detailPricing['licensee_applied_rrp'] : $detailPricing['licensee_rrp'];
						$change_in_rrp = isset($detailPricing['change_in_rrp']) ? $detailPricing['change_in_rrp'] : 0;
						$booking_commission = BookingCommission::create([
							'booking_id' => $ItenaryOrderLastId,
							'legdetail_id' => $LegDetail->leg_detail_id,
							'product_type' => 'hotel',
							'licensee_id' => $detailPricing['licensee_id'],
							'reseller_cost' => $detailPricing['reseller_cost'],
							'reseller_rrp_per' => $detailPricing['reseller_rrp_per'],
							'reseller_rrp' => $detailPricing['reseller_rrp'],
							'reseller_margin' => $detailPricing['reseller_margin'],
							'licensee_commission' => $detailPricing['licensee_commission'],
							'licensee_rrp' => $detailPricing['licensee_rrp'],
							'licensee_cost' => $detailPricing['licensee_cost'],
							'licensee_benefit' => $detailPricing['licensee_benefit'],
							'reseller_benefit' => $detailPricing['reseller_benefit'],
							'licensee_applied_rrp' => $licensee_applied_rrp,
							'change_in_rrp' => $change_in_rrp,
						]);
					}
				}
			}else{
				$hotel_notes_own = isset($data['hotel_notes_own']) ? $data['hotel_notes_own'] : '';
				$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> 'hotel',
						'leg_name'=> 'Own Arrangement',
						'notes'=> $hotel_notes_own,
					]);
			}
			if(!empty($leg['activities'])){
				foreach($leg['activities'] as $key => $activities){
					$activities_price = $activities['price'];
					if(empty($activities['price'])){
						$activities_price = 0.00;
					}else{
						$activities_price = $activities['price'];
						$activities_price = str_replace(',','',$activities_price);
					}
					$merchant_net_price = (isset($activities['merchant_net_price'])) ? $activities['merchant_net_price'] : 0;
					$activity_notes = isset($data['activity_notes']) ? $data['activity_notes'] : '';
					$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> $activities['leg_type'],
						'leg_id'=> 0,
						'leg_name'=> $activities['leg_name'],
						'from_date'=> $activities['from_date'],
						'price'=> $activities_price,
						'currency'=> $activities['currency'],
						'customer_currency'=>isset($hotel['customer_currency']) ? $hotel['customer_currency'] : 'AUD',
						'conversion_rate'=> isset($hotel['conversion_rate']) ? $hotel['conversion_rate'] : '1',
						//'customer_currency'=> $activities['customer_currency'],
						//'conversion_rate'=> $activities['conversion_rate'],
						'provider'=>$activities['provider'],
						'supplier_id'=>$activities['supplier_id'],
						'booking_id'=>$activities['booking_id'],
						'provider_booking_status'=>$activities['provider_booking_status'],
						'booking_error_code'=>$activities['booking_error_code'],
						'leg_type_name'=>$activities['leg_type_name'],
						'voucher_key'=>$activities['voucher_key'],
						'voucher_url'=>$activities['voucher_url'],
						'merchant_net_price' => $merchant_net_price,
						'tour_grade_code' => @$activities['tour_grade'],
						'itinerary_id' => @$activities['itineraryId'],
						'distributor_ref' => @$activities['distributorRef'],
						'item_id' => @$activities['itemId'],
						'distributor_item_ref' => @$activities['distributorItemRef'],
						'notes'=> $activity_notes,
						'special_requirements' => @$activities['specialRequirements'],
						'language_option_code' => @$activities['languageOptionCode'],
					]);

					if($activities['provider'] == 'viator')
					{
						if((isset($activities['hotelId']) && $activities['hotelId'] != null) || (isset($activities['pickupPoint']) && $activities['pickupPoint'] != null ) || (isset($activities['bookingQuestionAnswers']) && !empty($activities['bookingQuestionAnswers'])))
						{
							$ViatorBookingDetail = ViatorBookingDetail::create([
								'itenary_order_id' => $ItenaryOrderLastId,
								'itenary_leg_id' => $ItenaryLegLastId,
								'hotelId' => $activities['hotelId'],
								'pickupPoint' => $activities['pickupPoint'],
								'bookingQuestionAnswers' => json_encode($activities['bookingQuestionAnswers'])
							]);
						}
					}

					if(isset($activities['tourGrades']['detailPricing']) || isset($activities['detailPricing'])) {
						$detailPricing = isset($activities['tourGrades']['detailPricing']) ? $activities['tourGrades']['detailPricing'] : $activities['detailPricing'];
						$licensee_applied_rrp = isset($detailPricing['licensee_applied_rrp']) ? $detailPricing['licensee_applied_rrp'] : $detailPricing['licensee_rrp'];
						$change_in_rrp = isset($detailPricing['change_in_rrp']) ? $detailPricing['change_in_rrp'] : 0;
						$booking_commission = BookingCommission::create([
							'legdetail_id' => $LegDetail->leg_detail_id,
							'booking_id' => $ItenaryOrderLastId,
							'product_type' => 'activity',
							'licensee_id' => $detailPricing['licensee_id'],
							'reseller_cost' => $detailPricing['reseller_cost'],
							'reseller_rrp_per' => $detailPricing['reseller_rrp_per'],
							'reseller_rrp' => $detailPricing['reseller_rrp'],
							'reseller_margin' => $detailPricing['reseller_margin'],
							'licensee_commission' => $detailPricing['licensee_commission'],
							'licensee_rrp' => $detailPricing['licensee_rrp'],
							'licensee_cost' => $detailPricing['licensee_cost'],
							'licensee_benefit' => $detailPricing['licensee_benefit'],
							'reseller_benefit' => $detailPricing['reseller_benefit'],
							'licensee_applied_rrp' => $licensee_applied_rrp,
							'change_in_rrp' => $change_in_rrp,
						]);
					}

				}
			}else{
				$activity_notes_own = isset($data['activity_notes_own']) ? $data['activity_notes_own'] : '';
				$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> 'activities',
						'leg_name'=> 'Own Arrangement',
						'notes'=> $activity_notes_own,
					]);
			}
			if(!empty($leg['transport'])){
				foreach($leg['transport'] as $key => $transport){
					$transport_price = $transport['price'];
					if(empty($transport['price'])){
						$transport_price = 0.00;
					}else{
						$transport_price = $transport['price'];
						$transport_price = str_replace(',','',$transport_price);
					}
					$transport_notes = isset($data['transport_notes']) ? $data['transport_notes'] : '';
					$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> $transport['leg_type'],
						'leg_id'=> 0,
						'from_city_id'=> $transport['from_city_id'],
						'to_city_id'=> $transport['to_city_id'],
						'from_city_name'=> $transport['from_city_name'],
						'to_city_name'=> $transport['to_city_name'],
						'departure_text'=> $transport['departure_text'],
						'arrival_text'=> $transport['arrival_text'],
						'booking_summary_text'=> $transport['booking_summary_text'],
						'duration'=> $transport['duration'],
						'price'=> $transport_price,
						'currency'=> $transport['currency'],
						'customer_currency'=>isset($hotel['customer_currency']) ? $hotel['customer_currency'] : 'AUD',
						'conversion_rate'=> isset($hotel['conversion_rate']) ? $hotel['conversion_rate'] : '1',
						//'customer_currency'=> $transport['customer_currency'],
						//'conversion_rate'=> $transport['conversion_rate'],
						'provider'=>$transport['provider'],
						'supplier_id'=>$transport['supplier_id'],
						'booking_id'=>$transport['booking_id'],
						'provider_booking_status'=>$transport['provider_booking_status'],
						'booking_error_code'=>$transport['booking_error_code'],
						'leg_type_name'=>$transport['leg_type_name'],
						'notes'=>$transport_notes
					]);

					if(isset($transport['detailPricing'])) {
						$detailPricing = $transport['detailPricing'];
						$licensee_applied_rrp = isset($detailPricing['licensee_applied_rrp']) ? $detailPricing['licensee_applied_rrp'] : $detailPricing['licensee_rrp'];
						$change_in_rrp = isset($detailPricing['change_in_rrp']) ? $detailPricing['change_in_rrp'] : 0;
						$booking_commission = BookingCommission::create([
							'legdetail_id' => $LegDetail->leg_detail_id,
							'booking_id' => $ItenaryOrderLastId,
							'product_type' => 'transport',
							'licensee_id' => $detailPricing['licensee_id'],
							'reseller_cost' => $detailPricing['reseller_cost'],
							'reseller_rrp_per' => $detailPricing['reseller_rrp_per'],
							'reseller_rrp' => $detailPricing['reseller_rrp'],
							'reseller_margin' => $detailPricing['reseller_margin'],
							'licensee_commission' => $detailPricing['licensee_commission'],
							'licensee_rrp' => $detailPricing['licensee_rrp'],
							'licensee_cost' => $detailPricing['licensee_cost'],
							'licensee_benefit' => $detailPricing['licensee_benefit'],
							'reseller_benefit' => $detailPricing['reseller_benefit'],
							'licensee_applied_rrp' => $licensee_applied_rrp,
							'change_in_rrp' => $change_in_rrp,
						]);
					}
				}
			}else{
				$transport_notes_own = isset($data['transport_notes_own']) ? $data['transport_notes_own'] : ''; 
				$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> 'transport',
						'leg_name'=> 'Own Arrangement',
						'notes'=> $transport_notes_own,
					]);
			}
			
        }

        // Code check ok
        foreach($data['passenger_info'] as $key => $passenger){
        	$is_lead = 'No';
        	if($key == 0){
				$is_lead ='Yes';
			}
   			$PassengerInformation = PassengerInformation::create([
				'itenary_order_id'=> $ItenaryOrderLastId,
				//'title'=>$passenger['passenger_title'],
				'first_name'=>$passenger['passenger_first_name'],
				'last_name'=>$passenger['passenger_last_name'],
				//'gender'=>$passenger['passenger_gender'],
				'dob'=> date('Y-m-d',strtotime($passenger['passenger_dob'])),
				//'country'=>$passenger['passenger_country'],
				'email'=>$passenger['passenger_email'],
				'contact'=>$passenger['passenger_contact_no'],
				//'address_one'=>$passenger['passenger_address_one'],
				//'address_two'=>$passenger['passenger_address_two'],
				//'suburb'=>$passenger['passenger_suburb'],
				//'state'=>$passenger['passenger_state'],
				//'zip'=>$passenger['passenger_zip'],
				'is_lead'=>$is_lead,
				'skyward_number' => isset($passenger['skyward_number']) ? $passenger['skyward_number'] : '',
				
			]);
        }

      
   		$PassengerInformation = PassengerInformation::create([
			'itenary_order_id'=> $ItenaryOrderLastId,
			'first_name'=>$data['billing_info']['passenger_first_name'],
			'last_name'=>$data['billing_info']['passenger_last_name'],
			'dob'=> date('Y-m-d',strtotime($data['billing_info']['passenger_dob'])),
			'country'=>$data['billing_info']['passenger_country'],
			'email'=>$data['billing_info']['passenger_email'],
			'contact'=>$data['billing_info']['passenger_contact_no'],
			'address_one'=>$data['billing_info']['passenger_address_one'],
			'address_two'=>$data['billing_info']['passenger_address_one'],
			'suburb'=>$data['billing_info']['passenger_suburb'],
			'state'=>$data['billing_info']['passenger_state'],
			'zip'=>$data['billing_info']['passenger_zip'],
			'is_billing'=>1
		]);

		if(isset($data['RoomerFlexBooking']) && !empty($data['RoomerFlexBooking'])){

			$Roomerflex = Roomerflex::create([
				'itenary_order_id'=> $ItenaryOrderLastId,
				'request_token'=> $data['RoomerFlexBooking']['request_token'],
				'request_refund_rate'=> $data['RoomerFlexBooking']['request_refund_rate'],
				'request_fee'=> $data['RoomerFlexBooking']['request_fee'],
				'request_fee_per_night'=> $data['RoomerFlexBooking']['request_fee_per_night'],
				'request_currency'=> $data['RoomerFlexBooking']['request_currency'],
				'protection_success'=> $data['RoomerFlexBooking']['protection_success'],
				'protection_registered'=> $data['RoomerFlexBooking']['protection_registered'],
				'protection_token'=> $data['RoomerFlexBooking']['protection_token']
			]);
		}

        return \Response::json([
				'successful' => true,
				'message' => 'Payment done Successfully.'
		]);
	}

	public function getItineraryDetail($order_id) {
     $itinerary_order = ItenaryOrder::where('order_id',$order_id)->first();
     $itinerary_leg = ItenaryLeg::where('itenary_order_id',$order_id)->get();
     $passenger_information = PassengerInformation::where('itenary_order_id',$order_id)->get();

     $response['itinerary_order'] = $itinerary_order;
     $response['passenger_information'] = $passenger_information;

     foreach ($itinerary_leg as $leg) {
      $country = Country::getCountryByCode($leg['country_code']);
      $city = City::getCityDetail($leg['from_city_id']);
            $cityLatLong = CitiesLatLong::where('city_id',$leg['from_city_id'])->first();
      if(!empty($country['name'])) {
       $leg['country_name'] = $country['name'];
      } else {
       $leg['country_name'] = '';
      }

            if(!empty($city['description'])) {
                $leg['city_description'] = $city['description'];
            } else {
                $leg['city_description'] = '';
            }
            if(!empty($city['small'])) {
                $leg['city_image'] = 'https://cms.eroam.com/'.$city['small'];
            } else {
                $leg['city_image'] = 'http://dev.eroam.com/assets/images/no-image-2.jpg';
            }

      $response['itinerary_leg'][] = $leg;
              
            $leg_detail = LegDetail::where('itenary_order_id',$order_id)->where('itenary_leg_id',$leg['itenary_leg_id'])->get();
            $leg['leg_detail'] = $leg_detail;
            $city_detail = [];
            
            $city_detail = ['cityId'=>$cityLatLong['city_id'],'lat'=>$cityLatLong['lat'],'lng'=>$cityLatLong['lng'],'name'=> $city['name']];   
             $response['city_detail'][] = $city_detail;


        }
     return \response()->json([
    'successful' => true, 'response' => $response
    ]);
    }
}