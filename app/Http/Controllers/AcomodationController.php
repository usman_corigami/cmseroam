<?php
//priya
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Hotel;
use App\HotelSupplier;
use App\HotelCategory;
use App\HotelSeason;
use App\Country;
use App\Currency;
use App\HotelImage;
use App\HotelRoomType;
use App\HotelMarkup;
use App\HotelPrice;
use App\HotelBasePrice;
use App\AOTLocation;
use App\AotSupplier;
use App\AotSupplierUpdated;
use App\HotelMarkupPercentage;
use App\HotelMarkupAgentCommission;
use App\HotelMarkupSupplierCommission;
use File;
use Image;
use DB;
use App\Domain;
use Session;
use Auth;
use App\Licensee;


class AcomodationController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function callHotelList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'acomodation' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('acomodation');

        session(['page_name' => 'acomodation']);
        $aData = session('acomodation') ? session('acomodation') : array();

        $oRequest->session()->forget('acomodation');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'h.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
      
        $oHotelList = Hotel::geHotelList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        //print_r($oHotelList);exit;
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oHotelList->currentPage(),'acomodation');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::acomodation._hotel_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::acomodation.hotel_list' : 'WebView::acomodation._hotel_list_ajax';
        
        return \View::make($oViewName, compact('oHotelList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callHotelCreate(Request $oRequest, $nIdHotel ='') 
    {
      
        if($nIdHotel != '')
            $oHotel = Hotel::find($nIdHotel);          
           // print_r($HotelImg);exit;
        if ($oRequest->isMethod('post'))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                    'name'              => 'required',
                                    'description'       => 'required',
                                    'reception_phone'   => 'required',
                                    'address_1'         => 'required',
                                    'currency_id'       => 'required',
                                    //'reception_phone'   => 'numeric|regex:/[0-9]{10}/',
                                    'hotel_category_id' => 'required',
                                    'city_id'           => 'required',
                                    'domains.*'          => 'required',
                                    'child_allowed'     => 'required'
                                    ]);

            if($oValidator->fails()) 
            {
                //echo '<pre>'; print_r(Input::all());exit;
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $data  = Input::all();
            $domains = '';
			if($oRequest->domains){
				$domains = implode(',',$oRequest->domains);
            }
            //dd($domains);
            $data['domain_ids'] = $domains;
			$data['licensee_id'] = Auth::user()->licensee_id;
			$data['user_id'] = Auth::user()->id;
            $data['default_hotel_supplier_id'] = $data['supplier_id'];
            unset($data['random_id']);
            unset($data['domains']);
            unset($data['domain']);
            unset($data['hotel_id']);
            unset($data['_token']);
            //DB::enableQueryLog();
            //print_r($data);exit;
            $oHotel = Hotel::updateOrCreate(['id' => $oRequest->hotel_id],$data);
            HotelImage::where('activity_id',$oActivity->id)
                        ->where('type','video')
                        ->update(['random_hotel_id'=>1]);
            $aVideoUrl = $oRequest->video_url;
            foreach ($aVideoUrl as $key => $value) {
                $oVideo = ActivityImage::firstOrNew(['hotel_id' => $oHotel->id, 'type' => 'video', 'original' => $value]);
                $oVideo->random_hotel_id ='';
                $oVideo->save();
            }
            HotelImage::where('activity_id',$oActivity->id)
                        ->where('random_hotel_id','=',1)
                        ->delete();

           // dd(DB::getQueryLog());
            if($oRequest->hotel_id == '')
            {
                $aArray['from_hotel'] = 1;
                $aArray['id'] = $oHotel->id;
                $images = HotelImage::where('random_hotel_id','=',$oRequest->random_id)->update(['hotel_id' => $oHotel->id]);
                return redirect()->route('acomodation.hotel-season-create',[$aArray['id'],$aArray['from_hotel']]);
            }
                   
            if($oRequest->hotel_id != '')
            
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            return redirect()->route('acomodation.hotel-list');
        }
        
        if(isset($nIdHotel))
        { 
            $HotelImg = HotelImage::where('hotel_id',$nIdHotel)->get();
            
        }
        //print_r($oHotel);exit;
        $oSupplier = ['' => 'Select Supplier'] + HotelSupplier::orderBy('name','asc')->pluck('name','id')->toArray();
        $oCountry = ['' => 'Select Country'] + Country::orderBy('name','asc')->pluck('name','id')->toArray();
        $oCategory = ['' => 'Select Category'] + HotelCategory::orderBy('name','asc')->pluck('name','id')->toArray();
        $oCurrencies  = ['' => 'Select Currency'] + Currency::orderBy('code','asc')->pluck('code','id')->toArray();
        $DomainList=[];
        $licenseeList=[];
        if(Auth::user()->type!="eroamProduct")
        {
        $DomainList=Domain::join('user_domains', 'user_domains.domain_id', '=', 'domains.id')->where('user_domains.user_id', Auth::user()->id)  
                     ->select(
                        'domains.name as name',
                        'user_domains.domain_id as id'
                    )->orderBy('user_domains.domain_id')->get();}else {$licenseeList=Licensee::orderBy('id','asc')->pluck('first_name','id');}                                                     
       
                    return \View::make('WebView::acomodation.hotel_create',compact('oHotel','HotelImg','nIdHotel','oCategory','oCountry','oSupplier','oCurrencies','DomainList','licenseeList'));
    }   
    public function callHotelSupplierList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ac_supplier' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ac_supplier');

        session(['page_name' => 'ac_supplier']);
        $aData = session('ac_supplier') ? session('ac_supplier') : array();

        $oRequest->session()->forget('ac_supplier');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
      
        $oHotelList = HotelSupplier::geHotelSupplierList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oHotelList->currentPage(),'ac_supplier');
        //print_r($oHotelList);exit;
        if($oRequest->page > 1)
            $oViewName =  'WebView::acomodation._hotel_supplier_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::acomodation.hotel_supplier_list' : 'WebView::acomodation._hotel_supplier_list_ajax';
        
        return \View::make($oViewName, compact('oHotelList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    public function callHotelSupplierDelete($nIdSupplier) 
    {
        $oSupplier = HotelSupplier::find($nIdSupplier);
        $oSupplier->delete();
        return 1;
    }

    public function callHotelPublish($HotelId) 
    {        
        $hotel = Hotel::find($HotelId);
        $is_publish=$hotel->is_publish;
        if(($is_publish)=='1')
        {  
            \DB::table('zhotel')
            ->where('id', $HotelId)
            ->update(['is_publish' => "0"]);     
        }
        else
        {     
            \DB::table('zhotel')
            ->where('id', $HotelId)
            ->update(['is_publish' => "1"]);         
        }
        return 1;   
        
    }
    public function callHotelSupplierCreate(Request $oRequest,$nIdSupplier='') 
    {
        session(['page_name' => 'ac_supplier']);
        echo $nIdSupplier;
        if ($oRequest->isMethod('post'))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                                'name'       => 'required',
                                                'product_contact_phone'       => 'numeric|digits:10',
                                                'reservation_contact_free_phone'  => 'numeric|digits:10',
                                                'accounts_contact_phone'       => 'numeric|digits:10',
                                                'percentage' => 'required',
                                                'abbreviation' => 'required',
                                            ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }

            $aData   = $oRequest->except('_token');
            $oLabel  = HotelSupplier::updateOrCreate(['id' => $oRequest->supplier_id], $aData);
            
            if($oRequest->supplier_id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            return redirect()->route('acomodation.hotel-supplier-list');
        }    
        $oSupplier = HotelSupplier::find($nIdSupplier);
        return \View::make('WebView::acomodation.hotel_supplier_create',compact('nIdSupplier','oSupplier'));
    }
    
    public function callHotelLabelList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ac_label' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ac_label');

        session(['page_name' => 'ac_label']);
        $aData = session('ac_label') ? session('ac_label') : array();

        $oRequest->session()->forget('ac_label');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 20);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
      
        $oHotelList = HotelCategory::geHotelCategoryList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,'','ac_label');
        
        $oViewName = $oRequest->isMethod('GET') ? 'WebView::acomodation.hotel_label_list' : 'WebView::acomodation._hotel_label_list_ajax';
        
        return \View::make($oViewName, compact('oHotelList','sSearchStr','sOrderField','sOrderBy'));
    }
    
    public function callHotelSeasonList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ac_season' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ac_season');

        session(['page_name' => 'ac_season']);
        $aData = session('ac_season') ? session('ac_season') : array();

        $oRequest->session()->forget('ac_season');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'h.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
      
        $oHotelList = HotelSeason::geHotelSeasonList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oHotelList->currentPage(),'ac_season');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::acomodation._hotel_season_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::acomodation.hotel_season_list' : 'WebView::acomodation._hotel_season_list_ajax';
        
        return \View::make($oViewName, compact('oHotelList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callHotelSeasonCreate(Request $oRequest, $nId='', $nFromFlag='')
    {
        if ($oRequest->isMethod('post'))
        {
           // print_r($oRequest->all());
            $data  = Input::except('from_flag');
            $aRule = array();
            if($nFromFlag == '' && $nId != '')
                $data = Input::except(['name','hotel_id','hotel_supplier_id','from_flag']); //this fields are not updatable
            else {
                $aRule = [  'name' => 'required',
                            'hotel_id' => 'required_without:from_flag',
                            'hotel_supplier_id' => 'required'
                            ];
            }
            $aRule['from']   = 'required|date';
            $aRule['to']   = 'required|date|after:from';
            
            $oValidator = Validator::make($oRequest->all(), $aRule);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $nSeasonId='';
            if($oRequest->from_flag !='')
                $nHotelId = $oRequest->id;
            else{
                $nSeasonId = $oRequest->id;
                $nHotelId= $oRequest->hotel_id;
            } 
            //echo $nHotelId.'aa '.$nSeasonId;
            //print_r($oRequest->all());exit;
            $cancellation_array = [];
            $i = 0;
            foreach ($oRequest->days as $key => $days ) {
                $percent = $data['percent'][$key];
                if($days && $percent)
                {
                    $cancellation_array[$i]['days'] = $days;
                    $cancellation_array[$i]['percent'] = $percent;
                    $i++;
                }
            }
            //print_r($cancellation_array);
            $data['cancellation_formula'] = ($cancellation_array) ? json_encode($cancellation_array) : NULL;
            unset($data['days']);
            unset($data['percent']);
            unset($data['domains']);
            unset($data['id']);
            $data['hotel_id'] = $nHotelId;

            $domains = '';
			if($oRequest->domains){
				$domains = implode(',',$oRequest->domains);
            }
            //dd($domains);
            $data['domain_ids'] = $domains;
			$data['licensee_id'] = Auth::user()->licensee_id;
            $data['user_id'] = Auth::user()->id;
            
            //print_r($data);exit;
            $oHotelSeason = HotelSeason::updateOrCreate(['id' => $nSeasonId],$data);
            
            if($nSeasonId != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            if($oRequest->from_flag !=''){
                $aArray['from_hotel'] = 1;
                $aArray['id'] = $oHotelSeason->id;
                return redirect()->route('acomodation.hotel-price-create',[$aArray['id'],$aArray['from_hotel']]);
            }
            return redirect()->route('acomodation.hotel-season-list');
        }
        if($nFromFlag == '' && $nId != '')
        {
            $oSeason = HotelSeason::find($nId);
            $cancellation_formula = $oSeason->cancellation_formula;
            $decoded_formula = json_decode($cancellation_formula);
        }
        $aHotels = ['' => 'Select Hotel']+ Hotel::orderBy('name','asc')->pluck('name','id')->toArray();
        $oCurrencies  = ['' => 'Select Currency'] + Currency::orderBy('code','asc')->pluck('code','id')->toArray();
        $oSupplier = ['' => 'Select Supplier'] + HotelSupplier::orderBy('name','asc')->pluck('name','id')->toArray();
        //$DomainList= Domain::where('status',1)->get();
        $DomainList=Domain::join('user_domains', 'user_domains.domain_id', '=', 'domains.id')->where('user_domains.user_id', Auth::user()->id)  
        ->select(
           'domains.name as name',
           'user_domains.domain_id as id'
       )->orderBy('user_domains.domain_id')->get();      
        return \View::make('WebView::acomodation.hotel_season_create',compact('aHotels','oSupplier','oCurrencies','nId','nFromFlag','oSeason','decoded_formula','DomainList'));
    }
    
    public function callHotelPriceList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ac_price' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ac_price');

        session(['page_name' => 'ac_price']);
        $aData = session('ac_price') ? session('ac_price') : array();

        $oRequest->session()->forget('acomodation');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'zhotel.name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'h.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        //echo $sSearchBy;exit;
        $oHotelList = Hotel::getHotelPriceList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        //echo "<pre>";print_r($oHotelList);exit;
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oHotelList->currentPage(),'ac_price');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::acomodation._hotel_price_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::acomodation.hotel_price_list' : 'WebView::acomodation._hotel_price_list_ajax';
        
        return \View::make($oViewName, compact('oHotelList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callHotelPriceCreate(Request $oRequest, $nId='', $nFromFlag='')
    {
        //print_r('sdvdvsd');exit;
        $nHotelPriceId='';
        if($nId != '' && $nFromFlag != '')
        {
            $oHotelSeason = HotelSeason::find($nId);
            $nSeasonId = $nId;
            $oHotelPrice = array('hotel_id' => $oHotelSeason->hotel_id);
        }
        elseif($nId != '' && $nFromFlag == '')
        {
            $oHotelPrice = HotelPrice::where('id',$nId)->with('base_price_obj')->first();
            $oHotelSeason = HotelSeason::find($oHotelPrice->hotel_season_id);
            $oHotelPrice->hotel_supplier_id = (isset($oHotelSeason->hotel_supplier_id)) ? $oHotelSeason->hotel_supplier_id : '';
            $nHotelPriceId = $nId;
        }
        //print_r('sdvdvsd');exit;
        
        if ($oRequest->isMethod('post'))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                    'hotel_room_type_id' => 'required|numeric|min:1',
                                    'hotel_markup_id'    => 'required|numeric|min:1',
                                    'base_price'         => 'required|numeric|min:0',
                                    'with_breakfast'     => 'required',
                                    'hotel_supplier_id'     => 'required',
                                    'allotment'          => 'required|numeric|min:0',
                                    'release'            => 'required|numeric|min:0',
                                    'max_pax'        => 'required|numeric|min:1',
                                    ]);
            $data  = Input::only( 
                'hotel_id', 
                'hotel_room_type_id', 
                'with_breakfast', 
                'allotment', 
                'release',
                'max_pax'  ,
                'hotel_season_id'              
            );
            
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $oMarkup = HotelMarkup::where(['id' => Input::get('hotel_markup_id') ])->first();
            $data = array_merge($data, [
                            'hotel_markup_id'            => $oMarkup->id, 
                            'hotel_markup_percentage_id' => $oMarkup->hotel_markup_percentage_id
                        ]);
            if($oRequest->id_price == ''){
                $oBasePrice = HotelBasePrice::create([ 'base_price' => Input::get('base_price') ]);
                $data['hotel_base_price_id'] =  $oBasePrice->id;
                $hotel_price = HotelPrice::create($data);
                 Session::flash('message', trans('messages.success_inserted'));
            }else{
                $hotel_price = HotelPrice::updateOrCreate(['id'=> $oRequest->id_price],$data );
                $oBasePrice = HotelBasePrice::firstOrNew(['id' => $hotel_price->hotel_base_price_id]);
                $oBasePrice->base_price = $oRequest->base_price;
                $oBasePrice->save();
                Session::flash('message', trans('messages.success_updated'));
            } 
            
            if($oRequest->nId != '' && $oRequest->from_flag != '')
            {
                return redirect()->route('acomodation.hotel-list');
            }
            return redirect()->route('acomodation.hotel-price-list');
        }
        
        $oSupplier = ['' => 'Select Supplier'] + HotelSupplier::orderBy('name','asc')->pluck('name','id')->toArray();
        $oHotel = ['' => 'Select Hotel'] + Hotel::orderBy('name','asc')->pluck('name','id')->toArray();
        $oHotelRoomType = ['' => 'Select Hotel Room Type'] + HotelRoomType::orderBy('name','asc')->pluck('name','id')->toArray();
        $oCountry = ['' => 'Select Country'] + Country::orderBy('name','asc')->pluck('name','id')->toArray();
        $oCategory = ['' => 'Select Category'] + HotelCategory::orderBy('name','asc')->pluck('name','id')->toArray();
        $oCurrencies  = ['' => 'Select Currency'] + Currency::orderBy('code','asc')->pluck('code','id')->toArray();
        $oMarkup   = HotelMarkup::active()->default()->orderBy('is_default', 'ASC')->with('markup_percentage')->get();
        //echo "<pre>";print_r($oMarkup);exit;
        return \View::make('WebView::acomodation.hotel_price_create',compact('oHotelPrice','nId','oCategory','oCountry','oSupplier','oCurrencies',
                                                                            'oHotelRoomType','oMarkup','oHotelSeason','oHotel','nFromFlag','nHotelPriceId'));
    }
    
    public function getAllHotel()
    {
        return Hotel::select('id', 'name')->get();
    }
    
    public function callHotelMarkupList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ac_markup' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ac_markup');

        session(['page_name' => 'ac_markup']);
        $aData = session('ac_markup') ? session('ac_markup') : array();

        $oRequest->session()->forget('ac_markup');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'hm.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        
        $oHotelMarkupList = HotelMarkup::getHotelMarkupList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
       
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,'','ac_markup');
        
        if ($oHotelMarkupList) {
            foreach ($oHotelMarkupList as $key => $value) { //print_r($value);exit;
                switch ($value->allocation_type) {
                    case 'hotel':
                        //echo 'Hotel'.$value->allocation_type,$value['allocation_id'];exit;
                        $sHotel = Hotel::where('id',$value['allocation_id'])->value('name');
                        $oHotelMarkupList[$key]['allocation_name'] = 'Hotel - ' .$sHotel;
                        break;
                    case 'city':
                        $sCity = City::where('id',$value['allocation_id'])->value('name');
                        $oHotelMarkupList[$key]['allocation_name'] = 'City - ' .$sCity;
                        break;
                    case 'country':
                        $sCountry = Country::where('id',$value['allocation_id'])->value('name');
                        $oHotelMarkupList[$key]['allocation_name'] = 'Country - ' . $sCountry;
                        break;
                    case 'all':
                        $oHotelMarkupList[$key]['allocation_name'] = 'All';
                        break;
                }
            }
        }

        $oViewName =  ($oRequest->isMethod('Get')) ? 'WebView::acomodation.hotel_markup_list' : 'WebView::acomodation._more_hotel_markup_list';
        
        
        return \View::make($oViewName, compact('oHotelMarkupList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callHotelMarkupCreate(Request $oRequest, $nIdMarkup ='')
    {
        if ($oRequest->isMethod('post'))
        {
            $allocation_id_validator = '';
            if($oRequest->allocation_type != 'all')
                $allocation_id_validator = 'required';
            
            $data = Input::only('name','description','allocation_id');
            $data['allocation_type'] =  $oRequest->allocation_type;
            $data['is_active']  = ( !$oRequest->is_active ) ? FALSE: TRUE;
            $data['is_default'] = ( !$oRequest->is_default ) ? FALSE: TRUE;
            //print_r($data);exit; 
            $oValidator = Validator::make($oRequest->all(), [
                                        'name'                => 'required',
                                        'description'         => 'required',
                                        'allocation_type'     => 'required',
                                        'allocation_id'       => $allocation_id_validator,
                                        'markup_percentage'   => 'required',
                                        'agent_percentage'    => 'required',
                                        'supplier_percentage'    => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            if( $oRequest->is_default )
                HotelMarkup::where('is_default', TRUE)->update(['is_default' => FALSE]);
            
            $oHotelMarkup = HotelMarkup::updateOrCreate(['id' => $oRequest->id_activity ], $data );
            
            $oAMP = HotelMarkupPercentage::firstOrNew(['hotel_markup_id'    => $oHotelMarkup->id]); 
            $oAMP->percentage =  $oRequest->markup_percentage;
            $oAMP->save();
            
            $oAMAC = HotelMarkupAgentCommission::firstOrNew(['hotel_markup_id'  => $oHotelMarkup->id]);
            $oAMAC->percentage =  $oRequest->agent_percentage;
            $oAMAC->save();
            
            $oAMS = HotelMarkupSupplierCommission::firstOrNew(['hotel_markup_id'    => $oHotelMarkup->id]);
            $oAMS->percentage =  $oRequest->supplier_percentage;
            $oAMS->save();
            HotelMarkup::where('id', $oHotelMarkup->id)->update([
                            'hotel_markup_percentage_id' => $oAMP->id,
                            'hotel_markup_agent_commission_id' => $oAMAC->id,
                            'hotel_markup_supplier_commission_id' => $oAMS->id
                    ]);
            if($oRequest->id_activity != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::to('acomodation/hotel-markup-list');
       
        }
        if($nIdMarkup !=''){
            $oHotelMarkup = HotelMarkup::where('id',$nIdMarkup)->with(['markup_percentage','agent_commission','supplier_commission'])->first()->toArray();
            $oHotelMarkup['markup_percentage'] = $oHotelMarkup['markup_percentage']['percentage'];
            $oHotelMarkup['agent_percentage'] = $oHotelMarkup['agent_commission']['percentage'];
            $oHotelMarkup['supplier_percentage'] = $oHotelMarkup['supplier_commission']['percentage'];
        }
                
        return \View::make('WebView::acomodation.hotel_markup_create',compact('nIdMarkup','oHotelMarkup'));
        
    }
    
    public function ImageUpload(Request $oRequest) 
    {
        $success = FALSE;
        $data = array();
        $error = array();
        // determine if a file has been uploaded

        if (Input::hasFile('hotelImage')) 
        {
            $nHotelId = Input::get('hotel_id');
            $image = Input::file('hotelImage')[0];          
            $oRequest->offsetSet('extension',$image->getClientOriginalExtension());
            $oValidator = Validator::make($oRequest->all(), [
                                        'hotelImage' => 'required',
                                        'extension' => 'required|in:jpg,jpeg,png',
                                        'hotel_id' => 'required'
                                        ]);
            if($oValidator->fails()) 
            {
                return response()->json(['success' => $oValidator->errors()],422);
            }
            
            $image_validator = image_validator($image);
            // check if image is valid
            if ($image_validator['fails']) {
                $error = ['message' => $image_validator['message']];
            } else {
                if (!is_numeric($nHotelId)) 
                {   $nHotelId = substr($nHotelId, 5);
                    $values = array('random_hotel_id' => $nHotelId);
                }    
                else
                    $values = array('hotel_id' => $nHotelId);              

                $imageName = time().'.'.$image->getClientOriginalExtension();
                $filePath = 'hotels/' . $imageName;
                $t = \Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');
                $imageNamePath = \Storage::disk('s3')->url($filePath);

                $sort_order = HotelImage::where('hotel_id', '=', $nHotelId)->max('sort_order');
                $values = array_merge($values, ['sort_order' => $sort_order + 1]);
                $values = array_merge($values, ['original' => $imageName]);
                // STORE TO DATABASE
                $id = HotelImage::create($values);
                $values = array_merge($values, ['id' => $id]);

                if ($id) {
                    $success = TRUE;
                    $data = $values;
                  
                    $data = ['success' => true, $data,'id'=>$id->id,'imageNamePath'=>$imageNamePath];
                } else {
                    $error = ['success' => false];
                }
            }
        } else {
            $error = ['success' => false];
        }
        return response_format($success, $data,$id->id,$error,$imageNamePath);
    }
    
    public function getHotelInfo()
    {
        $id = Input::get('hotel_id');
        $oHotel = Hotel::find($id)->with('currency')->first()->toArray();
        return $oHotel;
    }
    
    public function getSeasonBySupplierId()
    {
        return HotelSeason::where([
                                    'hotel_supplier_id' => Input::get('supplier_id', NULL),
                                    'hotel_id' => Input::get('hotel_id', NULL),
                                ])->orderBy('name', 'asc')->get();
    }

    public function callHotelLabelCreate(Request $oRequest,$nIdLabel='') 
    {
        
        if ($oRequest->isMethod('post'))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                    'name'                         => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }

            $aLabels    = $oRequest->labels;
            $aData      = Input::only('name', 'description');
          
            $oLabel  = HotelCategory::updateOrCreate(['id' => $oRequest->label_id], $aData);
            DB::table('cache_reload')->update(['is_reload' => 1]);
            $aArray['from_label'] = 1;
            $aArray['id'] = $oLabel->id;
            
            if($oRequest->label_id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return redirect()->route('acomodation.hotel-label-list');
        }    
        if($nIdLabel != '')
        {
            $oLabel = HotelCategory::find($nIdLabel);           
        }   

        return \View::make('WebView::acomodation.hotel_label_create',compact('nIdLabel','oLabel'));
    }

    public function callLabelDelete($nIdLabel) 
    {
        $oLabel = HotelCategory::find($nIdLabel);
        $oLabel->delete();
        return 1;
    }

    public function ImageUploadDelete($himgId)
    {  
        $data=array();
        $error=array();
        $oLabel = HotelImage::where('id','=',$himgId);
        $oLabel->delete();
        $success = TRUE;
        return response_format($success, $data, $error);
    }

    public function callHotelSeasonDelete($nIdSeason) 
    {
        $oLabel = HotelSeason::find($nIdSeason);
        $oLabel->delete();
        return 1;
    }
    
    public function callPriceDelete($nIdPrice) 
    {
        $hotel_price = HotelPrice::find($nIdPrice);
        $hotel_bprice = HotelBasePrice::find($hotel_price->hotel_base_price_id);
        $hotel_markup = HotelMarkup::find($hotel_price->hotel_markup_id);
        $hotel_price->delete();
        $hotel_bprice->delete();
        $hotel_markup->delete();
        return 1;
    }
    
    public function callHotelRoomTypeList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ac_room' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ac_room');

        session(['page_name' => 'ac_room']);
        $aData = session('ac_room') ? session('ac_room') : array();

        $oRequest->session()->forget('ac_room');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
      
        $oHotelList = HotelRoomType::getRoomType($sSearchStr,$sOrderField,$sOrderBy);

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,'','','ac_room');
        
        $oViewName = $oRequest->isMethod('GET') ? 'WebView::acomodation.hotel_room_list' : 'WebView::acomodation._hotel_room_list_ajax';
        
        return \View::make($oViewName, compact('oHotelList','sSearchStr','sOrderField','sOrderBy'));
    }
    
    public function callRoomTypeCreate(Request $oRequest, $nIdRoom='')
    {
        if ($oRequest->isMethod('post'))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'pax' => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $aData = $oRequest->except(['id', '_token']);
          
            HotelRoomType::updateOrCreate(['id' => $oRequest->id], $aData);
            DB::table('cache_reload')->update(['is_reload' => 1]);
            if($oRequest->id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return redirect()->route('acomodation.hotel-room-list');
        }    
        if($nIdRoom != '')
        {
            $oRoom = HotelRoomType::find($nIdRoom);           
        }   

        return \View::make('WebView::acomodation.hotel_roomtype_create',compact('nIdRoom','oRoom'));
    }
    
    public function callRoomTypeDelete($nIdRoom) 
    {
        HotelRoomType::find($nIdRoom)->delete();
    }
    
    public function callAOTLocationList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'aot_location' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('aot_location');

        session(['page_name' => 'aot_location']);
        $aData = session('aot_location') ? session('aot_location') : array();

        $oRequest->session()->forget('aot_location');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
      
        $oHotelList = AOTLocation::getAotlocationList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oHotelList->currentPage(),'aot_location');
        //print_r($oHotelList);exit;
        if($oRequest->page > 1)
            $oViewName =  'WebView::acomodation._aotlocation_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::acomodation.aotlocation_list' : 'WebView::acomodation._aotlocation_list_ajax';
        
        return \View::make($oViewName, compact('oHotelList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    public function callAOTSupplierList(Request $oRequest) 
    {
//remove session when it comes from sidebar
        if(session('page_name') != 'aot_supplier' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('aot_supplier');

        session(['page_name' => 'aot_supplier']);
        $aData = session('aot_supplier') ? session('aot_supplier') : array();

        $oRequest->session()->forget('aot_supplier');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
      
        $oHotelList = AotSupplier::getAotSupplierList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oHotelList->currentPage(),'aot_supplier');
        //print_r($oHotelList);exit;
        if($oRequest->page > 1)
            $oViewName =  'WebView::acomodation._aotsupplier_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::acomodation.aotsupplier_list' : 'WebView::acomodation._aotsupplier_list_ajax';
        
        return \View::make($oViewName, compact('oHotelList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callAotSupplierUpdate(Request $oRequest,$nIdSupplier ='') 
    {
        if ($oRequest->isMethod('post'))
        {
            $data = Input::only('hotel_supplier_code','approved_by_eroam','default_hotel');

        $hotel_updated = AotSupplierUpdated::where('hotel_supplier_code',$data['hotel_supplier_code'])->first();

        if($hotel_updated){
            $data  = AotSupplierUpdated::where('hotel_supplier_code',$data['hotel_supplier_code'])->update($data);
        }else{
            $data  = AotSupplierUpdated::create($data);
        }

                Session::flash('message', trans('messages.success_updated'));
                return redirect()->route('acomodation.hotel-aotsupplier-list');
        return Redirect::back();
        }
        $supplier = AotSupplier::where('id',$nIdSupplier)->with('updated_supplier')->first();
        //echo "<pre>";print_r($supplier);exit;
        return \View::make('WebView::acomodation.aotsupplier_update', compact('supplier'));
    }
   
}
