<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Libraries\Gadventure;
use App\CategoryDef;
use Carbon\Carbon;
use App\Tour;
use App\Region;
use App\City;
use App\Country;
use App\TourCategory;

class GadventureApiController extends Controller {

    public function __construct() {
        $this->api = New Gadventure;
    }

    public function store_category() {
        $allCategories = $this->api->get_data('tour_categories');

        foreach ($allCategories['results'] as $key => $value) {
            $name_url = str_replace(' ', '_', $value['name']);
            $category = CategoryDef::where('category_from', 'ga')->where('category_from_id', $value['id'])->first();
            if (!$category) {
                $data = [
                    'category_name' => $value['name'],
                    'url' => $name_url,
                    'is_active' => 0,
                    'description' => $value['description'],
                    'category_from' => 'ga',
                    'category_from_id' => $value['id'],
                ];
                CategoryDef::create($data);
                $data = [];
            }
        }
    }

    public function store_tour() {
        $getTour = $this->api->get_data('tour_dossiers');
        $allTours = $getTour['results'];
        $links = isset($getTour['links']) ? $getTour['links'] : '';

        for ($i = 0; $i < 100; $i++) {
            if (isset($links) && !empty($links)) {
                //echo $links[0]['rel'];
                if (!empty($allTours)) {
                    foreach ($allTours as $tour) {

                        if ($tour['departures_end_date'] >= Carbon::now()) {
                            $tourDetail = $this->api->get_data('tour_dossiers', $tour['id']);
                            $itineraries = $tourDetail['itineraries'];

                            if ($itineraries[0]['duration'] > 1) {
                                $check_code = Tour::where('code', $tour['id'])->first();
                                if (!$check_code) {
                                    $category_ids = [];

                                    foreach ($tourDetail['categories'] as $cat_id) {
                                        array_push($category_ids, $cat_id['id']);
                                    }


                                    $allCategories = CategoryDef::whereIn('category_from_id', $category_ids)->where('category_from', 'ga')->pluck('category_name')->all();

                                    $categories = '';

                                    if ($allCategories) {
                                        $categories = implode(',', $allCategories);
                                    }

                                    $itinerary_data = '';
                                    foreach ($itineraries[0]['days'] as $itinerary) {
                                        $itinerary_data = $itinerary_data . '<b>' . $itinerary['label'] . '</b><br><p>' . $itinerary['body'] . '</p><br>';
                                    }

                                    $tripCountries = '';
                                    foreach ($tourDetail['geography']['visited_countries'] as $tc) {
                                        $tripCountries = $tc['name'] . "," . $tripCountries;
                                    }

                                    $transport = '';
                                    $accommodation = '';
                                    foreach ($tourDetail['details'] as $td) {
                                        if ($td['detail_type']['label'] == 'Accommodation') {
                                            $accommodation = $td['body'];
                                        }

                                        if ($td['detail_type']['label'] == 'Transport') {
                                            $transport = $td['body'];
                                        }
                                    }

                                    $region_id = '';
                                    $region = Region::where('name', 'like', '%' . $tourDetail['geography']['region']['name'] . '%')->first();
                                    if ($region) {
                                        $region_id = $region->id;
                                    } else {
                                        $region_data = [
                                            'name' => $tourDetail['geography']['region']['name']
                                        ];
                                        $region_id = Region::create($region_data)->id;
                                    }

                                    $from_city_id = '';
                                    $fromCity = City::where('name', 'like', '%' . $tourDetail['geography']['start_city']['name'] . '%')->first();
                                    if ($fromCity) {
                                        $from_city_id = $fromCity->id;
                                    } else {
                                        $from_city_data = [
                                            'name' => $tourDetail['geography']['start_city']['name'],
                                            'region_id' => $region_id,
                                        ];
                                        $from_city_id = City::create($from_city_data)->id;
                                    }

                                    $to_city_id = '';
                                    $toCity = City::where('name', 'like', '%' . $tourDetail['geography']['finish_city']['name'] . '%')->first();
                                    if ($toCity) {
                                        $to_city_id = $toCity->id;
                                    } else {
                                        $to_city_data = [
                                            'name' => $tourDetail['geography']['finish_city']['name'],
                                            'region_id' => $region_id,
                                        ];
                                        $to_city_id = City::create($to_city_data)->id;
                                    }

                                    $dept_country_id = '';
                                    $Country = Country::where('name', 'like', '%' . $tourDetail['geography']['start_country']['name'] . '%')->first();
                                    if ($Country) {
                                        $dept_country_id = $Country->id;
                                    } else {
                                        $from_country_data = [
                                            'name' => $tourDetail['geography']['start_country']['name'],
                                            'region_id' => $region_id,
                                            'code' => $tourDetail['geography']['start_country']['id'],
                                        ];
                                        $dept_country_id = Country::create($from_country_data)->id;
                                    }

                                    $dest_country_id = '';
                                    $Country = Country::where('name', 'like', '%' . $tourDetail['geography']['finish_country']['name'] . '%')->first();
                                    if ($Country) {
                                        $dest_country_id = $Country->id;
                                    } else {
                                        $to_country_data = [
                                            'name' => $tourDetail['geography']['finish_country']['name'],
                                            'region_id' => $region_id,
                                            'code' => $tourDetail['geography']['finish_country']['id'],
                                        ];
                                        $dest_country_id = Country::create($to_country_data)->id;
                                    }

                                    $current_price = '';
                                    foreach ($tourDetail['advertised_departures'] as $departure) {
                                        if ($departure['currency'] == "AUD") {
                                            $current_price = $departure['amount'];
                                            break;
                                        }
                                    }

                                    $data['tour_title'] = $tourDetail['name'];
                                    $data['tour_url'] = str_replace(' ', '_', $tourDetail['name']);
                                    ;
                                    $data['no_of_days_text'] = $itineraries[0]['duration'];
                                    $data['no_of_days'] = $itineraries[0]['duration'];
                                    $data['tripActivities'] = $categories;
                                    $data['is_active'] = 0;
                                    $data['Date_LastUpdate'] = date('Y-m-d H:i:s');
                                    $data['views'] = 0;
                                    $data['is_childAllowed'] = 0;
                                    $data['short_description'] = $tourDetail['description'];
                                    $data['is_approve'] = 0; //1
                                    $data['is_reviewed'] = 0; //1
                                    $data['sync_error'] = 'This is new Tour'; //'Tour Updated'
                                    $data['updated_by'] = 'Admin';
                                    $data['admin_id_updated'] = -1;
                                    $data['date_admin_updated'] = date('Y-m-d H:i:s');
                                    $data['updated_msg'] = 'Tour added by Admin';
                                    $data['tour_type_logo_id'] = 12;
                                    $data['is_deleted'] = 0;
                                    $data['saving_per_person'] = 0;
                                    $data['discount'] = '';
                                    $data['durationType'] = 'd';
                                    $data['code'] = $tourDetail['id'];
                                    $data['tour_code'] = "GAD " . $tourDetail['product_line'];
                                    $data['provider'] = 2;
                                    $data['long_description'] = $tourDetail['description'];
                                    $data['tour_currency'] = "AUD";
                                    $data['tripCountries'] = rtrim($tripCountries, ",");
                                    $data['xml_itinerary'] = $itinerary_data;
                                    $data['transport'] = $transport;
                                    $data['accommodation'] = $accommodation;

                                    $data['region_id'] = $region_id;
                                    $data['tripRegion'] = $region_id;
                                    $data['departure'] = $tourDetail['geography']['start_city']['name'];
                                    $data['destination'] = $tourDetail['geography']['finish_city']['name'];
                                    $data['from_city_id'] = $from_city_id;
                                    $data['de_countries'] = $dept_country_id;
                                    $data['countryData'] = $dept_country_id;
                                    $data['price'] = $current_price;
                                    $data['retailcost'] = $current_price;
                                    $data['AdultPriceSingle'] = $current_price;
                                    $data['dn_countries'] = $dest_country_id;
                                    $data['to_city_id'] = $to_city_id;

                                    $data['IsLive_PracticalDetail'] = 1;

                                    //$added_data = Tour::create($data);

                                    $tour_id = Tour::create($data)->tour_id;

                                    $allCategoriesGad = CategoryDef::whereIn('category_from_id', $category_ids)->where('category_from', 'ga')->pluck('category_id')->all();

                                    foreach ($allCategoriesGad as $key => $catId) {
                                        $activities['category_id'] = $catId;
                                        $activities['tour_id'] = $tour_id;
                                        TourCategory::create($activities);
                                    }
                                    foreach ($tourDetail['images'] as $tourImg) {
                                        if ($tourImg['type'] == 'LARGE_SQUARE' || $tourImg['type'] == 'MAP') {
                                            $nTourId = $tour_id;
                                            $image_link = $tourImg['image_href']; //Direct link to image
                                            $split_image = pathinfo($image_link);
                                            if(isset($split_image['extension']))
                                            {
                                                if (!is_numeric($nTourId)) {
                                                    $nTourId = substr($nTourId, 5);
                                                    $values = array('random_tour_id' => $nTourId, 'image_name' => $split_image['filename']);
                                                } else { // if rand tour id 
                                                    $values = array('tour_id' => $nTourId, 'image_name' => $split_image['filename']);
                                                }


                                                $image = file_get_contents($image_link);
                                                $imageName = time() . '.' . $split_image['extension'];
                                                $filePath = 'tours/' . $imageName;

                                                $t = \Storage::disk('s3')->put($filePath, $image, 'public');
                                                $imageUrl = \Storage::disk('s3')->url($filePath);

                                                $values = array_merge($values, ['title' => $imageName]);

                                                $sort_order = DB::table('tblimages')->where('tour_id', '=', $nTourId)->max('sort_order');
                                                $is_primary = 0;
                                                if ($tourImg['type'] == 'LARGE_SQUARE') {
                                                    $is_primary = 1;
                                                }
                                                $values = array_merge($values, ['is_active' => 1, 'sort_order' => $sort_order + 1, 'is_primary' => $is_primary]);
                                                // STORE TO DATABASE
                                                $id = DB::table('tblimages')->insertGetId($values);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ($links[0]['rel'] == 'next') {

                    $getTour = '';
                    $allTours = '';
                    $exp = explode('tour_dossiers', $links[0]['href']);
                    $getTour = $this->api->get_data('tour_dossiers' . $exp[1]);
                    $allTours = $getTour['results'];
                    $links = $getTour['links'];
                    $i++;
                } else {
                    $links = '';
                    $i = 100;
                }
            }
        }
    }

}

?>