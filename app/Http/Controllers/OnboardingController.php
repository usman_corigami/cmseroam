<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

use App\Country;
use App\Timezone;
use App\Licensee;
use App\DomainFrontend;
use App\Product;
use App\WebFont;
use App\Domain;
use App\DomainProduct;
use App\BackendConfig;
use App\Backend;
use App\User;
use App\UserDomain;
use App\InventoryConfig;
use App\DomainInventory;
use App\DomainLocation;
use App\LicenseeAccount;
use App\Region;
use App\City;
use App\LicenseeCommission;
use App\DomainExtras;

use Validator;
use File;

use Illuminate\Support\Facades\Input;

class OnboardingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const EROAM_EMAIL = 'info@eroam.com';

    public $S3LOGO = "https://eroam-dev.s3.us-west-2.amazonaws.com/onboardingLogo"; 

    public function index() {
        session(['page_name' => 'onboarding_list']);
        $licensees = Licensee::where('user_id',0)->with('domains')->orderBy('id','desc')->get();
        return view('WebView::onboarding.index',compact('licensees'));
    }

    public function view($licensee_id) {
        session(['page_name' => 'onboarding_list']);
        $licensee = Licensee::where('id',$licensee_id)->first();;

        $domains = Domain::where('licensee_id',$licensee_id)->with('products')->with('frontend')->with('location')->with('backend')->with('inventory')->get();
        $account = LicenseeAccount::where('licensee_id',$licensee_id)->first();
        $commissions = LicenseeCommission::where('licensee_id',$licensee_id)->get();
        return view('WebView::onboarding.view',compact('licensee','domains','account','commissions'));
    }

    public function general($licensee_id = null){
        session(['page_name' => 'onboarding']);
        $licensee = [];
        $update = false;
        if(!empty($licensee_id)) {
            $update = true;
            $licensee = Licensee::where('id',$licensee_id)->first();
        }
        $countries = ['' => 'Select Country'] + Country::pluck('name','id')->toArray();
         // ['' => 'Select Timezone'] + Timezone::pluck('name','id')->toArray();
        $result =Timezone::orderBy('utc','desc')->get();
        $timezones[''] = 'Select Timezone';
        foreach ($result as $row) {
            $timezones[$row['id']] = $row['utc'].' '.$row['name']; 
        }

        return \View::make('WebView::onboarding.general',compact('countries','timezones','licensee','update','licensee_id'));
    }

    public function generalAdd(Request $request,Licensee $licensee) {
        $rules = [
            'business_name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country_id' => 'required',
            'zip_code' => 'required',
        ];  

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return ['result' => 0,'errors' =>$validator->messages()];
        }


        $user = Licensee::create($request->all());
        
        return ['result' => 1,'msg' => 'Added Successfully' ,'url' => route('onboarding.frontend',$user->id)];
    }

    public function generalUpdate($id,Request $request) {
        $rules = [
            'business_name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'street' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country_id' => 'required',
            'zip_code' => 'required',
        ];  

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return ['result' => 0,'errors' =>$validator->messages()];
        }

        $arr = $request->all();
        $licensee = Licensee::find($id);
        $licensee->update($arr);
        return ['result' => 1,'msg' => 'Updated Successfully','flag' => 'false'];
        
    }

    public function frontend($licensee_id){   
        session(['page_name' => 'onboarding']);
        $update = false;
        $frontends = DomainFrontend::where('licensee_id',$licensee_id)->with('domain')->get();
        $instance = count($frontends) <= 1 ? 'single' : 'multiple';
        foreach ($frontends as $frontend) {
            $selected_products[$frontend['domain_id']] = DomainProduct::where('licensee_id',$licensee_id)->where('domain_id',$frontend['domain_id'])->get();
        }
        if (isset($selected_products)) {
            foreach ($selected_products as $skey => $selected_product) {
                $spros[$skey] = array_column($selected_product->toArray(), 'product_id');
            }
        }
        if(!empty($spros)) {
            $update = true;
        }
        $products = Product::select('name','id','status')->get();
        $webfonts = ['' => 'Select WebFont'] + WebFont::pluck('name','id')->toArray();
        $S3LOGO = $this->S3LOGO;
        return \View::make('WebView::onboarding.frontend',compact('products','webfonts','licensee_id','frontends','spros','S3LOGO','instance','update'));
    }

    public function addDomain(){
        $products = Product::select('name','id','status')->get();
        $licensee_id = Input::get('licensee_id');
        $webfonts = ['' => 'Select WebFont'] + WebFont::pluck('name','id')->toArray();
        $id = Input::get('id');
        return \View::make('WebView::onboarding.addDomain',compact('licensee_id','id','products','webfonts'));
    }

    public function deletedomain(){
        $domain_id = Input::get('domain_id');
        $licensee_id = Input::get('licensee_id');

        // Delete all domain content
        Domain::where('licensee_id',$licensee_id)->where('id',$domain_id)->delete();
        DomainProduct::where('licensee_id',$licensee_id)->where('domain_id',$domain_id)->delete();
        DomainLocation::where('licensee_id',$licensee_id)->where('domain_id',$domain_id)->delete();
        DomainFrontend::where('licensee_id',$licensee_id)->where('domain_id',$domain_id)->delete();
        DomainInventory::where('licensee_id',$licensee_id)->where('domain_id',$domain_id)->delete();

        $response = ['success'=>true];
        return $response;
    }

    public function uploadLogo(Request $oRequest) {
        $success = FALSE;
        $data = array();
        $error = array();
        // determine if a file has been uploaded

        if (Input::hasFile('themeLogo')){
            $logo = Input::get('logo');
            $id = Input::get('id');
            $image = Input::file('themeLogo')[0];
            $oRequest->offsetSet('extension',$image->getClientOriginalExtension());
            $oValidator = Validator::make($oRequest->all(), [
                'themeLogo' => 'required',
                'extension' => 'required|in:jpg,jpeg,png,svg'
            ]);
            
            if($oValidator->fails()){
                return response()->json(['success' => $oValidator->errors()],422);
            }
            
            
            $image_validator = domain_logo_validator($image);
            
            // check if image is valid
            if ($image_validator['fails']) {
                $error = ['message' => $image_validator['message']];
            } else {
                /*$file = $image->getClientOriginalName();
                $filename = pathinfo($file, PATHINFO_FILENAME);
                $filename = md5($filename) . '.' . $image->getClientOriginalExtension();
                // the path where the image is saved
                $destination = 'uploads/onboardingLogo/' . $logo;

                // CREATE ORIGINAL IMAGE
                $original_path = $destination . '/' . $filename;
                File::makeDirectory(public_path($destination), 0775, FALSE, TRUE); // create directory if not existing yet
                $image->move($destination, $filename);*/

                $imageName = time().'.'.$image->getClientOriginalExtension(); 
                $filePath = 'onboardingLogo/' . $imageName;
                $t = \Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');                   

                $success = TRUE;
                 
                $ndata['image_name'] = $imageName;
                $ndata['image_path'] = $this->S3LOGO;
                $ndata['id'] = $id;
                $data = ['success' => true, $ndata];
            }
        } else {
            $error = ['success' => false];
        }

        return response_format($success, $data, $error);
    }

    public function uploadFavicon(Request $oRequest) {
        $success = false;
        $data = array();
        $error = array();
        // determine if a file has been uploaded
        
        if (Input::hasFile('themeFavicon')){
            $logo = Input::get('favicon'); 
            $id = Input::get('id');
            $image = Input::file('themeFavicon')[0];
            $oRequest->offsetSet('extension',$image->getClientOriginalExtension());
            $oValidator = Validator::make($oRequest->all(), [
                'themeFavicon' => 'required',
                'extension'    => 'required|in:png,ico'
            ]);
            
            if($oValidator->fails()){ 
                return response()->json(['success' => $oValidator->errors()],422);
            }
            
            
            $image_validator = domain_favicon_validator($image);
            
            // check if image is valid
            if ($image_validator['fails']) {
                $error = ['message' => $image_validator['message']];
            } else {

                $imageName = time().'.'.$image->getClientOriginalExtension(); 
                $filePath = 'onboardingLogo/' . $imageName;
                $t = \Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');                   

                $success = TRUE;
                 
                $ndata['image_name'] = $imageName;
                $ndata['image_path'] = $this->S3LOGO;
                $ndata['id'] = $id;
                $data = ['success' => true, $ndata];
            }
        } else {
            $error = ['success' => false];
        }

        return response_format($success, $data, $error);
    }

    public function frontendAdd(Request $request) {
        $rules = [
            'products' => 'required',
            'webfont_id' => 'required',
            'theme_color' => 'required',
            'font_color' => 'required',
            'header_color' => 'required',
            'footer_color' => 'required',
            'search_color' => 'required',
            'domain_name' => 'required|unique:domains,name',
            'image_name' => 'required',
            'page_title' => 'required',
            'favicon_name' => 'required',
        ];  

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            foreach($errors as $mkey => $validationErrors){
                if ($mkey == 'products') {
                    $errors[$mkey] = ['You must select atleast one product to continue.']; 
                }elseif ($mkey == 'image_name') {
                    $errors[$mkey] = ['You must upload a logo to continue.']; 
                }elseif ($mkey == 'favicon_name') {
                    $errors[$mkey] = ['You must upload a favicon to continue.']; 
                }
            }
            return ['result' => 0,'errors' =>$errors];
        }

        $licensee_id = $request->licensee_id;

        $domain = new Domain();
        $domain->name = $request->domain_name;
        $domain->licensee_id = $request->licensee_id;
        $domain->save();
        $domain_id = $domain->id;

        $products = $request->products;
        $data = [];
        foreach ($products as $product) {
            $data[] = ['licensee_id' => $licensee_id,'domain_id' => $domain_id,'product_id' => $product];
        }

        DomainProduct::insert($data);

        $domain_frontend = new DomainFrontend();
        $domain_frontend->licensee_id = $licensee_id;
        $domain_frontend->domain_id = $domain_id;
        $domain_frontend->template_id = $request->template_id;
        $domain_frontend->logo = $request->image_name;
        $domain_frontend->webfont_id = $request->webfont_id;
        $domain_frontend->theme_color = $request->theme_color;
        $domain_frontend->font_color = $request->font_color;
        $domain_frontend->header_color = $request->header_color;
        $domain_frontend->footer_color = $request->footer_color;
        $domain_frontend->search_color = $request->search_color;
        $domain_frontend->consultant = ($request->consultant) ? 1 : 0;

        $domain_frontend->favicon = $request->favicon_name;
        $domain_frontend->page_title = $request->page_title;
        $domain_frontend->facebook_link = $request->facebook_link;
        $domain_frontend->twitter_link = $request->twitter_link;
        $domain_frontend->linkedin_link = $request->linkedin_link;
        $domain_frontend->instagram_link = $request->instagram_link;
        $domain_frontend->pinterest_link = $request->pinterest_link;
        $domain_frontend->google_plus_link = $request->google_plus_link;
        $domain_frontend->youtube_link = $request->youtube_link;

        $domain_frontend->save();

        $this->createCss($request->domain_name,$request->webfont_id,$request->theme_color,$request->font_color,$request->header_color,$request->footer_color,$request->search_color);

        $response = ['result' => 1,'msg' => 'Added Successfully'];

        Licensee::where('id',$licensee_id)->update(['instance'=>$request->instance]);

        if ($request->instance == 'single') {
            $response['url'] = route('onboarding.backend',[$licensee_id,$domain_id]);
        }

        return $response;
    }

    public function frontendUpdate($domain_id,Request $request) {
        $domains = Domain::where('id','!=',$domain_id)->where('name',$request->domain_name)->get();
        $d_count = $domains->count();

        $rules = [
            'products' => 'required',
            'webfont_id' => 'required',
            'theme_color' => 'required',
            'font_color' => 'required',
            'header_color' => 'required',
            'footer_color' => 'required',
            'search_color' => 'required',
            'domain_name' => 'required',
            'image_name' => 'required',
            'page_title' => 'required',
            'favicon_name' => 'required',
        ];  
        if($d_count >= 1) {
            $rules['domain_name'] = 'required|unique:domains,name';
        }

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            foreach($errors as $mkey => $validationErrors){
                if ($mkey == 'products') {
                    $errors[$mkey] = ['You must select atleast one product to continue.']; 
                }elseif ($mkey == 'image_name') {
                    $errors[$mkey] = ['You must upload a logo to continue.']; 
                }elseif ($mkey == 'favicon_name') {
                    $errors[$mkey] = ['You must upload a favicon to continue.']; 
                }
            }
            return ['result' => 0,'errors' =>$errors];
        }

        DomainProduct::where('licensee_id',$request->licensee_id)->where('domain_id',$domain_id)->delete();
        $products = $request->products;
        $data = [];
        foreach ($products as $product) {
            $data[] = ['licensee_id' => $request->licensee_id,'domain_id' => $domain_id,'product_id' => $product];
        }

        DomainProduct::insert($data);

        $domain = Domain::where('id',$domain_id)->update(['name'=>$request->domain_name]);

        $domain_frontend = DomainFrontend::where('domain_id',$domain_id)->update([
            'template_id' =>  $request->template_id,
            'logo' => $request->image_name,
            'webfont_id' => $request->webfont_id,
            'theme_color' => $request->theme_color,
            'font_color' => $request->font_color,
            'header_color' => $request->header_color,
            'footer_color' => $request->footer_color,
            'search_color' => $request->search_color,
            'consultant' => ($request->consultant) ? 1 : 0,

            'favicon' => $request->favicon_name,
            'page_title' => $request->page_title,
            'facebook_link' => $request->facebook_link,
            'twitter_link' => $request->twitter_link,
            'linkedin_link' => $request->linkedin_link,
            'instagram_link' => $request->instagram_link,
            'pinterest_link' => $request->pinterest_link,
            'google_plus_link' => $request->google_plus_link,
            'youtube_link' => $request->youtube_link
        ]);

        $this->createCss($request->domain_name,$request->webfont_id,$request->theme_color,$request->font_color,$request->header_color,$request->footer_color,$request->search_color);

        return ['result' => 1,'msg' => 'Updated Successfully','flag'=>'false'];
    }


    public function backend($licensee_id) {
        $domain_backend=[];
        session(['page_name' => 'onboarding']);
        $update = false;
        $types = BackendConfig::all();
        $total = DomainFrontend::where('licensee_id',$licensee_id)->count();
        $domain_ids = Domain::select(['id'])->where('licensee_id', $licensee_id)->get();
        $domain_ids = array_column($domain_ids->toArray(), 'id');

        $result = [];
        $domainIdArray = [];
        foreach ($types as $type) {
            $result[$type['type']][] = ['item' => $type['item'],'id' => $type['id'],'status' => $type['status']];
        }
        if (isset($licensee_id)) {
            $domain_backends = Backend::select(['config_id','id','domain_id','licensee_id'])->where('licensee_id', $licensee_id)->get();
            $domainIdArray = array_column($domain_backends->toArray(), 'domain_id');
            $domain_backend = array_column($domain_backends->toArray(), 'config_id');

        } 
        if(!empty($domain_backend)) {
            $update = true;
        }
        return \View::make('WebView::onboarding.backend',compact('licensee_id','result','domain_ids','domain_backend','total','domain_backends','update'));
    }

    public function backendAdd(Request $request) {
        $backend = $request->domain_backend;
        if($backend) {
            $result = [];
            foreach ($backend as $row) {
                $result[] = ['licensee_id'=> $request->licensee_id,'domain_id'=>$request->domain_id,'config_id' => $row];
            }
        }else{
            return ['result' => 0,'errors'=>['domain_backend'=>'You must select atleast one to continue']];
        }

        $backend = Backend::insert($result);
        return ['result' => 1,'msg'=>'Added Successfully', 'url'=> route('onboarding.geodata',[$request->licensee_id])];
    }

    public function backendUpdate(Request $request) { 
        //pr($request->all());die();
        //$backend = $request->domain_backend;
        if($request->licensee_id && $request->domain_id) {
            Backend::where('licensee_id', $request->licensee_id)->where('domain_id', $request->domain_id)->delete();
            $result = [];
            $backend = $request->domain_backend;
            $backend = array_unique($backend);
            foreach ($backend as $row) {
                $result[] = ['licensee_id'=> $request->licensee_id,'domain_id'=>$request->domain_id,'config_id' => $row];
            }
            $backend = Backend::insert($result);
        }else{
            return ['result' => 0,'errors'=>['domain_backend'=>'You must select atleast one to continue']];
        }
        return ['result' => 1,'msg'=>'Updated Successfully','flag'=>'false'];
    }


    public function geoData($licensee_id){
        session(['page_name' => 'onboarding']);
        $total = DomainFrontend::where('licensee_id',$licensee_id)->count();
        $update = false;
        $regions = Region::pluck('name','id')->toArray();
        $countries = [];
        $cities = [];

        $extrasDmains = [];
        $domain_ids = [];
        $flag = 2;
        $extrasDmains = DomainExtras::where('licensee_id',$licensee_id)->get();
        $domain_ids = Domain::select(['id'])->where('licensee_id', $licensee_id)->get();
        $domain_ids = array_column($domain_ids->toArray(), 'id');

        if(!empty($extrasDmains)) {
            $update = true;
        }
        return \View::make('WebView::onboarding.geo',compact('regions','licensee_id','flag','extrasDmains','domain_ids','update','total'));
    }

    public function getGeodata(){
        $regions = Input::get('regions');
        $countries = Input::get('countries');
        $response = [];

        if (!Input::has('countries')) {
            if (!empty($regions)) {
                $cregion = Region::select('name','id')->whereIn('id', $regions)->first();
                $countries = Country::whereIn('region_id', $regions)->with('region')->get();
                if (count($countries) > 0) {
                    foreach ($countries as $country) {
                        $finalcountries[$country['region']['id']][$country['id']] = $country;
                    }
                }else{
                    $finalcountries = [];   
                }
                $countries = $finalcountries;
            }else{
                $countries = [];
                $cregion = null;
            }
            $viewCountries = \View::make('WebView::onboarding.includes.geocountries', compact('countries','cregion'));
            $countriesHtml = $viewCountries->render();
            $response['countries'] = $countriesHtml;
        }

        $cities = [];
        $ccountry = Country::whereIn('id', $countries)->with('region')->first();
        $cities = City::whereIn('country_id', $countries)->with('countrywithregion')->get();
        if (count($cities) > 0) {
            foreach ($cities as $city) {
                $finalcities[$city['countrywithregion']['region']['id']][$city['country_id']][$city['id']] = $city;
            }
        }else{
            $finalcities = [];   
        }
        $cities = $finalcities;
        $defaulton = true;
        $viewCities = \View::make('WebView::onboarding.includes.geocities', compact('cities','ccountry','defaulton'));
        $citiesHtml = $viewCities->render();
        $response['cities'] = $citiesHtml;

        return $response;
    }


    public function geoDataSave(Request $request)   {
        
       $all = $request->all();
        $dataArray =[];

        if(count((array)$request->data) > 0){
             
                 foreach ($request->data as $key=> $value) {
                     $dataArray =  array(
                     'licensee_id' => $request->licensee_id,
                     'domain_id' => $request->domain_id,
                     'type' => $value,
                     'status'=>1
                     );
                     //echo "<pre>";print_r($dataArray);die;
                    $query = DomainExtras::create($dataArray);
                    
                }
        }
        
        return ['result' => 1,'msg' => 'Saved Successfully' ,'url' => route('onboarding.inventory',[$request->licensee_id,$request->domain_id])];
    }
    public function geoDataUpdate(Request $request){
        
        $all = $request->all();
        
        $dataArray =[];
    
        DomainExtras::where('licensee_id', $request->licensee_id)->where('domain_id',$request->domain_id)->delete();
        
        if(count((array)$request->data) > 0){
            
                 foreach ($request->data as $key=> $value) {
                     
                     $dataArray =  array(
                     'licensee_id' => $request->licensee_id,
                     'domain_id' => $request->domain_id,
                     'type' => $value,
                     'status'=>1
                     );
                    
                    $query = DomainExtras::create($dataArray);
                    
                }
        }
        return ['result' => 1,'msg' => 'Updated Successfully','flag'=>true];
    }
    public function account($licensee_id){
        session(['page_name' => 'onboarding']);
        $update = false;
        $notes = LicenseeAccount::select('notes','representative_id','licensee_id')->where('licensee_id', $licensee_id)->first();
        if(!empty($notes)) {
            $update = true;
        }
        return \View::make('WebView::onboarding.account',compact('licensee_id','notes','update'));
    }

    public function accountSave(Request $request){
        $rules = [
            'representative_id' => 'required',
        ];  

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()) {
            return ['result' => 0,'errors' =>$validator->messages()];
        }

        $user = LicenseeAccount::create($request->all());

        return ['result' => 1,'msg' => 'Added Successfully', 'url' => route('onboarding.general')];
    }

    public function accountUpdate($licensee_id,Request $request){
        $rules = [
            'representative_id' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()) {
            return ['result' => 0,'errors' =>$validator->messages()];
        }

        LicenseeAccount::where('licensee_id',$licensee_id)->update(['representative_id'=>$request->representative_id,'notes'=> $request->notes]);

        return ['result' => 1,'msg' => 'Updated Successfully','url' => route('onboarding') ,'flag' => true];
    }

    public function usermanagement($licensee_id)	{
		//echo $licensee_id;die;
        session(['page_name' => 'onboarding']);
        $types = [
            'licensee_administrator',
            'brand_administrator',
            'product_manager',
            'agent',
            'consultant',
        ];
        $users = User::where('licensee_id',$licensee_id)->whereIn('type',$types)->with('user_domains')->get();

        $update = false;
        if(!empty($users)) {
            $update = true;
        }
        $domains = Domain::select('name','id')->where('licensee_id',$licensee_id)->get();
        return \View::make('WebView::onboarding.usermanagement',compact('licensee_id','domains','users','update'));
    }

    private function getRandomPassword() {
        $length = 8; // pw length
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }
    public function userAdd(Request $request,User $user) {
        $rules = [
            'fname' => 'required',
            'lname' => 'required',
            'username' => 'required',
            'contact_no' => 'required',
            'domain_id' => 'required',
            'type' => 'required'
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()) {
            return ['result' => 0,'errors' => $validator->messages()];
        }

        $exist = User::select('id')->where('username',$request->username)->count();

        if ($exist > 0) {
            return ['result' => 0,'errors' => ['username'=>"Email Already Exists."]];
        }
        $licensseLogo = DomainFrontend::where('licensee_id',$request->licensee_id)->value('logo');

        $password = $this->getRandomPassword();
        $user->name = $request->fname.' '.$request->lname;
        $user->username = $request->username;
        $user->contact_no = $request->contact_no;
        $user->type = $request->type;
        $user->licensee_id = $request->licensee_id;   
        $user->user_id = session()->get('user_id');
        $user->password = bcrypt($password);
        $user->active = 1;
        $user->save();
        $arr = [];
        $ids = $request->domain_id;
			
        foreach($ids as $id) {
            $arr[] = ['user_id' => $user->id, 'domain_id'=> $id, 'licensee_id' => $request->licensee_id];
        }	

        $result = UserDomain::insert($arr);		
        $domains = Domain::select('name','id')->where('licensee_id',$request->licensee_id)->get();
        $licensseLogo = DomainFrontend::where('licensee_id',$request->licensee_id)->value('logo');
        
        Mail::send('WebView::onboarding.includes.user-mail', [
           'name' => $user->name,
           'email' => $user->username,
           'type' => $user->type,
           'logo' =>$licensseLogo,
           'domain' => $domains[0]['name'],
           'password' => $password
           ], function($message) use($user) {
               $message->to($user->username)->subject('New Role Assigned');
        });


        return ['result' => 1,'msg' => 'Added Successfully','domains'=>$domains];
    }

    public function userUpdate($licensee_id,Request $request){
        $id = $request->id;
        $i = 0; 
        $arr = [];
        $domains = $request->domain_id;

        User::where('id',$id)->update([
            'name' => $request->fname.' '.$request->lname,
            'username' => $request->username,
            'contact_no' => $request->contact_no,
            'type' => $request->type,
            'user_id' => session()->get('user_id')
        ]);

        foreach ($domains as $key => $domain) {
            foreach ($domain as $value) {
                $arr[] = ['user_id'=>$key,'domain_id'=>$value,'licensee_id'=>$licensee_id];
            }
        }
        
        UserDomain::where('user_id',$id)->delete();
        UserDomain::insert($arr);

        return ['result'=>1,'msg'=>'Updated Successfully','flag'=>'false','url'=>route('onboarding.usermanagement',[$request->licensee_id])];
    }

    public function userdetails($user_id,$licensee_id){
        $user = User::where('id',$user_id)->with('user_domains')->first();
        $domains = Domain::select('name','id')->where('licensee_id',$licensee_id)->get();
        return \View::make('WebView::onboarding.userdetails',compact('licensee_id','domains','user'));
    }

    public function inventory($licensee_id) {
        session(['page_name' => 'onboarding']);
        // $products = DomainProduct::where('licensee_id',$licensee_id)->where('domain_id',$domain_id)->get();
        // $parr = array_column($products->toArray(),'product_id');
        // $parr[] = 0;
        $update = false;
        $domain_ids = Domain::select(['id'])->where('licensee_id', $licensee_id)->get();
        $domain_ids = array_column($domain_ids->toArray(), 'id');
        $total = DomainFrontend::where('licensee_id',$licensee_id)->count();
        $data = InventoryConfig::where('status',1)->get();
        $inventory = DomainInventory::select(['domain_id','inventory_id'])->where('licensee_id',$licensee_id)->get();
        //$inventory = array_column($inventory->toArray(), 'inventory_id');
        
        $result = [];
        foreach ($data as $d) {
            if(!empty($d['parent'])) {
                $result[$d['source'].' '.$d['type']][$d['parent']][] = ['name' => $d['name'] ,'id' => $d['id'],'status'=>$d['status']];
            } else {
                $result[$d['source'].' '.$d['type']][] = ['name' => $d['name'],'id' => $d['id'],'status'=>$d['status']];
            }
        }

        if(!empty($inventory)) {
            $update = true;
        }
        return \View::make('WebView::onboarding.inventory',compact('result','licensee_id','domain_id','inventory','domain_ids','update','total'));
    }

    /*public function inventory($licensee_id,$domain_id) {
        session(['page_name' => 'onboarding']);
        // $products = DomainProduct::where('licensee_id',$licensee_id)->where('domain_id',$domain_id)->get();
        // $parr = array_column($products->toArray(),'product_id');
        // $parr[] = 0;
        $domain_ids = Domain::select(['id'])->where('licensee_id', $licensee_id)->get();
        $domain_ids = array_column($domain_ids->toArray(), 'id');

        $data = InventoryConfig::where('status',1)->get();
        $inventory = DomainInventory::where('licensee_id',$licensee_id)->where('domain_id',$domain_id)->get();
        $inventory = array_column($inventory->toArray(), 'inventory_id');

        $result = [];
        $update = false;
        foreach ($data as $d) {
            if(!empty($d['parent'])) {
                $result[$d['source'].' '.$d['type']][$d['parent']][] = ['name' => $d['name'] ,'id' => $d['id'],'status'=>$d['status']];
            } else {
                $result[$d['source'].' '.$d['type']][] = ['name' => $d['name'],'id' => $d['id'],'status'=>$d['status']];
            }
        }

        return \View::make('WebView::onboarding.inventory',compact('result','licensee_id','domain_id','inventory','domain_ids'));
    }*/

    public function inventoryAdd(Request $request) {
        $inventory = $request->inventory;
        if($inventory) {
            $result = [];
            foreach ($inventory as $row) {
                $result[] = ['licensee_id'=> $request->licensee_id,'domain_id'=>$request->domain_id,'inventory_id' => $row];
            }
        }else{
            return ['result' => 0,'errors'=>['inventory'=>'You must select atleast one to continue']];
        }

        $backend = DomainInventory::insert($result);
        return ['result' => 1,'msg'=>'Added Successfully', 'url'=> route('onboarding.usermanagement',[$request->licensee_id])];
    }

    public function inventoryUpdate($licensee_id,$domain_id,Request $request) {
        $inventory = $request->inventory;
        if($inventory) {
            DomainInventory::where('licensee_id', $licensee_id)->where('domain_id', $domain_id)->delete();
            $result = [];
            foreach ($inventory as $row) {
                $result[] = ['licensee_id'=> $request->licensee_id,'domain_id'=>$request->domain_id,'inventory_id' => $row];
            }
        }else{
            return ['result' => 0,'errors'=>['inventory'=>'You must select atleast one to continue']];
        }

        $backend = DomainInventory::insert($result);
        return ['result' => 1,'msg'=>'Updated Successfully','flag'=>'false'];
    }

    public function createCss($domain_name,$webfont_id,$theme_color,$font_color,$header_color,$footer_color,$search_color){
        $url  = 'https://dev.eroam.com/get-frontend';

        $font = WebFont::select()->where('id',$webfont_id)->first();

        $fields = array(
            'colors' => [
                'theme_color' => $theme_color,
                'font_color' => $font_color,
                'header_color' => $header_color,
                'footer_color' => $footer_color,
                'search_color' => $search_color,
            ],
            'fonts' => [
                'regular' => $font->regular,
                'medium' => $font->medium,
                'light' => $font->light,
                'bold' => $font->bold,
            ],
            'domain_name' => $domain_name,
        );

        $fields_string = json_encode($fields);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, count($fields));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        curl_close($curl);

        $data =  json_decode($result, true);
        return $data;
    }

    public function pricing($licensee_id) {
        session(['page_name' => 'onboarding']);
        $update = false;
        $products = Product::where('status',1)->get();
        $licensee_account = LicenseeAccount::where('licensee_id',$licensee_id)->first();        
        $licensee_commission = LicenseeCommission::where('licensee_id',$licensee_id)->with('products')->get();
        if(!empty($licensee_account)) {
            $update = true;
        }
        return \View::make('WebView::onboarding.pricing',compact('licensee_id','products','licensee_account','licensee_commission','update'));
    }

    public function pricingAdd(Request $request) {
        $rules = [
            'order_id_format' => 'required',
            'checkout_language' => 'required',
            'refund_policy' => 'required',
            'privacy_policy' => 'required',
            'terms_of_service' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()) {
            return ['result' => 0,'errors' => $validator->messages()];
        }
        $product_type = $request->product_type;
        $i = 0;
        $arr = [];
        foreach ($product_type as $type) {
            $arr[] = ['licensee_id'=>$request->licensee_id,'product_type'=>$type,'rate_type'=> $request['rate_type'.$i],'agent_enterprice'=>$request->agent_enterprice[$i],'agent_wholesale'=>$request->agent_wholesale[$i],'reseller_commission'=>$request['commission'.$i]];
            $i++;
        }
        
        LicenseeCommission::insert($arr);
        $licensee_account = new LicenseeAccount();
        
        $licensee_account->order_id_format = $request->order_id_format;
        $licensee_account->checkout_language = $request->checkout_language;
        $licensee_account->tax = $request->tax;
        $licensee_account->refund_policy = $request->refund_policy;
        $licensee_account->privacy_policy = $request->privacy_policy;
        $licensee_account->terms_of_service = $request->terms_of_service;
        $licensee_account->licensee_id = $request->licensee_id;
        $licensee_account->mail_host = $request->mail_host;
        $licensee_account->mail_port = $request->mail_port;
        $licensee_account->mail_username = $request->mail_username;
        $licensee_account->mail_password = $request->mail_password;
        $licensee_account->mail_address = $request->mail_address;
        $licensee_account->mail_name = $request->mail_name;
        $licensee_account->mail_encryption = $request->mail_encryption;
        $licensee_account->save();

        return ['result'=>1,'msg'=>'Added Successfully','url'=> route('onboarding.account',$request->licensee_id)];

    }

    public function pricingUpdate(Request $request) {
        $rules = [
            'order_id_format' => 'required',
            'checkout_language' => 'required',
            'refund_policy' => 'required',
            'privacy_policy' => 'required',
            'terms_of_service' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()) {
            return ['result' => 0,'errors' => $validator->messages()];
        }
        LicenseeCommission::where('licensee_id',$request->licensee_id)->delete();
        
        $product_type = $request->product_type;
        $i = 0;
        $arr = [];
        foreach ($product_type as $type) {
            $arr[] = ['licensee_id'=>$request->licensee_id,'product_type'=>$type,'rate_type'=> $request['rate_type'.$i],'agent_enterprice'=>$request->agent_enterprice[$i],'agent_wholesale'=>$request->agent_wholesale[$i],'reseller_commission'=>$request['commission'.$i]];
            $i++;
        }
        
        LicenseeCommission::insert($arr);
        LicenseeAccount::where('licensee_id',$request->licensee_id)->update([
            'order_id_format' => $request->order_id_format,
            'checkout_language' => $request->checkout_language,
            'tax' => $request->tax,
            'refund_policy' => $request->refund_policy,
            'privacy_policy' => $request->privacy_policy,
            'terms_of_service' => $request->terms_of_service,
            'mail_host' => $request->mail_host,
            'mail_port' => $request->mail_port,
            'mail_username' => $request->mail_username,
            'mail_password' => $request->mail_password,
            'mail_address' => $request->mail_address,
            'mail_name' => $request->mail_name,
            'mail_encryption' => $request->mail_encryption
        ]);

        return ['result'=>1,'msg'=>'Updated Successfully','flag'=>'false'];

    }
}

