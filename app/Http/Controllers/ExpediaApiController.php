<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\HotelMapping;
use App\CardInfo;
use Illuminate\Support\Facades\Log;

class ExpediaApiController extends Controller 
{
	private $host;
	private $apikey;
	private $secret;
	private $cid;
	private $minorRev;
	private $ver;
	private $path;
	private $locale;
	private $currencyCode;
	private $timestamp;
	private $sig;
	private $query;

	public function __construct(){
        parent::__construct();
		$this->eroamPercentage = 20;
		$this->host 		= 'http://api.ean.com/';
		$this->book_host 	= 'https://book.api.ean.com/';
        /*$this->apiKey       = "1jn4t7g731vbgetaifib3skism";
        $this->secret       = "5c6s845m2vhid";
        $this->cid          = '502698';
        $this->rateType     = 'MerchantStandard|MerchantPackage';*/
        $this->apiKey       = "78851t9fhppaqtuhh47stq5k6m";
        $this->secret       = "13dee4e3803c4e0f435df2f4c4";
        $this->cid          = '506304';
        $this->rateType     = 'MerchantStandard';
		$this->minorRev 	= '30';
		$this->supplierType = 'E';
		$this->ver 			= 'v3/';
		$this->path 		= "ean-services/rs/hotel/{$this->ver}";
		$this->locale 		= 'en_AU';
		$this->currencyCode = 'AUD';
		$this->timestamp 	= gmdate('U');
		$this->sig 			= md5($this->apiKey . $this->secret . $this->timestamp);
		$this->customerIpAddress  = $_SERVER['REMOTE_ADDR'];
		$this->query 		= "?cid={$this->cid}&apikey={$this->apiKey}&sig={$this->sig}&minorRev={$this->minorRev}&locale={$this->locale}&currencyCode={$this->currencyCode}&customerIpAddress={$this->customerIpAddress}";//&fullyRefundable=true";		      
	}

	public function getHotels()
    {
        $data = Input::all();
        //$data = '{"city":"Darwin","countryCode":"AU","arrivalDate":"2018-11-09","departureDate":"2018-11-11","numberOfAdults":"2","customerSessionId":null,"leg":"0","domain":"localeroam.com","search":null,"travel_preferences":null,"customeruseragent":"Google Chrome\/69.0.3497.100","search_input":{"_token":"5250comoXqqQ0OPtccQQUvTPoWu1OnUd6MthQSFG","country":null,"city":null,"to_country":"513","to_city":"9","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"manual","searchValType2":null,"searchVal2":null,"start_location":null,"end_location":"Darwin, Australia","start_date":"2018-10-09","rooms":"1","travellers":2,"total_children":"1","num_of_pax":["3"],"is_child":{"1":{"1":"0","3":"1"}},"firstname":{"1":{"1":"rekha","2":"dhara","3":"cone"}},"lastname":{"1":{"1":"test","2":"test","3":"test"}},"dob":{"1":{"1":"03-05-1985","2":"03-05-1985","3":"01-10-2018"}},"age":{"1":{"1":"33","2":"33","3":"0"}},"child":[[1]],"interests":[],"travel_preferences":null,"num_of_adults":[2],"num_of_children":[1],"pax":[[{"firstname":"rekha","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"dhara","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"cone","lastname":"test","dob":"01-10-2018","age":1,"child":1}]],"childrens":1},"default_nights":"2","city_id":9}';
        //$data = json_decode($data,true);
        //For prefernces
        $domain = Input::get('domain');
        // $domain = 'eroamnew.com';
        $preferences= '';
        if(isset($data['search_input']['travel_preferences']))
        {
            $preferences = $data['search_input']['travel_preferences'];
        }
        $starRatingStr = '';
        $starRating = '';
        if(isset($preferences[0]['accommodation'][0])){
            $starRating = $preferences[0]['accommodation'][0];
            if($starRating == 5){
                $starRating = 2;
            }elseif($starRating == 2){
                $starRating = 3;
            }elseif($starRating == 3){
                $starRating = 4;
            }elseif($starRating == 9){
                $starRating = 5;
            }
            $starRatingStr = '&minStarRating='.$starRating.'&maxStarRating='.$starRating.'';
            if($starRating == 0){
                $starRatingStr = '';
            }
        }
        $numberOfResults    = '50';
        $method             = 'list';
        $rateType           = $this->rateType;//'MerchantPackage';
        $customerSessionId  = '';
        if(isset($data['customerSessionId']) && !empty($data['customerSessionId'])){
            $customerSessionId = $data['customerSessionId'];
        }
        
        $num_of_rooms = $data['search_input']['rooms'];
        $hotel_request_data = $data['search_input'];

        $children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
            $children = $hotel_request_data['child'];
        }

        $expedia_request  = '';
        if($num_of_rooms == 1){
            $expedia_request .='&room1='.$hotel_request_data['num_of_adults'][0];
            if(isset($children[0]) && !empty($children[0])){
                $c_array = implode(',',$children[0]);
                $c_array = str_replace('0','1',$c_array);
                $expedia_request .=','.count($children[0]).','.$c_array;
            }
        }else{
            for ($i=0;$i<$num_of_rooms;$i++){
                $n = $i+1;
                $expedia_request .='&room'.$n.'='.$hotel_request_data['num_of_adults'][$i];
                if(isset($children[$i]) && !empty($children[$i])){
                    $c_array = implode(',',$children[$i]);
                    $c_array = str_replace('0','1',$c_array);
                    $expedia_request .=','.count($children[$i]).','.$c_array;
                }
            }
        }
      
        $nights = 0;
        $nights = $data['default_nights'];
        $cityId = $data['city_id'];

        $numberOfAdults     =$data['numberOfAdults'];
        $arrivalDate        = date('m/d/Y', strtotime($data['arrivalDate'])); 
        $departureDate      = date('m/d/Y', strtotime($data['departureDate']));

        $query  = $this->query;

        if(isset($data['customeruseragent'])){
            $data['customeruseragent'] = urlencode($data['customeruseragent']);
            $query .= "&customeruseragent={$data['customeruseragent']}";
        }

        if(isset($data['search'])){
            $query .= "&propertyName={$data['search']}";
        }

        if($customerSessionId == ''){
            $query .= "&rateType={$rateType}&city={$data['city']}&countryCode={$data['countryCode']}";
        }else{
            $query .= "&customerSessionId={$customerSessionId}&rateType={$rateType}&city={$data['city']}&countryCode={$data['countryCode']}";
        }
        //$query .= "&customerSessionId={$customerSessionId}&rateType={$rateType}&city={$data['city']}&countryCode={$data['countryCode']}";
        $cityData['cityId'] = $cityId;
        $hotelIdList = $this->expediaMappedHotels($cityId);
            if(!empty($hotelIdList)){
                $query .= "&hotelIdList={$hotelIdList}";
            }
        $query .= $starRatingStr;
        $query .= "&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$expedia_request}&numberOfResults={$numberOfResults}";
        //echo $this->host . $this->path.$method . $query; die;

        Log::channel('expedia_hotels')->info('Request : '.$this->host . $this->path.$method . $query);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->host . $this->path."{$method}" . $query);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $headers = curl_getinfo($ch);
        //echo '<pre>'; print_r($headers); print_r(json_decode($data,true)); die;

        Log::channel('expedia_hotels')->info('Response : '.$data);
        Log::channel('expedia_hotels')->info('--------------------------------------------');

        curl_close($ch);
        if ($headers['http_code'] != '200') {
            return \Response::json([
                'status' => 200,
                'data' => []
            ]);
            
        } else {
            $status = $headers['http_code'];
            $temp_result = json_decode($data,true);
            $data = json_decode($data);
            
            if($data->HotelListResponse && array_key_exists('EanWsError', $data->HotelListResponse)){
                return \Response::json([
                    'status' => 200,
                    'data' => []
                ]);
            } else {

                if(!isset($temp_result['HotelListResponse']['HotelList']['HotelSummary'][0])){
                    $HotelSummary['0'] = $temp_result['HotelListResponse']['HotelList']['HotelSummary'];
                    $hotelLists = $HotelSummary;
                    $hotelLists = json_decode(json_encode($hotelLists));
                }else{
                    $hotelLists= $data->HotelListResponse->HotelList->HotelSummary;
                }
                


                $hotels  = array();
                if ($domain) {
                    $commissions = getDomainData($domain);
                    if ($commissions['pricing'][0]['status'] == 0) {
                        $hotelLists = updateAccomodationPricing($hotelLists,'accomodation','expedia',$commissions['eroam'],$commissions['pricing']);
                    }
                }    

                if($hotelLists){   
                    $hotelLists = json_decode(json_encode($hotelLists));
                    foreach ($hotelLists as $key => $hotelList ) { 
                        if(isset($hotelList->thumbNailUrl)){
                            $hotel['hotelId'] =  $hotelList->hotelId;
                            $hotel['code'] =  $hotelList->hotelId;
                            $hotel['name'] = $hotelList->name;
                            $hotel['address'] = $hotelList->address1;
                            $hotel['city'] = $hotelList->city;
                            $hotel['stateProvinceCode'] = '';//$hotelList->stateProvinceCode;
                            if(array_key_exists('postalCode', $hotelList)){
                                $hotel['postalCode'] = $hotelList->postalCode;
                            }else{
                                $hotel['postalCode'] = '';
                            }
                            $hotel['hotelRating'] = $hotelList->hotelRating;
                            $hotel['propertyCategory'] = $hotelList->propertyCategory;
                            $hotel['rateCurrencyCode'] = $hotelList->rateCurrencyCode;
                            $hotel['description'] = html_entity_decode($hotelList->shortDescription);
                            $hotel['image'] = $this->changeImage("http://media.expedia.com".$hotelList->thumbNailUrl);
        
                            $hotel['highRate'] = $hotelList->highRate;
                            $hotel['lowRate'] = $hotelList->lowRate;
                            $hotel['latitude'] = $hotelList->latitude;
                            $hotel['longitude'] = $hotelList->longitude;
                            $hotel['countryCode'] = $hotelList->countryCode;
                            $hotel['hotelRating'] = $hotelList->hotelRating;
                            $hotel['hotelRatingDisplay'] = $hotelList->hotelRatingDisplay;

                            $hotel['rooms'] = [];
                            $groups = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo;
                            $i=0;
                            if(count(array($groups->ChargeableRateInfo)) > 0){
                                if(isset($groups->ChargeableRateInfo->detailPricing)) 
                                {
                                    $hotel['detailPricing'] = $groups->ChargeableRateInfo->detailPricing;   
                                }
                                $hotel['rooms'][$i]['roomTypeCode'] = $hotelList->RoomRateDetailsList->RoomRateDetails->roomTypeCode;
                                $hotel['rooms'][$i]['rateCode'] = $hotelList->RoomRateDetailsList->RoomRateDetails->rateCode;
                                $hotel['rooms'][$i]['maxRoomOccupancy'] = $hotelList->RoomRateDetailsList->RoomRateDetails->maxRoomOccupancy;
                                $hotel['rooms'][$i]['quotedRoomOccupancy'] = $hotelList->RoomRateDetailsList->RoomRateDetails->quotedRoomOccupancy;
                                $hotel['rooms'][$i]['roomDescription']  = $hotelList->RoomRateDetailsList->RoomRateDetails->roomDescription;
                                $hotel['rooms'][$i]['expediaPropertyId'] = $hotelList->RoomRateDetailsList->RoomRateDetails->expediaPropertyId;
                                $hotel['rooms'][$i]['currentAllotment'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->currentAllotment;
                                $hotel['rooms'][$i]['nonRefundable'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->nonRefundable;
                                $hotel['rooms'][$i]['rateType'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->rateType;
                                $temp_array = json_decode(json_encode($hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->RoomGroup),true);
                                if(isset($temp_array['Room'][0])){
                                    $nights = count($temp_array['Room'][0]['ChargeableNightlyRates']);
                                    $hotel['rooms'][$i]['rateKey'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->RoomGroup->Room[0]->rateKey;
                                }else{
                                    $nights = count($temp_array['Room']['ChargeableNightlyRates']);
                                    $hotel['rooms'][$i]['rateKey'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->RoomGroup->Room->rateKey;
                                }
                                
                                foreach ($groups->ChargeableRateInfo as $key1 => $val1){
                                    if($key1 == '@total'){
                                        $total = (($val1 * $this->eroamPercentage) / 100) + $val1;
                                        $total = $total / $numberOfAdults; 
                                        $total = $total / $nights; 
                                        $hotel['rooms'][$i]['rate'] = round($total,2);
                                        $hotel['rooms'][$i]['exptotal'] = $val1;
                                        break;
                                    }
                                }

                                $hotels[] = $hotel; 
                            }
                        
                        }
                    }                    
                }

                return \Response::json([
                    'status' => $status,
                    'data' => $hotels
                ]);
                
            }
        }
    }

	private function expediaMappedHotels($id){
		$results = HotelMapping::select('EANHotelID')->where('city_id',$id)->get();

		$hotelId = array();
		foreach ($results as $key => $value) {
			array_push($hotelId, $value->EANHotelID);
		}
		$hotelId = implode(',',$hotelId);
		return $hotelId;
	}
	private function changeImage($uri){
		$hotelImage = '';
		$hotelImage1 = $uri;
		$hotelImage2 = str_replace("_t.", "_y.", $uri);
		$hotelImage3 = str_replace("_t.", "_s.", $uri);

		if ($this->existsImage($hotelImage2) == 200) {
			 $hotelImage = $hotelImage2;
		} elseif($this->existsImage($hotelImage3) == 200) {
			 $hotelImage = $hotelImage3;
		} else {
			 $hotelImage = $hotelImage1;
		}
		return $hotelImage;
	}

	private function existsImage($uri)
	{
	    $ch = curl_init($uri);
	    curl_setopt($ch, CURLOPT_NOBODY, true);
	    curl_exec($ch);
	    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);

	    return $code == 200;
	}
    
    public function getHotelDetails(){
        $data = Input::all();
        // $data = '{"code":"183751","arrivalDate":"09-05-2018","departureDate":"09-08-2018","room":"&room1=1","roomTypeCode":"327338","includeDetails":"true","options":"HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES","rateKey":"22718007-9e6b-4e41-aa5f-a33395301481-5221-024","latitude":"-33.9601","longitude":"151.1567","customerSessionId":"AC110002-FA01-A916-5A22-FD1D13905107","type":1,"rateCode":""}';
        //$data = '{"code":"467919","arrivalDate":"10\/9\/2018","departureDate":"10\/11\/2018","room":"&room1=2,1,1","roomTypeCode":"200737889","includeDetails":"true","options":"HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES","rateKey":"0f66dff1-dc77-408a-b796-bcf7ca2c3825-5221-024","latitude":"-12.46006","longitude":"130.83667","customerSessionId":null,"type":1,"rateCode":"","customeruseragent":"Google Chrome\/69.0.3497.100"}';
        //$data = json_decode($data,true);

        $domain = request()->header('domain');
        $hotelID = $data['code'];
        $latitude = $data['latitude'];
        $longitude = $data['longitude'];
        $apiExperience = 'PARTNER_WEBSITE';
        $customerSessionId = '';
        if(isset($data['customerSessionId']) && !empty($data['customerSessionId'])){
            $data['customerSessionId'];
        }

        $customeruseragent = '';
        if(isset($data['customeruseragent'])){
            $data['customeruseragent'] = urlencode($data['customeruseragent']);
            $customeruseragent = urlencode($data['customeruseragent']);
            $this->query .= "&customeruseragent={$data['customeruseragent']}";
        }

        if($customerSessionId == ''){
            $query  = $this->query."&apiExperience={$apiExperience}&hotelId={$data['code']}&arrivalDate={$data['arrivalDate']}&departureDate={$data['departureDate']}{$data['room']}";
        }else{
            $query  = $this->query."&customerSessionId={$customerSessionId}&apiExperience={$apiExperience}&hotelId={$data['code']}&arrivalDate={$data['arrivalDate']}&departureDate={$data['departureDate']}{$data['room']}";
        }
    	//$query  = $this->query."&customerSessionId={$customerSessionId}&apiExperience={$apiExperience}&hotelId={$data['code']}&arrivalDate={$data['arrivalDate']}&departureDate={$data['departureDate']}{$data['room']}";

    	if($data['type'] == 1){
    		$query  .= "&includeDetails={$data['includeDetails']}&options={$data['options']}";
    		$return_data = $this->callExpediaAvaibility($query);
    	}else{
    		$query  .= "&rateCode={$data['rateCode']}&roomTypeCode={$data['roomTypeCode']}&includeDetails={$data['includeDetails']}&options={$data['options']}";

    		$return_data = $this->callExpediaAvaibility($query);
    		$data = $return_data['data'];
    		$data = json_decode($data);
			$data->latitude = $latitude;
			$data->longitude = $longitude;
    		if($data->HotelRoomAvailabilityResponse && array_key_exists('EanWsError', $data->HotelRoomAvailabilityResponse)){
				$query  = $this->query."&customerSessionId={$customerSessionId}&apiExperience={$apiExperience}&hotelId={$hotelId}&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$room}";
    			$query  .= "&includeDetails={$includeDetails}&options={$options}";
    			$return_data = $this->callExpediaAvaibility($query);
    		}else{
    			$hotelDetail = $data->HotelRoomAvailabilityResponse;
		 		return json_encode(array(
					'status' => 200,
					'data' => $hotelDetail));		
    		}
    	}

        $rateType = $this->rateType;
        $query  .= "&rateType={$rateType}";
        //echo $query; die;

    	$return_data = $this->callExpediaAvaibility($query);
    	$headers = $return_data['headers'];
    	$data = $return_data['data'];
        //echo '<pre>'; print_r($return_data); die;

		if ($headers['http_code'] != '200') {
		 	
            return \Response::json([
                'status' => 200,
                'data' => []
            ]);    
		}else{
			$data = json_decode($data);
			$data->latitude = $latitude;
			$data->longitude = $longitude;

			if($data->HotelRoomAvailabilityResponse && array_key_exists('EanWsError', $data->HotelRoomAvailabilityResponse)){		 
                if($data->HotelRoomAvailabilityResponse->EanWsError->category == 'SOLD_OUT'){
                    $detail['hotelID'] = $hotelID;
                    $detail['countryCode']= 'AU';
                    $detail['customeruseragent']= $customeruseragent;

                    $returnDataInfo = $this->hotelInfo2($detail);

                    $headersInfo = $returnDataInfo['headers'];
                    $dataInfo = json_decode($returnDataInfo['data']);
                    //echo '<pre>'; print_r($dataInfo); die;
                    if ($headersInfo['http_code'] != '200') {
                        return \Response::json([
                            'status' => 200,
                            'data' => []
                        ]);    
                    } else {
                        if($dataInfo->HotelInformationResponse && array_key_exists('EanWsError', $dataInfo->HotelInformationResponse)){  
                            return \Response::json([
                                'status' => 200,
                                'data' => []
                            ]);
                        } else {
                            $hotelDetail = $dataInfo->HotelInformationResponse;
                            $hotelDetail->hotelName = $hotelDetail->HotelSummary->name;
                            $hotelDetail->hotelCity = $hotelDetail->HotelSummary->city;
                            if(isset($hotelDetail->HotelDetails->specialCheckInInstructions)){
                                $hotelDetail->specialCheckInInstructions = $hotelDetail->HotelDetails->specialCheckInInstructions;
                            }
                            if(isset($hotelDetail->HotelDetails->checkInInstructions)){
                                $hotelDetail->checkInInstructions = $hotelDetail->HotelDetails->checkInInstructions;
                            }
                            return \Response::json([
                                'status' => 200,
                                'data' => $hotelDetail
                            ]); 
                        }
                    }  
                } else {	 	
    				return \Response::json([
                        'status' => 200,
                        'data' => []
                    ]);
                }
		 	} else {
		 		$hotelDetail = $data->HotelRoomAvailabilityResponse;
                if ($domain) {
                    $commissions = getDomainData($domain);
                    if ($commissions['pricing'][0]['status'] == 0) {
                        $hotelDetail = updateAccomodationPricing($hotelDetail,'accomodation','expedia',$commissions['eroam'],$commissions['pricing']);
                    }
                }                
                
                return \Response::json([
                    'status' => 200,
                    'data' => $hotelDetail
                ]); 
			}
		}	
	}

	public function callExpediaAvaibility($query){
        $method = 'avail';
        Log::channel('expedia_avaibility')->info('Request : '.$this->host . $this->path. $method . $query);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->host . $this->path."{$method}" . $query);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);
        Log::channel('expedia_avaibility')->info('Response : '. $data);
        Log::channel('expedia_avaibility')->info('--------------------------------------------');

		$return['headers'] = $headers;
		$return['data'] = $data;
		curl_close($ch);
		return $return;
	}

	public function hotelInfo(){
		$data = Input::all();
		$method = 'info';
    	$query  = $this->query;

        if(isset($data['customeruseragent'])){
            $data['customeruseragent'] = urlencode($data['customeruseragent']);
            $query .= "&customeruseragent={$data['customeruseragent']}";
        }

        $query .= "&hotelId={$data['hotelID']}&countryCode={$data['countryCode']}";
    	// initiate curl
        Log::channel('expedia_info')->info('Request : '.$this->host . $this->path. $method . $query);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->host . $this->path."{$method}" . $query);
		//curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);

		// close curl
		curl_close($ch);
        Log::channel('expedia_info')->info('Response : '.$data);
        Log::channel('expedia_info')->info('--------------------------------------------');

		if ($headers['http_code'] != '200') {
		 	 return \Response::json([
                    'status' => 200,
                    'data' => []
                ]); 
		} else {
				$data = json_decode($data);
				 return \Response::json([
                    'status' => 200,
                    'data' => $data
                ]); 
		}
	}

    public function hotelInfo2($data){
        //$data = Input::all();
        $method = 'info';
        $customeruseragent;
        $query  = $this->query;

        if(isset($data['customeruseragent'])){
            $query .= "&customeruseragent={$data['customeruseragent']}";
        }

        $query .= "&hotelId={$data['hotelID']}&countryCode={$data['countryCode']}";
        // initiate curl
        $ch = curl_init();
        Log::channel('expedia_info')->info('Request : '.$this->host . $this->path. $method . $query);
        curl_setopt($ch, CURLOPT_URL, $this->host . $this->path."{$method}" . $query);
        //curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $headers = curl_getinfo($ch);
        Log::channel('expedia_info')->info('Response : '.$data);
        Log::channel('expedia_info')->info('--------------------------------------------');

        $return['headers'] = $headers;
        $return['data'] = $data;
        // close curl
        curl_close($ch);
        return $return;
    }

	public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public function expediaBooking(){

		//Test Booking Credentials Static data provided by Expedia
		$data = Input::all();
        $response = $this->getCardinfo();
	    
	    $creditCardNumber = $this->safe_b64decode($response[0]['cardNumber']);
        $creditCardIdentifier = $this->safe_b64decode($response[0]['cvvNumber']);
        $creditCardExpirationMonth = $this->safe_b64decode($response[0]['expiryMonth']);
        $creditCardExpirationYear = $this->safe_b64decode($response[0]['expiryYear']);

        $email = 'test@travelnow.com';
        $firstName = 'Test Booking';
        $lastName = 'Test Booking';
        $homePhone = '2145370159';
        $workPhone = '2145370159';
        $creditCardType = 'CA';
        $address1 = 'travelnow';
        $city = 'Atlantic City';
        $stateProvinceCode = 'NJ';
        $countryCode = 'AU';
        $postalCode = '08401';

        /*$creditCardNumber = '4929199188636773';
        $creditCardIdentifier = '123';
        $creditCardExpirationMonth = '12';
        $creditCardExpirationYear = '2028';

        $email = 'res@eroam.com'; 
	    $firstName = 'Ross'; 
	    $lastName = 'Gregory';
	    $homePhone = '+61401575255';
	    $workPhone = '+61401575255';
	    $creditCardType = 'VI';
	    $address1 = '74/80 Balcombe Road'; 
	    $city = 'Mentone'; 
	    $stateProvinceCode = 'VIC';
	    $countryCode = 'AU';
	    $postalCode = '3194'; */

        if(isset($data['customerSessionId'])){
            $rateType = $data['rateType'];
        } else {
            $rateType = $this->rateType;
        }
		
		
        $query = $this->query;//.'&customerSessionId={$data["customerSessionId"]}';

        if(isset($data['customerSessionId'])){
            $query .= "&customerSessionId={$data['customerSessionId']}";
        }

        if(isset($data['customeruseragent'])){
            $data['customeruseragent'] = urlencode($data['customeruseragent']);
            $query .= "&customeruseragent={$data['customeruseragent']}";
        }

	    $query .= "&hotelId={$data['hotelId']}&arrivalDate={$data['arrivalDate']}&departureDate={$data['departureDate']}&supplierType=E&rateKey={$data['rateKey']}&roomTypeCode={$data['roomTypeCode']}&rateCode={$data['rateCode']}{$data['expedia_request']}&chargeableRate={$data['chargeableRate']}&email={$email}&firstName={$firstName}&lastName={$lastName}&homePhone={$homePhone}&workPhone={$workPhone}&creditCardType={$creditCardType}&creditCardNumber={$creditCardNumber}&creditCardIdentifier={$creditCardIdentifier}&creditCardExpirationMonth={$creditCardExpirationMonth}&creditCardExpirationYear={$creditCardExpirationYear}&address1={$address1}&city={$city}&stateProvinceCode={$stateProvinceCode}&countryCode={$countryCode}&postalCode={$postalCode}&rateType={$rateType}";
		
		$host = 'https://book.api.ean.com/';
		$ver 		= 'v3/';
		$method 	= 'res';
		$path 		= "ean-services/rs/hotel/{$ver}{$method}";
		

        Log::channel('expedia_booking')->info('Request : '.$host . $path . $query);
		//Calling curl for booking
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $host . $path . $this->query);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$query);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);
        Log::channel('expedia_booking')->info('Response : '.$data);
        Log::channel('expedia_booking')->info('--------------------------------------------');

		curl_close($ch);	
		$data = json_decode($data);
		$return_data = array();
		if(isset($data->HotelRoomReservationResponse) && array_key_exists('EanWsError', $data->HotelRoomReservationResponse)){
		 	 	$return_data['itineraryId'] = '';
				$return_data['numberOfRoomsBooked'] = '';
				$return_data['reservationStatusCode'] = 'NC';
		} else {
			$return_data['itineraryId'] = $data->HotelRoomReservationResponse->itineraryId;
			$return_data['numberOfRoomsBooked'] = $data->HotelRoomReservationResponse->numberOfRoomsBooked;
			$return_data['reservationStatusCode'] = $data->HotelRoomReservationResponse->reservationStatusCode;
		}
		return \Response::json([
                    'status' => 200,
                    'data' => $return_data
                ]);
		
	}
	private function getCardinfo(){
	    $results = CardInfo::where('id',4)->get();
		return $results; 						   
	}

	public function hotelVoucher(){
		$input = Input::all();
		
    	if(isset($input['itineraryId'])){
            $email      = 'test@travelnow.com';
	    	//$email	      = 'res@eroam.com';
	    	$method 	= 'itin';
	    	$query  = $this->query;

            if(isset($input['customeruseragent'])){
                $input['customeruseragent'] = urlencode($input['customeruseragent']);
                $query .= "&customeruseragent={$input['customeruseragent']}";
            }

            $query .= "&itineraryId={$input['itineraryId']}&email={$email}";
            Log::channel('expedia_voucher')->info('Request : '.$this->host . $this->path. $method . $query);
	    	// initiate curl
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->host . $this->path."{$method}" . $query);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$data = curl_exec($ch);
            Log::channel('expedia_voucher')->info('Response : '.$data);
            Log::channel('expedia_voucher')->info('--------------------------------------------');

            
			$headers = curl_getinfo($ch);
			$return['headers'] = $headers;
			$return['data'] = $data;
			curl_close($ch);
			$data = json_decode($data); 

			if(isset($data->HotelItineraryResponse->Itinerary)){
				$data = json_decode(json_encode($data->HotelItineraryResponse->Itinerary), true ); 
				$data['invoiceNumber'] = $input['invoiceNumber'];
				$data['email'] = $input['email1'];
				$data['bookingId'] = $data['itineraryId'];
				$data['bookingDate'] = date('j M Y,',strtotime($data['creationDate'])).' '.date('g:i a',strtotime($data['creationTime']));
				$hotel = $data['HotelConfirmation'];
				$data['valueAdds'] = '';

				$data['price'] = $input['rate'];
				$data['taxes'] = $input['taxes'];
				$data['city1'] = $input['city'];
				$data['country_code'] = $input['country_code'];
				$data['leg'] = $input['leg'];
				
				if(isset($hotel['confirmationNumber'])){  
					$data['name'] = ucfirst($hotel['ReservationGuest']['firstName'].' '.$hotel['ReservationGuest']['lastName']);
					$data['hotelName'] = $hotel['Hotel']['name'];
					$data['address'] = $hotel['Hotel']['address1'];
					$data['city'] = $hotel['Hotel']['city'];
					$data['checkIn'] = date('j M Y',strtotime($data['itineraryStartDate']));
					$data['checkOut'] = date('j M Y',strtotime($data['itineraryEndDate']));
					$data['checkIn1'] = date('j M',strtotime($data['itineraryStartDate']));
					$data['checkOut1'] = date('j M',strtotime($data['itineraryEndDate']));
					$data['nights'] = $hotel['nights'];
					$data['numberOfAdults']= $hotel['numberOfAdults'];

					$rate = $hotel['RateInfos']['RateInfo']['ChargeableRateInfo'];
					$data['currencyCode'] = $rate['@currencyCode'];
                	
					$data['rooms'] = 1;
					$data['creditCard'] = 0;

					$data['HotelFees'] = 0;
					if(isset($hotel['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount'])){
						$data['HotelFees'] = $hotel['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount'];
					}
					
					if(isset($hotel['RateInfos']['RateInfo']['cancellationPolicy'])){
						$data['cancellationPolicy'] = $hotel['RateInfos']['RateInfo']['cancellationPolicy'];
					}

					if(isset($hotel['Hotel']['specialCheckInInstructions'])){
						$data['instruction'] = $hotel['Hotel']['specialCheckInInstructions'];
					}

					if(isset($hotel['ValueAdds'])){
						$data['valueAdds'] = $hotel['ValueAdds']['ValueAdd'];
					}
					
					$data['roomDescription'] = $hotel['roomDescription'];
                    $data['affiliateConfirmationId'] = $hotel['affiliateConfirmationId'];
                    $data['confirmationNumber'][] = $hotel['confirmationNumber'];

                    
				} else {
					$hotels = $hotel;
					$guest = 0;
					$i 	   = 0;
                    $data['name'] = ucfirst($hotel[0]['ReservationGuest']['firstName'].' '.$hotel[0]['ReservationGuest']['lastName']);

					foreach ($hotels as $hotel) { $i++;
						$guest += $hotel['numberOfAdults'];
						
						$data['hotelName'] = $hotel['Hotel']['name'];
						
						$data['address'] = $hotel['Hotel']['address1'];
						$data['city'] = $hotel['Hotel']['city'];
						$data['checkIn'] = date('j M Y',strtotime($data['itineraryStartDate']));
						$data['checkOut'] = date('j M Y',strtotime($data['itineraryEndDate']));
						$data['checkIn1'] = date('j M',strtotime($data['itineraryStartDate']));
						$data['checkOut1'] = date('j M',strtotime($data['itineraryEndDate']));
						$data['nights'] = $hotel['nights'];

						$rate = $hotel['RateInfos']['RateInfo']['ChargeableRateInfo']; 
						$data['currencyCode'] = $rate['@currencyCode'];
						
						$data['creditCard'] = 0;
						$data['HotelFees'] = 0;
						if(isset($hotel['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount'])){
							$data['HotelFees'] = $hotel['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount'];
						}
						
						if(isset($hotel['RateInfos']['RateInfo']['cancellationPolicy'])){
							$data['cancellationPolicy'] = $hotel['RateInfos']['RateInfo']['cancellationPolicy'];
						}

						if(isset($hotel['Hotel']['specialCheckInInstructions'])){
							$data['instruction'] = $hotel['Hotel']['specialCheckInInstructions'];
						}
						
						if(isset($hotel['ValueAdds'])){
							$data['valueAdds'] = $hotel['ValueAdds']['ValueAdd'];
						}

						$data['roomDescription'] = $hotel['roomDescription'];	
                        $data['affiliateConfirmationId'] = $hotel['affiliateConfirmationId'];
                        $data['confirmationNumber'][] = $hotel['confirmationNumber'];
					}

					$data['numberOfAdults']= $guest;
					$data['rooms'] = $i;
				}
				return \Response::json([
                    'status' => 200,
                    'data' => $data
                ]);
			} 
		}
	}
    public function getPercentage(){
        $data['eroamPercentage'] = $this->eroamPercentage;
        return \Response::json([
            'status' => 200,
            'data' => $data
        ]);
        
    }
}