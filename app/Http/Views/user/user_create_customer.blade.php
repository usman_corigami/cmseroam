@extends( 'layout/mainlayout' )

@section('content')
    <div class="content-container">
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Customer']) }}</h1>
        <div class="row">
            @if ( Session::has( 'message' ) && Session::get( 'message' ) == 'success' )
                <div class="small-12 small-centered columns success-box">
                    <a href="{{ URL::to( 'agent/' . Session::get( 'user_id' ) ) }}">{{ Session::get( 'agent_name' ) }}</a> 
                    account has been created.
                </div>
            @endif

        </div>	
        <br/>
        <form action="{{ route('user.create-customer') }}" method="post" id="addForm">
            {{ csrf_field() }}
            <div class="box-wrapper">
                <p>{{ trans('messages.customer_detail') }}</p>
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.first_name') }}<span class="required">*</span></label>
                    <input type="text" id="name" class="form-control" placeholder="{{ trans('messages.first_name_placeholder') }}" name="first_name" value="{{ old('first_name') }}"/>
                </div>
                @if ( $errors->first( 'first_name' ) )
                    <small class="error">{{ $errors->first('first_name') }}</small>
                @endif
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.last_name') }} <span class="required">*</span></label>
                    <input type="text" id="domain" class="form-control" placeholder="{{ trans('messages.last_name_placeholder') }}" name="last_name" value="{{ old('last_name') }}" />
                </div>
                @if ( $errors->first( 'last_name' ) )
                    <small class="error">{{ $errors->first('last_name') }}</small>
                @endif
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.agent_username') }} 
                        <span class="required">*</span>
                        <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ trans('messages.agent_username_title') }}">
                            <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                        </span>
                    </label>
                    <small class="error username-doesnt-exist" style="width:100%;display:none;">
                        {{ trans('messages.agent_username_not_exist') }}
                    </small>
                    <small class="error username-exists" style="display:none;width:100%;">
                        {{ trans('messages.agent_username_exist') }}
                    </small>
                    <div class="input-group input-group-box">
	                <input type="email" id="email" class="form-control" placeholder="{{ trans('messages.agent_username_placeholder') }}" name="username" value="{{ old('username') }}" />
	               
	                <span class="input-group-btn">
                            <a href="#" class="button postfix check-username-btn btn btn-default"><i class="icon-search-domain" aria-hidden="true"></i></a>
	                </span>
                    </div>
                    
                </div>
                @if ( $errors->first( 'username' ) )
                    <small class="error">{{ $errors->first('username') }}</small>
                @endif

                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.password') }} <span class="required">*</span>
                        <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ trans('messages.agent_password_title') }}">
                            <i class="fa fa-question-circle fa-lg" aria-hidden="true" style=""></i>
                        </span>
                    </label>
                    <input type="text" name="password" value="*******" disabled class="form-control"/>
              		
                </div>
                
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.currency_label') }} <span class="required">*</span></label>	
                    <select name="currency_id" class="form-control">
                        <option disabled selected>{{ trans('messages.currency') }}</option>
                        @foreach ($oCurrency as $key => $currency)
                        <option value="{{ $currency->id }}">
                                {{ $currency->code }}
                        </option>
                        @endforeach;
                    </select>
              	 
                </div>
                @if ( $errors->first( 'currency_id' ) )
                    <div class="small-12 column">
                            <small class="error">{{ $errors->first('currency_id') }}</small>
                    </div>
                @endif
                
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.address') }} <span class="required">*</span></label>
                    <textarea name="address" class="form-control" rows="1" placeholder="{{ trans('messages.address_placeholder') }}">{{ old('address') }}</textarea>
                </div>
                @if ( $errors->first( 'address' ) )
                    <div class="small-12 column">
                        <small class="error">{{ $errors->first('address') }}</small>
                    </div>
                @endif        
                <div class="row">
                    <div class="m-t-20 row col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="button success btn btn-primary btn-block" type="submit" value="{{ trans('messages.create_account_btn') }}">
                            </div>
                            <div class="col-sm-6">
                                <a href="{{ route('user.list',['sUserType' => 'customer' ]) }}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('custom-css')
<style type="text/css">
	.error{
			color:red !important;
	}
	.success-msg {
		background: #67BB67;
		color: #fff;
		padding: 5px;
	}
	.success-msg a {
		color: #fff;
		text-decoration: underline;
	}
	.error_message{
		color:red !important;
	}
	.with_error{
		border-color: red !important;
	}
	.success_message{
		color:green !important;
		text-align: center;
	}
	div .with_error{
		border:1px solid black;
	}
</style>
@stop

@section('custom-js')
<script type="text/javascript">
    $( function() {
        $( '.generate-password' ).click( function( e ) {
            e.preventDefault();
            var generatedHash = Math.random().toString(36).slice(-16).toUpperCase();
            $( 'input[name="password"]' ).val( generatedHash );
        });
    });
        
    $('.check-username-btn').click(function(e){
        e.preventDefault();
        var username = $('input#username').val();

        $.ajax({
                method: "POST",
                url: siteUrl("user/username-exists"),
                data: { username:username },
                success: function(response){
                        console.log(response);
                        if(response == 1){
                                $('.username-exists').slideDown(200);
                        }else{
                                $('.username-doesnt-exist').slideDown(200);
                        }
                }
        });
    });
    $(function() {
        $("#addForm").validate({
            rules: {
                first_name      : "required",
                last_name       : "required",
                username        : "required",
                currency_id  : "required",
                address         : "required",
            },
            errorPlacement: function(error, element) {
                var placement = $(element).parent();
                if (placement) {
                  $(error).insertAfter(placement)
                } else {
                  error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
     });
</script>
@stop