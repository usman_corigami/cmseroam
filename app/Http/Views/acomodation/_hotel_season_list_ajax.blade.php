<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCountrySort(this,'hs.name');" width="20%">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'hs.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>{{ trans('messages.eroam_code') }} </th>
            <th onclick="getCountrySort(this,'s.name');">{{ trans('messages.supplier') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 's.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'hs.from');">{{ trans('messages.start') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'hs.from')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'hs.to');">{{ trans('messages.to') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'hs.to')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th class="text-center" width="20%">{{ trans('messages.thead_action')}}</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
    @if(count($oHotelList) > 0)
        <?php //print_r($oHotelList);exit;?>
        @include('WebView::acomodation._more_hotel_season_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oHotelList->count() , 'total'=>$oHotelList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oHotelList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oHotelList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('acomodation/hotel-season-list?page='+pageNumber),event,'table_record');
//                if(pageNumber > 1)
//                    getMoreListing(siteUrl('acomodation/hotel-season-list?page='+pageNumber),event,'city_list_ajax');
//                else
//                    getMoreListing(siteUrl('acomodation/hotel-season-list?page='+pageNumber),event,'table_record');
                $('#checkbox-00').prop('checked',false);
            }
        });
    });
</script>