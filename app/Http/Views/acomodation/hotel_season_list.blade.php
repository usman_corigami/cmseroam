@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.manage_list_title', ['name' => 'Accommodation Season']) }}</h1>

    <div class="row">
        <div class="small-12 small-centered columns delete-box hidden"></div> 
    </div>
    @if(Session::has('message'))
    <div class="row">
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div> 
    </div>
    <br>
    @endif
    <div class="box-wrapper">
        <a href="{{ route('acomodation.hotel-season-create') }}" class="plus-icon" title="Add">
            <i class="icon-plus"></i>
        </a>
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                    <input type="text" class="form-control m-t-10" placeholder="Search {{ trans('messages.hotel_season') }}" name="search_str" value="{{ $sSearchStr }}">
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                <select name="search-by" class="form-control m-t-10 search_by">
                    <option value="hs.name">Season Name</option>						 			
                    <option value="s.name">Supplier Name</option>						 			
                    <option value="co.name">Country Name</option>
                </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <button class="btn btn-default btn-sm" type="submit" onclick="getMoreListing(siteUrl('acomodation/hotel-season-list'),event,'table_record');"><i class="icon-search-domain"></i></button>
            </div>
        </div>
        <div class="table-responsive m-t-20 table_record">
           
            @include('WebView::acomodation._hotel_season_list_ajax')
      
        </div>
    </div>

</div>
@stop
@section('custom-js')
<script type="text/javascript">

function getCountrySort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    getMoreListing(siteUrl('acomodation/hotel-season-list'),event,'table_record');
}
$(document).on('click',".cmp_check",function () { 

    if($('.cmp_check:checked').length == $('.cmp_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
$(document).on('click','.label_check',function(){
    setupLabel();
});  
</script>
@stop