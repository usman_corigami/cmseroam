@foreach ($oHotelList as $aHotel)

    <tr class="clickable" data-toggle="collapse" id="row-{{$aHotel->id}}" data-target=".row-{{$aHotel->id}}">
        <td><i class="icon-unfold-less"></i></td>
        <td>
            <a href="javascript:void(0);">
                {{ $aHotel->name }}
            </a>
        </td>
        <td></td>  
        <td>-</td>
        <td>-</td>
        <td class="text-center">-</td>
        <td class="text-center">-</td>
        <td class="text-center">-</td>
        <td class="text-center">-</td>
        <td class="text-center">-</td>
    </tr> 
    @foreach ($aHotel->prices as $price)
        <?php $season = $price->season; ?>
        <tr class="collapse row-{{ $aHotel->id }}">
            <td>
                <label class="radio-checkbox label_check" for="checkbox-{{$price->id}}">
                    <input type="checkbox" id="checkbox-{{$price->id}}" value="{{$price->id}}">&nbsp;
                </label>
            </td>
            <td>
                <?php //echo "<pre>";print_r($price);exit;?>
                {{ ($price->room_type) ? $price->room_type->name : '' }}
                @if($price->with_breakfast)
                <br/><small>Breakfast included</small>
                @endif
            </td>
            <td>-</td>  
            <td>{{ ($season && $season->supplier) ? $season->supplier->name : '' }}</td>
            <td>
                {{ ($season) ? $season->name : ''}} <br/> ( {{date('d/m/y',strtotime(($season) ? $season->from : ''))}} - {{date('d/m/y',strtotime(($season) ? $season->to : ''))}} )
            </td>
            <td class="text-center">
                {{ ($season && $season->currency) ? $season->currency->code : ''}}
                {{number_format(round($price->price, 2), 2, '.', ',')}}
            </td>
            <td class="text-center">-</td>
            <td class="text-center">-</td>
            <td class="text-center">{{$price->allotment}}</td>
            <td>
                <a href="{{ route('acomodation.hotel-price-create',['nIdprice' => $price->id ])}}" class="button success tiny btn-primary btn-sm pull-left m-r-10">{{ trans('messages.update_btn') }}</a>
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('acomodation.hotel-price-delete',['nIdLabel'=> $price->id]) }}','{{ trans('messages.delete_label')}}')">
            </td>
        </tr>
    @endforeach
@endforeach