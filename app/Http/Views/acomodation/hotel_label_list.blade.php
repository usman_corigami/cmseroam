@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.manage_list_title', ['name' => 'Hotel Label']) }}</h1>

    <div class="row">
        <div class="small-12 small-centered columns delete-box hidden"></div> 
    </div>
    @if(Session::has('message'))
    <div class="row">
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div> 
    </div>
    <br>
    @endif
    <div class="box-wrapper">
        <a href="{{ route('acomodation.hotel-label-create') }}" class="plus-icon" title="Add">
            <i class="icon-plus"></i>
        </a>
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" placeholder="Search {{ trans('messages.hotel') }}" name="search_str" value="{{ $sSearchStr }}">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit" onclick="getMoreListing(siteUrl('acomodation/hotel-label-list'),event,'table_record');"><i class="icon-search-domain"></i></button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
        </div>
        <div class="table-responsive m-t-20 table_record">      
            @include('WebView::acomodation._hotel_label_list_ajax')
        </div>
    </div>
</div>
@stop
@section('custom-js')
<script type="text/javascript">

function getSortData(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    getMoreListing(siteUrl('acomodation/hotel-label-list'),event,'table_record');
}
$(document).on('click',".cmp_check",function () { 

    if($('.cmp_check:checked').length == $('.cmp_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
$(document).on('click','.label_check',function(){
    setupLabel();
});  
</script>
@stop