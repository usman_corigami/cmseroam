@foreach ($oLabelList as $aLabel)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-{{ $aLabel->id }}">
                <input type="checkbox" class="cmp_check" id="checkbox-{{ $aLabel->id }}" value="{{ $aLabel->id }}">&nbsp;
            </label>
        </td>
        <td>
            <a href="{{ route('common.create-label',[ 'nIdLabel' => $aLabel->id ])}}">{{ $aLabel->name }}</a>
        </td>	
        <td>{{ $aLabel->created_at }}</td>	
        <td>{{ $aLabel->updated_at }}</td>	
        <td class="text-center">
            <a href="{{ route('common.create-label',[ 'nIdLabel' => $aLabel->id ])}}" class="button success tiny btn-primary btn-sm m-r-10">{{ trans('messages.update_btn') }}</a>
            <input type="button" class="button btn-delete tiny btn-primary btn-sm m-r-10" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('common.delete-label',['nIdLabel'=> $aLabel->id]) }}','{{ trans('messages.delete_label')}}')">
        </td>	
    </tr> 
@endforeach

