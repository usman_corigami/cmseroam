@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if($nIdTourLogo != '')
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Tour Type Logo']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Tour Type Logo']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>
    <br>
    @if($nIdTourLogo == '')
        {{Form::open(array('url' => 'tour/tour-logo-create','id'=>'logo_form','method'=>'Post','enctype'=>'multipart/form-data')) }}
    @else
        {{ Form::model($oTourTypeLogo, array('url' => 'tour/tour-logo-create','method'=>'post','enctype'=>'multipart/form-data','id'=>'logo_form')) }}
    @endif
    <div class="box-wrapper">

        <p>Tour Type Logo Details</p>


        <div class="form-group m-t-30">
            <label class="label-control">Title <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            {{Form::text('title',Input::old('title'),['id'=>'title','class'=>$attributes,'placeholder'=>'Enter Title'])}}

            @if ( $errors->first( 'title' ) )
                <small for="name" generated="true" class="error">{{ $errors->first('title') }}</small>
            @endif
        </div>

        <div class="form-group m-t-30">
            <label class="label-control">Upload logo <span class="required" style="position: unset; font-size:12px;">  (Dimension 100 X 100, max file size. Supported file types, .jpeg, .jpg, .png). *</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            <input type="file" name="logo" id="logo_id" class="form-control" onchange="readURL(this);"> 
            <input type="hidden" value="{{ $nIdTourLogo }}" id="id_logo" name="id_logo" />
            @if ( $errors->first( 'logo' ) )
                <small for="name" generated="true" class="error">{{ $errors->first('logo') }}</small>
            @endif
        </div>
        <div class="form-group m-t-30 img_logo_cls" >
            <img id="img_logo" src="{{ trans('messages.image_url',['image_title' => $oTourTypeLogo->logo_path]) }}" class="img-responsive" height="60" width="60">
        </div>	
    </div>


    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{URL::to('tour/tour-type-logo-list')}}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
            </div>
        </div>
    </div>	
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>

    .error{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .error_message{
        color:red !important;
    }

    .success_message{
        color:green !important;
        text-align: center;
    }
</style>
@stop

@section('custom-js')
<script>


    $("#logo_form").validate({
        rules: {
            title: {
                required: true
            },
            logo: {
                required: function(element){
                            return $("#id_logo").val() < 0;
                        }
                extension: "jpg|png|jpeg"
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            form.submit();

        }

    });
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function (e) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = function () {
                    if (this.width < 60 || this.height < 60) {

                        $('.img_logo_cls').hide();
                        $('#logo_id').attr('src', '');
                        $('#logo_id').val('');
                        alert('Please select appropriate size.');
                    } else {
                        $('#img_logo')
                                .attr('src', e.target.result)
                                .width(60)
                                .height(60);
                        $('.img_logo_cls').show();
                    }
                };


            };
            reader.readAsDataURL(input.files[0]);

        }
    }
</script>
@stop
