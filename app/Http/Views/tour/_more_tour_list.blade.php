@foreach ($oTourList as $aTour)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-{{$aTour->tour_id}}">
                <input type="checkbox" class="cmp_tour_check" id="checkbox-{{$aTour->tour_id}}" value="{{$aTour->tour_id}}">&nbsp;
            </label>
        </td>
        <td> {{ $aTour->tour_code }}</td>
        <td id="title_{{$aTour->tour_id}}"> {{ $aTour->tour_title }}</td>
        <td> {{ number_format($aTour->price,2) }}</td>
        <td> {{ $aTour->provider_name }}</td>
        <td></td>
        <td>{!! ($aTour->is_active == 1) ? '<span>Active</span>' : '<span>InActive</span>' !!}</td>
        <td>{!! ($aTour->is_reviewed == 1) ? '<span>Reviewed</span>' : '<span>Un-Reviewed</span>' !!}</td>
        <td>
            <a href="{{ route('tour.tour-create', [ 'nTourId'=>$aTour->tour_id])}}" class="button success tiny btn-primary btn-sm pull-left m-r-10">{{ trans('messages.update_btn') }}</a>
            <input type="hidden" id="view_{{ $aTour->tour_id }}" value="{{ $aTour->views }}" >
        </td>
    </tr> 
@endforeach