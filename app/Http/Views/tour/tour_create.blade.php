@extends( 'layout/mainlayout' )

@section('content')
<?php 
    $noAddonOption  = array(1=>1,2,3,4,5,6);
    $random_tour_id =  (isset($nIdTour) && $nIdTour !='') ? $nIdTour : 'rand_'.rand(10000, 99999);
?>

<div class="content-container content-container2">
    @if(isset($oTour))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Tour Step 1']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Tour Step 1']) }}</h1>
    @endif
    <div class="box-wrapper">
        <div class="m-t-20">
            <div class="small-6 small-centered columns error_message_image error"></div>
            <form enctype="multipart/form-data" id="image-form">
                <div class="file-upload1">
                    <input type="hidden" id="tourId" name="tourId" value="<?php echo $random_tour_id; ?>"/>
                    <input type="file" class="file-input" id="tourImage" name="tourImage"/>
                </div>
                <span class="input-filename"></span>
                <span id="image-submit-load" style="display:none;">
                    <i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw image-file-loader"></i>
                </span>
                {{ csrf_field() }}
            </form>
        </div>

        <div class="m-t-20">
            <div class="table-responsive" id="panel-drag">
                <table class="table table-center-all">
                    <thead>
                        <tr>
                            <th>{{ trans('messages.re_order')}}</th>
                            <th>{{ trans('messages.preview')}}</th>
                            <th class="text-left">{{ trans('messages.image_name')}} <b class="caret"></b></th>
                            <th>{{ trans('messages.download')}}</th>
                            <th>{{ trans('messages.remove')}}</th>
                        </tr>
                    </thead>
                    <tbody class="sortable-list">
                        @if(isset($oTourImages) && count($oTourImages) > 0) 
                            <!-- loop through each image for this hotel -->
                            @foreach ($oTourImages as $key => $image)
                            <?php
                                //echo "<pre>";print_r($oTour->providers);exit;
                                $image_id =  explode("_",$image->image_id);
                                $img_cms = trans('messages.image_url',['image_title' => 'tours/'.$image->title]);
                                $img_at = 'http://www.adventuretravel.com.au'.$oTour->providers->folder_path.'OriginalImage/'.$image->title;
                                if (file_exists($img_cms)) 
                                    $img = $img_cms;
                                else
                                    $img = $img_at;
                            ?>
                                <tr data-id="{{ $image->image_id }}" id="image_{{ $image->image_id }}">
                                    <td>
                                        <a href="#" class="sortable-icon"><i class="icon-reorder-black"></i></a></td>
                                    <td class="table-no-padding">
                                        <div class="table-image">
                                            <img src="{{ $img }}" class="img-responsive">
                                        </div>
                                    </td>
                                    <?php  $img_name = explode("/",$image->image_thumb); ?>
                                    <td class="text-left blue-text">{{ $image->image_name }}</td>
                                    <td>
                                        <?php if($image->title): ?>
                                        <a href="{{ trans('messages.image_url',['image_title' => 'tours/'.$image->title]) }}" class="action-icon" download="{{ $image->image_name }} "><i class="icon-download"></i></a>
                                        <?php endif; ?>
                                    </td>
                                    <td><a href="#" class="action-icon image-click-event image-delete" data-action="delete" data-id="{{ $image->image_id }}" id="{{ $image->image_id }}" data-type="tour"><i class="icon-delete-forever"></i></a></td>
                                </tr>
                                
                            @endforeach 
                        <!-- show the default blank image whenever there is no image uploaded for image for the hotel -->
                        @else
                            <tr>
                                <td colspan="7">{{ trans('messages.no_tour_image')}}<td>
                            <tr>
                        @endif     
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="content-container">
    @if(isset($oTour))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Tour Step 2']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Tour Step 2']) }}</h1>
    @endif

    @if (Session::has('message'))
    <div class="small-12 small-centered columns success_message">{{ Session::get('message') }}</div>
    <br>
    @endif

    @if ($errors->any())
    <div class="small-6 small-centered columns error_message">{{$errors->first()}}</div>
    @endif
    <?php $attributes = 'form-control' ?>
    @if(isset($oTour))
    {{ Form::model($oTour, array('url' => route('tour.tour-create') ,'method'=>'POST','enctype'=>'multipart/form-data','id'=>'addTourForm')) }}
    @else
    {{Form::open(array('url' => route('tour.tour-create'),'method'=>'Post','enctype'=>'multipart/form-data','id'=>'addTourForm','accept-charset' =>'UTF-8' )) }}
    @endif
        {{ csrf_field() }}
        <div class="box-wrapper">

            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_title')}} <span class="required">*</span></label>
                {{Form::text('tour_title',Input::old('tour_title'),['placeholder'=>'Tour Title','id'=>'tour_title','class'=>$attributes])}}
                <!--<input placeholder="Tour Title" id="tour_title" class="form-control" name="tour_title" type="text">-->
            </div>
            <input type="hidden" value="{{ $nIdTour }}" name="tour_id" />
            {{ Input::old('dn_countries') }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_departure_country')}} <span class="required">*</span></label>
                        {{Form::select('de_countries',$oCountries,Input::old('de_countries'),['id'=>'dn_countries','class'=>$attributes.' select-country','data-type'=>'departure'])}}

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_destination_country')}} <span class="required">*</span></label>
                        {{Form::select('dn_countries',$oCountries,Input::old('dn_countries'),['id'=>'de_countries','class'=>$attributes.' select-country','data-type'=>'destination'])}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_departure_city')}} <span class="required">*</span></label>
                        <select id="departure" class="form-control select-city" data-type="departure" name="departure">
                            <option value="">{{ trans('messages.tour_departure_city')}}</option>
                            <?php 
                                if(isset($oDeCities)){
                                    foreach ($oDeCities as $key => $value) {
                                        $select = '';
                                        if($oTour->from_city_id == $key ) { $select = 'selected="selected"'; } 
                                        echo '<option value="'.$key.'" '.$select.'>'.$value.'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_destination_city')}} <span class="required">*</span></label>
                        <select id="destination" class="form-control select-city" data-type="destination" name="destination">
                            <option value="">{{ trans('messages.tour_destination_city')}}</option>
                            <?php 
                                if(isset($oDnCities)){
                                    foreach ($oDnCities as $key => $value) {
                                        $select = '';
                                        if($oTour->to_city_id == $key ) { $select = 'selected="selected"'; } 
                                        echo '<option value="'.$key.'" '.$select.'>'.$value.'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_duration')}} <span class="required">*</span></label>
                        <div class="row">
                            <div class="col-sm-6">
                                {{Form::text('no_of_days',Input::old('no_of_days'),['placeholder'=>'Duration','id'=>'no_of_days','class'=>$attributes, 'style'=>'border: solid 1px #f3f3f3; padding: 0 6px;'])}}
                            </div>
                            <div class="col-sm-6">
                                <?php $durrationType = ['d' => 'Days', 'h' => 'Hours']; ?>
                                {{Form::select('durationType',$durrationType,Input::old('durationType'),['id'=>'durationType','class'=>$attributes, 'style'=>'border: solid 1px #f3f3f3; padding: 0 6px;'])}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_transport')}} <span class="required">*</span></label>
                        {{Form::text('transport',Input::old('transport'),['placeholder'=>'Transport','id'=>'transport','class'=>$attributes])}}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_accommodation')}} <span class="required">*</span></label>
                {{Form::text('accommodation',Input::old('accommodation'),['placeholder'=>'Accommodation','id'=>'accommodation','class'=>$attributes])}}
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_group_min_size')}}</label>

                        {{Form::text('groupsize_min',Input::old('groupsize_min'),['placeholder'=>'Group Min Size','id'=>'groupsize_min','class'=>$attributes])}}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_group_max_size')}}</label>

                        {{Form::text('groupsize_max',Input::old('groupsize_max'),['placeholder'=>'Group Max Size','id'=>'groupsize_max','class'=>$attributes])}}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_retail_cost')}} <span class="required">*</span></label>
                {{Form::number('retailcost',Input::old('retailcost'),['placeholder'=>'Retail Cost','id'=>'retailcost','class'=>$attributes,'min'=>0])}}
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_discount')}}</label>

                        {{Form::number('discount',Input::old('discount'),['placeholder'=>'Discount','id'=>'discount','class'=>$attributes,'min'=>0])}}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_saving_per_person')}}</label>

                        {{Form::number('saving_per_person',Input::old('saving_per_person'),['placeholder'=>'Saving Per Person','id'=>'saving_per_person','class'=>$attributes,'step'=>'0.1','min'=>'0.0'])}}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_supplier')}} <span class="required">*</span></label>
                {{Form::select('provider',$oProvider, Input::old('provider'),['id'=>'provider','class'=>$attributes])}}
            </div>
            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_tour_type_logo')}}<span class="required">*</span></label>
                {{Form::select('tour_type_logo_id',$TourTypeLogo, Input::old('tour_type_logo_id'),['id'=>'tour_type_logo_id','class'=>$attributes])}}
            </div>

            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_child_allowed')}}</label><br />

                <label class="radio-checkbox label_radio" for="checkbox-childYes">
                    <input type="radio" class="is_childAllowed" name="is_childAllowed" id="checkbox-childYes" value="1" checked> Yes
                </label>

                <label class="radio-checkbox label_radio" for="checkbox-childNo">
                    <input type="radio" class="is_childAllowed" name="is_childAllowed" id="checkbox-childNo" value="0"> No
                </label>
            </div>
            <div class="form-group child_age_div">
                <label class="label-control">{{ trans('messages.tour_children_age')}}</label><br />
                {{Form::text('children_age',Input::old('children_age'),['placeholder'=>'Children Age','id'=>'children_age','class'=>$attributes])}}
            </div>

            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_mapimage')}}</label>
                {{ Form::file('mapfile',['class'=>$attributes])}}
            </div>

            <div class="form-group min-height-50">
                <label class="label-control">{{ trans('messages.tour_provider_pickups')}}</label>
                <div id="ProviderPickups">
                    <?php 
                        if(isset($oProviderPickups)){
                            $pickupHtml = '<div class="row"><div class="col-sm-1"><label class="radio-checkbox label_check" for="checkbox-suppliers"><input type="checkbox" class="checkbox-suppliers" name="checkbox-suppliers" id="checkbox-suppliers" value="1">&nbsp;</label></div><div class="col-sm-3">Pickup Time</div><div class="col-sm-8">Description</div></div><hr>';
                            foreach ($oProviderPickups as $pickup) {
                                $checked = '';
                                if(in_array($pickup->pickup_id, $oTourPickups)){ $checked = 'checked="checked"';}
                                $pickupHtml .= '<div class="row"><div class="col-sm-1"><label class="radio-checkbox label_check" for="checkbox-pickup-'.$pickup->pickup_id.'"><input class="cmp_supplier_check" type="checkbox" name="pickup[]" id="checkbox-pickup-'.$pickup->pickup_id.'" '.$checked.' value="'.$pickup->pickup_id.'">&nbsp;</label></div><div class="col-sm-3">'.$pickup->pickup_time.'</div><div class="col-sm-8">'.$pickup->description.'</div></div>';
                            }
                            echo $pickupHtml;
                        }
                        
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_pickup_locations')}}</label>
                <div class="row">
                    <div class="col-sm-3">
                        {{Form::text('pickupDateTime',Input::old('pickupDateTime'),['placeholder'=>'Pickup DateTime','id'=>'pickupDateTime','class'=>$attributes.' pickup-control'])}}
                        <label id="pickupDateTimeError" for="pickupDateTime" generated="true" class="error" style="display:none;">This field is required.</label>
                    </div>
                    <div class="col-sm-8">
                        {{Form::text('pickupDes',Input::old('pickupDes'),['placeholder'=>'Pickup Location','id'=>'pickupDes','class'=>$attributes.' pickup-control'])}}
                        <label id="pickupDesError" for="pickupDes" generated="true" class="error" style="display:none;">This field is required.</label>
                    </div>
                    <div class="col-sm-1">
                        <input class="btn btn-primary btn-block" type="button" name="btnAddPickup" value="Add" id="btnAddPickup">
                    </div>
                </div>

                <div id="PickupLocationsDiv"></div>
                <input type="hidden" id="PickupLocationsCount" value="0"> 
            </div>

            <div class="form-group min-height-50">
                <label class="label-control">{{ trans('messages.tour_provider_special_notes')}}</label>
                <div id="ProviderSpecialNotes">
                    <?php 
                        if(isset($oProviderNotes)){
                            $noteHtml   = '<div class="row"><div class="col-sm-1"><label class="radio-checkbox label_check" for="checkbox-notes"><input type="checkbox" class="checkbox-notes" name="checkbox-notes" id="checkbox-notes" value="1">&nbsp;</label></div><div class="col-sm-11">Description</div></div><hr>';
                            foreach ($oProviderNotes as $note) {
                                $checked = '';
                                if(in_array($note->special_note_id, $oTourNotes)){ $checked = 'checked="checked"';}
                                $noteHtml .= '<div class="row"><div class="col-sm-1"><label class="radio-checkbox label_check" for="checkbox-note-'.$note->special_note_id.'"><input type="checkbox" class="cmp_note_check" name="note[]" id="checkbox-note-'.$note->special_note_id.'" '.$checked.' value="'.$note->special_note_id.'">&nbsp;</label></div><div class="col-sm-8">'.$note->special_desc.'</div></div>';
                            }
                            echo $noteHtml;
                        }
                    ?>
                </div>
            </div>


            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_special_notes')}}</label>
                <div class="row">
                    <div class="col-sm-11">
                        {{ Form::textarea('specialNote',Input::old('specialNote'), ['placeholder' => 'Special Notes', 'rows' => '5','id'=>'specialNote','class'=>$attributes]) }}
                        <label id="specialNoteError" for="specialNote" generated="true" class="error" style="display:none;">This field is required.</label>
                    </div>
                    <div class="col-sm-1">
                        <input class="btn btn-primary btn-block" type="button" name="btnAddSpecialNotes" value="Add" id="btnAddSpecialNotes">
                    </div>
                </div>
                <div id="SpecialNotesDiv"></div>
                <input type="hidden" id="SpecialNotesCount" value="0"> 
            </div>

            <div class="form-group min-height-50">
                <label class="label-control">{{ trans('messages.tour_provider_standard_remarks')}}</label>
                <div id="ProviderStandardRemarks">
                    <?php 
                        if(isset($oProviderRemarks)){
                            $remarkHtml = '<div class="row"><div class="col-sm-1"><label class="radio-checkbox label_check" for="checkbox-remarks"><input type="checkbox" class="checkbox-remarks" name="checkbox-remarks" id="checkbox-remarks" value="1">&nbsp;</label></div><div class="col-sm-11">Description</div></div><hr>';
                            foreach ($oProviderRemarks as $remark) {
                                $checked = '';
                                if(in_array($remark->standard_remarks_id, $oTourRemarks)){ $checked = 'checked="checked"';}
                                $remarkHtml .= '<div class="row"><div class="col-sm-1"><label class="radio-checkbox label_check" for="checkbox-remark-'.$remark->standard_remarks_id.'"><input type="checkbox" class="cmp_remark_check" name="remark[]" id="checkbox-remark-'.$remark->standard_remarks_id.'" '.$checked.' value="'.$remark->standard_remarks_id.'">&nbsp;</label></div><div class="col-sm-8">'.$remark->standard_desc.'</div></div>';
                            }
                            echo $remarkHtml;
                        } 
                        
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_standard_remarks')}}</label>
                <div class="row">
                    <div class="col-sm-11">
                        {{ Form::textarea('standardRemarks',Input::old('standardRemarks'), ['placeholder' => 'Standard Remarks', 'rows' => '5','id'=>'standardRemarks','class'=>$attributes]) }}
                        <label id="standardRemarksError" for="standardRemarks" generated="true" class="error" style="display:none;">This field is required.</label>
                    </div>
                    <div class="col-sm-1">
                        <input class="btn btn-primary btn-block" type="button" name="btnAddStandardRemarks" value="Add" id="btnAddStandardRemarks">
                    </div>
                </div>

                <div id="StandardRemarksDiv"></div>
                <input type="hidden" id="StandardRemarksCount" value="0"> 
            </div>
            <p>{{ trans('messages.tour_overview')}}</p>
            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_overview')}} <span class="required">*</span></label>
                {{ Form::textarea('long_description',Input::old('long_description'), ['rows' => '5','id'=>'long_description','class'=>$attributes]) }}	
            </div>

            <p>{{ trans('messages.tour_itinerary')}}</p>
            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_itinerary')}} <span class="required">*</span></label>
                {{ Form::textarea('xml_itinerary',Input::old('xml_itinerary'), ['placeholder' => 'Itinerary', 'rows' => '5','id'=>'xml_itinerary','class'=>$attributes]) }}	

            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_region')}}</label>
                        {{Form::select('tripRegion',$oRegions, Input::old('tripRegion'),['id'=>'tripRegion','class'=>$attributes.' select-region'])}}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_categories')}} <span class="required">*</span></label>
                        {{Form::select('tripActivities[]',$oCategories,$oTourCategoty,['id'=>'tripActivities','class'=>$attributes])}}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">{{ trans('messages.tour_trip_countries')}} <span class="required">*</span></label>
                        {{Form::select('tripCountries[]',$oCountries,$oTripCounry,['id'=>'tripCountries','class'=>$attributes.' select-countries','data-type'=>'tripCountries','multiple'=>'multiple'])}}

                    </div>
                </div>
            </div>

            <p>{{ trans('messages.tour_meta_desc_section')}}</p>
            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_meta_desc')}}<span class="required">*</span></label>
                {{ Form::textarea('short_description',Input::old('short_description'), ['placeholder' => 'Introduction Summary & Meta Description', 'rows' => '5','id'=>'short_description','class'=>$attributes,'maxlength'=>'160']) }}
            </div>

            <p>{{ trans('messages.tour_additional_info')}}</p>
            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_additional_info')}}</label>
                {{ Form::textarea('xml_PracticalDetail',Input::old('xml_PracticalDetail'), ['placeholder' => 'Additional Info', 'rows' => '5','id'=>'xml_PracticalDetail','class'=>$attributes]) }}    
            </div>

            <p>{{ trans('messages.tour_remarks')}}</p>
            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_remarks')}}</label>
                {{ Form::textarea('tour_remarks',Input::old('tour_remarks'), ['placeholder' => 'Remarks', 'rows' => '5','id'=>'tour_remarks','class'=>$attributes]) }}
            </div>

            <p>{{ trans('messages.tour_meta_title')}}</p>
            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_meta_title')}}</label>
                {{ Form::textarea('meta_title',Input::old('meta_title'), ['placeholder' => 'Meta Title', 'rows' => '5','id'=>'meta_title','class'=>$attributes]) }}
            </div>

            <p>{{ trans('messages.tour_meta_keyword')}}</p>
            <div class="form-group">
                <label class="label-control">{{ trans('messages.tour_meta_keyword')}}</label>
                {{ Form::textarea('meta_keywords',Input::old('meta_keywords'), ['placeholder' => 'Meta Keyword', 'rows' => '5','id'=>'meta_keywords','class'=>$attributes]) }}
            </div>    
        </div>
        <div class="m-t-20 row">
            <div class="col-sm-6">
                <input type="hidden" name="random_tour_id" value="<?php echo $random_tour_id; ?>"/>
                @if(isset($oTour))
                <button type="submit" class="btn btn-primary btn-block" id="btnAddPayment">{{ trans('messages.tour_update_btn')}}</button>
                @else
                <button type="submit" class="btn btn-primary btn-block" id="btnAddPayment">{{ trans('messages.tour_add_btn')}}</button>
                @endif
            </div>
            <div class="col-sm-6">
                <a href="{{ URL::previous() }}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn')}}</a>
            </div>
        </div>
    </form>
</div>
@stop
<link href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet" />
@section('custom-js')
<script type="text/javascript" src="{{ asset('assets/js/moment.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}" ></script>

<script>
    $(document).ready(function() {

        var cmp_supplier_multiple = [] ;
         $(document).on('click','#checkbox-suppliers',function(){
            var cmp_supplier = [] ;
            if(this.checked) {
                
                // Iterate each checkbox
                $('.cmp_supplier_check').each(function() {
                    $(this).attr('checked', true);
                    $('label[for="' + $(this).attr('id') + '"]').addClass('c_on');
                    //$(this).closest('.label_check').addClass('c_on');
                    cmp_supplier.push($(this).val());
                    cmp_supplier_multiple.push($(this).val());
                });
            }else{
                $('.cmp_supplier_check').each(function() {
                     $(this).attr('checked', false);
                     $('label[for="' + $(this).attr('id') + '"]').removeClass('c_on');
                     //$(this).closest('.label_check').removeClass('c_on');
                });
                cmp_supplier_multiple = [] ;
            }
        });

        $(document).on('click','.cmp_supplier_check',function(){
            if(this.checked) {
                $(this).attr('checked', true);
                $(this).closest('.label_check').addClass('c_on');
                cmp_supplier_multiple.push($(this).val());
                $('.cmp_supplier_check').each(function() {
                    if(!this.checked) {
                       $('#checkbox-suppliers').prop('checked', false);
                       $('#checkbox-suppliers').closest('.label_check').removeClass('c_on');
                    }else{
                       $('#checkbox-suppliers').prop('checked', true);
                       $('#checkbox-suppliers').closest('.label_check').addClass('c_on');
                    }
                });
            }else{
                $(this).attr('checked', false);
                $(this).closest('.label_check').removeClass('c_on');
                $('#checkbox-suppliers').prop('checked', false);
                $('#checkbox-suppliers').closest('div .label_check').removeClass('c_on');
                cmp_supplier_multiple.pop();
            }
        });
    
        var cmp_remark_multiple = [] ;
         $(document).on('click','#checkbox-remarks',function(){
            var cmp_remark = [] ;
            if(this.checked) {
                
                // Iterate each checkbox
                $('.cmp_remark_check').each(function() {
                    $(this).attr('checked', true);
                    $('label[for="' + $(this).attr('id') + '"]').addClass('c_on');
                    //$(this).closest('.label_check').addClass('c_on');
                    cmp_remark.push($(this).val());
                    cmp_remark_multiple.push($(this).val());
                });
            }else{
                $('.cmp_remark_check').each(function() {
                     $(this).attr('checked', false);
                     $('label[for="' + $(this).attr('id') + '"]').removeClass('c_on');
                     //$(this).closest('.label_check').removeClass('c_on');
                });
                cmp_remark_multiple = [] ;
            }
        });

        $(document).on('click','.cmp_remark_check',function(){
            if(this.checked) {
                $(this).attr('checked', true);
                $(this).closest('.label_check').addClass('c_on');
                cmp_remark_multiple.push($(this).val());
                $('.cmp_remark_check').each(function() {
                    if(!this.checked) {
                       $('#checkbox-remarks').prop('checked', false);
                       $('#checkbox-remarks').closest('.label_check').removeClass('c_on');
                    }else{
                       $('#checkbox-remarks').prop('checked', true);
                       $('#checkbox-remarks').closest('.label_check').addClass('c_on');
                    }
                });
            }else{
                $(this).attr('checked', false);
                $(this).closest('.label_check').removeClass('c_on');
                $('#checkbox-remarks').prop('checked', false);
                $('#checkbox-remarks').closest('div .label_check').removeClass('c_on');
                cmp_remark_multiple.pop();
            }
        });
        
        
        
        var cmp_note_multiple = [] ;
         $(document).on('click','#checkbox-notes',function(){
            var cmp_note = [] ;
            if(this.checked) {
                
                // Iterate each checkbox
                $('.cmp_note_check').each(function() {
                    $(this).attr('checked', true);
                    $('label[for="' + $(this).attr('id') + '"]').addClass('c_on');
                    //$(this).closest('.label_check').addClass('c_on');
                    cmp_note.push($(this).val());
                    cmp_note_multiple.push($(this).val());
                });
            }else{
                $('.cmp_note_check').each(function() {
                     $(this).attr('checked', false);
                     $('label[for="' + $(this).attr('id') + '"]').removeClass('c_on');
                     //$(this).closest('.label_check').removeClass('c_on');
                });
                cmp_note_multiple = [] ;
            }
        });

        $(document).on('click','.cmp_note_check',function(){
            if(this.checked) {
                $(this).attr('checked', true);
                $(this).closest('.label_check').addClass('c_on');
                cmp_note_multiple.push($(this).val());
                $('.cmp_note_check').each(function() {
                    if(!this.checked) {
                       $('#checkbox-notes').prop('checked', false);
                       $('#checkbox-notes').closest('.label_check').removeClass('c_on');
                    }else{
                       $('#checkbox-notes').prop('checked', true);
                       $('#checkbox-notes').closest('.label_check').addClass('c_on');
                    }
                });
            }else{
                $(this).attr('checked', false);
                $(this).closest('.label_check').removeClass('c_on');
                $('#checkbox-notes').prop('checked', false);
                $('#checkbox-notes').closest('div .label_check').removeClass('c_on');
                cmp_note_multiple.pop();
            }
        });



        $('.is_childAllowed').click(function(){
            if($(this).val() == 1){
                $('.child_age_div').show();
            }else{
                $('.child_age_div').hide();
            }
        });
        $('#noAddon').hide();
       
        tinymce.init({
          selector:'#long_description',
          height: 250,
          theme: 'modern',
          plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools contextmenu colorpicker textpattern help',
          toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
          image_advtab: true,
          setup: function(editor) {
            editor.on('change', function(e) {
                var myContent = e.level.content;
                myContent = myContent.replace('&nbsp;',"");
                myContent = myContent.replace(/(<([^>]+)>)/ig,"");
                if(myContent.length > 160){
                    myContent = myContent.substring(0,159);
                } 

                $('#short_description').val('');
                $('#short_description').val(myContent.trim());
            });
          },
          templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
          ],
          content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
          ]
         });
        tinymce.init({
          selector:'#xml_itinerary,#xml_PracticalDetail',
          height: 250,
          theme: 'modern',
          plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools contextmenu colorpicker textpattern help',
          toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
          image_advtab: true,
          templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
          ],
          content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
          ]
         });
        
        $('#pickupDateTime').datetimepicker({});

    	$( '.select-country' ).change( function() {
    		var value = $( this ).val();
    		var	type  = $( this ).data( 'type' );

            if(type == 'destination'){
                var city  = 'Destination';
            } else {
                var city  = 'Departure';
            }
           
    		$.ajax({
                    url: "{{ route('common.get-cities-by-country') }}",
                    method: 'post',
                    data: {
                        country_id: value,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function( response ) 
                    {
                        var selectCity = $( '.select-city[data-type="'+ type +'"]' );
                        selectCity.html('<option value="" selected disabled>'+ city +' City</option>');
                        for(var i = 0; i < response.length; i++) {

                            var html = '<option value="'+ response[i].id +'">'+ response[i].name +'</option>';
                            $( '.select-city[data-type="'+ type +'"]' ).append(html);
                        }
                    }
    		});
    	});

    	$( '.select-region' ).change( function() {
    		var value = $( this ).val();
    		$.ajax({
    			url: "{{ route('common.get-countries-by-region') }}",
    			method: 'post',
    			data: {
    				region_id: value,
    				_token: '{{ csrf_token() }}'
    			},
    			success: function( response ) {
    				if ( response != null ) {
                                    var selectCountry = $( '.select-countries' );
                                    selectCountry.html('');
                                    for(var i = 0; i < response.length; i++) 
                                    {
                                        var html = '<option value="'+ response[i].id +'">'+ response[i].name +'</option>';
                                        selectCountry.append(html);
                                    }
    				}
    			}
    		});
    	});

        $( '#provider' ).change( function() {
            var value = $( this ).val();
            $.ajax({
                url: "{{ route('tour.provider-data') }}",
                method: 'post',
                data: {
                    provider_id: value,
                    _token: '{{ csrf_token() }}'
                },
                success: function( response ) {
                    if ( response != null ) {
                        $("#ProviderPickups").html(response[0]);
                        $("#ProviderSpecialNotes").html(response[1]);
                        $("#ProviderStandardRemarks").html(response[2]);
                        $('.label_check').click(function(){
                            setupLabel();
                        });
                    }
                }
            });
        });

        $("#btnAddPickup").click(function () {
            if($("#pickupDateTime").val() != '' && $("#pickupDes").val() != ''  ){
                $("#pickupDateTimeError").hide();
                $("#pickupDesError").hide();

                var count = $("#PickupLocationsCount").val()+1;
                var dateTime = $("#pickupDateTime").val();
                var desc = $("#pickupDes").val();
                var sHtml = '<div class="row" id="pickupDiv_'+count+'">'
                                +'<div class="col-sm-3">'+dateTime+'</div>'
                                +'<div class="col-sm-8">'+desc
                                    +'<input type="hidden" name="pickuptime[]" value="'+dateTime+'">'
                                    +'<input type="hidden" name="pickupDesc[]" value="'+desc+'">'
                                +'</div>'
                                +'<div class="col-sm-1">'
                                    +'<a href="javascript://" class="deletePickup action-icon" data-id="pickupDiv_'+count+'">'
                                        +'<i class="icon-delete-forever"></i>'
                                    +'</a>'
                                +'</div>'
                            +'</div>';
                $("#PickupLocationsCount").val(count);
                $("#pickupDateTime").val('');
                $("#pickupDes").val('');
                if(count == 1) { $("#PickupLocationsDiv").append('<hr>');}
                $("#PickupLocationsDiv").append(sHtml);
            } else if($("#pickupDateTime").val() == '') {
                $("#pickupDateTimeError").show();
            } else if($("#pickupDes").val() == '') {
                $("#pickupDesError").show();
            } else if($("#pickupDateTime").val() == '' && $("#pickupDes").val() == '') {
                $("#pickupDateTimeError").show();
                $("#pickupDesError").show();
            } else {
            }
        });
        
        $(document).on('click','.deletePickup,.deleteNote,.deleteRemark',function () {
            var dataId = $(this).attr('data-id');
            $('#'+dataId).remove();
        });
                            
        $("#btnAddSpecialNotes").click(function () {
            if($("#specialNote").val()!= ''){
                $("#specialNoteError").hide();
                var count = $("#SpecialNotesCount").val()+1;
                var desc = $("#specialNote").val();
                var Html = '<div class="row" id="noteDiv_'+count+'">'
                                +'<div class="col-sm-11">'+desc
                                    +' <input type="hidden" name="noteDesc[]" value="'+desc+'">'
                                +'</div>'
                                +'<div class="col-sm-1">'
                                    +'<a href="javascript://" class="deleteNote action-icon" data-id="noteDiv_'+count+'">'
                                        +'<i class="icon-delete-forever"></i>'
                                    +'</a>'
                                +'</div>'
                            +'</div>';
                $("#specialNote").val('');
                $("#SpecialNotesCount").val(count);
                if(count == 1) { $("#SpecialNotesDiv").append('<hr>');}
                $("#SpecialNotesDiv").append(Html);
            } else {
                $("#specialNoteError").show();
            }
        });
        
        $("#btnAddStandardRemarks").click(function () {
            if($("#standardRemarks").val()!= ''){
                $("#standardRemarksError").hide();
                var count = $("#StandardRemarksCount").val() + parseInt(1);
                var desc = $("#standardRemarks").val()
                var html 	= '<div class="row" id="remarkDiv_'+count+'">'
                            +'<div class="col-sm-11">'+desc
                                +' <input type="hidden" name="remarkDesc[]" value="'+desc+'">'
                            +'</div>'
                            +'<div class="col-sm-1">'
                                +'<a href="javascript://" class="deleteRemark action-icon" data-id="remarkDiv_'+count+'">'
                                    +'<i class="icon-delete-forever"></i>'
                                +'</a>'
                            +'</div>'
                        +'</div>';
                $("#standardRemarks").val('');
                $("#StandardRemarksCount").val(count);
                if(count == 1) { $("#StandardRemarksDiv").append('<hr>');}
                $("#StandardRemarksDiv").append(html);
            } else {
                $("#standardRemarksError").show();
            }
        });    
    })

    $(function() {

        $("#addTourForm").validate({
            rules: {
                tour_title          : "required",
                de_countries        : "required",
                dn_countries        : "required",
                departure           : "required",
                destination         : "required",
                no_of_days          : "required",
                tour_type_logo_id   : "required",
                transport           : "required",
                accommodation       : "required",
                provider            : "required",
                long_description    : "required",
                xml_itinerary       : "required",
                "tripActivities[]"  : "required", 
                "tripCountries[]"   : "required", 
                retailcost          : { required: true,  number: true },
                short_description   : { required: true,  maxlength: 160  }
            },
            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        $('#btnAddPayment.submit').click(function(){
            if ($("#addTourForm").valid())
                alert('succes');
            else
                alert($('#xml_itinerary').text().length);
        });
    });
</script>

<script type="text/javascript">
	$(document).ready(function() {

        $('.sortable-list')
            .sortable({
               revert       : true,
               connectWith  : ".sortable-list",
               stop         : function(event,ui){ 
                 
                 var image_id = new Array();
                 $('.sortable-list tr').each(function(){
                        id = $(this).attr('data-id');
                        image_id.push(id);
                 });
                 
                 if(image_id) {
                    $.ajax({
                        url: "{{ route('tour.sort-tour-images') }}",
                        method: 'post',
                        data: {
                            image_id: image_id,
                            _token: '{{ csrf_token() }}'
                        },
                        success: function( response ) {
                        }
                    });
                 }
               }
            });
		// enable fileuploader plugin
	    $('input[name="tourImage"]').fileuploader({

	        changeInput:'<div class="fileuploader-input">' +
	                        '<div class="fileuploader-input-inner">' +
	                            '<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Tour Images here to upload.</span></h3>' +
	                        '</div>' +
	                    '</div>' + 
	                    '<div class="fileupload-link"><span>Click here to upload images from your computer</span> (Dimension 1000 X 259, max file size, 2mb. Supported file types, .jpeg, .jpg, .png).</div>',
	        theme: 'dragdrop',
	        upload: {
	            url: siteUrl('tour/tour-images'),
	            data: {tour_id:$('#tourId').val()},
	            type: 'POST',
	            enctype: 'multipart/form-data',
	            start: true,
	            synchron: true,
	            beforeSend: null,
	            onSuccess: function(result, item) {
	                var data = result;

	                if(data.success === true){

                        $('#emptyTour').hide();
	                    $('.fileuploader-items-list').find('li').remove();
	                    item.name = data.data[0].image_name;
	                    item.image_id = data.data[0].image_id;

	                    //Append to html
	                    var img_html = '<tr data-id="'+data.data[0].image_id+'" id="image_'+data.data[0].image_id+'">';
                                img_html +='<td><a href="#" class="sortable-icon"><i class="icon-reorder-black"></i></a></td>';
                                img_html +='<td class="table-no-padding">';
                                img_html +='<div class="table-image">';
                                img_url = '{{ trans('messages.image_url',['image_title' => 'tours/']) }}';
                                img_html +='<img src="'+img_url+data.data[0].title+'" class="img-responsive">';
                                img_html +='</div>';
                                img_html +='</td>';
                                img_html +='<td class="text-left blue-text">'+data.data[0].image_name+'</td>';          
                                img_url = siteUrl(data.data[0].image_large);       
                                img_html +='<td><a href="'+img_url+'" class="action-icon" download><i class="icon-download"></i></a></td>';          
                                img_html +='<td><a href="javascript://" class="delete_tour_img action-icon image-click-event image-delete" data-action="delete" data-id="'+data.data[0].image_id+'" id="'+data.data[0].image_id+'" data-type="tour"><i class="icon-delete-forever"></i></a></td>';          
	                        img_html +='</tr>';        
	                    $('.sortable-list').append(img_html);

//                            $('.image-click-event').click(function(){
//                                    removeImage($(this).attr('id'));
//                            });
	                }
                        else{
                            $('.fileuploader-items-list').find('li').remove();
                        }
	                // if warnings
	                if (data.hasWarnings) {
	                    for (var warning in data.warnings) {
	                        alert(data.warnings);
	                    }
	                    
	                    item.html.removeClass('upload-successful').addClass('upload-failed');
	                    // go out from success function by calling onError function
	                    // in this case we have a animation there
	                    // you can also response in PHP with 404
	                    return this.onError ? this.onError(item) : null;
	                }
	                
	                item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
	                setTimeout(function() {
	                    item.html.find('.progress-bar2').fadeOut(400);
	                }, 400);
	            },
	            onError: function(result, item,response) {
                        $('.error_message_image').html('<span>The tour image must be a file of type: jpeg, jpg, png.</span>')
	            },
	            onProgress: function(data, item) {
	                var progressBar = item.html.find('.progress-bar2');
	                
	                if(progressBar.length > 0) {
	                    progressBar.show();
	                    progressBar.find('span').html(data.percentage + "%");
	                    progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
	                }
	            },
	            onComplete: null,
	        },

	        captions: {
	            feedback: '<span><i class="icon-add-photos"></i> Drag Tour Images here to upload.</span>',
	            feedback2: '<span><i class="icon-add-photos"></i> Drag Tour Images here to upload.</span>',
	            drop: '<span><i class="icon-add-photos"></i> Drag Tour Images here to upload.</span>'
	        },
	    });
	});
</script>
@stop

@section('custom-css')
	<style>
        #tripActivities, #tripCountries { background-position: top right;}
        #tripCountries { height:155px;}
        hr{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .form-group {
            margin-bottom: 15px !important;
        }

        .min-height-50{min-height: 50px;}
        .pickup-control{border: solid 1px #f3f3f3; min-height: 50px; padding: 6px;}
        .pickup-control:focus, .pickup-control:active{ border: solid 1px #f3f3f3; box-shadow: none; }


        .mce-branding { display:none !important;}

        /*--- datetimepicker ----*/
        .bootstrap-datetimepicker-widget .datepicker table tr th.picker-switch{background-color: #27A9E0; color: #fff;}
        .bootstrap-datetimepicker-widget .datepicker table tr td, .bootstrap-datetimepicker-widget .datepicker table tr th{padding: 8px 10px; text-align: center;}
        .bootstrap-datetimepicker-widget .datepicker table tr th{background-color: #27a9df; color: #fff;}
        .bootstrap-datetimepicker-widget .picker-switch.accordion-toggle table.table-condensed{margin: 0 auto; font-size: 22px;}
        .bootstrap-datetimepicker-widget .datepicker table tr td{padding: 11px 10px; border-radius: 50%; min-width: 42px; text-align: center;}
        .timepicker-picker table.table-condensed tr td{text-align: center;}
        .bootstrap-datetimepicker-widget .datepicker table tr td span{width: 20%; line-height: 57px; display: inline-block;}
        .bootstrap-datetimepicker-widget .datepicker table tr td span.decade{width: 30%;}
        .timedatepicker .input-group-addon{padding: 0; background-color: transparent; border: none; font-size: 20px;}
    </style>
@stop