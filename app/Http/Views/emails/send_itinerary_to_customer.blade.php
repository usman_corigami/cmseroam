<p>Hello {{ $user }},</p>

<p>Travel Agent {{ $agent }} created an itinerary for you.</p>

<p><a href="{{ $link }}">Click here to view full details.</a></p>

<p>Thank you.</p>

