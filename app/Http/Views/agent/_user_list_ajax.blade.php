<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getUserSort(this,'id');">{{ trans('Account ID') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'id')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getUserSort(this,'name');"> <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>{{ trans('First Name') }}</th>
            <th > {{ trans('Last Name') }}</th>
            <th> {{ trans('Agent (Consultant)') }}</th> 
            <th onclick="getUserSort(this,'create_at');"> {{ trans('Created Date') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'create_at')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">Active</th>
        </tr>
    </thead>
    <tbody class="user_list_ajax">
    @if(count($oUserList) > 0)
        @include('WebView::agent._more_user_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oUserList->count() , 'total'=>$oUserList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oUserList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oUserList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                    callAgentUserListing(event,'table_record',pageNumber);
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
</script>