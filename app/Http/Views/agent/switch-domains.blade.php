@extends('layout/mainlayout')
@section('content')
<div class="content-container">
    <h1 class="page-title">Switch Domains</h1>

    <div class="alert" role="alert" id="error_msg"></div>
    <div class="box-wrapper">
       <table class="table">
           <thead>
             <tr>
               <th>Domain</th>
               <th>Action</th>
             </tr>
           </thead>
           <tbody>
          <?php $title = 'Switch Domain'; ?>
           @foreach($domains as $domain)
            <?php if($domain['domains']['id'] ==  $current_domain) {
              $title = 'Current Domain';
            } ?>
             <tr>
               <td>{{ $domain['domains']['name'] }}</td>
               <td>
                   <a href="@if($title == 'Current Domain'){{'#'}}@else{{ route('agent.switch-domain') }}@endif" data-domain-id="{{ $domain['domains']['id'] }}" class="button success tiny btn-primary btn-sm m-b-3 @if($title == 'Switch Domain') {{'switch-domain-btn'}} @endif" title="{{ $title }}">{{ $title }}</a>
               </td>
             </tr>
             <?php 
              $title = 'Switch Domain';
            ?>
            @endforeach
          
           </tbody>
       </table>
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/agent.js') }}"></script>
@endpush