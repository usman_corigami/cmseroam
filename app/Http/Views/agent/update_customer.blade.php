@extends('layout/mainlayout')
@section('content')
@php
	$interests = array(
	  15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
	  5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
	  13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
	  29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
	  11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
	  20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
	  );
@endphp
<div class="content-container">
    <h1 class="page-title">Update Customer</h1>

    <div class="alert" role="alert" id="error_msg"></div>
	<form method="post" action="{{ route('agent.update-customer') }}" class="add-form" id="customer-form">
	{{ csrf_field() }}
    <input type="hidden" name="id" value="{{$userDetails->id}}">
    <input type="hidden" name="user_id" value="{{$userDetails->user_id}}">
    <div class="box-wrapper">
    	<p>Personal Information</p>
		<div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Account ID</label>
                    <input type="text" name="account_id" value="{{!empty($userDetails->account_id) ? $userDetails->account_id:''}}" id="account_id" class="form-control" placeholder="Account ID">
                </div>      
            </div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="label-control">Name</label>
					<input type="text" name="name" value="{{!empty($userDetails->first_name) ? $userDetails->first_name:''}}" id="name" class="form-control" placeholder="Name">
				</div>		
			</div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Family Name</label>
                    <input type="text" name="family_name" value="{{!empty($userDetails->last_name) ? $userDetails->last_name:''}}" id="family_name" class="form-control" placeholder="Family Name">
                </div>      
            </div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="label-control">Other Name</label>
					<input type="text" name="other_name" value="{{!empty($userDetails->other_name) ? $userDetails->other_name:''}}" id="other_name" class="form-control" placeholder="Other Name">
				</div>		
			</div>
        
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Gender</label>
                    <select name="gender" id="gender" class="form-control">
                        <option value="">Select Gender</option>
                        <option value="Male" <?php echo ($userDetails->gender=="Male") ? 'selected':'' ?> >Male</option>
                        <option value="Female" <?php echo ($userDetails->gender=="Female") ? 'selected':'' ?>>Female</option>
                        <option value="Other" <?php echo ($userDetails->gender=="Other") ? 'selected':'' ?> >Other</option>
                    </select>
                </div>  
            </div>
		
            <div class="col-sm-6">
        		<div class="form-group">
        			<label class="label-control">Nationality</label>
        			<select name="nationality" id="nationality" class="form-control">
        				<option value="">Select Nationality</option>
                        <?php
                        foreach ($countriesList as $val) { ?>
                           <option value="{{ $val->name }}" <?php echo ($userDetails->nationality==$val->name) ? 'selected':'' ?>>{{ $val->name }}</option>
                        <?php } ?>
        			</select>
                </div>  
        	</div>

			<div class="col-sm-6">
				<div class="form-group">
					<label class="label-control">Year of Birth</label>
                    <select name="birth_year" id="birth_year" class="form-control">
                        <option value="">Select Birth Year</option>
                        <?php
                        $latest_year = date('Y');
                        $earliest_year = ($latest_year-100);
                        
                        foreach ( range( $latest_year, $earliest_year ) as $i )
                        { ?>
                            <option value="{{ $i }}" <?php echo ($userDetails->birth_year==$i) ? 'selected':'' ?>>{{ $i }}</option>
                        <?php } ?>
                    </select>
				</div>		
			</div>             
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Passport Number</label>
                    <input type="text" name="passport_number" value="{{!empty($userDetails->passport_number) ? $userDetails->passport_number:''}}" id="passport_number" class="form-control" placeholder="Passport Number">
                </div>      
            </div>            
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Passport Expiry Date</label>
                    <input type="text" data-date-format="mm/dd/yy"  name="passport_expiry_date" value="{{!empty($userDetails->passport_expiry_date) ? $userDetails->passport_expiry_date:''}}" id="passport_expiry_date" class="form-control datepicker" placeholder="Passport Expiry Date">
                </div>      
            </div>            
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Passport Issue Date</label>
                    <input type="text" data-date-format="mm/dd/yy" name="passport_issue_date" value="{{!empty($userDetails->passport_issue_date) ? $userDetails->passport_issue_date:''}}" id="passport_issue_date" class="form-control datepicker" placeholder="Passport Issue Date">
                </div>      
            </div>
		</div>	
			
    </div>
    <div class="box-wrapper">
        <p>Contact Details</p>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Phone</label>
                    <input type="text" name="contact_no" value="{{!empty($userDetails->contact_no) ? $userDetails->contact_no :''}}" id="contact_no" class="form-control" placeholder="Phone">
                </div>      
            </div>            
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Email</label>
                    <input type="email" name="email" value="{{!empty($userDetails->email) ? $userDetails->email :''}}" id="email" class="form-control" placeholder="Email">
                </div>      
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Address One</label>
                    <input type="text" name="address_one" value="{{!empty($userDetails->address_one) ? $userDetails->address_one :''}}" class="form-control" placeholder="Address One">
                </div>      
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Address Two</label>
                    <input type="text" name="address_two" value="{{!empty($userDetails->address_two) ? $userDetails->address_two :''}}" class="form-control" placeholder="Address Two">
                </div>      
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">City</label>
                    <input type="text" name="city" value="{{!empty($userDetails->city) ? $userDetails->city :''}}" class="form-control" placeholder="City">
                </div>      
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">State / Province / Region</label>
                    <input type="text" name="state" value="{{!empty($userDetails->state) ? $userDetails->state :''}}" class="form-control" placeholder="State">
                </div>      
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">ZIP / Postal Code</label>
                    <input type="text" name="zip" value="{{!empty($userDetails->zip) ? $userDetails->zip :''}}" class="form-control" placeholder="ZIP">
                </div>      
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Country</label>
                    <select name="country_id" class="form-control">
                        <option value="">Select Country</option>
                        <?php
                        foreach ($countriesList as $val) { ?>
                           <option value="{{ $val->id }}" <?php echo ($userDetails->country_id==$val->id) ? 'selected':'' ?>>{{ $val->name }}</option>
                        <?php } ?>
                    </select>
                </div>  
            </div>
            
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Contact Method</label>
                    <select name="contact_method" id="contact_method" class="form-control">
                        <option value="">Select Contact Method</option>
                        <option value="Phone" <?php echo ($userDetails->pref_contact_method=="Phone") ? 'selected':'' ?>>Phone</option>
                        <option value="SMS" <?php echo ($userDetails->pref_contact_method=="SMS") ? 'selected':'' ?>>SMS</option>
                        <option value="Email" <?php echo ($userDetails->pref_contact_method=="Email") ? 'selected':'' ?>>Email</option>
                        <option value="Postal" <?php echo ($userDetails->pref_contact_method=="Postal") ? 'selected':'' ?>>Postal</option>
                        <option value="Carrier Pigeon" <?php echo ($userDetails->pref_contact_method=="Carrier Pigeon") ? 'selected':'' ?>>Carrier Pigeon</option>
                    </select>
                </div>
            </div>

        </div>  
            
    </div>
    <div class="box-wrapper">
    	<p>Personal Preferences</p>
    	<div class="form-group">
            <?php $getProducts = explode(',', $userDetails->freq_used_products); ?>
    		<label class="label-control"> Frequently Used Products</label>
    		<select name="freq_used_products[]" id="freq_used_products"  multiple="multiple" class="form-control freq_product" placeholder="Select Frequently Used Product">
    			<option value="Flight" <?php echo (in_array('Flight', $getProducts)) ? 'selected':'' ?>>Flight</option>
    			<option value="Accommodation" <?php echo (in_array('Accommodation', $getProducts)) ? 'selected':'' ?>>Accommodation</option>
    			<option value="Transfer" <?php echo (in_array('Transfer', $getProducts)) ? 'selected':'' ?>>Transfer</option>
    			<option value="Insurance" <?php echo (in_array('Insurance', $getProducts)) ? 'selected':'' ?>>Insurance</option>
    			<option value="Cruise" <?php echo (in_array('Cruise', $getProducts)) ? 'selected':'' ?>>Cruise</option>
    			<option value="Car-Hire" <?php echo (in_array('Car-Hire', $getProducts)) ? 'selected':'' ?>>Car-Hire</option>
    			<option value="Activities" <?php echo (in_array('Activities', $getProducts)) ? 'selected':'' ?>>Activities</option>
    			<option value="Tours" <?php echo (in_array('Tours', $getProducts)) ? 'selected':'' ?>>Tours</option>
    		</select>
    	</div>
    	<p>Accommodation</p>
    	<div class="row">
    		<div class="col-sm-6">
		    	<div class="form-group">
		    		<label class="label-control"> Accommodation Type </label>
		    		<select name="accommodation_type" id="accommodation_type" class="form-control" placeholder="Select Accommodation Type">
		    			<option value="1" <?php echo ($userDetails->pref_hotel_categories=="1") ? 'selected':'' ?>> 1 Star</option>
						<option value="5" <?php echo ($userDetails->pref_hotel_categories=="5") ? 'selected':'' ?>> 2 Star</option>
						<option value="2" <?php echo ($userDetails->pref_hotel_categories=="2") ? 'selected':'' ?>> 3 Star</option>
						<option value="3" <?php echo ($userDetails->pref_hotel_categories=="3") ? 'selected':'' ?>> 4 Star</option>
						<option value="9" <?php echo ($userDetails->pref_hotel_categories=="9") ? 'selected':'' ?>> 5 Star</option>
		    		</select>
		    	</div>
    		</div>
    		<div class="col-sm-6">
    			<div class="form-group">
                    <label class="label-control"> Room Type </label>
                    <select class="form-control" name="room_type" id="room_type">
                        <option value="">Select Accommodation Type</option>
                        <option value="23" <?php echo ($userDetails->pref_hotel_room_types=="23") ? 'selected':'' ?>>Smoking</option>
                        <option value="24" <?php echo ($userDetails->pref_hotel_room_types=="24") ? 'selected':'' ?>>Non-Smoking</option>
                    </select>
                </div>
    		</div>
    	</div>
    	
    	<p>Transport</p>
    	<div class="row">
    		<div class="col-sm-6">
    			<div class="form-group">
    				<label class="label-control">Transport Type</label>
    				<select class="form-control" name="transport_type" id="transport_type">
    					<option value="">Select Transport Type</option>
    					<option value="1" <?php echo ($userDetails->pref_transport_types=="1") ? 'selected':'' ?>>Flight</option>
    					<option value="2" <?php echo ($userDetails->pref_transport_types=="2") ? 'selected':'' ?>>Overland</option>
    				</select>
    			</div>
    		</div>
    		<div class="col-sm-6">
		    	<div class="form-group">
		    		<label class="label-control">Cabin Class</label>
		    		<select class="form-control" name="cabin_class" id="cabin_class">
						<option value="">Select Cabin Class</option>
						<option value="Y" <?php echo ($userDetails->pref_cabin_class=="Y") ? 'selected':'' ?>>Economy</option>
						<option value="C" <?php echo ($userDetails->pref_cabin_class=="C") ? 'selected':'' ?>>Business</option>
						<option value="F" <?php echo ($userDetails->pref_cabin_class=="F") ? 'selected':'' ?>>First</option>
						<option value="S" <?php echo ($userDetails->pref_cabin_class=="S") ? 'selected':'' ?>>Premium Economy</option>
				   	</select>
		    	</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-sm-6">
                <?php $getSeatType = explode(',', $userDetails->pref_seat_type); ?>  
    			<div class="form-group">
    				<label class="label-control">Seat Type</label>
    				<select class="form-control seat_type" name="seat_type[]" id="seat_type" multiple="multiple">
    					<option value="">Select Seat Type</option>
    					<option value="Window" <?php echo (in_array('Window', $getSeatType)) ? 'selected':''  ?> >Window</option>
    					<option value="Aisle" <?php echo(in_array('Aisle', $getSeatType)) ? 'selected':''  ?> >Aisle</option>
    					<option value="Front" <?php echo(in_array('Front', $getSeatType)) ? 'selected':''  ?> >Front</option>
    					<option value="Back" <?php echo(in_array('Back', $getSeatType)) ? 'selected':''  ?> >Back</option>
    				</select>
    			</div>
    		</div>
    		<div class="col-sm-6">
                <?php $getMealtype = explode(',', $userDetails->pref_meal_type); ?>
		    	<div class="form-group">
		    		<label class="label-control">Meal Type</label>
		    		<select class="form-control meal_type" name="meal_type[]" id="meal_type" multiple="multiple">
    					<option value="">Select Meal Type</option>
    					<option value="Vegan" <?php echo(in_array('Vegan', $getMealtype)) ? 'selected':''  ?> >Vegan</option>
    					<option value="Non Vegetarian" <?php echo(in_array('Non Vegetarian', $getMealtype)) ? 'selected':''  ?>> Non Vegetarian</option>
    					<option value="Diabetic" <?php echo(in_array('Diabetic', $getMealtype)) ? 'selected':''  ?>>Diabetic</option>
    					<option value="Non-Lactose" <?php echo(in_array('Non-Lactose', $getMealtype)) ? 'selected':''  ?>>Non-Lactose</option>
    					<option value="Fruit Platter" <?php echo(in_array('Fruit Platter', $getMealtype)) ? 'selected':''  ?>>Fruit Platter</option>
    					<option value="Gluten Free" <?php echo(in_array('Gluten Free', $getMealtype)) ? 'selected':''  ?>>Gluten Free</option>
    					<option value="Low Fat" <?php echo(in_array('Low Fat', $getMealtype)) ? 'selected':''  ?>>Low Fat</option>
    					<option value="Raw Vegetable" <?php (in_array('Raw Vegetable', $getMealtype)) ? 'selected':''  ?>>Raw Vegetable</option>
    				</select>
		    	</div>
    		</div>
    	</div>
    	<p>Activities</p>
    	<div class="form-group">
    		<label class="label-control">Activity / Tour Type</label>
            <?php $getIntreset = explode(',', $userDetails->interests); ?>
    		<select name="interests[]" id="interests"  multiple="multiple" class="form-control interests" placeholder="Select Interests">
    			@foreach($interests as $key => $interest) 
    				<option value="{{ $key }}" <?php echo(in_array($key, $getIntreset)) ? 'selected':''  ?>>{{ $interest }}</option>
    			@endforeach
    		</select>
    	</div>
    </div>
    <div class="box-wrapper">
    	<p>Conversation Notes</p>
    	<div class="form-group">
            <label class="label-control">Conversation Notes</label>
    		<textarea name="additional_info" id="additional_info"  class="form-control">{{!empty($userDetails->additional_info) ? $userDetails->additional_info:''}}</textarea>
    	</div>
    </div>
	<div class="m-t-20 row">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="row">
				<div class="col-sm-6">
					<button type="submit" name="submit" class="btn btn-primary btn-block">Save</button>
				</div>
				<div class="col-sm-6">
				   <a href="{{ route('user.index')}}"  class="btn btn-primary btn-block">Cancel</a>
				</div>
			</div>
		</div>
    </div>
	</form>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/agent.js') }}"></script>
<script type="text/javascript">
    $(".freq_product,.seat_type,.meal_type,.interests").selectize();
</script>
<script>

    $("#customer-form").validate({
        ignore: [],
        rules: {
            name: {
                required: true
            },
            family_name: {
                required: true
            },
            gender: {
                required: true
            },
            contact_no: {
                required: true,
            },
            email: {
                required: true,
				email: true
            },
            contact_method: {
                required: true
            },
        },

        errorPlacement: function (label, element) {
            label.addClass('error_c');
            label.insertAfter($(element).parent('.form-group'));
        },
        submitHandler: function (form) {
            $('.add-form').sumbit();
        }
    });

    $('.datepicker').datepicker({
        format: 'mm-dd-yyyy'
    });
 
</script>
@endpush