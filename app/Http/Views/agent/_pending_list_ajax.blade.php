<table class="table">
    <thead>
        <tr>
           
            <th onclick="getTourSort(this,'id');">{{ trans('messages.booking_id') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'id')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'first_name');">{{ trans('messages.customer_name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'first_name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getTourSort(this,'i.created_at');">{{ trans('messages.created_date') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.created_at')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'i.from_date');">{{ trans('messages.departure_date') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.from_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'i.to_date');">{{ trans('messages.return_date') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.to_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'i.created_at');">{{ trans('messages.booking_type') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.created_at')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            <th>{{ trans('messages.agent_consultant') }}</th>
            </th>
            <th onclick="getTourSort(this,'status');">{{ trans('messages.booking_status') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'status')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <!--th class="text-center">{{ trans('messages.thead_action') }}</th-->
        </tr>
    </thead>
    <tbody class="user_list_ajax">
    @if(count($pendingBookings) > 0)
        @include('WebView::agent._more_pending_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>