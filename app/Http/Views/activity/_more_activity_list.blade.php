@foreach ($oActivityList as $aActivity)	
			
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aActivity->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aActivity->id;?>" value="<?php echo $aActivity->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="{{ route('activity.activity-create',[ 'nIdActivity' => $aActivity->id ])}}">
                {{ $aActivity->name }}
            </a>
        </td>
    <td>{{ $aActivity->city_name }} </td>
        <td>{{ $aActivity->supplier_name }}</td>
        <td>{{ $aActivity->operator_name }}</td>
        <td>{{ $aActivity->duration }}</td>
        <td>{{ $aActivity->currency_code }}</td>
        <td>
            @php
                $domain_array = [];
                if($aActivity->domains){
                    $domain_array = explode(',',$aActivity->domains);
                    foreach($domain_array as $key1=>$value1){
                        echo domianName($value1)->name.'<br/>';                         
                    }
                }
            @endphp
        </td>
        <td> @if(($aActivity->is_publish)=='1'){{ trans('messages.publish') }}@endif @if(($aActivity->is_publish)!='1'){{ trans('messages.unpublish') }}@endif</td>
        <td class="text-left">
            <div class="switch tiny switch_cls">
                <a href="{{ route('activity.activity-create',[ 'nIdActivity' => $aActivity->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('activity.activity-delete',['nIdActivity'=> $aActivity->id]) }}','{{ trans('messages.delete_label')}}')">
                @if(($aActivity->is_publish)=='1')
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.unpublish_btn') }}" onclick="callPublishRecord(this,'{{ route('activity.activity-publish',['nIdActivity'=> $aActivity->id]) }}')">
                @endif
                @if(($aActivity->is_publish)!='1')
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.publish_btn') }}" onclick="callPublishRecord(this,'{{ route('activity.activity-publish',['nIdActivity'=> $aActivity->id]) }}')">
                @endif
            </div>
        </td>
    </tr> 
@endforeach