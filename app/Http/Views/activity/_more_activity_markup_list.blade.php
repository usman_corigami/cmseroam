<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCountrySort(this,'am.name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'am.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'c.name');">{{ trans('messages.city') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'c.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'acs.name');">{{ trans('messages.supplier') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'acs.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'ao.name');"> {{ trans('messages.operator') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'ao.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getCountrySort(this,'a.duration');"> {{ trans('messages.duration') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'a.duration')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getCountrySort(this,'cu.code');"> {{ trans('messages.thead_currency') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'cu.code')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">{{ trans('messages.thead_action')}}</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
    @if(count($oActivityMarkupList) > 0)
        @foreach ($oActivityMarkupList as $aActivity) <?php //echo '<pre>'; print_r($aActivity);exit; ?>		
            <tr>
                <td>
                    <label class="radio-checkbox label_check" for="checkbox-{{ $aActivity->id }}">
                        <input type="checkbox" class="cmp_check" id="checkbox-{{ $aActivity->id }}" value="{{ $aActivity->id }}">&nbsp;
                    </label>
                </td>
                <td>
                    <a href="{{ route('activity.create-activity-markup',[$aActivity->id]) }}">{{ $aActivity->name }}</a>
                </td>
                <td>{{ $aActivity->allocation_name }}</td>
                <td>{{ $aActivity->mark_percentage.'%' }}</td>
                <td>{{ $aActivity->agent_percentage.'%' }}</td>
                <td>
                    <?php if ($aActivity->is_default): ?>
                        <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                    <?php else: ?>
                        <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                    <?php endif; ?>
                </td>								
                <td>
                    <?php if ($aActivity->is_active): ?>
                        <i class="fa fa-check fa-lg" aria-hidden="true"></i>
                    <?php else: ?>
                        <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                    <?php endif; ?>
                </td>
                <td>
                    <a href="{{ route('activity.create-activity-markup',[$aActivity->id]) }}" class="button success tiny btn-primary btn-sm pull-left m-r-10">{{ trans('messages.update_btn') }}</a>
                </td>	
            </tr>
        @endforeach
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oActivityMarkupList->count() , 'total'=>$oActivityMarkupList->count() ]) }}</p></div>

</div>