@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if(isset($oActivitySupplier))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Activity Supplier']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Activity Supplier']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
        <div class="small-6 small-centered columns error-box">{{$errors->first()}}</div>
        @endif
    </div>
    <br>
    <div class="box-wrapper">

        <p>{{ trans('messages.add_new_supplier') }}</p>
        @if(isset($oActivitySupplier))
        {{ Form::model($oActivitySupplier, array('url' => route('activity.create-activity-supplier') ,'method'=>'POST','enctype'=>'multipart/form-data','id'=>'addTourForm')) }}
        @else
        {{Form::open(array('url' => 'activity/create-activity-supplier','method'=>'Post','enctype'=>'multipart/form-data')) }}
        @endif
        <div class="form-group m-t-30">

            <label class="label-control">{{ trans('messages.name') }}<span class="required">*</span></label>

            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>'form-control','placeholder'=>'Enter Name'])}}
        </div>
        @if ( $errors->first( 'name' ) )
            <small class="error">{{ $errors->first('name') }}</small>
        @endif

        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.contact_title') }}</label>
            <div>
                <label class="radio-checkbox label_radio" for="radio-03">
                    <input type="radio" id="radio-03" value="MR" name="marketing_contact_title" {{ ($oActivitySupplier && $oActivitySupplier->marketing_contact_title == 'MR') ?  'checked' : '' }}> MR
                </label> 
                <label class="radio-checkbox label_radio" for="radio-04">
                    <input type="radio" id="radio-04" value="MS" name="marketing_contact_title" {{ ($oActivitySupplier && $oActivitySupplier->marketing_contact_title == 'MS') ?  'checked' : '' }}> MS
                </label>
                <label class="radio-checkbox label_radio" for="radio-05">
                    <input type="radio" id="radio-05" value="MRS" name="marketing_contact_title" {{ ($oActivitySupplier && $oActivitySupplier->marketing_contact_title == 'MRS') ?  'checked' : '' }}> MRS
                </label>
            </div>
        </div>
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.contact_name') }}<span class="required">*</span></label>
            {{Form::text('marketing_contact_name',Input::old('marketing_contact_name'),['id'=>'marketing_contact_name','class'=>'form-control','placeholder'=>'Enter Contact Name'])}}

        </div>
        @if ( $errors->first( 'marketing_contact_name' ) )
        <small class="error">{{ $errors->first('marketing_contact_name') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.contact_email') }}<span class="required">*</span></label>
            {{Form::text('marketing_contact_email',Input::old('marketing_contact_email'),['id'=>'marketing_contact_email','class'=>'form-control','placeholder'=>'Enter Contact Email'])}}

        </div>
        @if ( $errors->first( 'marketing_contact_email' ) )
        <small class="error">{{ $errors->first('marketing_contact_email') }}</small>
        @endif
        
        <input type="hidden" value="{{ $nIdActivitySupplier }}" name="id_activity" />
        
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.contact_number') }}<span class="required">*</span></label>

            {{Form::text('marketing_contact_phone',Input::old('marketing_contact_phone'),['id'=>'marketing_contact_phone','class'=>'form-control','placeholder'=>'Enter Contact Number'])}}

        </div>
        @if ( $errors->first( 'marketing_contact_phone' ) )
        <small class="error">{{ $errors->first('marketing_contact_phone') }}</small>
        @endif
    </div>  
    <div class="box-wrapper">

        <p>{{ trans('messages.as_reservation_detail') }}</p>
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.as_reservation_contact_name') }}<span class="required">*</span></label>
            {{Form::text('reservation_contact_name',Input::old('reservation_contact_name'),['id'=>'reservation_contact_name','class'=>'form-control','placeholder'=>'Enter Reservation Contact Name'])}}

        </div>
        @if ( $errors->first( 'reservation_contact_name' ) )
        <small class="error">{{ $errors->first('reservation_contact_name') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.as_reservation_contact_email') }}<span class="required">*</span></label>
            {{Form::text('reservation_contact_email',Input::old('reservation_contact_email'),['id'=>'reservation_contact_email','class'=>'form-control','placeholder'=>'Enter Reservation Contact Email'])}}

        </div>
        @if ( $errors->first( 'reservation_contact_email' ) )
        <small class="error">{{ $errors->first('reservation_contact_email') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.as_reservation_landline') }} <span class="required">*</span></label>
            {{Form::text('reservation_contact_landline',Input::old('reservation_contact_landline'),['id'=>'reservation_contact_landline','class'=>'form-control','placeholder'=>'Enter Reservation Landline'])}}

        </div>
        @if ( $errors->first( 'reservation_contact_landline' ) )
        <small class="error">{{ $errors->first('reservation_contact_landline') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.as_free_phone') }} <span class="required">*</span></label>
            {{Form::text('reservation_contact_free_phone',Input::old('reservation_contact_free_phone'),['id'=>'reservation_contact_free_phone','class'=>'form-control','placeholder'=>'Enter Free Phone'])}}

        </div>
        @if ( $errors->first( 'reservation_contact_free_phone' ) )
        <small class="error">{{ $errors->first('reservation_contact_free_phone') }}</small>
        @endif
    </div>

    <div class="box-wrapper"> 
        <p>{{ trans('messages.as_contact_detail') }}</p>
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.account_name') }} <span class="required">*</span></label>
            {{Form::text('accounts_contact_name',Input::old('accounts_contact_name'),['id'=>'accounts_contact_name','class'=>'form-control','placeholder'=>'Enter Account Name'])}}

        </div>
        @if ( $errors->first( 'accounts_contact_name' ) )
        <small class="error">{{ $errors->first('accounts_contact_name') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.account_email') }} <span class="required">*</span></label>
            {{Form::text('accounts_contact_email',Input::old('accounts_contact_email'),['id'=>'accounts_contact_email','class'=>'form-control','placeholder'=>'Enter Account Email'])}}

        </div>
        @if ( $errors->first( 'accounts_contact_email' ) )
        <small class="error">{{ $errors->first('accounts_contact_email') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.account_title') }} <span class="required">*</span></label>
            {{Form::text('accounts_contact_title',Input::old('accounts_contact_title'),['id'=>'accounts_contact_title','class'=>'form-control','placeholder'=>'Enter Account Title'])}}

        </div>
        @if ( $errors->first( 'accounts_contact_title' ) )
        <small class="error">{{ $errors->first('accounts_contact_title') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.account_phone') }} <span class="required">*</span></label>
            {{Form::text('accounts_contact_phone',Input::old('accounts_contact_phone'),['id'=>'accounts_contact_phone','class'=>'form-control','placeholder'=>'Enter Account Phone'])}}

        </div>
        @if ( $errors->first( 'accounts_contact_phone' ) )
        <small class="error">{{ $errors->first('accounts_contact_phone') }}</small>
        @endif
    </div>		  
    <div class="box-wrapper"> 
        <p>{{ trans('messages.as_description') }}</p>
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.as_image_logo') }} <span class="required">*</span></label>
            {{ Form::file('logo',['class'=>'form-control'])}}

        </div>
        @if ( $errors->first( 'logo' ) )
        <small class="error">{{ $errors->first('logo') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.as_description') }} <span class="required">*</span></label>
            {{Form::textarea('description',Input::old('description'),['id'=>'description','rows'=>5,'class'=>'form-control'])}}

        </div>
        @if ( $errors->first( 'description' ) )
        <small class="error">{{ $errors->first('description') }}</small>
        @endif
    </div>  
    <div class="box-wrapper"> 
        <p>Notes / Remarks</p>
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.as_special_note') }} <span class="required">*</span></label>
            {{Form::textarea('special_notes',Input::old('special_notes'),['id'=>'special_notes','rows'=>5,'class'=>'form-control'])}}

        </div>
        @if ( $errors->first( 'special_notes' ) )
        <small class="error">{{ $errors->first('special_notes') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.as_remark') }} <span class="required">*</span></label>
            {{Form::textarea('remarks',Input::old('remarks'),['id'=>'remarks','rows'=>5,'class'=>'form-control'])}}

        </div>
        @if ( $errors->first( 'remarks' ) )
        <small class="error">{{ $errors->first('remarks') }}</small>
        @endif
    </div>  
    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{ route('activity.activity-supplier-list')}}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
            </div>
        </div>
    </div>	
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>
    .error{
        color:red !important;
    }
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    div .with_error{
        border:1px solid black;
    }
</style>
@stop

@section('custom-js')
<script>

    $(function () {

        tinymce.init({
            selector: '#description',
            height: 200,
            menubar: false
        });
        tinymce.init({
            selector: '#remarks',
            height: 200,
            menubar: false
        });
        tinymce.init({
            selector: '#special_notes',
            height: 200,
            menubar: false
        });
    });
</script>
@stop
