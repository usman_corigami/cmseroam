@extends('layout/mainlayout')

@section('custom-css')
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/map.css') }}">
<style>
	#map {
		padding: 0;
		margin: 0;
		height: 450px;
		background: #EAEAEA;
	}
	.route-box {
		border-color: #62ABC3;
		padding: 11px;
		padding-right: 42px;
		position: relative;
	}
	.route-box a.show-on-map {
		font-size: 15px;
		color: #333333;
		transition: .24s ease-in-out 0s;
	}
	.route-box a.show-on-map.active,
	.route-box a.show-on-map:hover {
		color: #008cba;
	}
	.route-controls {
		margin-top: 1rem;
	}
	.route-controls a {
		padding: 5px 9px !important;
		margin: 0;
	}
	.route-controls a i {
		font-size: 14px;
	}
</style>
@stop

@section('content')
<div class="content-container">
    <h1 class="page-title">{{ strtoupper($route_plan->name) }}</h1>
    <div class="box-wrapper">
        <div class="large-4 column">
            @foreach ($route_plan->routes as $route)
            <div class="route-container">
                <fieldset class="route-box">
                    <a href="#." data-route-id="{{ $route->id }}" class="show-on-map {{ $route->is_default == 1 ? 'active' : '' }}">
                        <?php $oRoutesleg = $route->route_legs->toArray(); ?>
                        @foreach ($route->route_legs as $route_leg)
                            {{ $route_leg->city->name }} 
                            @if($route_leg->id != $oRoutesleg[count($oRoutesleg)-1]['id']) 
                                <i class="fa fa-long-arrow-right"></i>
                            @endif
                        @endforeach
                    </a>
                    <div class="route-controls">
                        @if ($route->is_default == 1)
                        <a href="#." data-tooltip aria-haspopup="true" title="This is the default route." disabled class="button warning tiny btn-primary"><i class="fa fa-star"></i></a>
                        @else
                        <a href="#." data-tooltip aria-haspopup="true" title="Set this as default route." data-route-id="{{ $route->id }}" data-route-plan-id="{{ $route->route_plan_id }}" class="set-default-btn button secondary tiny"><i class="fa fa-star"></i></a>
                        @endif
                        {{-- <a href="#." data-tooltip aria-haspopup title="Edit Route" class="button secondary tiny"><i class="fa fa-pencil">                            </i></a> --}}
                        <a href="#." data-tooltip aria-haspopup title="Delete Route" data-route-id="{{ $route->id }}" class="delete-route-btn button secondary tiny"><i class="fa fa-trash"></i></a>
                    </div>
                </fieldset>
            </div>
            @endforeach
        </div>
        <div class="large-8 column">
            <div id="map-loader">
                <h5><i class="fa fa-circle-o-notch fa-spin"></i> LOADING MAP</h5>
            </div>
            <div id="map"></div>
        </div>
    </div>	
</div>            


<input type="hidden" id="routes" value='{{{ json_encode($routes) }}}'>
@stop

@section('custom-js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>
<script type="text/javascript" src="{{ url('assets/js/markerlabel.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/infobox.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/compass.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/eroam-map.js') }}"></script>
<script type="text/javascript">
$('.show-on-map').click(function(event) {
    event.preventDefault();
    $('.show-on-map').removeClass('active');
    $(this).addClass('active');
    var routeId = $(this).data('route-id');
    var routes = JSON.parse($('#routes').val());
    var selected = '';
    $.grep(routes, function(v, k) {
            if (v.id == routeId) {
                    selected = v;
            }
    });
    Map.viewRoute(selected);
});
$('.delete-route-btn').click(function(event) {
    event.preventDefault();
    debugger;
    var routeId = $(this).data('route-id');
    if (routeId != '' && confirm('Are you sure you want to delete this route?') ) {
        $.ajax({
            method: 'post',
            url: siteUrl('route/route-delete'),
            data: {
                    _token: '{{ csrf_token() }}',
                    routeId: routeId
            },
            success: function() {
                    window.location.reload();
            }
        });
    }
});
$('.set-default-btn').click(function(event) {
    event.preventDefault();
    var routeId = $(this).data('route-id');
    var routePlanId = $(this).data('route-plan-id');
    if (routeId != '') {
        $.ajax({
            method: 'post',
            url: siteUrl('route/route-default'),
            data: {
                    _token: '{{ csrf_token() }}',
                    routeId: routeId,
                    routePlanId: routePlanId
            },
            success: function() {
                    window.location.reload();
            }
        });
    }
});
$(document).ready(function() {
    Map.init('view');
}); // END OF DOCUMENT READY FUNCTION
</script>
@stop