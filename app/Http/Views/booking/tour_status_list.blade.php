@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.manage_list_title', ['name' => 'Tour']) }}</h1>

    <div class="row">
        <div class="small-12 small-centered columns delete-box hidden"></div> 
    </div>
    @if(Session::has('message'))
    <div class="row">
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div> 
    </div>
    <br>
    @endif
    <div class="box-wrapper">
			<div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" name="search_str" value="{{ $sSearchStr }}" placeholder="Seach Bookings">
					<span class="input-group-btn">
					    <button class="btn btn-default" onclick="getMoreListing('{{route('booking.tour-booking-listing','0')}}',event,'table_record');" type="button"><i class="icon-search-domain"></i></button>
					</span>
                </div>
				<input type="hidden" name="order_field" value="{{ $sOrderField }}" />
				<input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            </div>
			<div class="col-md-4 col-sm-4">
                <div class="input-group input-group-box">
                    <select name="type" id="type" class="form-control" onChange="searchByType('{{route('booking.tour-booking-listing','0')}}',event,'table_record');">
					<option value="all" <?php if($type == 'all'){ echo "selected";}else{ echo ""; }?>> All</option>
					<option value="personal"  <?php if($type == 'personal'){ echo "selected";}else{ echo ""; }?>>Personal</option>
					</select>
                </div>
            </div>
		</div>
        <div class="row m-t-30 search-wrapper">
            <div class="col-md-7 col-sm-7 m-t-10"> {{ trans('messages.search_results') }}</div>
            <div class="col-md-5 col-sm-5">
                <div class="dropdown">
                        <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>ACTION</button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="javascript://" id="activate">{{ trans('messages.activate') }}</a></li>
                            <li><a href="javascript://" id="deactivate">{{ trans('messages.deactivate') }}</a></li>
                            <li><a href="javascript://" id="mark_as_reviewed">{{ trans('messages.mark_as_reviewed') }}</a></li>
                            <li><a href="javascript://" id="mark_as_unreviewed">{{ trans('messages.mark_as_unreviewed') }}</a></li>
                            <li><a href="javascript://" id="delete">{{ trans('messages.delete_action') }}</a></li>
                            <li><a href="javascript://" id="manage_views">{{ trans('messages.manage_views') }}</a></li>
                            <li><a href="javascript://" id="manage_dates">{{ trans('messages.manage_dates') }}</a></li>
                        </ul>
                </div>
            </div>
        </div>
        <div class="m-t-30">
            <label>{{ trans('messages.show_record') }}</label>
            <select class="select-entry" name="show_record" onchange="getMoreListing(siteUrl('tour/tour-list?page=1'),event,'table_record');">
                <option value="10" {{ ($nShowRecord == 10) ? 'selected="selected"' : '' }}>10</option>
                <option value="20" {{ ($nShowRecord == 20) ? 'selected="selected"' : '' }}>20</option>
                <option value="30" {{ ($nShowRecord == 30) ? 'selected="selected"' : '' }}>30</option>
                <option value="50" {{ ($nShowRecord == 50) ? 'selected="selected"' : '' }}>50</option>
            </select>
            <label>{{ trans('messages.entries') }}</label>
        </div>
        <div class="table-responsive m-t-20 table_record">
           
            @include('WebView::booking._tour_status_list_ajax')
      
        </div>
    </div>

</div>
<a href="#" data-toggle="modal" data-target="#updateViewModal" title="Manage View" class="manage_view" style="display:none;">View</a>
<div class="modal fade" id="updateViewModal" data-index="0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ trans('messages.manage_tour_views') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="box-wrapper" style="padding:10px;">
                        <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="label-control">{{ trans('messages.tour_title') }}</label>
                                                <input type="text" class="form-control" value="" id="tourTitle" />
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="label-control">{{ trans('messages.views_count') }}</label>
                                                <input type="text" class="form-control" value="" id="tourViews" />
                                                <input type="hidden" value="" id="tourViewId" />
                                    </div>
                                </div>
                        </div>
                      <div class="small-6 small-centered columns error_message_view" style="display:none"></div>
                    </div>  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default update_view">{{ trans('messages.save_btn') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.cancel_btn') }}</button>
                </div>
        </div>
    </div>
</div>
@stop
@section('custom-js')
<script type="text/javascript">
function getTourSort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
	getMoreListing(siteUrl('booking/tour-booking-listing?page=1'),event,'table_record');
    
}
function getShowType(){
		return $("#type").val();
	}
	
	function searchByType(sUrl,oEvent,sClass)
	{
		showLoader();
		var type = getShowType();
		var orderField = getOrderField();
		var orderBy = getSortingOrder();
		var showRecord = getShowRecord();
		var searchBy = $(".search_by").val();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: "POST",
			url: sUrl,
			 data: {type: type, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
			success: function (data) {
				$('.'+sClass).html(data);
				$('.switch1-state1').bootstrapSwitch();
				hideLoader();
			},
			error: function (data) {
			}
		});
	}
	
	function getBookingPaginationListing(sUrl,oEvent,sClass)
{
    showLoader();
	var type = getShowType();
    //var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    //var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: sUrl,
        data: {order_field: orderField, order_by: orderBy,show_record:showRecord,type:type},
        success: function (data) {
            $('.'+sClass).html(data);
            $('.switch1-state1').bootstrapSwitch();
            hideLoader();
        },
        error: function (data) {
        }
    });
}

</script>
@stop