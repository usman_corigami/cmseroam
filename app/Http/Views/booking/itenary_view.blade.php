@extends( 'layout/mainlayout' )
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }
</style>
@stop
@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.booking_information') }}</h1>
    <div>
        <a href="{{ route('booking.itenary-pdf',['nId'=>$oItinerary['order_id']] )}}"><button class="button success btn btn-primary">View PDF</button></a>
    </div> 
    <div class="box-wrapper m-t-20">
        <?php
        $name       = 'Guest';
        $country    = '';
        $email      = '';
        $contact    = '';
        $gender     = '';
        $dob     = '';
        foreach ($oItinerary['Passenger'] as $key => $value) {
            //if ($value->is_lead == 'Yes') {

                $name = $value->first_name . ' ' . $value->last_name;
                $email = $value->email;
                $contact = $value->contact;
                $gender = $value->gender;
                $dob = date('d-m-Y', strtotime($value->dob));

                foreach ($value['Country'] as $key1 => $value1) {
                    $country = $value1->name;
                }
            //}
        }
        ?>
        <p>{{ trans('messages.booking_id') }}: {{$oItinerary['invoice_no']}}</p>
        <div class="row">
            <div class="col-sm-6">
                <p>{{ trans('messages.customer_name') }}: {{$name}}</p>
            </div>
            <div class="col-sm-6">
                <p>{{ trans('messages.customer_email') }}: {{$email}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p>{{ trans('messages.customer_contact') }}: {{$contact}}</p>
            </div>
            <div class="col-sm-6">
                <p>{{ trans('messages.customer_gender') }}: {{$gender}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p>{{ trans('messages.customer_dob') }}: {{$dob}}</p>
            </div>
            <div class="col-sm-6">
                <p>{{ trans('messages.customer_country') }}: {{$country}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p>{{ trans('messages.days') }}: {{intval($oItinerary['total_days'])}}</p>
            </div>
            <div class="col-sm-6">
                <p>{{ trans('messages.total_amount') }}: {{$oItinerary['currency']}} {{$oItinerary['cost_per_day']}} per day (TOTAL {{$oItinerary['currency']}} {{$oItinerary['total_per_person']}} (per person))</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p>{{ trans('messages.date') }}: {{ date( 'j M Y', strtotime($oItinerary['from_date']))}} - {{ date( 'j M Y',strtotime($oItinerary['to_date']))}}</p>
            </div>
            <div class="col-sm-6">
                <p>{{ trans('messages.travellers') }}: {{$oItinerary['num_of_travellers']}}</p>
            </div>
        </div>
    </div>

    <?php
    $last_key = count($oItinerary['ItenaryLegs']) - 1;
    foreach ($oItinerary['ItenaryLegs'] as $key => $value) {
        ?>
        <div class="box-wrapper">
            <p>{{$value['from_city_name']}}, {{$value['country_code']}}</p>
    <?php
    foreach ($oItinerary['LegDetails'] as $key1 => $value1) {
        if ($value['itenary_order_id'] == $value1['itenary_order_id'] && $value['itenary_leg_id'] == $value1['itenary_leg_id']) {
            if ($value1['leg_type'] == 'hotel') {
                if ($value1['leg_name'] != 'Own Arrangement') {//echo "<pre>"; print_r($value1);exit;
                    ?>

                            <div class="m-t-20">
                                <p><i class="fa fa-hotel"></i>  
                                    {{ trans('messages.booking_accommodation') }}
                                </p>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.booking_accommodation_name') }}: {{$value1['leg_name']}}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{$value1['nights']}} {{ trans('messages.nights') }}</p>
                                    </div>
                                </div>  
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.check_in') }}: {{ date( 'j M Y', strtotime($value1['from_date']))}}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.check_out') }}: {{ date( 'j M Y', strtotime($value1['to_date']))}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.price') }}: {{$value1['currency']}} {{ceil($value1['price'])}}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}
                                            <a href="#" data-toggle="modal" data-target="#updateBookingIDModal" title="Update Booking Id" class="update_booking_id" leg-detail-id="{{$value1['leg_detail_id']}}" booking-id="{{$value1['booking_id']}}"><i class="fa fa-pencil-square-o"></i></a>
                                        </p> 
                                    </div>
                                </div>
                                <?php
                                if (!empty($value1['provider'])) {
                                    ?>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <p>{{ trans('messages.provider_name') }}: {{$value1['provider']}}</p>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <p>{{ trans('messages.booked_room') }}: {{$value1['hotel_total_room']}}</p>
                                                    </div>
                                                </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php
                                            $name = "";
                                            if (isset($value1->HotelSuppliers[0]) && !empty($value1->HotelSuppliers[0])) {
                                                $name = $value1->HotelSuppliers[0]['name'];
                                            }
                                            ?>
                                            <p>{{ trans('messages.provider_name') }}: {{$name}}</p>
                                        </div>

                                        <div class="col-sm-6">
                                            <?php
                                            $phone = "";
                                            if (isset($value1->HotelSuppliers[0]) && !empty($value1->HotelSuppliers[0])) {
                                                $phone = $value1->HotelSuppliers[0]['accounts_contact_phone'];
                                            }
                                            ?>
                                            <p>{{ trans('messages.provider_contact') }}P: {{$phone}}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php
                                            $email = "";
                                            if (isset($value1->HotelSuppliers[0]) && !empty($value1->HotelSuppliers[0])) {
                                                $email = $value1->HotelSuppliers[0]['accounts_contact_email'];
                                            }
                                            ?>
                                            <p>{{ trans('messages.provider_email') }}: {{$email}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.booking_status') }}: {{!empty($value1['provider_booking_status']) ? $value1['provider_booking_status'] : 'NOT CONFIRMED'}}</p>
                                        </div>
                                    </div>
                        <?php
                    }
                    ?>
                            </div>
                                <?php
                            } else {
                                ?>
                            <div class="m-t-20">
                                <p><i class="fa fa-hotel"></i> {{ trans('messages.booking_accommodation') }} 

                                </p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>{{$value1['leg_name']}}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}
                                            <a href="#" data-toggle="modal" data-target="#updateBookingIDModal" title="Update Booking Id" class="update_booking_id" leg-detail-id="{{$value1['leg_detail_id']}}" booking-id="{{$value1['booking_id']}}"><i class="fa fa-pencil-square-o"></i></a>
                                        </p> 
                                    </div>

                                </div>
                            </div>
                    <?php
                }
            }
            if ($value1['leg_type'] == 'activities') {
                if ($value1['leg_name'] != 'Own Arrangement') {
                   
                    ?>

                            <div class="m-t-20">
                                <p><i class="fa fa-map-o"></i>{{ trans('messages.activity_summary') }}

                                </p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.activity_name') }}: {{$value1['leg_name']}}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.date') }}: {{ date( 'j M Y', strtotime($value1['from_date']))}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.price') }}: {{$value1['currency']}} {{ceil($value1['price'])}}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}
                    <?php
                    if ($value1['provider'] != 'viator') {
                        ?>
                                                <a href="#" data-toggle="modal" data-target="#updateBookingIDModal" title="Update Booking Id" class="update_booking_id" leg-detail-id="{{$value1['leg_detail_id']}}" booking-id="{{$value1['booking_id']}}"><i class="fa fa-pencil-square-o"></i></a>
                                                <?php
                                            }
                                            ?>
                                        </p> 
                                    </div>
                                </div>
                    <?php
                    if (!empty($value1['provider'])) {
                        ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.provider_name') }}: {{$value1['provider']}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.booking_status') }}: {{!empty($value1['booking_id']) ? 'CONFIRMED' : 'NOT CONFIRMED'}}</p>
                                        </div>
                                    </div>
                        <?php
                        if (!empty($value1['booking_error_code'])) {
                            ?>  
                                        <div class="row">  
                                            <div class="col-sm-6">
                                                <p>{{ trans('messages.booking_error_code') }}: {{$value1['booking_error_code']}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>{{ trans('messages.booking_error_message') }}: {{$value1['provider_booking_status']}}</p>
                                            </div>
                                        </div>
                            <?php
                        }

                        /*if (!empty($value1['voucher_url'])) {
                            ?>  
                                        <div class="row">  
                                            <div class="col-sm-6">
                                                <p>{{ trans('messages.voucher_url') }}: <a style="color: #337ab7;text-decoration: underline;" href="{{$value1['voucher_url']}}" target="_blank">Click Here</a></p>
                                            </div>

                                        </div>
                            <?php
                        }*/
                    } else {
                        ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.provider_name') }}: {{isset($value1->ActivitySuppliers[0]['name'])? $value1->ActivitySuppliers[0]['name']:''}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.provider_contact') }}: {{isset($value1->ActivitySuppliers[0]['accounts_contact_phone'])? $value1->ActivitySuppliers[0]['accounts_contact_phone']:''}}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.provider_email') }}: {{isset($value1->ActivitySuppliers[0]['accounts_contact_email'])? $value1->ActivitySuppliers[0]['accounts_contact_email']:''}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.booking_status') }}: {{!empty($value1['provider_booking_status']) ? $value1['provider_booking_status'] : 'NOT CONFIRMED'}}</p>
                                        </div>
                                    </div>
                        <?php
                    }
                    ?>
                            </div>
                                <?php
                            } else {
                                ?>
                            <div class="m-t-20">
                                <p><i class="fa fa-hotel"></i> {{ trans('messages.activity_summary') }}

                                </p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>{{$value1['leg_name']}}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}
                                            <a href="#" data-toggle="modal" data-target="#updateBookingIDModal" title="Update Booking Id" class="update_booking_id" leg-detail-id="{{$value1['leg_detail_id']}}" booking-id="{{$value1['booking_id']}}"><i class="fa fa-pencil-square-o"></i></a>
                                        </p> 
                                    </div>

                                </div>
                            </div>
                    <?php
                }
            }
            if ($value1['leg_type'] == 'transport') {
                if ($value1['leg_name'] != 'Own Arrangement') {
                    ?>
                            <div class="m-t-20">
                                <p><i class="fa fa-bus"></i> {{ trans('messages.transport') }} 

                                </p>
                    <?php
                    $depart = $value1['departure_text'];
                    $arrive = $value1['arrival_text'];
                    if ($value1['leg_name'] == 'Flight') {
                        if (!empty($value1['booking_summary_text'])) {
                            $leg_depart = explode("Depart:", $value1['booking_summary_text'], 2);
                            $leg_arrive = explode("Arrive:", $value1['booking_summary_text'], 2);

                            if (isset($leg_depart[1])) {
                                $depart = explode(") )", $leg_depart[1], 2);
                                $depart = $depart[0] . ') )';
                            } else {
                                $depart = '';
                            }
                            if (isset($leg_arrive[1])) {
                                $arrive = explode(") )", $leg_arrive[1], 2);
                                $arrive = $arrive[0] . ') )';
                            } else {
                                $arrive = '';
                            }
                        } else {
                            $depart = '';
                            $arrive = '';
                        }
                    }
                    ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                <?php
                                $leg_name = explode('Depart:', $value1['booking_summary_text']);
                                if (isset($value1['booking_summary_text']) && !empty($value1['booking_summary_text'])) {
                                    $leg_name = trim(strip_tags($leg_name[0]));
                                } else {
                                    $leg_name = $value1['leg_name'];
                                }
                                ?>
                                        <p>{{ trans('messages.service') }}: {{$leg_name}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <p>{{ trans('messages.depart') }}: {!!$depart!!}</p>
                                    </div>
                                    <div class="col-sm-6 col-sm-offset-1">
                                        <p>{{ trans('messages.arrive') }}: {!!$arrive!!}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.price') }}: {{$value1['currency']}} {{ceil($value1['price'])}}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ trans('messages.booking_id') }}: {{$value1['booking_id']}}
                    <?php
                    if ($value1['provider'] != 'mystifly') {
                        ?>
                                                <a href="#" data-toggle="modal" data-target="#updateBookingIDModal" title="Update Booking Id" class="update_booking_id" leg-detail-id="{{$value1['leg_detail_id']}}" booking-id="{{$value1['booking_id']}}"><i class="fa fa-pencil-square-o"></i></a>
                                                <?php
                                            }
                                            ?>
                                        </p> 
                                    </div>
                                </div>
                                            <?php
                                            if (!empty($value1['provider'])) {
                                                ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.provider_name') }}: {{$value1['provider']}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.booking_status') }}: {{!empty($value1['booking_id']) ? 'CONFIRMED' : 'NOT CONFIRMED'}}</p>
                                        </div>
                                    </div>
                        <?php
                        if (!empty($value1['booking_error_code'])) {
                            ?>  
                                        <div class="row">  
                                            <div class="col-sm-6">
                                                <p>{{ trans('messages.booking_error_code') }}: {{$value1['booking_error_code']}}</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>{{ trans('messages.booking_error_message') }}: {{$value1['provider_booking_status']}}</p>
                                            </div>
                                        </div>
                            <?php
                        }
                        ?> 
                                    <?php
                                } else {
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.provider_name') }}: {{isset($value1->TransportSuppliers[0]['name'])? $value1->TransportSuppliers[0]['name']:''}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.provider_contact') }}: {{isset($value1->TransportSuppliers[0]['accounts_contact_phone'])? $value1->TransportSuppliers[0]['accounts_contact_phone']:''}}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.provider_email') }}: {{isset($value1->TransportSuppliers[0]['accounts_contact_email'])? $value1->TransportSuppliers[0]['accounts_contact_email']:''}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.booking_status') }}: {{!empty($value1['provider_booking_status']) ? $value1['provider_booking_status'] : 'NOT CONFIRMED'}}</p>
                                        </div>
                                    </div>
                        <?php
                    }
                    ?>
                            </div>
                                <?php
                            } else {
                                if ($key != $last_key) {
                                    ?>
                                <div class="m-t-20">
                                    <p><i class="fa fa-hotel"></i> {{ trans('messages.transport') }} 

                                    </p>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.self_drive') }}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ trans('messages.booking_id') }}: {{ $value1['booking_id'] }}
                                                <a href="#" data-toggle="modal" data-target="#updateBookingIDModal" title="Update Booking Id" class="update_booking_id" leg-detail-id="{{$value1['leg_detail_id']}}" booking-id="{{$value1['booking_id']}}"><i class="fa fa-pencil-square-o"></i></a>
                                            </p> 
                                        </div>

                                    </div>
                                </div>
                        <?php
                    }
                }
            }
        }
    }
    ?>
        </div>
            <?php
        }
        ?>
</div>

<!-- Modal -->
<div class="modal fade" id="updateBookingIDModal" data-index="0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add / Update Booking Id</h4>
            </div>
            <div class="modal-body">
                <div class="box-wrapper">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="label-control">Booking Id</label>

                                <input type="text" name="booking_id" id="booking_id" class="form-control" placeholder="Booking Id">
                                <input type="hidden" name="leg_detail_id" id="leg_detail_id">
                            </div>
                        </div>
                    </div>
                    <div class="small-6 small-centered columns error_message" style="display:none">The booking id field is required.</div>
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default save_booking_id">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@stop
@section('custom-js')
<script>
    $('.update_booking_id').click(function () {
        var leg_detail_id = $(this).attr('leg-detail-id');
        var booking_id = $(this).attr('booking-id');
        $('#leg_detail_id').val(leg_detail_id);
        $('#booking_id').val(booking_id);
    });


    $('body').on('click', '.save_booking_id', function (event) {
        var leg_detail_id = $('#leg_detail_id').val();
        var booking_id = $('#booking_id').val();

        if (booking_id) {
            $.ajax({
                method: 'post',
                url: '{{ route('booking.set-booking-id') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    leg_detail_id: leg_detail_id,
                    booking_id: booking_id
                },
                success: function (response) {
                    if (response) {
                        //location.reload();
                    }
                }
            });
        } else {
            $('.error_message').show();
        }

    });


</script>
@stop
