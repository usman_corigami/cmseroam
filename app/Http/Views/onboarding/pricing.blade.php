@extends('layout/mainlayout')

@section('content')
<div class="content-container">
    <div class="m-t-10">
        <?php
        if(isset($licensee_id) && $licensee_id!=''){ $licensee_id = $licensee_id; } else { $licensee_id = ''; } 
        ?>
        @include('WebView::onboarding.includes.onboarding-steps', array('id' => $licensee_id))
    </div>
    <div class="alert" role="alert" id="error_msg"></div>
    <h1 class="page-title">Step 7: Account Configuration</h1>
    <form action="@isset($licensee_account['id']){{route('pricing.update',$licensee_id)}}@else{{route('pricing.add')}}@endisset" class="add-form" method="post">
    {{ csrf_field() }}
    @isset($licensee_account['id'])
        @method('PUT')
    @endisset
    <input type="hidden" name="licensee_id" value="{{ $licensee_id }}">
    <div class="box-wrapper">
      <p>Payment Terms</p>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="radio-checkbox label_check m-t-10 disabled" for="checkbox-1"><input type="checkbox" id="checkbox-1" value="1" disabled>Payment Term Option One (1)</label>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="radio-checkbox label_check m-t-10 disabled" for="checkbox-2"><input type="checkbox" id="checkbox-2" value="1" disabled>Payment Term Option Two (2)</label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="radio-checkbox label_check m-t-10 disabled" for="checkbox-3"><input type="checkbox" id="checkbox-3" value="1" disabled>Payment Term Option Three (3)</label>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="radio-checkbox label_check m-t-10 disabled" for="checkbox-4"><input type="checkbox" id="checkbox-4" value="1" disabled>Payment Term Option Four (4)</label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="radio-checkbox label_check m-t-10 disabled" for="checkbox-5"><input type="checkbox" id="checkbox-5" value="1" disabled>Payment Term Option Five (5)</label>
          </div>
        </div>
      </div>
    </div>
    <div class="box-wrapper">
      <p>Gross / Net Configuration</p>
        <div class="row">
          <div class="col-sm-12">
              <div class="box-wrapper">
                <p>Accomodation</p>
                <input type="hidden" name="product_type[]" value="accomodation">
                <div class="row">
                  <div class="col-sm-6">
                      <div class="form-group">
                        <label class="label-control">Rate Type <span class="aestrik">*</span></label><br>
                        <label class="radio-checkbox label_radio m-r-10" for="net0"><input type="radio" id="net0" value="net" name="rate_type0" @if(isset($licensee_commission[0]) && $licensee_commission[0]['rate_type'] === 'net') {{ 'checked' }} @endif>Net</label>
                        <label class="radio-checkbox label_radio" for="gross0"><input type="radio" id="gross0" value="gross" name="rate_type0" @if(isset($licensee_commission[0]) && $licensee_commission[0]['rate_type'] === 'gross') {{ 'checked' }}@endif>Gross</label>
                      </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Commission(%) <span class="aestrik">*</span></label>
                      <input type="text" name="commission0" id="commission0" autocomplete="nofill" class="form-control" placeholder="Commission" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[0]['reseller_commission'])) {{ $licensee_commission[0]['reseller_commission'] }}@endif">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Agent Enterprice</label>
                      <input type="text" name="agent_enterprice[]" id="agent_enterprice0" class="form-control" placeholder="Agent Enterprice" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[0]['agent_enterprice'])) {{ $licensee_commission[0]['agent_enterprice'] }}@endif"> 
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Agent Wholesale</label>
                      <input type="text" name="agent_wholesale[]" id="agent_wholesale0" class="form-control" placeholder="Agent Wholesale" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[0]['agent_wholesale'])) {{ $licensee_commission[0]['agent_wholesale'] }}@endif"> 
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
              <div class="box-wrapper">
                <p>Activity</p>
                <input type="hidden" name="product_type[]" value="activity">
                <div class="row">
                  <div class="col-sm-6">
                      <div class="form-group">
                        <label class="label-control">Rate Type <span class="aestrik">*</span></label><br>
                        <label class="radio-checkbox label_radio m-r-10" for="net1"><input type="radio" id="net1" value="net" name="rate_type1" @if(isset($licensee_commission[1]) && $licensee_commission[1]['rate_type'] === 'net') {{ 'checked' }}@endif>Net</label>
                        <label class="radio-checkbox label_radio" for="gross1"><input type="radio" id="gross1" value="gross" name="rate_type1" @if(isset($licensee_commission[1]) && $licensee_commission[1]['rate_type'] === 'gross') {{ 'checked' }}@endif>Gross</label>
                      </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Commission(%) <span class="aestrik">*</span></label>
                      <input type="text" name="commission1" id="commission1" autocomplete="nofill" class="form-control" placeholder="Commission" value="@if(isset($licensee_commission[1]) && isset($licensee_commission[1]['reseller_commission'])) {{ $licensee_commission[1]['reseller_commission'] }}@endif">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Agent Enterprice</label>
                      <input type="text" name="agent_enterprice[]" id="agent_enterprice1" class="form-control" placeholder="Agent Enterprice" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[1]['agent_enterprice'])) {{ $licensee_commission[1]['agent_enterprice'] }}@endif"> 
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Agent Wholesale</label>
                      <input type="text" name="agent_wholesale[]" id="agent_wholesale1" class="form-control" placeholder="Agent Wholesale" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[1]['agent_wholesale'])) {{ $licensee_commission[1]['agent_wholesale'] }}@endif"> 
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
              <div class="box-wrapper">
                <p>Transport</p>
                <input type="hidden" name="product_type[]" value="transport">
                <div class="row">
                  <div class="col-sm-6">
                      <div class="form-group">
                        <label class="label-control">Rate Type <span class="aestrik">*</span></label><br>
                        <label class="radio-checkbox label_radio m-r-10" for="net2"><input type="radio" id="net2" value="net" name="rate_type2" @if(isset($licensee_commission[2]) && $licensee_commission[2]['rate_type'] === 'net') {{ 'checked' }}@endif>Net</label>
                        <label class="radio-checkbox label_radio" for="gross2"><input type="radio" id="gross2" value="gross" name="rate_type2" @if(isset($licensee_commission[2]) && $licensee_commission[2]['rate_type'] === 'gross') {{ 'checked' }}@endif>Gross</label>
                      </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Commission(%) <span class="aestrik">*</span></label>
                      <input type="text" name="commission2" id="commission2" autocomplete="nofill" class="form-control" placeholder="Commission" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[2]['reseller_commission'])) {{ $licensee_commission[2]['reseller_commission'] }}@endif"> 
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Agent Enterprice</label>
                      <input type="text" name="agent_enterprice[]" id="agent_enterprice2" class="form-control" placeholder="Agent Enterprice" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[2]['agent_enterprice'])) {{ $licensee_commission[2]['agent_enterprice'] }}@endif"> 
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Agent Wholesale</label>
                      <input type="text" name="agent_wholesale[]" id="agent_wholesale2" class="form-control" placeholder="Agent Wholesale" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[2]['agent_wholesale'])) {{ $licensee_commission[2]['agent_wholesale'] }}@endif"> 
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
              <div class="box-wrapper">
                <p>Tour </p>
                <input type="hidden" name="product_type[]" value="tour">
                <div class="row">
                  <div class="col-sm-6">
                      <div class="form-group">
                        <label class="label-control">Rate Type <span class="aestrik">*</span></label><br>
                        <label class="radio-checkbox label_radio m-r-10" for="net3"><input type="radio" id="net3" value="net" name="rate_type3" @if(isset($licensee_commission[3]) && $licensee_commission[3]['rate_type'] === 'net') {{ 'checked' }}@endif>Net</label>
                        <label class="radio-checkbox label_radio" for="gross3"><input type="radio" id="gross3" value="gross" name="rate_type3" @if(isset($licensee_commission[3]) && $licensee_commission[3]['rate_type'] === 'gross') {{ 'checked' }}@endif>Gross</label>
                      </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Commission(%) <span class="aestrik">*</span></label>
                      <input type="text" name="commission3" id="commission3" autocomplete="nofill" class="form-control" placeholder="Commission" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[3]['reseller_commission'])) {{ $licensee_commission[3]['reseller_commission'] }}@endif"> 
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Agent Enterprice</label>
                      <input type="text" name="agent_enterprice[]" id="agent_enterprice3" class="form-control" placeholder="Agent Enterprice" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[3]['agent_enterprice'])) {{ $licensee_commission[3]['agent_enterprice'] }}@endif"> 
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="label-control">Agent Wholesale</label>
                      <input type="text" name="agent_wholesale[]" id="agent_wholesale3" class="form-control" placeholder="Agent Wholesale" value="@if(isset($licensee_commission[0]) && isset($licensee_commission[3]['agent_wholesale'])) {{ $licensee_commission[3]['agent_wholesale'] }}@endif"> 
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
    <div class="box-wrapper">
      <p>Pay by User Configuration</p>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Amount Per User</label>
            <input type="text" name="" class="form-control disabled" disabled placeholder="Amount Per User"> 
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Payment Frequency</label>
            <input type="text" name="" class="form-control disabled" disabled placeholder="Payment Frequency"> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Payment Providers</label>
            <select class="form-control disabled" disabled>
              <option>eRoam Payment</option>
              <option value="">eWay</option>
              <option value="">SplitPayments</option>
              <option value="">PayPal</option>
              <option value="">Stripe</option>
              <option value="">Alipay</option>
              <option value="">WeChat Pay</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="box-wrapper"> 
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="radio-checkbox label_check m-t-10 disabled" for="checkbox-6"><input type="checkbox" id="checkbox-6" disabled value="1">Client (Licensee) 'Custom' Payment</label>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Manual Payment</label>
            <select class="form-control disabled" disabled>
              <option>Bank Deposit</option>
              <option>Cash on Delivery</option>
              <option>Money Order</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Taxes</label>
            <input type="text" class="form-control" name="tax" id="tax" placeholder="Taxes" value="@isset($licensee_account['tax']){{$licensee_account['tax']}}@endisset" />
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Edit Order ID Format <span class="aestrik">*</span></label>
            <input type="text" class="form-control" name="order_id_format" id="order_id_format" placeholder="Edit Order ID Format" value="@isset($licensee_account['order_id_format']){{$licensee_account['order_id_format']}}@endisset" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Consumer Accounts</label>
            <select class="form-control disabled" disabled>
              <option>Accounts are Disabled</option>
              <option>Accounts are Optional</option>
              <option>Accounts are Mandatory</option>
            </select>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Checkout / Payment Confirmation Language <span class="aestrik">*</span></label>
            <input type="text" class="form-control" name="checkout_language" id="checkout_language" placeholder="Checkout / Payment Confirmation Language" value="@isset($licensee_account['checkout_language']){{$licensee_account['checkout_language']}}@endisset" />
          </div>
        </div>
      </div>
    </div>

    <div class="box-wrapper">
      <p>Mail Configuration</p>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Mail HOST <span class="aestrik">*</span></label>
            <input type="text" name="mail_host" id="mail_host"  class="form-control" placeholder="Mail HOST" value="@isset($licensee_account['mail_host']){{$licensee_account['mail_host']}}@endisset">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Mail PORT <span class="aestrik">*</span></label>
            <input type="text" name="mail_port" id="mail_port"  class="form-control" placeholder="Mail PORT" value="@isset($licensee_account['mail_port']){{$licensee_account['mail_port']}}@endisset">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Mail Username <span class="aestrik">*</span></label>
            <input type="text" name="mail_username" id="mail_username" placeholder="Mail Username" class="form-control" value="@isset($licensee_account['mail_username']){{$licensee_account['mail_username']}}@endisset">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Mail Password <span class="aestrik">*</span></label>
            <input type="text" name="mail_password" id="mail_password" placeholder="Mail Password" class="form-control" value="@isset($licensee_account['mail_password']){{$licensee_account['mail_password']}}@endisset">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Mail From Address</label>
            <input type="text" name="mail_address" id="mail_address" placeholder="Mail from Address" class="form-control" value="@isset($licensee_account['mail_address']){{$licensee_account['mail_address']}}@endisset">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Mail From Name</label>
            <input type="text" name="mail_name" id="mail_name" placeholder="Mail from Name" class="form-control" value="@isset($licensee_account['mail_name']){{$licensee_account['mail_name']}}@endisset">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="label-control">Mail Encryption</label>
            <input type="text" name="mail_encryption" id="mail_encryption" placeholder="Mail Encryption" class="form-control" value="@isset($licensee_account['mail_encryption']){{$licensee_account['mail_encryption']}}@endisset">
          </div>
        </div>
      </div>
    </div>
    <div class="box-wrapper">
      <p>Refund, Privacy and TOS Statement</p>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <label class="label-control">Refund Policy</label>
            <textarea class="form-control" name="refund_policy" id="refund_policy" placeholder="Refund Policy">@isset($licensee_account['refund_policy']){{$licensee_account['refund_policy']}}@endisset</textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <label class="label-control">Privacy Policy</label>
            <textarea class="form-control" name="privacy_policy" id="privacy_policy" placeholder="Privacy Policy">@isset($licensee_account['privacy_policy']){{$licensee_account['privacy_policy']}}@endisset</textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <label class="label-control">Terms of Service</label>
            <textarea class="form-control" name="terms_of_service" id="terms_of_service" placeholder="Terms of Service">@isset($licensee_account['terms_of_service']){{$licensee_account['terms_of_service']}}@endisset</textarea>
          </div>
        </div>
      </div>
    </div>
    <div class="m-t-20 row">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="row">
                        <button type="sumbit" name="" class="btn btn-primary btn-block">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-t-20 row">
      <div class="col-sm-offset-2 col-sm-8">
        <div class="row">
          <div class="col-sm-6">
            <a href="{{ route('onboarding.usermanagement',$licensee_id) }}" class="btn btn-primary btn-block">Previous</a>
          </div>
          <div class="col-sm-6">
            <a href="{{ route('onboarding.account',$licensee_id) }}" class="btn btn-primary btn-block">Next</a>
          </div>
        </div>
      </div>
    </div>
    </form>
</div>
@endsection

@push('scripts')
  <script type="text/javascript" src="{{ asset('assets/js/jquery.validate.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/onboarding.js') }}"></script>
  <script type="text/javascript">
    tinymce.init({
      selector: 'textarea',
      init_instance_callback: function (editor) {
        editor.on('click', function (e) {
          console.log('Element clicked:', e.target.nodeName);
        });
      }
    });
    $(".add-form").validate({
        ignore: [],
        rules: {
            'rate_type0': {
                required: true
            },
            'rate_type1': {
                required: true
            },
            'rate_type2': {
                required: true
            },
            'rate_type3': {
                required: true,
            },
            commission0: {
                required: true,
            },
            commission1: {
                required: true,
            },
            commission2: {
                required: true,
            },
            commission3: {
                required: true,
            },
            'order_id_format':{
                required: true,
            },
            'checkout_language':{
                required: true,
            }
        },

        errorPlacement: function (label, element) {
            label.addClass('error_c');
            if ($(element).attr('type') == 'radio') {
              label.insertAfter($(element).parent('label').parent('.form-group'));
            }else{
              label.insertAfter($(element).parent('.form-group'));
            }
        },
        submitHandler: function (form) {
            $('.add-form').sumbit();
        }
    });
	
	$('#commission0, #commission1,#commission2,#commission3').on("keypress", function (evt) {
		var $txtBox = $(this);
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
            return false;
        else {
            var len = $txtBox.val().length;
            var index = $txtBox.val().indexOf('.');
            if (index > 0 && charCode == 46) {
              return false;
            }
            if (index > 0) {
                var charAfterdot = (len + 1) - index;
                if (charAfterdot > 3) {
                    return false;
                }
            }
        }
        return $txtBox; //for chaining
    });
  </script>
@endpush