@if(isset($ccountry) && $ccountry != null)
<div class="col-sm-12 ccountry{{$ccountry->id}} m-t-10">
	<p><b>{{$ccountry->name}} ({{$ccountry->region->name}})</b></p>
	@if(count($cities) < 1)
	<p>No cities available in this region.</p>
	<hr>
	@endif
</div>
@endif
@foreach($cities as $rKey => $region)
	@foreach($region as $ckey => $country)
		@if(!isset($ccountry))
		<div class="col-sm-12 ccountry{{$ckey}} m-t-10">
			<p><b>{{$countries[$rKey][$ckey]['name']}} ({{$regions[$rKey]}})</b></p>
		</div>
		@endif
		@foreach($country as $city)
			<div class="col-sm-6">
			    <div class="form-group">
			        <label class="radio-checkbox label_check m-t-10 @if(isset($sel_cities) && in_array($city['id'],$sel_cities)) c_on @endif @if(isset($defaulton)) c_on @endif" for="city{{$city['id']}}"><input type="checkbox" class="geocities" name="cities[]" id="city{{$city['id']}}" value="{{$city['id']}}" data-region="{{$city['countrywithregion']['region']['id']}}" data-country="{{$city['country_id']}}" @if(isset($sel_cities) && in_array($city['id'],$sel_cities)) checked @endif @if(isset($defaulton)) checked @endif>{{$city['name']}}</label>
			    </div>
			</div>
		@endforeach
	@endforeach
@endforeach