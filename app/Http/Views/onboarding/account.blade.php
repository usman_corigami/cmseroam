@extends( 'layout/mainlayout' )
@section('content')
<div class="content-container">
    <div class="m-t-10">
        <?php
        if(isset($licensee_id) && $licensee_id!=''){ $licensee_id = $licensee_id; } else { $licensee_id = ''; } 
        ?>
        @include('WebView::onboarding.includes.onboarding-steps', array('id' => $licensee_id))
    </div>
    <div class="alert" role="alert" id="error_msg"></div>
    <h1 class="page-title">Step 8: Account Notes</h1>
    <form method="post" action="{{route('account.update',$licensee_id)}}" class="add-form">
        {{csrf_field()}}
        @method('PUT')
        <input type="hidden" value="{{$licensee_id}}" name="licensee_id" id="licensee_id">
        <div class="box-wrapper">
            <div class="form-group">
                <label class="label-control">eRoam Sales Representative</label>
                <select class="form-control" name="representative_id">
                    <option value="0">Select</option>
                </select>
            </div>
            <div class="form-group">
                <label class="label-control">Additional Notes</label>
                <textarea name="notes" class="form-control" placeholder="Additional Notes">@isset($notes) {{$notes['notes']}} @endisset</textarea>
            </div>
        </div>
        <div class="m-t-20 row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ route('onboarding.pricing',$licensee_id) }}" class="btn btn-primary btn-block">Previous</a>
                    </div>
                    <div class="col-sm-6">
                        <button type="sumbit" name="" class="btn btn-primary btn-block">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('assets/js/blockui.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/onboarding.js')}}"></script>
@endpush