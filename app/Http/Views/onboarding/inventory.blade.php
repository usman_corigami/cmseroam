@extends( 'layout/mainlayout' )
@section('content')
<div class="content-container">
    <div class="m-t-10">
        <?php
        if(isset($licensee_id) && $licensee_id!=''){ $licensee_id = $licensee_id; } else { $licensee_id = ''; } 
        ?>
        @include('WebView::onboarding.includes.onboarding-steps', array('id' => $licensee_id))
    </div>
    <div class="alert" role="alert" id="error_msg"></div>
    <h1 class="page-title">Step 5: Inventory MarketPlace</h1>
    
    <div class="formbox">
        <?php $x=0; ?>
        @if($total < 1)
        <form class="add-form" method="post" action="{{route('inventory.add')}}">
            <input type="hidden" name="licensee_id" value="{{ $licensee_id }}">
            <div class="box-wrapper">
                <a data-toggle="collapse" data-parent="#accordion" href="#firstdomain"><p>Consumer Inventory MarketPlace</p></a>
                <div class="collapse" id="firstdomain">
                    @foreach($result as $key => $arr)
                    <div class="box-wrapper">
                        <p>{{ $key }} Product Inventory</p>
                        <div class="row">
                            @foreach($arr as $key => $val)
                            @if(array_key_exists('name',$val))
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="radio-checkbox label_check m-t-10 @if($val['status'] == 0) disabled @endif" for="checkbox-{{ $val['id'].$x }}"><input type="checkbox" name="inventory[]" id="checkbox-{{ $val['id'].$x }}" value="{{ $val['id'] }}" @if($val['status'] == 0) disabled @endif >{{ $val['name'] }}</label>
                                </div>
                                
                            </div>
                            @else
                            <div class="box-wrapper">
                                <p>{{ $key }}</p>
                                <div class="row">
                                    @foreach($val as $k => $v)
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="radio-checkbox label_check m-t-10 @if($v['status'] == 0) disabled @endif" for="checkbox-{{ $v['id'].$x }}"><input type="checkbox" name="inventory[]" id="checkbox-{{ $v['id'].$x }}" value="{{ $v['id'] }}" @if($v['status'] == 0) disabled @endif >{{ $v['name'] }}</label>
                                        </div>
                                    </div>
                                    @endforeach     
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                    <div class="m-t-20 row">
                        <div class="col-sm-8">
                            <div class="error_here"></div>
                        </div>
                    </div>
                    
                    <div class="m-t-20 row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="row">
                                        <button type="sumbit" name="" class="btn btn-primary btn-block">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>    
        @endif
        @foreach($domain_ids as $domain_id)
        <form class="add-form" method="post" action="@empty($inventory){{ route('inventory.add') }}@else{{ route('inventory.update',[$licensee_id,$domain_id]) }}@endempty">
            {{ csrf_field() }}
            @empty(!$inventory)
                @method('PUT')
            @endisset
            <input type="hidden" name="licensee_id" value="{{ $licensee_id }}">
            <input type="hidden" name="domain_id" value="{{ $domain_id }}">
            <div class="box-wrapper">
                <a data-toggle="collapse" data-parent="#accordion" href="#firstdomain{{$x}}"><p>Consumer Inventory MarketPlace</p></a>
                <div class="collapse" id="firstdomain{{$x}}">
                    @foreach($result as $key => $arr)
                    <div class="box-wrapper">
                        <p>{{ $key }} Product Inventory</p>
                        <div class="row">
                            @foreach($arr as $key => $val)
                            @if(array_key_exists('name',$val))
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="radio-checkbox label_check m-t-10 @if($val['status'] == 0) disabled @endif" for="checkbox-{{ $val['id'].$x }}"><input type="checkbox" name="inventory[]" id="checkbox-{{ $val['id'].$x }}" value="{{ $val['id'] }}" @if($val['status'] == 0) disabled @endif <?php
                                    foreach($inventory as $row){
                                        if($row->inventory_id==$val['id'] && $row->domain_id==$domain_id ){ echo 'checked'; }
                                    }
                                    ?>>{{ $val['name'] }}</label>
                                </div>
                                
                            </div>
                            @else
                            <div class="box-wrapper">
                                <p>{{ $key }}</p>
                                <div class="row">
                                    @foreach($val as $k => $v)
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="radio-checkbox label_check m-t-10 @if($v['status'] == 0) disabled @endif" for="checkbox-{{ $v['id'].$x }}"><input type="checkbox" name="inventory[]" id="checkbox-{{ $v['id'].$x }}" value="{{ $v['id'] }}" @if($v['status'] == 0) disabled @endif <?php
                                            foreach($inventory as $row){
                                                if($row->inventory_id==$v['id'] && $row->domain_id==$domain_id ){ echo 'checked'; }
                                            }
                                            ?>>{{ $v['name'] }}</label>
                                        </div>
                                    </div>
                                    @endforeach     
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                    <div class="m-t-20 row">
                        <div class="col-sm-8">
                            <div class="error_here"></div>
                        </div>
                    </div>
                    
                    <div class="m-t-20 row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="row">
                                        <button type="sumbit" name="" class="btn btn-primary btn-block">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </form>
        <?php $x++; ?>
        @endforeach
            <div class="m-t-20 row">
                <div class="col-sm-offset-2 col-sm-8">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{ route('onboarding.geodata',[$licensee_id]) }}" class="btn btn-primary btn-block">Previous</a>
                        </div>
                        <div class="col-sm-6">
                            <a href="{{ route('onboarding.usermanagement',[$licensee_id]) }}" class="btn btn-primary btn-block">Next</a>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/onboarding.js') }}"></script>
@endpush