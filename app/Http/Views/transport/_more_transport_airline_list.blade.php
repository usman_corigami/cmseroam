@if(count($oTransportAirlineList) > 0)
    @foreach ($oTransportAirlineList as $aTransportAirline)	
        <tr>
            <td>
                <label class="radio-checkbox label_check" for="checkbox-{{ $aTransportAirline->id }}">
                    <input type="checkbox" class="cmp_check" id="checkbox-{{ $aTransportAirline->id }}" value="{{ $aTransportAirline->id }}">&nbsp;
                </label>
            </td>
            <td>{{ $aTransportAirline->airline_code }}</td>
            <td>{{ $aTransportAirline->airline_name }}</td>
            <td>{{ $aTransportAirline->country_name }} {{ ($aTransportAirline->country_code)? '(' . $aTransportAirline->country_code . ')':'' }}</td>
            <td>
                <a href="{{ route('transport.airline-create',['nId'=>$aTransportAirline->id]) }}" class="button success tiny btn-primary btn-sm pull-left m-r-10" style="font-size: 11px;width: 64px;">{{ trans('messages.update_btn') }}</a>
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('transport.airlines-delete',['nId'=> $aTransportAirline->id]) }}','{{ trans('messages.delete_label')}}')">
            </td>	
        </tr>
    @endforeach
@else
    <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
@endif