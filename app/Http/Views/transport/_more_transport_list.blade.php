@foreach ($oTransportList as $aTransport)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aTransport->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aTransport->id;?>" value="<?php echo $aTransport->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="{{ route('transport.transport-create',[ 'nIdActivity' => $aTransport->id ])}}">
                {{ $aTransport->from_city_name }}
            </a>
        </td>
        <td>{{ $aTransport->to_city_name }}</td>
        <td>{{ $aTransport->transport_type }}</td>
		<td>
		@php
			$domain_array = [];
			if($aTransport->domains){
				$domain_array = explode(',',$aTransport->domains);
				foreach($domain_array as $key1=>$value1){
                    echo domianName($value1)->name.'<br/>';					
				}
			}
		@endphp
		</td>
        <td> @if(($aTransport->is_publish)=='1'){{ trans('messages.publish') }}@endif @if(($aTransport->is_publish)!='1'){{ trans('messages.unpublish') }}@endif</td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <a href="{{ route('transport.transport-create',[ 'id' => $aTransport->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('transport.transport-delete',['id'=> $aTransport->id]) }}','{{ trans('messages.delete_label')}}')">
                @if(($aTransport->is_publish)=='1')
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.unpublish_btn') }}" onclick="callPublishRecord(this,'{{ route('transport.transport-publish',['nId'=> $aTransport->id]) }}')">
                @endif
                @if(($aTransport->is_publish)!='1')
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.publish_btn') }}" onclick="callPublishRecord(this,'{{ route('transport.transport-publish',['nId'=> $aTransport->id]) }}')">
                @endif
            </div>
        </td>
    </tr> 
@endforeach