@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if($nId != '' && $nIdFrom == '')
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Transport Season']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Transport Season']) }}</h1>
    @endif
    
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
        <div class="small-6 small-centered columns error-box">{{$errors->first()}}</div>
        @endif
    </div>
    <br>
    <div class="box-wrapper">

        <p>Transport Season Details</p>
        @if($nId != '' && $nIdFrom == '')
        {{ Form::model($oTransportSeason, array('url' => route('transport.transport-season-create') ,'method'=>'POST','enctype'=>'multipart/form-data','id'=>'addTourForm')) }}
        {{ Form::hidden('id', $nId) }}
        @else
        {{Form::open(array('url' => 'transport/transport-season-create','method'=>'Post','enctype'=>'multipart/form-data','id'=>'addTourForm')) }}
        @endif
        
        <?php 
            $nPrice='';
            $aOperates = array();
        ?>
        @if($nId !='' && $nIdFrom != '')
        {{ Form::hidden('transport_id', $nId) }}
        @else
        <?php $nIdTrans= (isset($oTransportSeason) && isset($oTransportSeason->transport_id)) ? $oTransportSeason->transport_id : Input::old('transport_id');
         $nPrice= (isset($oTransportSeason) && isset($oTransportSeason->base_price_obj->base_price)) ? $oTransportSeason->base_price_obj->base_price : Input::old('price'); 
         $aOperates = (isset($oTransportSeason) && isset($oTransportSeason->operates)) ? explode(',', $oTransportSeason->operates) : Input::old('operator'); 
         //print_r($aOperates);exit;
         ?>
        <div class="row">
            @if((auth::user()->type) !="admin")		
			<div class="form-group">
				<label class="label-control">Domains <span class="required">*</span></label>
				  <select  class="form-control" multiple="true" name="domains[]">
						@if(isset($allDomain) && $allDomain->count() > 0)
							@foreach($allDomain as $key=>$domain)
								<option 	value="{{$domain->id}}"  id="domain_{{$domain->id}}" >{{$domain->name}}</option>
							@endforeach
						@endif	
					</select>
				</div>
			@endif
			
            <div class="form-group">
                <label class="label-control">Transport <span class="required">*</span></label>
                <div class="row">
                    <div class="col-sm-10">
                        {{Form::select('transport_id',$oTransports,$nIdTrans,['id'=>'transport_id','class'=>'form-control'])}}
                    </div>
                    <div class="col-sm-2 text-right">
                        <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="vertical-align: sub;"
                              title="The transport field is comprised of Operator name + Transport type + Orgin + Destination.">
                            <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>		
            </div>
        </div>  
        @endif	  
        <div class="form-group m-t-30">
            <label class="label-control">Season Name <span class="required">*</span></label>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>'form-control','placeholder'=>'Enter Season Name'])}}
        </div>
        @if ( $errors->first( 'name' ) )
        <small class="error">{{ $errors->first('name') }}</small>
        @endif
        
        <div class="form-group m-t-30">
            <label class="label-control">Passenger Type <span class="required">*</span></label>
            {{ Form::select('passenger_type_id',$oPassenger,Input::old('passenger_type_id'),['id'=>'passenger_type_id', 'class'=>'form-control'])}}
        </div>
        @if ( $errors->first( 'passenger_type_id' ) )
        <small class="error">{{ $errors->first('passenger_type_id') }}</small>
        @endif
        
        <div class="form-group m-t-30">
            <label class="label-control">Date From <span class="required">*</span></label>
            {{Form::text('from',Input::old('from'),['id'=>'from','class' =>'form-control','placeholder'=>'Select Date From'])}}
        </div>
        @if ( $errors->first( 'from' ) )
        <small class="error">{{ $errors->first('from') }}</small>
        @endif
        
        <div class="form-group m-t-30">
            <label class="label-control">Transport Supplier <span class="required">*</span></label>
            {{Form::select('transport_supplier_id',$oTransportSupplier,Input::old('transport_supplier_id'),['id'=>'transport_supplier_id','class'=>'form-control'])}}
        </div>
        @if ( $errors->first( 'transport_supplier_id' ) )
        <small class="error">{{ $errors->first('transport_supplier_id') }}</small>
        @endif	
        
        <div class="form-group m-t-30">
            <label class="label-control">Date To <span class="required">*</span></label>
            {{Form::text('to',Input::old('to'),['id'=>'to','class' =>'form-control','placeholder'=>'Select Date To'])}}
        </div>
        @if ( $errors->first( 'to' ) )
        <small class="error">{{ $errors->first('to') }}</small>
        @endif
    
        <div class="box-wrapper"> 
            <p>Transport Price Package Details</p>  
            <div class="form-group m-t-30">
                <label class="label-control">Price <span class="required">*</span></label>
                {{Form::number('price',$nPrice,['id'=>'price','step'=>"any",'min'=>'1','class'=>'form-control','placeholder'=>'Enter Price'])}}
            </div>
            @if ( $errors->first( 'price' ) )
            <small class="error">{{ $errors->first('price') }}</small>
            @endif
            
            <div class="form-group m-t-30">
                <label class="label-control">Allotment <span class="required">*</span></label>
                {{Form::number('allotment',Input::old('allotment'),['id'=>'allotment','min'=>'1','class'=>'form-control','placeholder'=>'Enter Allotment'])}}
            </div>
            @if ( $errors->first( 'allotment' ) )
            <small class="error">{{ $errors->first('allotment') }}</small>
            @endif
            
            <div class="row">
                <div class="form-group">
                    <label class="label-control">Operates <span class="required">*</span></label>
                    <div class="row">
                        <div class="col-sm-10"> 
                            {{Form::select('operates[]',$operates,$aOperates,array('multiple'=>'true','class'=>'form-control'))}}
                        </div>
                        <div class="col-sm-2 text-right"> 
                            <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="vertical-align: sub;"
                                  title="To select multiple values, press and hold CTRL while clicking on the values you want.">
                                <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>		
                </div>	

            </div>	
            @if ( $errors->first( 'operates' ) )
            <small class="error">{{ $errors->first('operates') }}</small>
            @endif
            
            <div class="form-group m-t-30">
                <label class="label-control">Release <span class="required">*</span></label>
                {{Form::number('release',Input::old('release'),['id'=>'release','min'=>'1','class'=>'form-control','placeholder'=>'Enter Release'])}}
            </div>
            @if ( $errors->first( 'release' ) )
            <small class="error">{{ $errors->first('release') }}</small>
            @endif
            
            <div class="form-group m-t-30">
                <label class="label-control">Minimum Pax <span class="required">*</span></label>
                {{Form::number('minimum_pax',Input::old('minimum_pax'),['id'=>'minimum_pax','min'=>'1','class'=>'form-control','placeholder'=>'Enter Minimum Pax'])}}		
            </div>
            @if ( $errors->first( 'minimum_pax' ) )
            <small class="error">{{ $errors->first('minimum_pax') }}</small>
            @endif
            
            <div class="form-group m-t-30">
                <label class="label-control">Currency <span class="required">*</span></label>
                {{Form::select('currency_id',$oCurrencies,Input::old('currency_id'),['id'=>'currency_id','class'=>'form-control'])}}
            </div>
            @if ( $errors->first( 'currency_id' ) )
            <small class="error">{{ $errors->first('currency_id') }}</small>
            @endif			 	 
        </div>
        <div class="box-wrapper">
            <p>Cancellation Policy</p>
            <div class="form-group m-t-30">
                {{Form::textarea('cancellation_policy',Input::old('cancellation_policy'),['placeholder'=>'Description','id'=>'cancellation_policy','class'=>'form-control' . 'cancellation_policy'])}}
            </div> 
        </div>  
        
        <div class="box-wrapper">
            <p>Cancellation Policy Percentage</p>
            <div class="row tb_added">
                @if(Input::old('days'))
                @foreach(Input::old('days') as $key => $day)
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.day_before') }}</label>

                        <input placeholder="Enter Days Before" type="number" class="form-control" name="days[]" min="1" id="days" value="{{$day}}">
                    </div> 
                </div> 
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.percentage') }}</label>

                        <input placeholder="Enter Percentage" type="number" class="form-control" name="percent[]" min="1" id="percent" value="{{Input::old('percent')[$key]}}">

                    </div> 
                </div> 
                @endforeach
                @elseif(isset($decoded_formula))
                @foreach ($decoded_formula as $key => $formula)
                <div class="row tb_added">
                    <div class="col-sm-6">
                        <div class="form-group m-t-30">
                            <label class="label-control">{{ trans('messages.day_before') }}</label>
                            <input type="number" class="form-control" name="days[]" min="1" id="days" value="{{$formula->days}}" placeholder="Enter Days Before">
                        </div> 
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group m-t-30">
                            <label class="label-control">{{ trans('messages.percentage') }}</label>
                            <input type="number" class="form-control" name="percent[]" min="1" id="percent" value="{{$formula->percent}}" placeholder="Enter Percentage">
                        </div> 
                    </div> 
                </div>
                @endforeach
                @else
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.day_before') }}</label>
                        {{Form::number('days[]',Input::old('days.0'),['id'=>'days','min'=>'1','class'=>'form-control','placeholder'=>'Enter Days Before'])}}
                    </div> 
                </div> 
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.percentage') }}</label>
                        {{Form::number('percent[]',Input::old('percent.0'),['id'=>'percent','min'=>'1','class'=>'form-control','placeholder'=>'Enter Percentage'])}}

                    </div> 
                </div> 
                @endif
            </div> 
            <div class="clones"></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        <i class="fa fa-minus-square right fa-2x" id="fa-minus" aria-hidden="true"></i>
                        <i class="fa fa-plus-square right fa-2x" id="fa-plus" aria-hidden="true"></i>
                    </div> 
                </div>      
            </div>     
        </div>  
        <div class="row">
        <div class="m-t-20 col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-sm-6">
                    {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
                </div>
                <div class="col-sm-6">
                    <a href="{{URL::to('transport/transport-season-list')}}" class="btn btn-primary btn-block">Cancel</a>
                </div>
            </div>
        </div>	
        </div>
    {{ Form::hidden('from_flag', $nIdFrom) }}
{{Form::close()}}
</div>
</div>
@stop

@section('custom-css')
<style>

		.multiselect {
	  width: 100%;
	}

	.selectBox {
	  position: relative;
	}

	.selectBox select {
	  width: 100%;
	  /* font-weight: bold; */
	}

	.overSelect {
	  position: absolute;
	  left: 0;
	  right: 0;
	  top: 0;
	  bottom: 0;
	}

	#checkboxes {
	  display: none;
	  border: 1px #dadada solid;
	}

	#checkboxes label {
	  display: block;
	}

	#checkboxes label:hover {
	  background-color: #1e90ff;
	}
	
	
    .error{
        color:red !important;
    }
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
    }
    div .with_error{
        border:1px solid black;
    }
    label {
        font-weight: 700;
    }
    .preview-image {
        width: 40%;
        margin-bottom: 25px;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }
</style>
@stop

@section('custom-js')

<script>

	var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

    $(function () {

        $("#from,#to").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        });

        tinymce.init({
            selector: '#cancellation_policy',
            height: 250,
            menubar: false
        });
        $('#fa-plus').click(function () {
            $('.tb_added:last').clone().appendTo(".clones");
            var numItems = $('.tb_added').length;
            $('.tb_added:last input').val('');

        });
        $('#fa-minus').click(function () {
            var numItems = $('.tb_added').length;
            if (numItems > 1) {
                $('.tb_added:last').remove();
            }


        });

    });
    
    $(function() {

        $("#addTourForm").validate({
            rules: {
                name        : 'required',
                transport_id : 'required_without:transport_data',
                from         : 'required|date',
                to           : 'required|date|after:date_from',
                price        : 'required',
                allotment    : 'required',
                release      : 'required',
                minimum_pax  : 'required',
                operates     : 'required',
                currency_id     : 'required',
                transport_supplier_id : 'required'
            },
            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });

</script>
@stop
