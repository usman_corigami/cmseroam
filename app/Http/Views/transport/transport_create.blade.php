@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if($nId != '')
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Transport Leg']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Transport Leg']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
        <div class="small-6 small-centered columns error-box">{{$errors->first()}}</div>
        @endif
    </div>
    <br>
    @if($nId != '')
        {{ Form::model($oTransport, array('url' => route('transport.transport-create') ,'method'=>'POST','enctype'=>'multipart/form-data','id'=>'addTourForm')) }}
    @else
        <form method="POST" action="{{ route('transport.transport-create') }}" accept-charset="UTF-8" id="addTourForm" enctype="multipart/form-data"> 
    @endif
    <div class="box-wrapper">
 
        @if((auth::user()->type) !="admin"&& (auth::user()->type) !="eroamProduct")
			<div class="form-group m-t-30">
				<label class="label-control">Domains <span class="required">*</span></label>
					<select  class="form-control" multiple="true" name="domains[]">
						@if(isset($allDomain) && $allDomain->count() > 0)
							@foreach($allDomain as $key=>$domain)
								<option @if(isset($oTransport))@if ( in_array( $domain->id, explode(",",$oTransport->domain_id) ) ) selected   @endif  @endif	value="{{$domain->id}}"  id="domain_{{$domain->id}}" >{{$domain->name}}</option>
							@endforeach
						@endif	
					</select>
			</div>
			@elseif((auth::user()->type) !="admin" && (auth::user()->type) =="eroamProduct")
			<div class="form-group">
				<label class="label-control">Licenses <span class="required">*</span></label>
					<select  class="form-control"  onChange="getDomainsbyLicenseId(this.value);"  name="licensee" id="licensee">
						<option value="">Select License</option>
						@if( isset($licensee) && count($licensee) > 0 )
							@foreach($licensee as $key=>$value)
									<option @if((isset($oTransport)) && ($key==$oTransport->license_id)) selected  @endif value="{{$key}}" id="{{$key}}">{{$value}}</option>
							@endforeach
						@endif
					</select>
			</div>
			<div class="form-group m-t-30">
				<label class="label-control">Domains <span class="required">*</span></label>
					<select @if( Auth::user()->type=='eroamProduct' && $nId != '') disabled @endif  class="form-control"   id="Domains" multiple="true" name="domains[]">
						@if( isset($domain) && $domain->count() > 0 )
								@foreach($domain as $key=>$value)
								<option @if(isset($oTransport))@if ( in_array( $value->id, explode(",",$oTransport->domain_id) ) ) selected   @endif  @endif   value='{{$value->id}}' id='{{$value->id}}'>{{$value->name}}</option>
								@endforeach
						@endif	
					</select>
			</div>
			@endif
			
		<p>Origin & Destinations</p>   
        <p class="right">Origin</p>
        <div class="form-group">
            <label class="label-control">Country <span class="required">*</span></label>
            {{Form::select('de_countries',$oCountries,Input::old('de_countries'),['id'=>'de_countries','class'=>'form-control'.' select-country','data-type'=>'departure'])}}
        </div>
				

        <div class="form-group">
            <label class="label-control">City <span class="required">*</span></label>
            <select id="from_city" class="form-control select-city" data-type="departure" name="from_city_id">
                <option value="" selected disabled>Select City</option>
                <?php 
                    if(isset($oDeCities)){
                        foreach ($oDeCities as $key => $value) {
                            $select = '';
                            if($oTour->departure == $value ) { $select = 'selected="selected"'; } 
                            echo '<option value="'.$value.'" '.$select.'>'.$value.'</option>';
                        }
                    }
                ?>
            </select>
        </div>      
        <div class="form-group">
            <label class="label-control">Address From </label>
            {{Form::text('address_from',Input::old('address_from'),['placeholder'=>'Enter Address From','id'=>'address_from','class'=>'form-control'])}}
        </div>
        <p class="right">Destination</p>   	
        <div class="form-group">
            <label class="label-control">Country <span class="required">*</span></label>
            {{Form::select('dn_countries',$oCountries,Input::old('dn_countries'),['id'=>'dn_countries','class'=>'form-control'.' select-country','data-type'=>'destination'])}}
        </div>       
        <div class="form-group">
            <label class="label-control">City <span class="required">*</span></label>
            <select id="destination" class="form-control select-city" data-type="destination" name="to_city_id">
                <option value="" selected disabled>Select City</option>
                <?php 
                    if(isset($oDnCities)){
                        foreach ($oDnCities as $key => $value) {
                            $select = '';
                            if($oTour->to_city_id == $value ) { $select = 'selected="selected"'; } 
                            echo '<option value="'.$value.'" '.$select.'>'.$value.'</option>';
                        }
                    }
                ?>
            </select>
        </div>       
        <div class="form-group">
            <label class="label-control">Address To</label>
            {{Form::text('address_to',Input::old('address_to'),['placeholder'=>'Enter Address To','id'=>'address_to','class'=>'form-control'])}}
        </div>
    </div>  
    <div class="box-wrapper">

        <p>General Information</p>
        <div class="form-group m-t-30">
            <label class="label-control">Transport Type <span class="required">*</span></label>
            {{ Form::select('transport_type_id',$aTransportType, Input::old('transport_type_id'), ['class' =>'form-control']) }}
        </div>
        
        @if ( $errors->first( 'transport_type_id' ) )
        <small class="error">{{ $errors->first('transport_type_id') }}</small>
        @endif
        <div class="form-group">
            <?php $default = (isset($oTransport['[default]'])) ? $oTransport['[default]'] : Input::old( 'default' ); 
          
            ?>
            <label for="default">Set as Default</label>
            <input id="default" name="default" type="checkbox" class="switch1-state1" value="1" {{ ($default == '1') ? 'checked' : '' }}>
        </div>       
        <div class="form-group">
            <label class="label-control">Departure Time</label>
            {{Form::text('etd',Input::old('etd'),['placeholder'=>'Enter Departure Time','id'=>'etd','class'=>'form-control'])}}
        </div>
        <div class="form-group">
            <label class="label-control">Arrival Time</label>
            {{Form::text('eta',Input::old('eta'),['placeholder'=>'Enter Arrival Time','id'=>'eta','class'=>'form-control'])}}
        </div>      
        <div class="form-group">
            <label class="label-control">Arrives</label>
            <select id="arrives_on" name="arrives_on" class="form-control">
                <?php $nArrivesOn = (isset($oTransport->arrives_on)) ? $oTransport->arrives_on :Input::old( 'arrives_on' ); ?>
                <option {{ $nArrivesOn == '0' ? 'selected' : '' }} value="0">on Same Day</option>
                <option {{ $nArrivesOn == '1' ? 'selected' : '' }} value="1">After 1 Day</option>
                <option {{ $nArrivesOn == '2' ? 'selected' : '' }} value="2">After 2 Days</option>
                <option {{ $nArrivesOn == '3' ? 'selected' : '' }} value="3">After 3 Days</option>
                <option {{ $nArrivesOn == '4' ? 'selected' : '' }} value="4">After 4 Days</option>
                <option {{ $nArrivesOn == '5' ? 'selected' : '' }} value="5">After 5 Days</option>
            </select>

        </div>
        <div class="form-group">
            <label class="label-control">Currency</label>
            {{ Form::select( 'currency_id', $oCurrencies, Input::old( 'currency_id' ), ['id' => 'currency_id','class' =>'form-control']) }}
        </div>
        
        <div class="form-group">
            <label class="label-control">Phone</label>
            {{Form::text('phone',Input::old('phone'),['placeholder'=>'Enter Phone','id'=>'phone','class'=>'form-control'])}}
        </div>
    </div>
    <div class="box-wrapper">
        <p>Other Information</p>
        <div class="form-group">
            <label class="label-control">Supplier <span class="required">*</span></label>
            {{ Form::select( 'default_transport_supplier_id', $aTransportSupplier, Input::old('default_transport_supplier_id'), ['id' => 'default_transport_supplier_id','class' =>'form-control']) }}
        </div>
        
        @if ( $errors->first( 'default_transport_supplier_id' ) )
        <small class="error">{{ $errors->first('default_transport_supplier_id') }}</small>
        @endif
        
        <div class="form-group">
            <label class="label-control">Operator <span class="required">*</span></label>
            {{ Form::select( 'operator_id', $aTransportOperator, Input::old('operator_id'), ['id' => 'operator_id','class' =>'form-control']) }}
        </div>
        
        @if ( $errors->first( 'operator' ) )
        <small class="error">{{ $errors->first('operator') }}</small>
        @endif
    </div>
    
    <div class="box-wrapper"> 
        <div class="form-group m-t-30">
            <label class="label-control">Voucher Comments</label>
            {{ Form::textarea('voucher_comments',Input::old('voucher_comments'), ['placeholder' => 'Voucher Comments', 'rows' => '5','id'=>'voucher_comments','class'=>'form-control']) }}
        </div>
        <div class="form-group">
            <label class="label-control">Notes</label>
            {{ Form::textarea('notes',Input::old('notes'), ['placeholder' => 'Notes', 'rows' => '5','id'=>'specialNote','class'=>'form-control']) }}
        </div>
    </div>   
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" value="{{ $nId }}">
    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{ route('transport.transport-list') }}" class="btn btn-primary btn-block">Cancel</a>
            </div>
        </div>
    </div>	
</form>
</div>
@stop

@section('custom-css')


<style>
	.multiselect {
	  width: 100%;
	}

	.selectBox {
	  position: relative;
	}

	.selectBox select {
	  width: 100%;
	  /* font-weight: bold; */
	}

	.overSelect {
	  position: absolute;
	  left: 0;
	  right: 0;
	  top: 0;
	  bottom: 0;
	}

	#checkboxes {
	  display: none;
	  border: 1px #dadada solid;
	}

	#checkboxes label {
	  display: block;
	}

	#checkboxes label:hover {
	  background-color: #1e90ff;
	}


    .error{
        color:red !important;
    }
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    #allocation-id-container{
        display:none;
    }
    .clockface .outer:hover, .clockface .inner:hover{background-color: #27A9E0; color: #fff; border-radius: 100px;}
</style>
<link href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet" />
@stop

@section('custom-js')
<script type="text/javascript" src="{{ asset('assets/js/moment.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}" ></script>
<script>

var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
    
$(function() {
    $( "#addTourForm" ).validate({
        rules: {
            de_countries : 'required',
            from_city_id : 'required',
            //address_from : 'required',
            dn_countries :'required',
            to_city_id : 'required',
            //address_to : 'required',
            transport_type_id : 'required',
            currency_id : 'required',
            default_transport_supplier_id : 'required',
            phone : 'number',
            operator_id : 'required',
            transport_type_id : 'required',
        },
        errorPlacement: function(error, element) {
            var placement = $(element).parent();
            if (placement) {
              $(error).insertAfter(placement)
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
                form.submit();
            }
    });
});
    $(function () {

        $('.switch1-state1').bootstrapSwitch();
        tinymce.init({
            selector: '#notes',
            height: 250,
            menubar: false
        });
        tinymce.init({
            selector: '#voucher_comments',
            height: 250,
            menubar: false
        });

        $selectCountry = $('.select-country');

        $('#top-breadcrumbs').html(
                '<li><a href="/">Home</a></li>' +
                '<li><a href="{{ URL::to( 'transports' ) }}">Manage Transports</a></li>' +
                '<li class="current"><a href="#">Update Transport Leg</a></li>'
                );

        $('#etd, #eta').datetimepicker({ format: 'hh:mm A' });
    });
    
function getCity(value,type)
{
    if(type == 'destination'){
        var city  = 'Destination';
        var select_id= '{{ isset($oTransport) ? $oTransport->to_city_id : ''}}';
    } else {
        var city  = 'Departure';
        var select_id= '{{ isset($oTransport) ? $oTransport->from_city_id : ''}}';
    }
    console.log(select_id);
    if(value != '')
    {
        $.ajax({
            url: "{{ route('common.get-cities-by-country') }}",
            method: 'post',
            data: {
                country_id: value,
                _token: '{{ csrf_token() }}'
            },
            success: function( response ) 
            {
                var selectCity = $( '.select-city[data-type="'+ type +'"]' );
                selectCity.html('<option value="" selected disabled>'+ city +' City</option>');
                 var html ='';
                for(var i = 0; i < response.length; i++) 
                {
                    var selected = "";
                    console.log(response[i].id);
                    if(select_id == response[i].id) {debugger;
                        selected = "selected='selected'";}
                    html += '<option value="'+ response[i].id +'" '+selected+'>'+ response[i].name +'</option>';
                }
                $( '.select-city[data-type="'+ type +'"]' ).append(html);
            }
        });
    }
}

$( '.select-country' ).change( function() {
    var value = $(this).val();
    var	type  = $(this).data( 'type' );
    getCity(value,type);
});
$(document).ready(function(){
    $( '.select-country' ).each(function(){
        var value = $(this).val();
        var type  = $(this).data( 'type' );
        console.log(type);
        getCity(value,type);
    });
});

function getDomainsbyLicenseId(licensee_id)
{
	$.ajax({
		url: "{{ route('common.get-domains-by-licensee_id') }}",
		method: 'post',
		data: {
			licensee_id: licensee_id,
			_token: '{{ csrf_token() }}'
		},
		success: function( response ) 
		{
			if(response){
				$("#Domains").html(response);
			}
		}
	});
}

</script>
@stop
