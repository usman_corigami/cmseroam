<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = [
        'route_plan_id','is_default'
    ];
    protected $table = 'zroutes';
    protected $primaryKey = 'id';
    
    public function route_plan() {
        return $this->belongsTo('App\RoutePlan');
    }
        
    public function route_legs() {
        return $this->hasMany('App\RouteLeg', 'route_id', 'id')->with('city')->orderBy('sequence','asc');
    }
}
