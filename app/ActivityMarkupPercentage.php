<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityMarkupPercentage extends Model
{
    protected $table = 'zactivitymarkuppercentages';
    protected $primaryKey = 'id';
    protected $fillable = ['activity_markup_id','percentage'];
}
