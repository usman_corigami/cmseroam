<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Suburb extends Model {
    
    
    protected $dates = ['deleted_at'];

    protected $table = 'zsuburbs';

    protected $guarded = array('id');

    protected $primaryKey = 'id';
    public $timestamps = false;
    use SoftDeletes;
    
    public function city()
    {
        return $this->hasOne('App\City','id','city_id')->with('Country');
    }

    public function hb_zone()
    {
    	return $this->hasOne('App\HBZone','id','hb_zone_id');
    }
    
    public static function geSuburbList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return Suburb::from('zsuburbs as s')
                    ->leftJoin('zcities as c','c.id','=','s.city_id')
                    ->leftJoin('zcountries as co','co.id','=','c.country_id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->select(
                        's.id as id',
                        'c.name as city_name',
                        'co.name as country_name',
                        's.name as name'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->withTrashed()
                    ->paginate($nShowRecord);
    }
}
 