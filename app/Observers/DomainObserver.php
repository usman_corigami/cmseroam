<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;

class DomainObserver
{
    /**
     * Handle the model "created" event.
     *
     * @param  \App\Model  $model
     * @return void
     */
    public function created(Model $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \App\Model  $model
     * @return void
     */
    public function updated(Model $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \App\Model  $model
     * @return void
     */
    public function deleted(Model $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \App\Model  $model
     * @return void
     */
    public function restored(Model $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \App\Model  $model
     * @return void
     */
    public function forceDeleted(Model $model)
    {
        //
    }
}
