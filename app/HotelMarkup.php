<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class HotelMarkup extends Model
{
    protected $table = 'zhotelmarkups';
    protected $guarded = [];
    protected $primaryKey = 'id';
    
    public function markup_percentage(){
        return $this->hasOne('App\HotelMarkupPercentage','hotel_markup_id','id');
    }       

    public function agent_commission(){
        return $this->hasOne('App\HotelMarkupAgentCommission','hotel_markup_id','id');
    }
    
    public function supplier_commission(){
        return $this->hasOne('App\HotelMarkupSupplierCommission','hotel_markup_id','id');
    }
    
    public function scopeActive($query){
        $query->where(['is_active' => 1]);
    }

    public function scopeDefault($query){
        $query->where(['is_default' => 1]);
    }
    
    public static function getHotelMarkupList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord) 
    {
        $oQuery = HotelMarkup::from('zhotelmarkups as hm')
                                ->leftjoin('zhotelmarkuppercentages as hmp','hmp.hotel_markup_id','=','hm.id')
                                ->leftjoin('zhotelmarkupagentcommissions as hma','hma.hotel_markup_id','=','hm.id')
                                ->select('hm.id as id','hm.name as name','hmp.percentage as percentage','hm.allocation_type as allocation_type',
                                          'hmp.percentage as mark_percentage','hma.percentage as agent_percentage','allocation_id as allocation_id')
                                ->orderBy($sOrderField, $sOrderBy);
        if($sSearchStr != ''){                    
            if($sSearchBy == 'name')
                $oQuery->where('name', 'like', '%' . $sSearchStr . '%');
            elseif($sSearchBy == 'hotel')
            {
                $aIdList    = Hotel::where( 'name', 'like', '%' . $sSearchStr . '%' )->pluck('id');
                $oQuery->whereIn('allocation_id', $aIdList)
                        ->where(['allocation_type' => 'hotel']);
            }
            elseif($sSearchBy == 'city')
            {
                $aIdList    = City::where( 'name', 'like', '%' . $sSearchStr . '%' )->pluck('id');
                $oQuery->whereIn('allocation_id', $aIdList)
                        ->where(['allocation_type' => 'city']);
            }
            elseif($sSearchBy == 'country')
            {
                $aIdList    = Country::where( 'name', 'like', '%' . $sSearchStr . '%' )->pluck('id');
                $oQuery->whereIn('allocation_id', $aIdList)
                        ->where(['allocation_type' => 'country']);
            }
        }
                
        $oQuery->groupBy('hm.id');
        return $oQuery->get();
    }
}
