<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Dates extends Model
{
    protected $fillable = [
        'tour_id', 'StartDate','EndDate','AdultPrice','ChildPrice','Availability','adv_purchase','season_id','AdultPriceSingle','ChildPriceSingle'
        ,'AdultSupplement','ChildSupplement'
    ];
    protected $table = 'tbldates';
    protected $primaryKey = 'Date_ID';
    public $timestamps = false;
    
    public static function getTourDate($tour_id,$season,$currentdate) {
        //DB::enableQueryLog();
        $dates= Dates::from('tbldates')
                    ->where('tour_id','=', $tour_id)
                    ->where('season_id','=', $season)
                    ->where('StartDate','>=',$currentdate)
                    ->get();
        ///dd(DB::getQueryLog());
        return $dates;
    }
}
