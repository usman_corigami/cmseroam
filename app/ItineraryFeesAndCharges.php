<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItineraryFeesAndCharges extends Model
{

	protected $table = 'zitinerariesfeesandcharges';

    protected $guarded = array('id');

    use SoftDeletes;

    protected $dates = ['deleted_at']; 

}

?>