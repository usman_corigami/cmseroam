<?php

// created by migs
namespace App;
use Illuminate\Database\Eloquent\Model;
use Input;
use Session;
use Auth;
//use Eloquent;


class Transport extends Model {


    protected $table = 'ztransports';
    protected $dates = ['deleted_at'];
    protected $fillable = ['from_city_id',
    'to_city_id',
    'transport_type_id', 
    '[default]' ,      
    'currency_id',      
    'etd',               
    'eta',            
    'arrives_on',       
    'default_transport_supplier_id',
    'operator_id',      
    'address_from',   
    'address_to' ,      
    'phone',            
    'voucher_comments',
    'notes',
	'is_publish',
	'domain_id',
	'license_id',
	'user_id',
	'created_by'
];

    protected $guarded = array('id');
     public $timestamps = true;

    public function transport_type(){
        return $this->hasOne('App\TransportType', 'id', 'transport_type_id');
    }
    
    public function operator(){
        return $this->hasOne('App\TransportOperator', 'id', 'operator_id');
    }
    
    public static function input() {
        return Input::only(
            'from_city', 'to_city', 'transport_type', 'currency', 'default',
            'etd', 'eta', 'arrives_on', 'default_transport_supplier_id', 'operator',
            'address_from', 'address_to', 'phone', 'voucher_comments', 'notes'
        );
    }

    public static function rules() {
        return [
            'from_city'      => 'required|different:to_city',
            'to_city'        => 'required',
            'transport_type' => 'required',
            'default_transport_supplier_id'       => 'required',
            'operator'       => 'required'
        ];
    }

    public static function insert_to_db( $input ) {

        return Transport::create([
            'from_city_id'      => $input['from_city'],
            'to_city_id'        => $input['to_city'],
            'transport_type_id' => $input['transport_type'],
            '[default]'         => !empty($input['default']) ? 1 :0,
            'currency_id'       => $input['currency'],
            'etd'               => $input['etd'],
            'eta'               => $input['eta'],
            'arrives_on'        => $input['arrives_on'],
            'default_transport_supplier_id'=> $input['default_transport_supplier_id'],
            'operator_id'       => $input['operator'],
            'address_from'      => $input['address_from'],
            'address_to'        => $input['address_to'],
            'phone'             => $input['phone'],
            'voucher_comments'  => $input['voucher_comments'],
            'notes'             => $input['notes']
        ]);
    }

    public static function update_row( $id, $input ) { 
        return Transport::where( ['id' => $id] )->update([
            'from_city_id'      => $input['from_city'],
            'to_city_id'        => $input['to_city'],
            'transport_type_id' => $input['transport_type'],
            '[default]'           => !empty($input['default']) ? 1:0,
            'currency_id'       => $input['currency'],
            'etd'               => $input['etd'],
            'eta'               => $input['eta'],
            'arrives_on'        => $input['arrives_on'],
            'default_transport_supplier_id'       => $input['default_transport_supplier_id'],
            'operator_id'       => $input['operator'],
            'address_from'      => $input['address_from'],
            'address_to'        => $input['address_to'],
            'phone'             => $input['phone'],
            'voucher_comments'  => $input['voucher_comments'],
            'notes'             => $input['notes']
        ]);
    }

    public function from_city(){
        return $this->hasOne('\App\City', 'id', 'from_city_id');
    }

    public function to_city(){
        return $this->hasOne('\App\City', 'id', 'to_city_id');
    }

    public function supplier(){
        return $this->hasOne('\App\TransportSupplier', 'id', 'default_transport_supplier_id');
    }

    public function price(){
        return $this->hasMany('\App\TransportPrice', 'transport_id', 'id')
            ->with('currency', 'supplier', 'passenger')->price_summary();
    }

    public function labels()
    {
        return $this->belongsToMany('Label', 'zTransportLabelsPivot', 'transport_id', 'label_id');
    }

    public function transporttype(){
        return $this->hasOne('App\TransportType','id','transport_type_id');
    }

    public function currency(){
        return $this->hasOne('App\Currency','id','currency_id');
    }

    public function orderByPrice($query){
        return $query->whereHas('price', function($q) {
            $q->orderBy('price','ASC');
        });
    }

    // created by miguel; used in TransportPricesController @ index to shorten code;
    public function scopeGetRelationships($query, $whereIn_condition)
    {  
        $aData = session('transport_price_listing') ? session('transport_price_listing') : array();
        /*print_r($aData);
        exit();*/
        $transport_season_listing_data = session('transport_season_listing')?session('transport_season_listing'):array();
        $sOrderBy = count($transport_season_listing_data) ? $transport_season_listing_data['sOrderBy'] : 'desc';
        $sOrderField = count($transport_season_listing_data) ? $transport_season_listing_data['sOrderField'] : 'id';
        if( $sOrderField == 'fc.name'):
            $orderFromCity = $sOrderField;
        else: 
            $orderFromCity = 'id';
        endif;
        $user_type=Auth::user()->type;
		
        $query->join('zcities as fc', 'fc.id', '=', 'ztransports.from_city_id')
            ->join('zcities as tc', 'tc.id', '=', 'ztransports.to_city_id')
            ->join('ztransporttypes as tt', 'tt.id', '=', 'ztransports.transport_type_id')
            ->join('ztransportoperators as to', 'to.id', '=', 'ztransports.operator_id')
            ->select('ztransports.id', 
                'from_city_id', 
                'to_city_id', 
                'transport_type_id', 
                'operator_id', 
                'fc.name as from_city_name',
				'ztransports.domain_id',
				'ztransports.license_id',
				'ztransports.user_id'
				)
            ->with([ 
                'price' => function($query) use ($whereIn_condition)
                {
                    if($whereIn_condition)
                    {
                        $query->whereIn('ztransportprices.id', $whereIn_condition);
                    }
                    $query->join('ztransportsuppliers as ts', 'ts.id', '=', 'ztransportprices.transport_supplier_id')
                        ->addSelect('ts.name as transport_supplier_name')
                        ->with([
                            'supplier' => function($query)
                            {
                                $query->addSelect('id', 'name');
                            },
                            'currency' => function($query)
                            {
                                $query->addSelect('id', 'name', 'code');
                            }
                        ]);
                },
                'transporttype' => function($query)
                {
                    $query->addSelect('id', 'name');
                }, 
                'from_city' => function($query)
                {
                    $query->addSelect('id', 'name');
                },
                'to_city'   => function($query)
                {
                    $query->addSelect('id', 'name');
                },
                'operator' => function($query)
                {
                    $query->addSelect('id', 'name');
                }
            ])
			->where(function($query) use ($user_type)  {
                            if(isset($user_type) && $user_type!='admin' && $user_type!="eroamProduct") {
                                $query->where('ztransports.user_id', Auth::user()->id);
                            }
			})    
            ->orderBy($sOrderField, $sOrderBy);
    }

    public function scopeSeasonPrice($query,$from_date){

        return $query->whereHas('price', function($q) use($from_date) {
            $q->where('from','<=',$from_date)->where('to','>=',$from_date);
        });
    }


    public function scopeSupplierName($query,$supp_name){
        $query->wherehas('supplier',function($q) use($supp_name){
            return $q->where('name','like','%'.$supp_name.'%');
        });
    }
    
    public static function getTransportList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
		$user_type=Auth::user()->type;
		//echo Auth::user()->id;die;
        return Transport::from('ztransports as t')
                        ->leftJoin('zcities as c','c.id','=','t.from_city_id')
                        ->leftJoin('zcities as ct','ct.id','=','t.to_city_id')
                        ->leftJoin('zcountries as co','co.id','=','c.country_id')
                        ->leftJoin('zcountries as cot','cot.id','=','ct.country_id')                    
                        ->leftJoin('ztransportsuppliers as cs','cs.id','=','t.default_transport_supplier_id')
                        ->leftJoin('ztransporttypes as tt','tt.id','=','t.transport_type_id')
                        ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                                if($sSearchBy == 'city'){
                                    $query->where('c.name','like','%'.$sSearchStr.'%');
                                    $query->orWhere('ct.name','like','%'.$sSearchStr.'%');
                                }
                                elseif($sSearchBy == 'country'){
                                    $query->where('co.name','like','%'.$sSearchStr.'%');
                                    $query->orWhere('cot.name','like','%'.$sSearchStr.'%');
                                }
                                elseif($sSearchBy == 'transport_type'){
                                    $query->where('tt.name','like','%'.$sSearchStr.'%');                            
								}
                            })
                        ->where('t.deleted_at',NULL)
						->where(function($query) use ($user_type)  {
								if(isset($user_type) && $user_type!='admin' && $user_type!="eroamProduct") {
									$query->where('t.user_id', Auth::user()->id);
								}
                        })    
                        ->select(
                            't.id as id',
                            't.is_publish as is_publish',
							't.domain_id as domains',
                            'c.name as from_city_name',
                            'ct.name as to_city_name',
                            'tt.name as transport_type'
                            )
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);  
    }

}

