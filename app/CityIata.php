<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CityIata extends Model {

	protected $table = 'zcity3iatacode';
	protected $primaryKey = 'id';
    protected $guarded = array('id');

	public $timestamps = false;    
   
}