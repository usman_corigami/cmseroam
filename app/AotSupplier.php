<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class AotSupplier extends Model {

    protected $table = 'zaotsuppliers';

    protected $guarded = array('id');
    
    public $timestamps = false;
    public function updated_supplier(){
        return $this->hasOne('App\AotSupplierUpdated','hotel_supplier_code','SupplierCode');
    }



    public function rooms(){
    	return $this->hasMany('AotRooms','SupplierCode','SupplierCode');
    }
    
    public static function getAotSupplierList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord) 
    {
        return AotSupplier::from('zaotsuppliers as ao')                                                        
                        ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                                $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                            })
                        ->select('*')
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);   
    }

}

?>