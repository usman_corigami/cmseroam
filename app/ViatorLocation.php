<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViatorLocation extends Model
{
    protected $fillable = [
        'destinationId','destinationName','destinationType','iataCode','latitude','longitude','timeZone'
    ];
    protected $table = 'zviatorlocations';
    protected $primaryKey = 'id';
}
