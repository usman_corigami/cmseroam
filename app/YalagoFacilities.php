<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YalagoFacilities extends Model
{
    protected $fillable = [
    	'yalagoFacilityId','Title','FacilityType','FacilityGroup'
    ];
    protected $table = 'yalago_facilities';
    protected $primaryKey = 'facilityId';

    public static function getYalagoFacilityNames($FacilityIds) {
        return YalagoFacilities::from('yalago_facilities')
			->select('yalagoFacilityId','Title', 'FacilityType', 'FacilityGroup')
            ->whereIn('yalagoFacilityId',$FacilityIds)
            ->get();
        
    }
}