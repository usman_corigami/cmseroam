<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDomain extends Model
{

    protected $table = 'user_domains';
    protected $fillable = ['user_id','domain_id','licensee_id'];
	protected $primaryKey = 'id';

    public function domains() {
    	return $this->belongsTo('App\Domain','domain_id');
    }

}
