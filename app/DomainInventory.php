<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomainInventory extends Model
{
    protected $table = 'domain_inventory';

    public function inventory_config() {
    	return $this->belongsTo('App\InventoryConfig','inventory_id');
    }
}
