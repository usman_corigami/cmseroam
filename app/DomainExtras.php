<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomainExtras extends Model{
    protected $table = 'domain_extras';
    protected $fillable = ['licensee_id','domain_id','type','status'];
	protected $primaryKey = 'id';
}
