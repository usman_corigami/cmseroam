<?php
if (!function_exists('image_validator'))
{	
    function image_validator($image){
        $fails   = TRUE;
        $message = '';

        if( $image->isValid() ){
            return 0;
            $ext = strtolower($image->getClientOriginalExtension());


            $extension = ['jpeg','jpg','png','bmp', 'JPG'];
            if(in_array($ext,$extension)){
                    if($image->getSize() < 25000000){
                            $fails = FALSE;
                    }else{
                            $message = 'File size should not exceed 25MB.';
                    }	
            }else{
                    $message = 'File uploaded is not an image.';
            }
        }else{
                $message = 'File uploaded is not valid.';
        }
        return array('fails' => $fails, 'message' => $message);
    }
}

function getAllCities($cities_id){  
    // start add by miguel to filter out cities without latlong
    $city_ids_with_latlong = \App\LatLong::whereIn('city_id',$cities_id)->pluck('city_id');
    // end add by miguel to filter out cities without latlong

    $cities = \App\City::select(
            'zcities.id','zcities.name','zcities.region_id','zcities.geohash',
            'zcities.optional_city','zcities.default_nights','zcities.description','zcities.small_image','zcities.row_id','zcities.country_id','zcities.is_disabled','zcities.airport_codes','zcities.timezone_id',
            'zcountries.Name as country_name'
        )
        ->join('zcountries', 'zcountries.id', '=', 'zcities.country_id')
        ->join('zregions', 'zregions.id', '=', 'zcountries.region_id')
        ->with('image','timezone', 'ae_city_mapped','hb_destinations_mapped', 'viator_locations_mapped', 'aot_supplier_locations_mapped', 'latlong','iata','iatas', 'suburbs')
        ->where('zcities.is_disabled', 0)
        ->where('zcountries.show_on_eroam', 1)
        ->where('zregions.show_on_eroam', 1)
        ->whereIn('zcities.id', $city_ids_with_latlong) // add by miguel to filter out cities without latlong
        //->ae_city()
        ->get();
       

    // code to manually append hb_zone data in the suburbs because if parameter limit error on mssql; fix by miguel on 2017-04-26
   
    foreach( $cities as $c_key => $city ){
        foreach( $city->suburbs as $s_key => $suburb ){
            $s = \App\Suburb::where( 'id', $suburb->hb_zone_id )->first();
            $cities[ $c_key ]->suburbs[ $s_key ]->hb_zone = $s;
        }
    }
    $data['data'] = $cities;
    $data['city_ids_with_latlong'] = $city_ids_with_latlong;
    return $data;
}




if (!function_exists('resize_image_to_thumbnail'))
{
    function resize_image_to_thumbnail($img)
    {
        // declare width and height variables
        $width  = $img->width();
        $height = $img->height();

        // if image width and heigth is equal to or greater than 150 and is equal in length 
        if( $width >= 150 && $height >= 150  && $width == $height){
                // resize to 150
                $img->resize(150, 150, function ($constraint) {
                $constraint->aspectRatio();
                });		

        // if image width and heigth is equal to or greater than 150 and width is greater than height
        }elseif( $width >= 150 && $height >= 150  && $width > $height){
                // resize to 150
                $img->resize(null, 150, function ($constraint) {
                $constraint->aspectRatio();
                });	
                // crop to 150x150
                $img->crop(150, 150);

        // if image width and heigth is equal to or greater than 150 and width is lesser than height
        }elseif( $width >= 150 && $height >= 150  && $width < $height){
                // resize to 150
                $img->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
                });	
                // crop to 150x150
                $img->crop(150, 150);

        // if only image width is equal to or greater than 150
        }elseif( $width >= 150 && $height < 150 ){
                // resize width to 150; height maybe shorter
                $img->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
                });	

        // if only image height is equal to or greater than 150
        }elseif( $width < 150 && $height >= 150 ){
                // resize height to 150; width maybe shorter
                $img->resize(null, 150, function ($constraint) {
                $constraint->aspectRatio();
                });	
        }

        // if none of the conditions are met (width and height are lesser than 150), no changes to the image will be made;
        return $img;
    }
}

if (!function_exists('resize_image_to_small'))
{
    function resize_image_to_small($img)
    {	
        // declare width and height variables
        $width  = $img->width();
        $height = $img->height();

        // height is greater than 300; 
        if( $height > 300){
                // resize to 300
                $img->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
                });	
        }

        return $img;
    }	
}

if (!function_exists('resize_image_to_medium'))
{
    function resize_image_to_medium($img)
    {
        // declare width and height variables
        $width  = $img->width();
        $height = $img->height();

        // width is greater than 768; 
        if( $width > 768){
                // resize to 300
                $img->resize(768, null, function ($constraint) {
                $constraint->aspectRatio();
                });	
        }

        return $img;
    }	
}

if (!function_exists('resize_image_to_large'))
{
    function resize_image_to_large($img){	

        // declare width and height variables
        $width  = $img->width();
        $height = $img->height();

        // height is greater than 1024; 
        if( $height > 1024){
                // resize to 300
                $img->resize(null, 1024, function ($constraint) {
                $constraint->aspectRatio();
                });	
        }

        return $img;
    }	
}

if (!function_exists('response_format')){

	function response_format($success, $data, $error){
		if(is_array($data)){
			if(count($data) < 1)
				$data = NULL;
		}
		if(is_array($error)){
			if(count($error) < 1)
				$error = NULL;
		}		
		$result = array(
			'success' => $success,
			'data' => $data,
			'error' => $error
		);
		return Response::json( $result );
	}

}
if (!function_exists('resize_image_for_tour')){
    function resize_image_for_tour($img) {

        // declare width and height variables
        $width = $img->width();
        $height = $img->height();

        // if image width and heigth is equal to or greater than 150 and is equal in length 
        if ($width >= 245 && $height >= 169 && $width == $height) {
            // resize to 150
            $img->resize(245, 169, function ($constraint) {
                $constraint->aspectRatio();
            });

            // if image width and heigth is equal to or greater than 150 and width is greater than height
        } elseif ($width >= 245 && $height >= 169 && $width > $height) {
            // resize to 150
            $img->resize(null, 169, function ($constraint) {
                $constraint->aspectRatio();
            });
            // crop to 150x150img->crop(300, 175);
            $img->crop(245, 169);

            // if image width and heigth is equal to or greater than 150 and width is lesser than height
        } elseif ($width >= 245 && $height >= 169 && $width < $height) {
            // resize to 150
            $img->resize(245, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            // crop to 150x150
            $img->crop(245, 169);

            // if only image width is equal to or greater than 150
        } elseif ($width >= 245 && $height < 169) {
            // resize width to 150; height maybe shorter
            $img->resize(245, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            //$img->crop(245, 169);
            // if only image height is equal to or greater than 150
        } elseif ($width < 245 && $height >= 169) {
            // resize height to 150; width maybe shorter
            $img->resize(null, 169, function ($constraint) {
                $constraint->aspectRatio();
            });
            //$img->crop(245, 169);
        }

        // if none of the conditions are met (width and height are lesser than 150), no changes to the image will be made;
        return $img;
    }
}

if (!function_exists('resize_custom_image')){
    function resize_custom_image($img,$width,$height){	
        $img->resize($width, $height, function ($constraint) {
        $constraint->aspectRatio();
        });		
        return $img;
    }
}

if (!function_exists('setsession')){
    function setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$nPage,$sSessionName){	
        $aArray = [
          'search_str' => $sSearchStr,
          'search_by' => $sSearchBy,
          'order_field' => $sOrderField,
          'order_by' => $sOrderBy,
          'show_record' => $nShowRecord,
          'page_number' => $nPage,
        ];
        session([$sSessionName => $aArray]);
    }
}

function setPendingSession($sOrderField,$sOrderBy,$sSessionName){    
    $aArray = [
      'order_field' => $sOrderField,
      'order_by' => $sOrderBy,
    ];
    session([$sSessionName => $aArray]);
}

if (!function_exists('getDomainData')){
    function getDomainData($domain) {       
        $pricing = App\Domain::where('name',$domain)->with('licenseeCommission')->get();
        $eroam = App\EroamCommission::all();
        $data = [
            'pricing' => $pricing,
            'eroam' => $eroam
        ];
        return $data;
    }
}

if (!function_exists('updatePricing')){
    function updatePricing($data,$type,$supplier,$eroam,$licensee) {
        
        foreach ($eroam->toArray() as $suppliers) {
            if ($suppliers['supplier'] == $supplier) {
                $e_supplier = $suppliers;
                break;
            }
        }
        foreach ($licensee->toArray()[0]['licensee_commission'] as $commissions) {
            if ($commissions['product_type'] == $type) {
                $l_commission = $commissions;
                break;
            }
        }

        if(is_object($data)) {
            $data = json_decode($data->content(),TRUE);
        }

        if (isset($data['data'][0])) {
            foreach ($data['data'] as $key => $data_node) {

                $price = isset($data_node['merchantNetPriceFrom']) ? $data_node['merchantNetPriceFrom'] : $data_node['merchantNetPrice'];
                
                // Add 5% for viator as described by dhara
                $percentage = (5/100)*$price;
                $price += $percentage;

                if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                    $detailPricing = net_to_net($price,$e_supplier,$l_commission);
                    $final_price = $detailPricing['licensee_rrp'];
                }elseif($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross'){
                    $detailPricing = net_to_gross($price,$e_supplier,$l_commission);
                    $final_price = $detailPricing['reseller_rrp'];
                }elseif($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                    $detailPricing = gross_to_net($price,$e_supplier,$l_commission);
                    $final_price = $detailPricing['licensee_rrp'];
                }elseif ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                    $detailPricing = gross_to_gross($price,$e_supplier,$l_commission);
                    $final_price = $detailPricing['reseller_rrp'];
                }
                if (isset($data_node['merchantNetPriceFrom'])) {
                    $data['data'][$key]['merchantNetPriceFrom'] = round($final_price,2);
                    $data['data'][$key]['merchantNetPriceFromFormatted'] = '$'.round($final_price,2);
                }else{
                    $data['data'][$key]['merchantNetPrice'] = round($final_price,2);
                    $data['data'][$key]['merchantNetPriceFormatted'] = '$'.round($final_price,2);
                }

                $data['data'][$key]['detailPricing'] = $detailPricing;
            }
        }else{
            if(isset($data['data']['merchantNetPrice']))
            {
                $price = isset($data['data']['merchantNetPriceFrom']) ? $data['data']['merchantNetPriceFrom'] : $data['data']['merchantNetPrice'];
            }
            else
            {
                $price = 0;
            }

            // Add 5% for viator as described by dhara
            $percentage = (5/100)*$price;
            $price += $percentage;

            if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                $detailPricing = net_to_net($price,$e_supplier,$l_commission);
                $final_price = $detailPricing['licensee_rrp'];
            }elseif($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross'){
                $detailPricing = net_to_gross($price,$e_supplier,$l_commission);
                $final_price = $detailPricing['reseller_rrp'];
            }elseif($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                $detailPricing = gross_to_net($price,$e_supplier,$l_commission);
                $final_price = $detailPricing['licensee_rrp'];
            }elseif ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                $detailPricing = gross_to_gross($price,$e_supplier,$l_commission);
                $final_price = $detailPricing['reseller_rrp'];
            }
            if (isset($data['data']['merchantNetPriceFrom'])) {
                $data['data']['merchantNetPriceFrom'] = round($final_price,2);
                $data['data']['merchantNetPriceFromFormatted'] = '$'.round($final_price,2);
            }else{
                $data['data']['merchantNetPrice'] = round($final_price,2);
                $data['data']['merchantNetPriceFormatted'] = '$'.round($final_price,2);
            }
            $data['data']['detailPricing'] = $detailPricing;
        }
        

        $response = json_encode($data);

        return $response;
    }
}

if (!function_exists('updateAccomodationPricing'))
{   
    function updateAccomodationPricing($data,$type,$supplier,$eroam,$licensee){
        foreach ($eroam->toArray() as $suppliers) {
            if ($suppliers['supplier'] == $supplier) {
                $e_supplier = $suppliers;
                break;
            }
        }
        foreach ($licensee->toArray()[0]['licensee_commission'] as $commissions) {
            if ($commissions['product_type'] == $type) {
                $l_commission = $commissions;
                break;
            }
        }
        $data = json_decode(json_encode($data), True);
        if(array_key_exists('HotelList', $data)) {
            $price = $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'];
            $price_night = $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'];

            if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                $price = net_to_net($price,$e_supplier,$l_commission);     
                $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['licensee_rrp'];
                $price_night = net_to_net($price_night,$e_supplier,$l_commission);
                $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['licensee_rrp'];
            }
            if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
                $price = net_to_gross($price,$e_supplier,$l_commission);     
                $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['reseller_rrp'];
                $price_night = net_to_gross($price_night,$e_supplier,$l_commission);     
                $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['reseller_rrp'];
            }
            if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                $price = gross_to_net($price,$e_supplier,$l_commission);     
                $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['licensee_rrp'];
                $price_night = gross_to_net($price_night,$e_supplier,$l_commission);     
                $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['licensee_rrp'];
            }
            if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                $price = gross_to_gross($price,$e_supplier,$l_commission);     
                $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['reseller_rrp'];
                $price_night = gross_to_gross($price_night,$e_supplier,$l_commission);     
                $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['reseller_rrp'];
            }

            $data['HotelList']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['detailPricing'] = $price;
        } else {
            if(array_key_exists('HotelRoomResponse', $data)) {

                if(isset($data['HotelRoomResponse']['rateCode'])) {
                    $HotelRoomResponse = $data['HotelRoomResponse'];
                    $data['HotelRoomResponse'] = array();
                    $data['HotelRoomResponse'][0] = $HotelRoomResponse;
                }
                
                foreach ($data['HotelRoomResponse'] as $key => $value) {
                    $price = $value['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'];
                    $price_night = $value['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'];

                    $rate = '@rate';
                    $room = $value['RateInfos']['RateInfo']['RoomGroup']['Room'];
                    if(isset($room[0])){
                        foreach($room as $chargeKey => $charges){
                            if(isset($charges['ChargeableNightlyRates'][0])){
                                foreach($charges['ChargeableNightlyRates'] as $k => $c){
                                    if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                                        $price = net_to_net($c[$rate],$e_supplier,$l_commission);    
                                    }

                                    if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
                                        $price = net_to_gross($c[$rate],$e_supplier,$l_commission);    
                                    }

                                    if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                                        $price = gross_to_net($c[$rate],$e_supplier,$l_commission);    
                                    }

                                    if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                                        $price = gross_to_gross($c[$rate],$e_supplier,$l_commission);    
                                    }

                                    $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['RoomGroup']['Room'][$chargeKey]['ChargeableNightlyRates'][$k]['@rate'] =  $price['licensee_rrp'];    
                                }
                            } else{
                                if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                                    $price = net_to_net($charges['ChargeableNightlyRates'][$rate],$e_supplier,$l_commission);    
                                }

                                if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
                                    $price = net_to_gross($charges['ChargeableNightlyRates'][$rate],$e_supplier,$l_commission);    
                                }

                                if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                                    $price = gross_to_net($charges['ChargeableNightlyRates'][$rate],$e_supplier,$l_commission);    
                                }

                                if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                                    $price = gross_to_gross($charges['ChargeableNightlyRates'][$rate],$e_supplier,$l_commission);    
                                }
                                $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['RoomGroup']['Room'][$chargeKey]['ChargeableNightlyRates']['@rate'] =  $price['licensee_rrp'];    
                            }  
                        }
                    }else{
                        $charges = $room;                 
                        if(isset($charges['ChargeableNightlyRates'][0])){
                            foreach($charges['ChargeableNightlyRates'] as $k => $c){
                                if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                                    $price = net_to_net($c[$rate],$e_supplier,$l_commission);    
                                }

                                if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
                                    $price = net_to_gross($c[$rate],$e_supplier,$l_commission);    
                                }

                                if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                                    $price = gross_to_net($c[$rate],$e_supplier,$l_commission);    
                                }

                                if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                                    $price = gross_to_gross($c[$rate],$e_supplier,$l_commission);    
                                }
                                $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['RoomGroup']['Room']['ChargeableNightlyRates'][$k]['@rate'] =  $price['licensee_rrp'];    
                            }
                        } else{
                            if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                                $price = net_to_net($charges['ChargeableNightlyRates'][$rate],$e_supplier,$l_commission);    
                            }

                            if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
                                $price = net_to_gross($charges['ChargeableNightlyRates'][$rate],$e_supplier,$l_commission);    
                            }

                            if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                                $price = gross_to_net($charges['ChargeableNightlyRates'][$rate],$e_supplier,$l_commission);    
                            }

                            if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                                $price = gross_to_gross($charges['ChargeableNightlyRates'][$rate],$e_supplier,$l_commission);    
                            }
                            $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['RoomGroup']['Room']['ChargeableNightlyRates']['@rate'] =  $price['licensee_rrp'];                
                        } 
                    }

                    if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                        $price = net_to_net($price,$e_supplier,$l_commission);     
                        $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['licensee_rrp'];
                        $price_night = net_to_net($price_night,$e_supplier,$l_commission);
                        $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['licensee_rrp'];
                    }
                    if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
                        $price = net_to_gross($price,$e_supplier,$l_commission);     
                        $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['reseller_rrp'];
                        $price_night = net_to_gross($price_night,$e_supplier,$l_commission);     
                        $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['reseller_rrp'];
                    }
                    if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                        $price = gross_to_net($price,$e_supplier,$l_commission);     
                        $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['licensee_rrp'];
                        $price_night = gross_to_net($price_night,$e_supplier,$l_commission);     
                        $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['licensee_rrp'];
                    }
                    if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                        $price = gross_to_gross($price,$e_supplier,$l_commission);     
                        $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['reseller_rrp'];
                        $price_night = gross_to_gross($price_night,$e_supplier,$l_commission);     
                        $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['reseller_rrp'];
                    }
                    $data['HotelRoomResponse'][$key]['RateInfos']['RateInfo']['ChargeableRateInfo']['detailPricing'] = $price;
                }
            } else {
                foreach ($data as $key => $value) {
                    if(array_key_exists('RoomRateDetailsList', $value)) {
                        $price = $value['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'];
                        $price_night = $value['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal']; 
                        
                        if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                            $price = net_to_net($price,$e_supplier,$l_commission);     
                            $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['licensee_rrp'];
                            $price_night = net_to_net($price_night,$e_supplier,$l_commission);
                            $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['licensee_rrp'];
                        }
                        if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
                            $price = net_to_gross($price,$e_supplier,$l_commission);     
                            $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['reseller_rrp'];
                            $price_night = net_to_gross($price_night,$e_supplier,$l_commission);     
                            $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['reseller_rrp'];
                        }
                        if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                            $price = gross_to_net($price,$e_supplier,$l_commission);     
                            $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['licensee_rrp'];
                            $price_night = gross_to_net($price_night,$e_supplier,$l_commission);     
                            $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['licensee_rrp'];
                        }
                        if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                            $price = gross_to_gross($price,$e_supplier,$l_commission);     
                            $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['reseller_rrp'];
                            $price_night = gross_to_gross($price_night,$e_supplier,$l_commission);     
                            $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['reseller_rrp'];
                        }
                        $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['detailPricing'] = $price;
                    }

                    if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
                        $price = gross_to_net($price,$e_supplier,$l_commission);     
                        $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['licensee_rrp'];
                        $price_night = gross_to_net($price_night,$e_supplier,$l_commission);     
                        $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['licensee_rrp'];
                    }
                    if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                        $price = gross_to_gross($price,$e_supplier,$l_commission);     
                        $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'] = $price['reseller_rrp'];
                        $price_night = gross_to_gross($price_night,$e_supplier,$l_commission);     
                        $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@nightlyRateTotal'] = $price_night['reseller_rrp'];
                    }
                    if(isset($data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'])) {
                        $data[$key]['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['detailPricing'] = $price;
                    }
                }
            }
        }
        return $data;
    }
}

if (!function_exists('updateTourPricing')) {
    function updateTourPricing($data,$type,$supplier,$eroam,$licensee){
        foreach ($eroam->toArray() as $suppliers) {
            if ($suppliers['supplier'] == $supplier) {
                $e_supplier = $suppliers;
                break;
            }
        }
        foreach ($licensee->toArray()[0]['licensee_commission'] as $commissions) {
            if ($commissions['product_type'] == $type) {
                $l_commission = $commissions;
                break;
            }
        }

        $price = $data['price'];

        if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
            $price = net_to_net($price,$e_supplier,$l_commission);     
            $data['price'] = $price['licensee_rrp'];
        }
        if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
            $price = net_to_gross($price,$e_supplier,$l_commission);     
            $data['price'] = $price['reseller_rrp'];
        }
        if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
            $price = gross_to_net($price,$e_supplier,$l_commission);     
            $data['price'] = $price['licensee_rrp'];
        }
        if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
            $price = gross_to_gross($price,$e_supplier,$l_commission);     
            $data['price'] = $price['reseller_rrp'];
        }

        $data['detailPricing'] = $price;

        return $data;
    }
}

if (!function_exists('updateTourDatePricing')) {
    function updateTourDatePricing($data,$type,$supplier,$eroam,$licensee){
        foreach ($eroam->toArray() as $suppliers) {
            if ($suppliers['supplier'] == $supplier) {
                $e_supplier = $suppliers;
                break;
            }
        }
        foreach ($licensee->toArray()[0]['licensee_commission'] as $commissions) {
            if ($commissions['product_type'] == $type) {
                $l_commission = $commissions;
                break;
            }
        }

        $data = $data->toArray();
        $AdultPrice = $data['AdultPrice'];
        $AdultPriceSingle = $data['AdultPriceSingle'];

        if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
            $AdultPrice_detail = net_to_net($AdultPrice,$e_supplier,$l_commission);
            $AdultPriceSingle_detail = net_to_net($AdultPriceSingle,$e_supplier,$l_commission);
            $data['AdultPrice'] = $AdultPrice_detail['licensee_rrp'];
            $data['AdultPrice_detailPricing'] = $AdultPrice_detail;
            $data['AdultPriceSingle'] = $AdultPriceSingle_detail['licensee_rrp'];
            $data['AdultPriceSingle_detailPricing'] = $AdultPriceSingle_detail;
        }
        if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
            $AdultPrice_detail = net_to_gross($AdultPrice,$e_supplier,$l_commission);
            $AdultPriceSingle_detail = net_to_gross($AdultPriceSingle,$e_supplier,$l_commission);
            $data['AdultPrice'] = $AdultPrice_detail['reseller_rrp'];
            $data['AdultPrice_detailPricing'] = $AdultPrice_detail;
            $data['AdultPriceSingle'] = $AdultPriceSingle_detail['reseller_rrp'];
            $data['AdultPriceSingle_detailPricing'] = $AdultPriceSingle_detail;
        }
        if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'net') {
            $AdultPrice_detail = gross_to_net($AdultPrice,$e_supplier,$l_commission);
            $AdultPriceSingle_detail = gross_to_net($AdultPriceSingle,$e_supplier,$l_commission);
            $data['AdultPrice'] = $AdultPrice_detail['licensee_rrp'];
            $data['AdultPrice_detailPricing'] = $AdultPrice_detail;
            $data['AdultPriceSingle'] = $AdultPriceSingle_detail['licensee_rrp'];
            $data['AdultPriceSingle_detailPricing'] = $AdultPriceSingle_detail;
        }
        if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
            $AdultPrice_detail = gross_to_gross($AdultPrice,$e_supplier,$l_commission);
            $AdultPriceSingle_detail = gross_to_gross($AdultPriceSingle,$e_supplier,$l_commission);
            $data['AdultPrice'] = $AdultPrice_detail['reseller_rrp'];
            $data['AdultPrice_detailPricing'] = $AdultPrice_detail;
            $data['AdultPriceSingle'] = $AdultPriceSingle_detail['reseller_rrp'];
            $data['AdultPriceSingle_detailPricing'] = $AdultPriceSingle_detail;
        }

        return $data;
    }
}

if (!function_exists('domain_logo_validator'))
{   
    function domain_logo_validator($image){
        $fails   = TRUE;
        $message = '';

        if( $image->isValid() ){
            $ext = strtolower($image->getClientOriginalExtension());
            $extension = ['jpeg','jpg','png','bmp', 'JPG','svg'];
            if(in_array($ext,$extension)){
                list($width,$height) = getimagesize($image);
                if($width <= 200 && $height <= 60){
                        $fails = FALSE;
                }else{
                        $message = 'File must be less than or equals 200*60.';
                }   
            }else{
                    $message = 'File uploaded is not an image.';
            }
        }else{
                $message = 'File uploaded is not valid.';
        }
        return array('fails' => $fails, 'message' => $message);
    }
}

if (!function_exists('domain_favicon_validator'))
{   
    function domain_favicon_validator($image){
        $fails   = TRUE;
        $message = '';

        if( $image->isValid() ){
            $ext = strtolower($image->getClientOriginalExtension());
            $extension = ['ico','png'];
            if(in_array($ext,$extension)){
                list($width,$height) = getimagesize($image);
                if($width <= 16 && $height <= 16){
                        $fails = FALSE;
                }else{
                        $message = 'File must be less than or equals 16*16.';
                }   
            }else{
                    $message = 'File uploaded is not an image.';
            }
        }else{
                $message = 'File uploaded is not valid.';
        }
        return array('fails' => $fails, 'message' => $message);
    }
}

if (!function_exists('getInventory'))
{
    function getInventory($domain) {
        $inventory = App\Domain::where('name',$domain)->with('inventory')->first();
        return ['inventory' => $inventory['inventory']];
    }
}
if (!function_exists('split_name'))
{
    function split_name($name)
    {   
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return array($first_name, $last_name);
    }   
}

if (!function_exists('getAgentName'))
{
    function getAgentName($id) {
        $user_agent = App\User::where('id',$id)->first();
        return $user_agent['name'] ;
    }
}

if (!function_exists('getCityName'))
{
    function getCityName($id) {
		$city_name = App\City::where('id',$id)->first();
		return $city_name['name'];
    }
}

if (!function_exists('getProviderName'))
{
    function getProviderName($id) {
		$provider = App\Provider::where('provider_id',$id)->first();
		return $provider['provider_name'];
    }
}


if (!function_exists('domianName'))
{
     function domianName($domain_id){
     return App\Domain::select('name')->where('id',$domain_id)->first();
    }
}

if (!function_exists('allDmains'))
{
     function allDmains(){
     return App\Domain::all();
    }
}

if(!function_exists('callRegionName'))
{
	function callRegionName($region_id){
		 return App\Region::select('id','name')->where('id',$region_id)->first();
	}
}	


if(!function_exists('callCountryName'))
{
	function callCountryName($country_id){
		 return App\Country::select('id','name','region_id')->where('id',$country_id)->first();
	}
}	

if(!function_exists('callCountriesByRegionid'))
{
	function callCountriesByRegionid($region_id){
		 return App\Country::select('id','name','region_id')->where('region_id',$region_id)->get();
	}
}

if(!function_exists('callCityByCountryid'))
{
	function callCityByCountryid($country_id){
		 return App\City::select('id','name','region_id','country_id')->where('country_id',$country_id)->get();
	}
}


include('newhelper.php');