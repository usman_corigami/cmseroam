<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use stdClass;

class MystiflyOrderTicketCall implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $client;
    private $UniqueID;
    private $sesionId;

    const ACCOUNT_NUMBER = 'MCN000201';
    const USERNAME       = 'ATGXML';
    const PASSWORD       = 'ATG2017_XML';
    const TARGET         = 'Test';    
    const ENDPOINT_URL   = 'http://onepointdemo.myfarebox.com/V2/OnePoint.svc?singleWsdl';

    public function __construct($UniqueID,$sesionId)
    {
        $this->UniqueID = $UniqueID;
        $this->sesionId = $sesionId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    
    public function handle()
    {
        $jsonData = array();
        $jsonData['UniqueID'] = $this->UniqueID;
        $res = '';

        Log::channel('mystifly_ticket')->info('Request : '.json_encode($jsonData));
    
        $request = [
                'SessionId' => $this->sesionId,
                'UniqueID'  => $this->UniqueID,
                'Target'    => self::TARGET
            ];

        try{

            $this->client = new \SoapClient( self::ENDPOINT_URL, 
                array(  'trace'      => 1, 
                'cache_wsdl' => WSDL_CACHE_NONE
                )
            );

            $response              = $this->client->TicketOrder( (object) [
                'rq' => $request
            ]);
            $res                   = get_object_vars($response);

        }catch(\Exception $e){
          
            Log::channel('mystifly_ticket')->info('Job error : '.$e->getMessage());
        }
       
        Log::channel('mystifly_ticket')->info('Response : '.\Response::json( $res ));
        Log::channel('mystifly_ticket')->info('--------------------------------------------');
    }
}
