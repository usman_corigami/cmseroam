<?php

namespace App;
use Eloquent;
// created by migs

class TransportPassengerType extends Eloquent {

    protected $table = 'ztransportpassengertypes';

    protected $guarded = [];
    protected $primaryKey = 'id';
}

?>